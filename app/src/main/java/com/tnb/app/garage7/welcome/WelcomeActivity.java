package com.tnb.app.garage7.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.common.country.CountryDialogFragment;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.main.MainActivity;
import com.tnb.app.garage7.presentation.DialogFragment.TerminosDialogoFragment;
import com.tnb.app.garage7.login.LoginActivity;
import com.tnb.app.garage7.signup.SignupActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class WelcomeActivity extends AppCompatActivity
        implements HasSupportFragmentInjector,
        CountryDialogFragment.OkButtonListener,
        GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = WelcomeActivity.class.getSimpleName();

    public static final String COUNTRY_ID = "countryId";

    private static final String COUNTRY_NAME = "countryName";

    public static final int SIGN_IN_CODE = 777;

    public static final String LOGIN_WITH_GOOGLE = "LOGIN_WITH_GOOGLE";

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);
        ButterKnife.bind(this);

        initGoogleApiClient();
    }

    private void initGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @OnClick(R.id.btn_singGoogle)
    public void signInGoogle() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, SIGN_IN_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(LOGIN_WITH_GOOGLE, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.left_out, R.anim.left_in);
            finish();
        } else {
            Toast.makeText(this, R.string.not_log_in, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_singUp)
    public void signUp() {
        CountryDialogFragment cdf = new CountryDialogFragment();
        cdf.show(getSupportFragmentManager(), "CountryDialogFragment");
    }

    @OnClick(R.id.btn_singIn)
    public void signIn() {
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
    }

    @OnClick(R.id.terminos)
    public void terms() {
        TerminosDialogoFragment newFragment = new TerminosDialogoFragment();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @OnClick(R.id.iv_salir)
    public void exitApp() {
        finish();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onClick(Country country) {
        if (country != null) {
            Intent intent = new Intent(WelcomeActivity.this, SignupActivity.class);
            intent.putExtra(COUNTRY_ID, country.id);
            intent.putExtra(COUNTRY_NAME, country.country);
            startActivity(intent);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

