package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.SerializedName;

public class FavoriteRequest {

    @SerializedName("favorite_id")
    public final String favoriteId;

    @SerializedName("type")
    public final String type;

    public FavoriteRequest(String favoriteId, String type) {
        this.favoriteId = favoriteId;
        this.type = type;
    }
}
