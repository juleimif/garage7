package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 10/09/2017.
 */

public class CarRequest {
    @SerializedName("usuario")
    @Expose
    public String usuario;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("model")
    @Expose
    public String model;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("mileage")
    @Expose
    public String mileage;
    @SerializedName("transmition")
    @Expose
    public String transmition;
    @SerializedName("gas")
    @Expose
    public String gas;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("door_number")
    @Expose
    public String doorNumber;
    @SerializedName("descriptions")
    @Expose
    public String descriptions;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("photos")
    @Expose
    public String photos;

    public CarRequest(String usuario, String brand, String model, String year, String mileage, String transmition, String gas, String color, String doorNumber, String descriptions, String location, String price, String photos) {
        this.usuario = usuario;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.mileage = mileage;
        this.transmition = transmition;
        this.gas = gas;
        this.color = color;
        this.doorNumber = doorNumber;
        this.descriptions = descriptions;
        this.location = location;
        this.price = price;
        this.photos = photos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getTransmition() {
        return transmition;
    }

    public void setTransmition(String transmition) {
        this.transmition = transmition;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "CarRequest{" +
                "usuario='" + usuario + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year='" + year + '\'' +
                ", mileage='" + mileage + '\'' +
                ", transmition='" + transmition + '\'' +
                ", gas='" + gas + '\'' +
                ", color='" + color + '\'' +
                ", doorNumber='" + doorNumber + '\'' +
                ", descriptions='" + descriptions + '\'' +
                ", location='" + location + '\'' +
                ", price='" + price + '\'' +
                ", photos='" + photos + '\'' +
                '}';
    }
}
