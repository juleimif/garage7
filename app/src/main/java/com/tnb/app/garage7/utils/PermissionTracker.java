package com.tnb.app.garage7.utils;

/**
 * Created by Cesar on 11/05/2017.
 */

public interface PermissionTracker {
    /**
     * this method verify permissions from receive params
     */
    void verifyPermission(String permission);

    /**
     * this method verify result from user permissions selections
     * @param result
     * @param MY_PERMISSIONS_REQUEST
     */
    void onResult(boolean result, int MY_PERMISSIONS_REQUEST);

    /**
     * check if user modify permission from settings app
     */
    void checkIfpermissionChanged();
}
