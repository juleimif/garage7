package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.response.OperacionesUsuarioResponse;

import java.util.List;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface OperacionesUserView {
    void showOperaciones(List<OperacionesUsuarioResponse> body);
    Context getContext();
    void showProgress();
    void hideProgress();
    void showRelod();

}
