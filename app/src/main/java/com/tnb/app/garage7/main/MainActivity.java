package com.tnb.app.garage7.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.Banner;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.vo.Resource;
import com.tnb.app.garage7.welcome.WelcomeActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.tnb.app.garage7.login.LoginActivity.USER_EMAIL;
import static com.tnb.app.garage7.utils.AppConstants.STORAGE_BASE_URL;
import static com.tnb.app.garage7.welcome.WelcomeActivity.LOGIN_WITH_GOOGLE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        HasSupportFragmentInjector,
        GoogleApiClient.OnConnectionFailedListener {

    @Inject ViewModelProvider.Factory viewModelFactory;

    MainViewModel mainViewModel;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private GoogleApiClient mGoogleApiClient;

    @BindView(R.id.nav_view) NavigationView nav_view;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.drawer_layout) DrawerLayout drawer;

    @BindView(R.id.tab) View tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_old);
        ButterKnife.bind(this);

        initViewModel();
        initGoogleApiClient();
        initDrawerLayout();
        initNavigationView();
        if (hasLoginWithSocialNetwork()) {
            getUserDataFromGoogle();
        } else {
            getUserDataFromRepository();
        }
    }

    private void initNavigationView() {
        nav_view.setNavigationItemSelectedListener(this);
        nav_view.setItemIconTintList(null);
    }

    private void initDrawerLayout() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initViewModel() {
        mainViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MainViewModel.class);
        mainViewModel.getBannerLiveData().observe(this, new Observer<Resource<List<Banner>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<Banner>> listResource) {

            }
        });
    }

    private boolean hasLoginWithSocialNetwork() {
        return getIntent().getBooleanExtra(LOGIN_WITH_GOOGLE, false);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            View header = nav_view.getHeaderView(0);
            TextView tv_nombre = (TextView) header.findViewById(R.id.tv_nombre);

            CircleImageView tv_image = (CircleImageView) header.findViewById(R.id.iv_profile);
            TextView tv_correo = (TextView) header.findViewById(R.id.tv_correo);

            tv_nombre.setText(account.getDisplayName());
            tv_correo.setText(account.getEmail());

            Glide.with(this)
                    .load(account.getPhotoUrl())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.icon_user))
                    .into(tv_image);
        }
    }

    private void getUserDataFromRepository() {
        Intent i = getIntent();
        String email = i.getStringExtra(USER_EMAIL);
        mainViewModel.init(email);
        mainViewModel.getUserLiveData().observe(this, this::handleUserResponse);
    }

    private void getUserDataFromGoogle() {
        OptionalPendingResult<GoogleSignInResult> opr =
                Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void handleUserResponse(Resource<User> user) {
        if (user.data != null) {
            //NavigationView nav_view = findViewById(R.id.nav_view);
            View header = nav_view.getHeaderView(0);
            TextView tv_nombre = (TextView) header.findViewById(R.id.tv_nombre);

            CircleImageView tv_image = (CircleImageView) header.findViewById(R.id.iv_profile);
            TextView tv_correo = (TextView) header.findViewById(R.id.tv_correo);

            tv_nombre.setText(user.data.getFirstName());
            tv_correo.setText(user.data.getEmail());

            Glide.with(this)
                    .load( STORAGE_BASE_URL + user.data.getAvatarUrl())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.icon_user))
                    .into(tv_image);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_log_out) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        if (hasLoginWithSocialNetwork()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if (status.isSuccess()) {
                        goLogInScreen();
                    }
                }
            });
        }
        else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mainViewModel.deleteAllUsers();
                }
            }).start();
            goLogInScreen();
        }
    }

    private void goLogInScreen() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);

        finish();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
