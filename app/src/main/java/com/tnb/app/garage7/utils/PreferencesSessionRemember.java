package com.tnb.app.garage7.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Juleimis on 23/06/2017.
 */

public class PreferencesSessionRemember {
    private final String TAG = PreferencesSessionRemember.class.getSimpleName();

    private static final String KEY_USER_EMAIL = "USER_EMAIL";
    private static final String KEY_USER_PASSWORD = "USER_PASSWORD";
    private static final String PREFER_NAME = "Preference";


    public SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Constructor
    public PreferencesSessionRemember(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }


    public void clearPreferences(){
        setUserEmail("");
        setUserPassword("");

    }

    public void setUserEmail(String userEmail){
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.apply();
        editor.commit();
    }
    public String getUserEmail() {return pref.getString(KEY_USER_EMAIL, "");}

    public void setUserPassword(String userPassword){
        editor.putString(KEY_USER_PASSWORD, userPassword);
        editor.apply();
        editor.commit();
    }

    public String gettUserPassword() {return pref.getString(KEY_USER_PASSWORD, "");}


}
