package com.tnb.app.garage7.presentation.publicaciones.presenter;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface PublicacionesPresenter {
    void getPublication(String idPublicacion);
}
