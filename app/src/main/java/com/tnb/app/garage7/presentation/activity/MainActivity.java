package com.tnb.app.garage7.presentation.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.Constants;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.domain.presenter.GetInfoPresenter;
import com.tnb.app.garage7.data.view.ProfileView;
import com.tnb.app.garage7.presentation.DialogFragment.FiltrosDialogoFragment;
import com.tnb.app.garage7.presentation.fragment.CarOfertaFragment;
import com.tnb.app.garage7.presentation.fragment.DetailsCarFragment;
import com.tnb.app.garage7.presentation.fragment.HomeFragment;
import com.tnb.app.garage7.presentation.fragment.ProfileOpcionesFragment;
import com.tnb.app.garage7.presentation.fragment.SavesFragment;
import com.tnb.app.garage7.presentation.fragment.MessagesFragment;
import com.tnb.app.garage7.presentation.fragment.TipoPublicacionFragment;
import com.tnb.app.garage7.presentation.fragment.WelcomePublicationFragment;
import com.tnb.app.garage7.utils.AnimationUtil;
import com.tnb.app.garage7.utils.PermissionManager;
import com.tnb.app.garage7.utils.PermissionTracker;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import android.Manifest;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements
        FiltrosDialogoFragment.showFilterListener, GoDetails,
        WelcomePublicationFragment.goOpcion,
        ProfileView, NavigationView.OnNavigationItemSelectedListener,
        PermissionTracker {
    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.nav_view) NavigationView    nav_view;
    @BindView(R.id.drawer_layout) DrawerLayout drawer_layout;
    @BindView(R.id.toolbar) Toolbar            toolbar;
    @BindView(R.id.tab) View                   tab;
    private ViewPager pager;
    private TabLayout tabLayout;
    private ActionBarDrawerToggle toggle;
    private String aux = "";
    private boolean boolMenu = true ;
    private MenuItem searchViewItem, filterItem;

    //Implemnt mvp
    private GetInfoPresenter getInfoPresenter;
    private ResponseUser userModelSingleton;
    private ResponseUser userDataModel;
    private PreferencesSessionManager preferencesUserManager;
    private Fragment frag;
    private FragmentTransaction fragTansaction;
    private ArrayList<String> historialFragments;

    @Override
    public int getLayout() {
        return R.layout.activity_main_old;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.VISIBLE);

        preferencesUserManager = new PreferencesSessionManager(this);

        //Add historial fragment
        historialFragments = new ArrayList<>();
        historialFragments.add(WelcomePublicationFragment.TAG);

        //init views
        pager = (ViewPager) findViewById(R.id.viewPagerOpciones);
        pager.setAdapter(new ViewPagerOpcionesAdapter(getSupportFragmentManager(),
                MainActivity.this, "", "", "",
                "", "", "", 0));
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_opciones);

//        frag = new WelcomePublicationFragment();
//        frag = WelcomePublicationFragment.newInstance("","");
//        frag = new ExampleFragment();
        frag = HomeFragment.newInstance();
        fragTansaction = getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.contenedorPrincipalFragmento, frag);
        fragTansaction.commit();
        toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(this);
        nav_view.setItemIconTintList(null);

        /** Inicialiamos Perfil*/
        userDataModel = ResponseUser.getInstance();
        View header = nav_view.getHeaderView(0);
        TextView tv_nombre = (TextView) header.findViewById(R.id.tv_nombre);

        CircleImageView tv_image = (CircleImageView) header.findViewById(R.id.iv_profile);
        TextView tv_correo = (TextView) header.findViewById(R.id.tv_correo);
        Log.i(TAG, "onCreate: " + preferencesUserManager.getUsereAvatar());
        tv_image.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               startActivity(new Intent(getContext(), ProfileActivity.class));
               overridePendingTransition(R.anim.left_out, R.anim.left_in);
           }
       });
        if(preferencesUserManager.getUsereName() != null
                && preferencesUserManager.getUsereEmail()!= null){
            tv_nombre.setText("¡Hola " +preferencesUserManager.getUsereName()+"!");
            tv_correo.setText(preferencesUserManager.getUsereEmail());

            Glide.with(getContext())
                    .load(preferencesUserManager.getUsereAvatar())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.icon_user))
                    .into(tv_image);
        }
    }

    public void initView() {
        tabLayout.setupWithViewPager(pager);
        tabLayout.setVisibility(View.VISIBLE);
        userModelSingleton = ResponseUser.getInstance();
        userDataModel = ResponseUser.getInstance();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment fragment;
        String TAG = "";
        if (id == R.id.nav_home) {
            searchViewItem.setVisible(false);
            filterItem.setVisible(false);
            tabLayout.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
            toolbar.setTitle("GARAGE7");
            fragment =  HomeFragment.newInstance();
            pushFragment(fragment, TAG);
        }

        if (id == R.id.nav_save) {
            tabLayout.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
//            toolbar.setTitle("Guardados");
            fragment = SavesFragment.newInstance("", "");
            pushFragment(fragment, TAG);
        }

        if (id == R.id.nav_box) {
            tabLayout.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
//            toolbar.setTitle("Mensajes");
            fragment = MessagesFragment.newInstance("", "");
            pushFragment(fragment, TAG);
        }

        if (id == R.id.nav_cuenta) {
            tabLayout.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
            searchViewItem.setVisible(false);
            filterItem.setVisible(false);
//            toolbar.setTitle("GARAGE7");
            // startActivity(new Intent(this, ProfileOpcionesFragment.class));
            fragment = ProfileOpcionesFragment.newInstance("","");
            pushFragment(fragment, TAG);
        }

        if (id == R.id.nav_publicar) {
            tabLayout.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
//            toolbar.setTitle("Publicar");
            fragment = TipoPublicacionFragment.newInstance("","");
            pushFragment(fragment, TAG);
        }

        if (id == R.id.nav_log_out) {
            logout();
        }

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers();
        }
        return true;
    }

    public void setFragmentHistorial(String tag) {
        historialFragments.add(tag);
    }


    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void logout() {
        preferencesUserManager.clearPreferences();
        startActivity(new Intent(this, BienvenidaActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.right_out);

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view_menu_item, menu);

        filterItem = menu.findItem(R.id.action_setting);
        searchViewItem = menu.findItem(R.id.action_search);
        searchViewItem.setVisible(false);
        filterItem.setVisible(false);

        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                pager = (ViewPager) findViewById(R.id.viewPagerOpciones);
                pager.setAdapter(new ViewPagerOpcionesAdapter(getSupportFragmentManager(),
                        MainActivity.this, Utils.ucFirst(query), "", "", "","" , "", 0));
                tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_opciones);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_setting) {
            callFilters();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilters() {

        FiltrosDialogoFragment newFragment = new FiltrosDialogoFragment();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void setInfoToView() {
        Log.i(TAG, "setInfoToView: ");
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showToast(String mssg) {

    }

    @Override
    public void go(int opcion) {
        switch (opcion) {
            case 0:
                Log.i(TAG, "go car: ");
                filterItem.setVisible(true);
                searchViewItem.setVisible(true);

                //pager.setVisibility(View.VISIBLE);
                popAlbum(pager);
                popAlbum2(tab);
                pager.setCurrentItem(0);


                break;
            case 1:
                Log.i(TAG, "go motocycle: ");
                tab.setVisibility(View.VISIBLE);
                pager.setVisibility(View.VISIBLE);
                filterItem.setVisible(true);
                searchViewItem.setVisible(true);
                initView();
                pager.setCurrentItem(1);
                break;
        }
    }

    private void popAlbum(ViewPager pager1) {
        pager1.setVisibility(View.VISIBLE);
        initView();

        new AnimationUtil(getContext(), R.anim.left_out)
                .setLinearInterpolator().startAnimation(pager1);
    }

    private void popAlbum2(View pager2) {
        pager2.setVisibility(View.VISIBLE);
        initView();

        new AnimationUtil(getContext(), R.anim.left_out)
                .setLinearInterpolator().startAnimation(pager2);
    }


    @Override
    public void details() {
        filterItem.setVisible(false);
        searchViewItem.setVisible(false);
        tab.setVisibility(View.INVISIBLE);

        frag = new DetailsCarFragment();
        pushFragment(frag, TAG);
    }

    @Override
    public void goMainActivity() {
        frag = new CarOfertaFragment();
        pushFragment(frag, TAG);
    }

    @Override
    public void goOtherFragmnet(Fragment fragmnet, String tag) {
        Log.i(TAG, "goOtherFragmnet: " + tag);
//        toolbar.setTitle(tag);
        pushFragment(fragmnet, tag);

    }

    @Override
    public void showAjustes() {
        filterItem.setVisible(true);
        searchViewItem.setVisible(true);
    }

    @Override
    public void verifyPermission(String permission) {
        if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Crashlytics.log(Log.DEBUG,TAG, "--->Device is upper for external than 6.0");
                if (PermissionManager.getInstance().checkPermission(this, permission)) {

                } else {
                    this.requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            } else {

            }
        }

    }

    @Override
    public void onResult(boolean result, int MY_PERMISSIONS_REQUEST) {
        if (MY_PERMISSIONS_REQUEST == Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {

            if (result) {

            } else {
                String message = "Necesita permiso";
                new AlertDialog.Builder((this))
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!ActivityCompat.shouldShowRequestPermissionRationale(getParent(),
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                    dialog.cancel();
                                    PermissionManager permissionmanager = PermissionManager.getInstance();
                                    permissionmanager.dialogToSetting("necesitas ir a las configuraciones");
                                } else {
                                    verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                                }
                            }
                        })
                        .setNegativeButton(this.getString(R.string.exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .create()
                        .show();
            }
        }

    }

    @Override
    public void checkIfpermissionChanged() {

    }

    @Override
    public void getInfListener(String model, String year, String km, String locale, String disel, String trasmition, int price) {

        pager = (ViewPager) findViewById(R.id.viewPagerOpciones);
        pager.setAdapter(new ViewPagerOpcionesAdapter(getSupportFragmentManager(),
                MainActivity.this, model, year, km, locale, disel, trasmition, price));
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_opciones);
    }
}
