package com.tnb.app.garage7.domain.presenter;

import com.tnb.app.garage7.domain.model.request.NegociacionRequest;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface CrearNegociacionPresenter {
    void crearNegociacion(NegociacionRequest negociacionRequest);
}
