package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;

import java.util.List;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface PublicacionesView {
    void showPublicaciones(List<OperacionesConcesionarioResponse> body);
    Context getContext();
    void showProgress();
    void hideProgress();
    void showRelod();

}
