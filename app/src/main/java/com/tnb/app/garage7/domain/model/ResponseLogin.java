package com.tnb.app.garage7.domain.model;

/**
 * Created by Juleimis on 16/06/2017.
 */

public class ResponseLogin {
    public String status;
    public String message;
    public String token;
    public String iduser;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    @Override
    public String toString() {
        return "ResponseLogin{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", token='" + token + '\'' +
                ", iduser='" + iduser + '\'' +
                '}';
    }
}
