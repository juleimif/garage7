package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.CarPhoto;

import java.util.List;

@Dao
public interface CarPhotoDAO {

    @Query("SELECT * FROM CarPhoto")
    LiveData<List<CarPhoto>> findAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCarsPhotos(List<CarPhoto> carPhotosList);

    @Query("DELETE FROM CarPhoto")
    void deleteAll();
}
