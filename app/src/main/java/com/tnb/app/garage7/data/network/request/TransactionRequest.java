package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.VehicleType;

public class TransactionRequest {

    @SerializedName("seller_id")
    private final String sellerId;

    @SerializedName("garage_id")
    private final String garageId;

    @SerializedName("purchasable_id")
    private final String vehicleId;

    @SerializedName("type")
    private final VehicleType type;


    public TransactionRequest(String sellerId, String garageId,
                              String vehicleId, VehicleType type) {
        this.sellerId = sellerId;
        this.garageId = garageId;
        this.vehicleId = vehicleId;
        this.type = type;
    }
}
