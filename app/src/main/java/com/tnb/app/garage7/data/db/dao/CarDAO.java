package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.Car;

import java.util.List;

@Dao
public interface CarDAO {

    @Query("SELECT * FROM Car")
    LiveData<List<Car>> findAll();

    @Query("SELECT * FROM Car where id = :id")
    LiveData<Car> searchCarById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Car... Car);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCars(List<Car> carList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createCarIfNotExists(Car Car);

    @Query("DELETE FROM Car")
    void deleteAll();
}
