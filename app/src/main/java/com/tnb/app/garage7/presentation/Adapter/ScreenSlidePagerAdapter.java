package com.tnb.app.garage7.presentation.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tnb.app.garage7.presentation.activity.ProcessPublicationActivity;
import com.tnb.app.garage7.presentation.fragment.CreatePublicactionFragment;
import com.tnb.app.garage7.presentation.fragment.CreatePublicactionFragment2;
import com.tnb.app.garage7.presentation.fragment.FormInfCarFragment;
import com.tnb.app.garage7.presentation.fragment.FormPhotosFragment;

/**
 * Created by Luisana Abache on 09/07/2017.
 */

public class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_PAGES = 4;
    public int type;

    public ScreenSlidePagerAdapter(FragmentManager fm, ProcessPublicationActivity processPublicationActivity, int type) {
        super(fm);
        this.type = type;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CreatePublicactionFragment().newInstance(type);
            case 1:
                return new CreatePublicactionFragment2().newInstance(type);
            case 2:
                return new FormPhotosFragment().newInstance(type);
            case 3:
                return new FormInfCarFragment().newInstance(type);
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
