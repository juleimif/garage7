package com.tnb.app.garage7.domain.presenter;

import android.os.Bundle;
import android.util.Log;

import com.tnb.app.garage7.data.view.ProfileView;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class GetInfmpl implements GetInfoPresenter {
    public static final String TAG = GetInfmpl.class.getSimpleName();
    ProfileView profileView;


    public GetInfmpl(ProfileView profileView){
        this.profileView = profileView;
    }

    @Override
    public void getMyProfileRequest() {

        Bundle params = new Bundle();
        params.putString("fields", "email,name,first_name,last_name,birthday,picture.type(large)");
        Log.i(TAG, "getMyProfileRequest: ");

    }

    @Override
    public void onDestroy() {
        profileView = null;
    }


}
