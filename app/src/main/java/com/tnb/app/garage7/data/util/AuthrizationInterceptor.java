package com.tnb.app.garage7.data.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.tnb.app.garage7.utils.AppConstants.SPK_ACCESS_TOKEN;

public class AuthrizationInterceptor implements Interceptor {

    SharedPreferences preferences;

    @Inject
    public AuthrizationInterceptor(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        // Request customization: add request headers
        Request.Builder requestBuilder = original.newBuilder()
                .header("Authorization", token());

        Request request = requestBuilder.build();
        Log.e("Interceptor", "Authorization: "+ request.header("Authorization"));
        return chain.proceed(request);
    }

    private String token() {
        return preferences.getString(SPK_ACCESS_TOKEN, "Bearer 0123abcd");
    }
}
