package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tnb.app.garage7.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class WelcomePublicationFragment extends Fragment {
    public static final String TAG = WelcomePublicationFragment.class.getSimpleName();

    public interface goOpcion{
        void go(int opcion);
    }
    goOpcion listener;

    public WelcomePublicationFragment() {
        // Required empty public constructor
    }

    public static WelcomePublicationFragment newInstance(String param1, String param2) {
        WelcomePublicationFragment fragment = new WelcomePublicationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_welcome_publication, container, false);

        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick({R.id.iv_car, R.id.iv_moto})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_car:
                listener.go(0);
                break;

            case R.id.iv_moto:
                listener.go(1);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (goOpcion) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}

