package com.tnb.app.garage7.presentation.Adapter;

import android.content.ContentUris;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.Concesionario;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.techery.properratingbar.ProperRatingBar;

public class ConcesionarioAdapter extends RecyclerView.Adapter<ConcesionarioAdapter.ViewHolder> {

    private BrandItemClick mItemClickListener;
    private List<Concesionario>  listConcesionario;


    public ConcesionarioAdapter(Context applicationContext, List<Concesionario> response, BrandItemClick brokerItemClick) {
        this.listConcesionario = response;
        this.mItemClickListener = brokerItemClick;
    }


    @Override
    public ConcesionarioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_concesionario, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Concesionario currentItem = listConcesionario.get(position);
        holder.tv_name_concesionario.setText(currentItem.getName());
        int a;
        try {
            a = Integer.parseInt(currentItem.getPuntuacion());
            holder.lowerRatingBar.setRating(Math.round(a));

        } catch (NumberFormatException nfe) {
            // Handle the condition when str is not a number.
        }

    }

    @Override
    public int getItemCount() {

        return listConcesionario.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.lowerRatingBar) ProperRatingBar lowerRatingBar;
        @BindView(R.id.tv_name_concesionario) TextView tv_name_concesionario;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(listConcesionario.get(position));
        }
    }

    public interface BrandItemClick {
        void onBrokerClick(Concesionario clickedBroker);
    }
}
