package com.tnb.app.garage7.presentation.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.response.OperacionesUsuarioResponse;
import com.tnb.app.garage7.domain.presenter.OperacionesUserImpl;
import com.tnb.app.garage7.domain.presenter.UpdateStatusImpl;
import com.tnb.app.garage7.data.view.OperacionesUserView;
import com.tnb.app.garage7.data.view.UpdateStatusView;
import com.tnb.app.garage7.presentation.Adapter.OperacionesAdapter;
import com.tnb.app.garage7.presentation.DialogFragment.CalificarDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.ConfirmarPagoDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.DetalleNegociacionDialogoFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OperacionesFragment extends Fragment implements OperacionesUserView, UpdateStatusView, ConfirmarPagoDialogoFragment.pagoDialogListener, CalificarDialogoFragment.pagoDialogListener {
    public static final String TAG = OperacionesFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list)
    RecyclerView recyclerView;
    @BindView(R.id.ly_list_save)
    CoordinatorLayout ly_list_save;
    @BindView(R.id.cont_publicactiones)
    LinearLayout cont_publicactins;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.reload)
    ImageButton reload;
    private OperacionesAdapter operacionesAdapter;
    private PreferencesSessionManager preferencesUserManager;
    GoDetails listener;
    private OperacionesUserImpl operacionesUserPresenter;
    private UpdateStatusImpl updateStatus;
    private String idNegociacion;
    private int typeOperacion;


    public OperacionesFragment() {
        // Required empty public constructor
    }

    public static OperacionesFragment newInstance(String param1, String param2) {
        OperacionesFragment fragment = new OperacionesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_operaciones, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());
        operacionesUserPresenter = new OperacionesUserImpl(this);
        updateStatus = new UpdateStatusImpl(this);
        operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());
                    }
                }, 0);
            }
        });

        return view;
    }

    private void initViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    @Override
    public void showOperaciones(List<OperacionesUsuarioResponse> body) {
        Log.i(TAG, "showOperaciones: " + body.size());
        initViews();
        operacionesAdapter = new OperacionesAdapter(getContext(), body, new OperacionesAdapter.PublicationItemClick() {
            @Override
            public void onBrokerClick(OperacionesUsuarioResponse clickedBroker) {
                Log.i(TAG, "onBrokerClick: " + clickedBroker.getStatusNegociacion());
                idNegociacion = clickedBroker.getNegociacion();
                typeOperacion = clickedBroker.getIs();

                if (typeOperacion == 1) {//Es una compra

                    switch (clickedBroker.getStatusNegociacion()) {

                        case "1": //la cita fue aceptadp por el concesionario
                            showMessage(1, "Cita concesionario", getResources().getString(R.string.cita_confirmacion) + " " + clickedBroker.getConseName() + ". " + getResources().getString(R.string.cita_confirmacion_conca), "Ok", "Declinar");
                            break;
                        case "2": //la cita fue aceptadp por el concesionario
                            showMessage(2, "Cita concesionario", "Tu solicitud ha sido aceptada, espera una llamada del concesionario para" +
                                    " concretar la cita. Una vez que estes seguro de comprar el vehículo pisa el boton Comprar para" +
                                    " pasar al siguiente paso", "Comprar", "Volver");
                            break;
                        case "3":
                            showMessage(3, "Compra confirmada", getResources().getString(R.string.compra_confirmada), "Registrar pago", "Volver");
                            break;
                        case "4":
                            showMessage(4, "Pago en proceso", "Tu pago está siendo procesado, por favor espera.", "Ok", "");

                            break;
                        case "5":
                            showMessage(5, "Pago recibido", "El concesionario ha confirmado la recepción del pago, ya puedes retirar " +
                                    "el vehículo, espera una llamada del concesionario para concretar la cita de entrega. Una vez que recibas el vehículo " +
                                    "confirma para finalizar la operación", "Confirmar entrega", "Volver");
                            break;
                        case "6":
                            showMessage(7, "Confirmación de entrega", "Recibiste el vehículo?", "Si, lo recibí", "No lo recibí");
                            break;
                        case "7":
                            DetalleNegociacionDialogoFragment newFragment = new DetalleNegociacionDialogoFragment();
                            showDialogFragment(newFragment);
                            break;
                    }
                } else {//Es una venta
                    switch (clickedBroker.getStatusNegociacion()) {
                        case "1":
                            showMessage(1, "Cita concesionario", "Alguien ha ofertado tu vehículo, espera una llamada del concesionario para concretar la cita.", "Ok", "");
                            break;
                        case "3":
                            showMessage(3, "Compra confirnmada", "El comprador ha confirmado la  negociación, una vez realice el pago, le informaremos para proceder a la entrega del vehículo.", "Ok", "");
                            break;
                        case "5":
                            showMessage(5, "Confirmación de transferencia", "El concesionario a confirmado la recepción del pago, ya puedes jacer la entrega del vehículo, espera una llamada del concesionario para concretar la cita de entrega. Una vez entregado el vehículo confirma la entrega. ", "Confirmar entrega", "Volver");
                            break;


                    }


                }

            }
        });
        recyclerView.setAdapter(operacionesAdapter);

        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }

    }

    public void showDialogCalificar() {
        FragmentManager fm = getFragmentManager();
        CalificarDialogoFragment editNameDialogFragment = new CalificarDialogoFragment();
        // SETS the target fragment for use later when sending results
        editNameDialogFragment.setTargetFragment(OperacionesFragment.this, 300);
        editNameDialogFragment.show(fm, "fragment_edit_name");

    }

    private void showAlert(final int type, String message, String tvConfirm) {
        Log.i(TAG, "showAlert: " + type);

        new AlertDialog.Builder((getActivity()))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(tvConfirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        switch (type) {
                            case 2:
                                Log.i(TAG, "confirme la cita: ");
                                updateStatus.UpdateStatus(idNegociacion, "3");
                                operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());

                                //llamaos al dialog de para pagar el vehculo
                                showMessage(3, "Compra confirmada", getResources().getString(R.string.compra_confirmada), "Registrar pago", "Volver");

                                break;
                            case 5:
                                showMessage(4, "Compra confirmada", getResources().getString(R.string.compra_confirmada), "Registrar pago", "");
                                break;
                        }
                    }
                })
                .setNegativeButton(this.getString(R.string.declinar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }


    private void showMessage(final int type, String titulo, String mensaje, String opcionOk, String opcionCancel) {
        Log.i(TAG, "showMessage: " + type);

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(titulo)
                .setDescription(mensaje)
                .setPositiveText(opcionOk)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (typeOperacion == 1) {//Es una compra
                            switch (type) {
                                case 2:
                                    showAlert(2, getResources().getString(R.string.cita_concesionario), getString(R.string.confirmar));
                                    break;
                                case 3:
                                    confirmPay();
                                    break;
                                case 5:
                                    updateStatus.UpdateStatus(idNegociacion, "6");
                                    operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());

                                    break;
//                            case 4:
                                case 7:
                                    showDialogCalificar();
                                    break;
//                            case 4:
//                                break;
//                            case 5:
//                                showMessage(0, "Confirmación de entrega", "Recibiste el vehículo?", "Si, lo recibí", "No lo recibí");
//                                break;
                            }
                        } else {
                            switch (type) {
                                case 5:
                                    updateStatus.UpdateStatus(idNegociacion, "6");
                                    operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());
                                    showMessage(0, "Entrega confirmada", "Ya has confirmado la entrega del vehículo, estamos procesando la información. Por favor espere","Ok", "");

                            }
                        }

                    }
                })
                .setNegativeText(opcionCancel);


        dialogHeader_7.show();
    }

    @Override
    public void showProgress() {
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {

        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void showRelod() {
        reload.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());
        reload.setVisibility(View.INVISIBLE);
    }

    public void confirmPay()

    {

        FragmentManager fm = getFragmentManager();
        ConfirmarPagoDialogoFragment editNameDialogFragment = new ConfirmarPagoDialogoFragment();
        // SETS the target fragment for use later when sending results
        editNameDialogFragment.setTargetFragment(OperacionesFragment.this, 300);
        editNameDialogFragment.show(fm, "fragment_edit_name");

    }


    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showNotify(String s) {

    }

    @Override
    public void onFinishPayDialog() {
        Log.i(TAG, "hice el pago del vehiculo: ");
        updateStatus.UpdateStatus(idNegociacion, "4");
        operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());


    }

    @Override
    public void onFinishVotageDialog() {
        Log.i(TAG, "califique al vendedor: ");
        updateStatus.UpdateStatus(idNegociacion, "7");
        operacionesUserPresenter.getOperaciones(preferencesUserManager.getUserId());


    }
}
