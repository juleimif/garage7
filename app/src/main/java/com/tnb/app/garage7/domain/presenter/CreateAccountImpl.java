package com.tnb.app.garage7.domain.presenter;

import android.util.Log;

import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.RegisterResponse;
import com.tnb.app.garage7.domain.model.ResponseSession;
import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.domain.model.response.AutocompletarEstados;
import com.tnb.app.garage7.data.view.CreateAccountView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class CreateAccountImpl implements CreateAccountPresenter {
    public static final String TAG = CreateAccountImpl.class.getSimpleName();
    public CreateAccountView createAccountView;

    public CreateAccountImpl(CreateAccountView loginView){
        this.createAccountView = loginView;
    }

    @Override
    public void CreateAccount(User user) {
        createAccountView.showLoading();
        Log.i(TAG, "CreateAccount: " + user.toString());
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        inter.setRegistreUser(user).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                Log.e(TAG, "onResponse: " + response.code() );
                createAccountView.hideLoading();
                if (response.code() == 200){
                    Log.i(TAG, "onResponse: ");
                    responseLogin(response.body());

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                createAccountView.hideLoading();


            }
        });

    }

    //AutoCompletar de las ciudades
    @Override
    public void searchCity(String ciudad, String pais) {
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        Log.i(TAG, "searchCity: " + "ciudad " + ciudad + " pais " + pais);

        if(pais.equals("Venezuela")){
            Log.i(TAG, "searchCity: " + inter.getEstadosVenezuela("car").request().url());

            inter.getEstadosVenezuela(ciudad).enqueue(new Callback<AutocompletarEstados>() {
                @Override
                public void onResponse(Call<AutocompletarEstados> call, Response<AutocompletarEstados> response) {
                    Log.i(TAG, "onResponse 1: ");

                    if (response.code()==200){
                        if(response.body().getStatus().equals("true")){
                            createAccountView.showCitys(response.body().getData());
                        }
                        else
                        {
                            Log.e(TAG, "onResponse: "+ response.body().getStatus());
                        }
                    }
                }

                @Override
                public void onFailure(Call<AutocompletarEstados> call, Throwable t) {
                    Log.e(TAG, "onFailure 1: " + t.getMessage());


                }
            });

        }
        else
            if(pais.equals("Panamá")){

                inter.getEstadosPanama(ciudad).enqueue(new Callback<AutocompletarEstados>() {
                    @Override
                    public void onResponse(Call<AutocompletarEstados> call, Response<AutocompletarEstados> response) {
                        Log.i(TAG, "onResponse 2: ");
//                        createAccountView.showCitys(response.body().getData());

                    }

                    @Override
                    public void onFailure(Call<AutocompletarEstados> call, Throwable t) {
                        Log.e(TAG, "onFailure 2: " + t.getMessage());
                        createAccountView.hideLoading();

                    }
                });
            }

            else
            {

                inter.getEstadosColombia(ciudad).enqueue(new Callback<AutocompletarEstados>() {
                    @Override
                    public void onResponse(Call<AutocompletarEstados> call, Response<AutocompletarEstados> response) {
                        Log.i(TAG, "onResponse 3: ");
//                        createAccountView.showCitys(response.body().getData());
                    }

                    @Override
                    public void onFailure(Call<AutocompletarEstados> call, Throwable t) {
                        Log.e(TAG, "onFailure 3: " + t.getMessage());
                        createAccountView.hideLoading();

                    }
                });

            }
    }

    private void respuestaServicio(String message) {
        createAccountView.showNotify(message);
    }

    private void responseLogin(RegisterResponse body) {
        if(body.status.equals("true")){
            Log.i(TAG, "responseLogin: " + body.getIduser());
            getInfoUser(body.getIduser().getIdusuarios());
        }

        else{
            createAccountView.showNotify(body.message);
        }
    }


    //Obtenemos informacion del usuario para guardar session
    public void getInfoUser(String id_use) {

        final PreferencesSessionManager preferencesUserManager = new PreferencesSessionManager(createAccountView.getContext());

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "login 2: " + service.getInfoUser(id_use).request().url());
        service.getInfoUser(id_use).enqueue(new Callback<List<ResponseSession>>() {
            @Override
            public void onResponse(Call<List<ResponseSession>> call, Response<List<ResponseSession>> response) {
                if (response.code() == 200) {
                    preferencesUserManager.setUserName(response.body().get(0).getName());
                    preferencesUserManager.setUserEmail(response.body().get(0).getEmail());
                    preferencesUserManager.setUserId(String.valueOf(response.body().get(0).getIdusuarios()));
                    preferencesUserManager.setUserAvatar(response.body().get(0).getProfile_photo());
                    preferencesUserManager.setUserCountry(response.body().get(0).getCountry());
                    preferencesUserManager.setUserIdConcesionario(response.body().get(0).getIsconsecionario());


                    //Vamos a la actibidad principal
                    createAccountView.gotoMain();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ResponseSession>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                createAccountView.hideLoading();

            }
        });
    }

}
