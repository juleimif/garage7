package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 02/10/2017.
 */

public class ConcesionarioResponse {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public ConcesionarioData data;

    public String getStatus() {
        return status;
    }

    public ConcesionarioData getData() {
        return data;
    }
}
