package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;
import com.tnb.app.garage7.domain.presenter.SolicitudesImpl;
import com.tnb.app.garage7.domain.presenter.UpdateStatusImpl;
import com.tnb.app.garage7.data.view.SolicitudesView;
import com.tnb.app.garage7.data.view.UpdateStatusView;
import com.tnb.app.garage7.presentation.Adapter.ProcesosAdapter;
import com.tnb.app.garage7.presentation.DialogFragment.ConfirmarPagoDialogoFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProcesosFragment extends Fragment implements SolicitudesView, UpdateStatusView, ConfirmarPagoDialogoFragment.pagoDialogListener {
    public static final String TAG = ProcesosFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list)
    RecyclerView recyclerView;
    @BindView(R.id.ly_list_save)
    CoordinatorLayout ly_list_save;
    @BindView(R.id.cont_publicactiones)
    LinearLayout cont_publicactins;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.reload)
    ImageButton reload;
    private ProcesosAdapter procesosAdapter;
    private PreferencesSessionManager preferencesUserManager;
    private SolicitudesImpl solicitudesPresenter;
    private List<OperacionesConcesionarioResponse> myListSolicitudes;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private UpdateStatusImpl updateStatus;
    private String idNegociacion;


    public ProcesosFragment() {
        // Required empty public constructor
    }

    public static ProcesosFragment newInstance(String param1, String param2) {
        ProcesosFragment fragment = new ProcesosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_operaciones, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());
        myListSolicitudes = new ArrayList<OperacionesConcesionarioResponse>();
        solicitudesPresenter = new SolicitudesImpl(this);
        updateStatus = new UpdateStatusImpl(this);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
                    }
                }, 0);
            }
        });


        return view;
    }

    private void initViews() {
        for (OperacionesConcesionarioResponse e : myListSolicitudes) {
            myListSolicitudes.remove(e);
        }
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showNotify(String s) {

    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
        reload.setVisibility(View.INVISIBLE);
    }


    @Override
    public void showSolicitudes(List<OperacionesConcesionarioResponse> body) {
        initViews();
        for (int i = 0; i < body.size(); i++) {
            Log.i(TAG, "showSolicitudesEnPoceso: " + body.get(i).getStatusNegociacion());

            if (body.get(i).getStatusNegociacion().equals("2") || body.get(i).getStatusNegociacion().equals("3") ||
                    body.get(i).getStatusNegociacion().equals("4") || body.get(i).getStatusNegociacion().equals("5") ||
                    body.get(i).getStatusNegociacion().equals("6")) {//es una solicitud en proceso

                OperacionesConcesionarioResponse dates = new OperacionesConcesionarioResponse(body.get(i).getIdpostCars(), body.get(i).getTypePostC(), body.get(i).getUsuarioC(),
                        body.get(i).getPriceC(), body.get(i).getBrandC(), body.get(i).getModelC(), body.get(i).getIdvendedor(), body.get(i).getRatingVendedor(), body.get(i).getNegociacion(),
                        body.get(i).getRatingComprador(), body.get(i).getStatusNegociacion(), body.get(i).getPhoto1(), body.get(i).getUsername());
                myListSolicitudes.add(dates);
            }
        }

        if (myListSolicitudes.size() > 0) {
            Log.i(TAG, "showSolicitudesEnPoceso 2: " + myListSolicitudes.size());
            cont_publicactins.setVisibility(View.GONE);
            procesosAdapter = new ProcesosAdapter(getContext(), myListSolicitudes, new ProcesosAdapter.PublicationItemClick() {
                @Override
                public void onBrokerClick(OperacionesConcesionarioResponse clickedBroker) {
                    idNegociacion = clickedBroker.getNegociacion();
                    switch (clickedBroker.getStatusNegociacion()) {
                        case "3":
                            showMessage(2, "Compra confirmada", "El comprador ha confirmado la negociación, espera una transferecnia por un moto de: " + clickedBroker.getPriceC() + " VEF," + " en las próximas 24 horas.", "Ok", "");//SI SOLICITUD ES IGUAL
                            break;
                        case "4":
                            showMessage(4, "Confirmación de transferencia", "El comprador ha hecho la transferencia de: " + clickedBroker.getPriceC() + " Revise su cuenta, si efectivamnete recibiste la transferencia confirma la recepción de la misma", "Confirmar", "Volver");//SI SOLICITUD ES IGUAL
                            break;
                        case "6":
                            showMessage(6, "Entrega confirmada", "El comprador ha confirmado la recepción. Ya puedes pagar al vendedor. El pago debe ser realizado por transferencia bancaria. Una vez hayas realizado el pago  confirmanos para continuar. Te hemos enviado a tu correo los datos bancarios del vendedor. ", "Registrar pago", "Volver");//SI SOLICITUD ES IGUAL
                            break;


                    }

                }
            });
            recyclerView.setAdapter(procesosAdapter);
            // If is refreshing
            if (swipe_container.isRefreshing()) {
                // Hide refreshing
                swipe_container.setRefreshing(false);
            }
            // If isn't refreshing
            else {
                // Hide ProgressBar
                progressBar.setVisibility(View.GONE);
            }

        } else {
            cont_publicactins.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

            Log.i(TAG, "esto e suna solicitud 2: " + myListSolicitudes.size());
        }


    }

    @Override
    public void showProgress() {
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void hideProgress() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void showRelod() {
        reload.setVisibility(View.VISIBLE);
    }

    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            for (OperacionesConcesionarioResponse e : myListSolicitudes) {
                myListSolicitudes.remove(e);
            }
            solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;

        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }


    private void showMessage(final int type, String titulo, String mensaje, String opcionOk, String opcionCancel) {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(titulo)
                .setDescription(mensaje)
                .setPositiveText(opcionOk)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        switch (type) {
                            case 4:
                                showMessage(0, "Confirmación de transferencia", "Has confirmado el pago, ya hecho esto se puede proceder a la entrega del vehículo. Por favor asegúrate  de que se realice la operación dentro del concesionario para mayor seguridad de las partes. Comprador y Vendedor esperan tu llamada para concretar la entrega. Una vez realizada la entrega el comprador deberá confirmar para fializar la operación. ", "Ok", "");//SI SOLICITUD ES IGUAL
                                break;
                            case 6:
                                confirmPay();
                                break;
                            case 0:
                                Log.i(TAG, "ya confirme la recepcion de la transferencia: "+ idNegociacion);
                                updateStatus.UpdateStatus(idNegociacion, "5");
                                solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
                                break;


                        }

                    }
                })
                .setNegativeText(opcionCancel);


        dialogHeader_7.show();
    }

    public void confirmPay()

    {

        FragmentManager fm = getFragmentManager();
        ConfirmarPagoDialogoFragment editNameDialogFragment = new ConfirmarPagoDialogoFragment();
        // SETS the target fragment for use later when sending results
        editNameDialogFragment.setTargetFragment(ProcesosFragment.this, 300);
        editNameDialogFragment.show(fm, "fragment_edit_name");

    }

    @Override
    public void onFinishPayDialog() {
        Log.i(TAG, "ya le pague al vendedor: ");
        updateStatus.UpdateStatus(idNegociacion, "5");

    }
}
