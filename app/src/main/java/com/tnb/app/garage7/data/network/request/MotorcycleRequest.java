package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.SerializedName;

public class MotorcycleRequest {

    public final String model;

    public final String brand;

    public final String color;

    public final String city;

    public final String country;

    @SerializedName("country_id")
    public final String countryId;

    public final String description;

    public final String price;

    public final String year;

    public final String mileage;

    public final String displacement;

    @SerializedName("is_active")
    public final boolean isActive;

    public MotorcycleRequest(String model, String brand, String color, String city,
                             String country, String countryId, String description,
                             String price, String year, String mileage, String displacement,
                             boolean isActive) {
        this.model = model;
        this.brand = brand;
        this.color = color;
        this.city = city;
        this.country = country;
        this.countryId = countryId;
        this.description = description;
        this.price = price;
        this.year = year;
        this.mileage = mileage;
        this.displacement = displacement;
        this.isActive = isActive;
    }
}
