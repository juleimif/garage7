package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class AutocompletarMarcas {

    @SerializedName("marca")
    @Expose
    public String marca;

    public String getMarca() {
        return marca;
    }
}
