package com.tnb.app.garage7.domain.presenter;

import android.util.Log;

import com.tnb.app.garage7.data.view.CarListContract;
import com.tnb.app.garage7.data.view.PublicationsContract;
import com.tnb.app.garage7.domain.model.modelCar.Carros;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;
import com.tnb.app.garage7.domain.model.response.PublicacionsResponse;
import com.tnb.app.garage7.presentation.fragment.PublicacionesActivasFragment;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Acer on 11/2/2018.
 */

public class PublicationsImp implements PublicationsContract.Presenter {
    Retrofit retrofit;
    PublicationsContract.View mView;
    public static final String TAG = PublicationsImp.class.getSimpleName();


    @Inject
    public PublicationsImp(Retrofit retrofit, PublicationsContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }

    @Override
    public void loadPublicatonsByStatus(EstatusPublicacion body) {
        retrofit.create(ApiInterface.class).getMePublications(body).enqueue(new Callback<PublicacionsResponse>() {
            @Override
            public void onResponse(Call<PublicacionsResponse> call, Response<PublicacionsResponse> response) {
                Log.i(TAG, "onResponse: " + response.body().getData());
                mView.showPublicaions(response.body().getData());
            }

            @Override
            public void onFailure(Call<PublicacionsResponse> call, Throwable t) {
                mView.showError(t.getMessage());
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}