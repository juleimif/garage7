package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Garage;

public class GarageResponse {

    @SerializedName(value = "data")
    private Garage garage;

    public Garage garage() {
        return garage;
    }
}
