package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.RegisterResponse;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.data.view.LoginView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.activity.CreateAccountActivity;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class ChoiceCountryDialogoFragment extends DialogFragment {
    public static final String TAG = ChoiceCountryDialogoFragment.class.getSimpleName();

    String[] PAIS = {"Colombia", "Panamá", "Venezuela"};
    private String pais = "";
    @BindView(R.id.sp_pais) MaterialBetterSpinner sp_pais;
    private Activity activity;
    private String name, email, image;
    private int typeFrom;
    private ResponseUser userModelSingleton;
    ProgressDialog progress;
    LoginView listener;
    PreferencesSessionManager preferencesUserManager;

    public static ChoiceCountryDialogoFragment newInstance(int typeFrom, String personNamw, String personEmail, String base64Img) {

        ChoiceCountryDialogoFragment fragment = new ChoiceCountryDialogoFragment();
        Bundle args = new Bundle();
        args.putInt("type", typeFrom);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_choice_pais);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);
        // get inf
        typeFrom = getArguments().getInt("type", 0);
        Log.i(TAG, "onCreateDialog: " + typeFrom);

        userModelSingleton = ResponseUser.getInstance();

        preferencesUserManager = new PreferencesSessionManager(getContext());

        name = userModelSingleton.getName();
        email = userModelSingleton.getEmail();
        image = userModelSingleton.getAvatarURL();

        ArrayAdapter<String> arrayAdapterPais = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, PAIS);
        sp_pais.setAdapter(arrayAdapterPais);

        sp_pais.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pais = adapterView.getItemAtPosition(i).toString();

            }
        });

        return dialogo;
    }

    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                if(pais.equals("")){
                    sp_pais.setError("Elija un país");
                }
                else
                if(typeFrom==0){
                    //llamamos al activity registro
                    Intent intent = new Intent(getContext(), CreateAccountActivity.class);
                    intent.putExtra("country", pais);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);
                    dismiss();


                }
                else
                    CreateAccount();
                break;
        }
    }

    public void CreateAccount() {
        progress = new ProgressDialog(getContext());
        progress.setTitle("Cargando");
        progress.setMessage("Por favor espere...");
        progress.show();
        dismiss();

        User user = new User(name, "", "", pais, "", "","", "", email, "0", "", "0", image);
        Log.i(TAG, "CreateAccount: " + user.toString());

        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        inter.setRegistreUser(user).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(progress != null){
                    progress.dismiss();
                }

                if (response.code() == 200){
                    reponseRegistro(response.body());

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                if(progress != null){
                    progress.dismiss();
                    dismiss();
                }

                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    private void reponseRegistro(RegisterResponse body) {

        //Guardamos la sesion
        Log.i(TAG, "reponseRegistro: " + body.toString());
        preferencesUserManager.setUserName(name);
        preferencesUserManager.setUserEmail(email);
        preferencesUserManager.setUserId(body.getIduser().getIdusuarios());
        preferencesUserManager.setUserCountry(pais);
        listener.startProfileActivity();

    }

    private void respuestaServicio(String message) {
        Log.e(TAG, "respuestaServicio: "  + message );

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (LoginView) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
