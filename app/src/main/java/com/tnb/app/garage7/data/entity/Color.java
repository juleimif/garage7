package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Color {

    @NonNull
    public final String id;

    @NonNull
    public final String color;

    public Color(@NonNull String id, @NonNull String color) {
        this.id = id;
        this.color = color;
    }
}
