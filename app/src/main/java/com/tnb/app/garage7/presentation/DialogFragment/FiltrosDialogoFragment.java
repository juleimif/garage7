package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.data.view.CreateAccountView;
import com.tnb.app.garage7.data.view.LoginView;
import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;
import com.tnb.app.garage7.domain.model.response.AutocompletarMarcas;
import com.tnb.app.garage7.domain.model.response.AutocompletarMarcasResponse;
import com.tnb.app.garage7.domain.presenter.CreateAccountImpl;
import com.tnb.app.garage7.presentation.Adapter.BrandsAdapter;
import com.tnb.app.garage7.presentation.Adapter.EstadosAdapter;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.utils.PreferencesFiltersManager;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class FiltrosDialogoFragment extends DialogFragment implements CreateAccountView {
    @BindView(R.id.input_model)
    EditText input_model;
    @BindView(R.id.input_año)
    EditText input_año;
    @BindView(R.id.input_km)
    EditText input_km;
    @BindView(R.id.input_localidad)
    EditText input_localidad;
    @BindView(R.id.sp_disel)
    MaterialBetterSpinner sp_combustible;
    @BindView(R.id.sp_trans)
    MaterialBetterSpinner sp_transmision;
    @BindView(R.id.tv_price_menor)
    TextView tv_price_menor;
    @BindView(R.id.tv_price_mayor)
    TextView tv_price_mayor;
    @BindView(R.id.rv_brands)
    RecyclerView rv_brands;
    int price = 0;
    String combustible,ciudad, marca, transmision = "";
    String[] COMBUSTIBLE = {"Gasolina", "GNV"};
    String[] TRANSMISION = {"Automática", "Sincrónica"};
    ArrayAdapter<String> arrayAdapter4;
    PreferencesFiltersManager preferencesFiltersManager;
    private BrandsAdapter brandsAdapter;
    private EstadosAdapter estadosAdapter;
    private CreateAccountImpl createAccountImpl;
    @BindView(R.id.rv_ciudades) RecyclerView rv_ciudades;

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void gotoMain() {

    }

    @Override
    public void showNotify(String s) {

    }

    @Override
    public void showCitys(List<AutoCompletarEstadosResponse> data) {
        estadosAdapter = new EstadosAdapter(getContext(),data, new EstadosAdapter.BrandItemClick() {
            @Override
            public void onBrokerClick(AutoCompletarEstadosResponse clickedBroker) {
                Log.i(TAG, "onBrokerClick: " +clickedBroker.getEstado() );
                input_localidad.setText(clickedBroker.getEstado());
                rv_ciudades.setVisibility(View.GONE);
            }
        });
        rv_ciudades.setAdapter(estadosAdapter);
        rv_ciudades.setVisibility(View.VISIBLE);

    }

    public interface showFilterListener {
        void getInfListener(String model, String year, String km, String locale, String disel, String trasmition, int price);
    }

    showFilterListener listener;

    public static final String TAG = FiltrosDialogoFragment.class.getSimpleName();

    public static FiltrosDialogoFragment newInstance(String personName, String personEmail, String base64Img) {
        FiltrosDialogoFragment f = new FiltrosDialogoFragment();
        Bundle args = new Bundle();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_filtro_busqueda);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this, dialogo);
        createAccountImpl = new CreateAccountImpl(this);

        preferencesFiltersManager = new PreferencesFiltersManager(getContext());
        arrayAdapter4 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, COMBUSTIBLE);
        sp_combustible.setAdapter(arrayAdapter4);
        sp_combustible.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                combustible = parent.getItemAtPosition(position).toString();
                sp_combustible.setSelection(position);

            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, TRANSMISION);
        sp_transmision.setAdapter(arrayAdapter);
        sp_transmision.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                transmision = parent.getItemAtPosition(position).toString();
            }
        });


        input_model.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                marca = String.valueOf(charSequence);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i(TAG, "afterTextChanged: ");
                if (marca.length() > 2)
                    getBrandsCars(marca);
            }
        });

        input_año.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rv_brands.setVisibility(View.GONE);
                rv_ciudades.setVisibility(View.GONE);
                if (s.length() > 4) {
                    input_año.setError("Formato incorrecto");
                    input_año.setText("");

                } else if (s.length() > 3) {
                    if (Integer.parseInt(String.valueOf(s)) < 1950 || Integer.parseInt(String.valueOf(s)) > 2018) {
                        input_año.setError("Ingrese un año válido");
                        input_año.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        input_localidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ciudad = String.valueOf(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(ciudad.length()>2)
                    createAccountImpl.searchCity(ciudad, "Venezuela");
            }
        });

        initView();
        return dialogo;
    }

    public void initView() {
        rv_brands.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_brands.setLayoutManager(layoutManager);

        input_model.setText(preferencesFiltersManager.getModel());
        input_km.setText(preferencesFiltersManager.getKm());
        input_año.setText(preferencesFiltersManager.getYear());
        input_localidad.setText(preferencesFiltersManager.getLocation());

//        if (preferencesFiltersManager.getPrice().equals("1")) {
//            tv_price_menor.setTextColor(getResources().getColor(R.color.colorPrimary));
//            tv_price_mayor.setTextColor(getResources().getColor(R.color.bac_color));
//        } else if (preferencesFiltersManager.getPrice().equals("2")) {
//            tv_price_mayor.setTextColor(getResources().getColor(R.color.colorPrimary));
//            tv_price_menor.setTextColor(getResources().getColor(R.color.bac_color));
//        }
    }


    @OnClick({R.id.iv_exit, R.id.btn_ok, R.id.iv_delete, R.id.tv_price_mayor, R.id.tv_price_menor})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_exit:
                dismiss();
                break;
            case R.id.btn_ok:
                preferencesFiltersManager.setModel(input_model.getText().toString());
                preferencesFiltersManager.setKm(input_km.getText().toString());
                preferencesFiltersManager.setYear(input_año.getText().toString());
                preferencesFiltersManager.setPrice(String.valueOf(price));
                preferencesFiltersManager.setLocation(input_localidad.getText().toString());

                listener.getInfListener(input_model.getText().toString(), input_año.getText().toString(), input_km.getText().toString(), input_localidad.getText().toString(), combustible, transmision, price);
                dismiss();
                break;
            case R.id.iv_delete:
                input_model.setText("");
                input_año.setText("");
                input_km.setText("");
                input_localidad.setText("");
                sp_combustible.setSelection(0);
                tv_price_menor.setTextColor(getResources().getColor(R.color.bac_color));
                tv_price_mayor.setTextColor(getResources().getColor(R.color.bac_color));
                preferencesFiltersManager.clearPreferences();

                break;
            case R.id.tv_price_mayor:
                price = 2;
                tv_price_mayor.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_price_menor.setTextColor(getResources().getColor(R.color.bac_color));
                break;
            case R.id.tv_price_menor:
                tv_price_menor.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_price_mayor.setTextColor(getResources().getColor(R.color.bac_color));
                price = 1;
                break;
        }
    }


    public void getBrandsCars(String brand) {
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        Log.i(TAG, "getBrandsCars: " + inter.getBrands(brand).request().url());
        inter.getBrands(brand).enqueue(new Callback<AutocompletarMarcasResponse>() {
            @Override
            public void onResponse(Call<AutocompletarMarcasResponse> call, Response<AutocompletarMarcasResponse> response) {
                if (response.code() == 200) {
                    brandsAdapter = new BrandsAdapter(getContext(), response.body().getData(), new BrandsAdapter.BrandItemClick() {
                        @Override
                        public void onBrokerClick(AutocompletarMarcas clickedBroker) {
                            Log.i(TAG, "onBrokerClick 1: ");
                            input_model.setText(clickedBroker.getMarca());
                            rv_brands.setVisibility(View.GONE);
                        }
                    });
                    rv_brands.setAdapter(brandsAdapter);
                    brandsAdapter.notifyDataSetChanged();  // data set changed
                    rv_brands.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AutocompletarMarcasResponse> call, Throwable t) {
                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (showFilterListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
