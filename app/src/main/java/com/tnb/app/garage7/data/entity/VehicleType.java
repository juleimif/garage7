package com.tnb.app.garage7.data.entity;

public enum VehicleType {

    CAR, MOTORCYCLE;

    public String toString() {
        String type = "carro";
        switch(this) {
            case MOTORCYCLE: type = "moto"; break;
            case CAR: type = "carro"; break;
        }
        return type;
    }
}
