package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tnb.app.garage7.R;


public class Fotos extends BaseFragment{

    public Fotos() {
        // Required empty public constructor
    }

    public static Fotos newInstance(int type_publication) {
        Fotos fragment = new Fotos();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_form_photos;
    }


    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {


    }

}
