package com.tnb.app.garage7.injector.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.tnb.app.garage7.GarageAplication;
import com.tnb.app.garage7.injector.module.ViewModelModule;
import com.tnb.app.garage7.injector.qualifires.ForApplication;
import com.tnb.app.garage7.data.network.Webservice;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;
import static com.tnb.app.garage7.utils.AppConstants.SHARED_PREFERENCES_FILE_NAME;

/**
 * Created by Acer on 11/2/2018.
 */
@Module(includes = {
        ViewModelModule.class,
        NetworkModule.class
})
public class AppModule {

    private final GarageAplication application;

    public AppModule(GarageAplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Webservice provideWebservice(Retrofit retrofit) {
        return retrofit.create(Webservice.class);
    }

    @Provides @Singleton @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides @Singleton
    SharedPreferences provideSharedPreferences() {
        return application.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME,
                MODE_PRIVATE);
    }
}
