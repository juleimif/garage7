package com.tnb.app.garage7.presentation.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.Adapter.ViewPagerPublicacionesAdapter;
import com.tnb.app.garage7.presentation.fragment.BaseFragment;

public class PublicacionesFragment extends BaseFragment {
    public static final String TAG = PublicacionesFragment.class.getSimpleName();

    private View view;

    private ViewPager pager;
    private TabLayout tabLayout;

    private String tabTitles[] = new String[]{"Activas", "Pausadas", "Finalizadas"};

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_list_publicaciones;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        this.view = view;
        Log.i(TAG, "publicaciones: ");
        initView();

    }

    public void initView() {

        pager = (ViewPager) view.findViewById(R.id.viewPagerOpciones);
        pager.setAdapter(new ViewPagerPublicacionesAdapter(getChildFragmentManager(), tabTitles));

        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs_opciones);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setVisibility(View.VISIBLE);

    }


}
