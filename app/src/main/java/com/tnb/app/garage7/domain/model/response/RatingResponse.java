package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class RatingResponse {
    @SerializedName("AVG(rating_comprador)")
    @Expose
    public String aVGRatingComprador;
}
