package com.tnb.app.garage7.data.network.request;

import com.tnb.app.garage7.data.entity.TransactionState;

public class UpdateTransactionRequest {

    public final String state;

    public UpdateTransactionRequest(String state) {
        this.state = state;
    }
}
