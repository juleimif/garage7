package com.tnb.app.garage7.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;
import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.domain.presenter.CreateAccountImpl;
import com.tnb.app.garage7.data.view.CreateAccountView;
import com.tnb.app.garage7.presentation.Adapter.EstadosAdapter;
import com.tnb.app.garage7.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateAccountActivity extends BaseActivity implements CreateAccountView, Validator.ValidationListener {
    private static final String TAG = CreateAccountActivity.class.getSimpleName();

    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;
    @NotEmpty(message = "Debe ingresar su nombre")
    @BindView(R.id.input_nombre)
    EditText input_nombre;
    @NotEmpty(message = "Debe ingresar su apellido")
    @BindView(R.id.input_apellido)
    EditText input_apellido;
    @NotEmpty(message = "Debe ingresar su cedula")
    @BindView(R.id.input_cedula)
    EditText input_cedula;
    @NotEmpty(message = "Debe ingresar su ciudad")
    @BindView(R.id.input_ciudad)
    EditText input_ciudad;
    @NotEmpty(message = "Debe ingresar su telefono")
    @BindView(R.id.input_telefono)
    EditText input_telefono;
    @NotEmpty(message = "Debe ingresar su correo")
    @BindView(R.id.input_correo)
    EditText input_correo;
    @NotEmpty(message = "Debe ingresar su contraseña")
    @BindView(R.id.input_password)
    EditText input_contraseña;
    @NotEmpty(message = "Debe confirmar su contraseña")
    @BindView(R.id.input_password_conf)
    EditText input_contraseña_conf;
    @BindView(R.id.rv_ciudades)
    RecyclerView rv_ciudades;
    private boolean boolCiudad = false;

    private Validator validator;
    private CreateAccountImpl createAccountImpl;
    private String genero, pais, ciudad;
    private EstadosAdapter estadosAdapter;
    private static AutoCompleteTextView autoComplete_textView;
    String formattedNumber = null;

    String[] GENERO = {"Femenino", "Masculino"};

    @Override
    public int getLayout() {
        return R.layout.activity_create_account;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        validator = new Validator(this);
        validator.setValidationListener(this);

        //get country
        pais = getIntent().getStringExtra("country");
        Log.i(TAG, "onCreateView: " + pais);

        //Implements presenter
        createAccountImpl = new CreateAccountImpl(this);


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, GENERO);

        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.sp_sexo);
        materialDesignSpinner.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                genero = adapterView.getItemAtPosition(i).toString();
            }
        });

        input_telefono.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    input_telefono.removeTextChangedListener(this);
                    String value = input_telefono.getText().toString();

                    if (value != null && !value.equals("")) {
                        if (value.length() == 4) {
                            input_telefono.setText(input_telefono.getText().toString() + "-");

                            input_telefono.setSelection(input_telefono.getText().toString().length());
                        }

                    }
                    input_telefono.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    input_telefono.addTextChangedListener(this);
                }
//                if (s.length() > 11) {
//                    input_telefono.setError("Formato incorrecto");
//                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        input_cedula.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

               /* Log.i(TAG, "onTextChanged: " + pais);
                if(pais.length()>0) {
                    if (pais.equals("Venezuela")) {
                        if (s.length() > 8 || s.length() < 6) {
                            input_cedula.setError("Formato incorrecto");
                        }
                    }
                    if (pais.equals("Panamá")) {
                        if (s.length() > 8 || s.length() < 6) {
                            input_cedula.setError("Formato incorrecto");
                        }
                    }
                    if (pais.equals("Colombia")) {
                        if (s.length() > 8 || s.length() < 6) {
                            input_cedula.setError("Formato incorrecto");
                        }
                    }
                }
                else
                input_cedula.setError("Seleccione un país");*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        input_ciudad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ciudad = String.valueOf(s);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ciudad.length() > 2) {
                    if (input_ciudad.hasFocus()){
                        createAccountImpl.searchCity(ciudad, pais);
                    }
                }

            }
        });

    }

    @OnClick(R.id.iv_salir)
    public void salir(View view) {
        onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);

    }

    @OnClick(R.id.btn_singIn)
    public void registar(View view) {
        if (Utils.validateEmail(input_correo.getText().toString()) == false)
            input_correo.setError("Formato incorrecto");

        if (Utils.validatePass(input_contraseña.getText().toString()) == false) {
            Log.i(TAG, "validatePasworld 1: ");
            input_contraseña.requestFocus();
            input_contraseña.setError("La contraseña debe contener números, letras y un minímo de 10 caracteres");
        }
        if (Utils.validatePass(input_contraseña_conf.getText().toString()) == false) {
            Log.i(TAG, "validatePasworld:  2");
            input_contraseña_conf.requestFocus();
            input_contraseña_conf.setError("La contraseña debe contener números, letras y un minímo de 10 caracteres");
        }
        if (validatePasworld(input_contraseña.getText().toString(), input_contraseña_conf.getText().toString()) == true) {
            validator.validate();
        }

    }

    @Override
    public void onValidationSucceeded() {
        Log.i(TAG, "onValidationSucceeded: ");

        User user = new User(Utils.ucFirst(input_nombre.getText().toString()),
                Utils.ucFirst(input_apellido.getText().toString()),
                genero, pais, input_ciudad.getText().toString(),
                input_ciudad.getText().toString(),
                input_cedula.getText().toString(), input_telefono.getText().toString(),
                input_correo.getText().toString(), "0",
                input_contraseña_conf.getText().toString(), "0", "");

        createAccountImpl.CreateAccount(user);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean validatePasworld(String p1, String p2) {

        if (p1.equals(p2)) {
            Log.i(TAG, "son iguales: ");
            return true;
        } else {
            Utils.showSnackBar(getApplicationContext(), "Las contraseñas no coinciden", ly_account);
            return false;
        }
    }


    @Override
    public void showLoading() {
        progress = new ProgressDialog(this);
        progress.setTitle("Resgistrando");
        progress.setMessage("Por favor espere...");
        progress.show();
    }

    @Override
    public void hideLoading() {
        if (progress != null) {
            progress.dismiss();
        }

    }

    @Override
    public void gotoMain() {
        startActivity(new Intent(CreateAccountActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
        finish();
    }

    @Override
    public void showNotify(String s) {
        Utils.showSnackBar(getApplicationContext(), s, ly_account);
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showCitys(List<AutoCompletarEstadosResponse> data) {

        estadosAdapter = new EstadosAdapter(getContext(), data, new EstadosAdapter.BrandItemClick() {
            @Override
            public void onBrokerClick(AutoCompletarEstadosResponse clickedBroker) {
                Log.i(TAG, "onBrokerClick: " + clickedBroker.getEstado());
                input_ciudad.setFocusableInTouchMode(false);
                input_ciudad.setFocusable(false);

                input_ciudad.setText(clickedBroker.getEstado());
                rv_ciudades.setVisibility(View.GONE);

                input_ciudad.setFocusableInTouchMode(true);
                input_ciudad.setFocusable(true);
            }
        });
        rv_ciudades.setVisibility(View.VISIBLE);
        rv_ciudades.setAdapter(estadosAdapter);


    }

}
