package com.tnb.app.garage7.presentation.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.request.UpdateStatusCarRequest;
import com.tnb.app.garage7.domain.model.response.Concesionario;
import com.tnb.app.garage7.domain.presenter.ChangeStatusPubImpl;
import com.tnb.app.garage7.domain.presenter.ChangeStatusPubPresenter;
import com.tnb.app.garage7.data.view.ChangeStatusPubView;
import com.tnb.app.garage7.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdatePublicacionFragment extends Fragment implements ChangeStatusPubView {
    public static final String TAG = UpdatePublicacionFragment.class.getSimpleName();
    View view;
    @BindView(R.id.iv_mod_ubicacion) EditText iv_mod_ubicacion;
    @BindView(R.id.iv_mod_descripción) EditText iv_mod_descripción;
    @BindView(R.id.ly_account) CoordinatorLayout ly_account;
    private ChangeStatusPubPresenter changeStatusPubPresenter;
    private UpdateStatusCarRequest updateStatusCarRequest;
    private Concesionario concesionarioSingl;
    private String idPublicacion;
    private ProgressDialog progress;


    public UpdatePublicacionFragment() {
        // Required empty public constructor
    }

    public static UpdatePublicacionFragment newInstance(String idPublicacion) {
        UpdatePublicacionFragment fragment = new UpdatePublicacionFragment();
        Bundle args = new Bundle();
        args.putString("type", idPublicacion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_update_publication, container, false);
        ButterKnife.bind(this, view);
        idPublicacion = getArguments().getString("type", "");
        changeStatusPubPresenter = new ChangeStatusPubImpl(this);
        return view;
    }

    @OnClick({R.id.iv_exit, R.id.iv_mod_ubicacion, R.id.iv_mod_precio, R.id.iv_mod_descripción, R.id.btn_pausar,
            R.id.btn_finalizar, R.id.btn_ver_publicacion})
    public void save(View view) {
        switch (view.getId()) {
            case R.id.iv_exit:
                showDialog("¿Desea guardar los datos modificados?", 1);
                break;
            case R.id.iv_mod_ubicacion:
                Log.i(TAG, "save: ");
                iv_mod_ubicacion.setFocusable(true);
                break;
            case R.id.iv_mod_precio:
                iv_mod_ubicacion.setFocusable(true);
                break;
            case R.id.iv_mod_descripción:
                iv_mod_descripción.setFocusable(true);
                break;
            case R.id.btn_pausar:
                showDialog("¿Desea pausar esta publicación?", 2);

                break;
            case R.id.btn_finalizar:
                showDialog("¿Desea finalizar esta publicación?", 3);

                break;
            case R.id.btn_ver_publicacion:

                break;
        }
    }

    public void showDialog(String message, final int i) {

        final MaterialStyledDialog.Builder dialog = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle("Importante")
                .setDescription(message)
                .setPositiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        switch(i){
                            case 1:
                                //Volvemos atras
                                getFragmentManager().popBackStack();
                                break;
                            case 2:
                                //cambiamos el estatus de la publicacion a pausadas
                                updateStatusCarRequest = new UpdateStatusCarRequest(idPublicacion, "4");
                                changeStatusPubPresenter.changeStatus(updateStatusCarRequest);
                                break;
                            case 3:
                                //cambiamos el estatus de la publicacion a finalizadas
                                updateStatusCarRequest = new UpdateStatusCarRequest(idPublicacion, "5");
                                changeStatusPubPresenter.changeStatus(updateStatusCarRequest);

                                break;
                        }
                    }
                })
                .setNegativeText("Cancelar")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        switch(i){
                            case 1:
                                getFragmentManager().popBackStack();
                                break;
                            case 2:
                                break;
                        }

                    }
                });
        dialog.show();
    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getContext());
        progress.setTitle("Cargando");
        progress.setMessage("Por favor espere...");
        progress.show();
    }

    @Override
    public void hideLoading() {

        if(progress != null){
            progress.dismiss();
        }

    }

    @Override
    public void showToast(String mssg, int type) {
        Utils.showSnackBar(getActivity(),mssg,ly_account);

    }

    @Override
    public void goBack() {
        getFragmentManager().popBackStack();
    }
}
