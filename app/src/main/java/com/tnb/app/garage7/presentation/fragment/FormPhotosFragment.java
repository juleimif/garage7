package com.tnb.app.garage7.presentation.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.NextInterface;
import com.tnb.app.garage7.domain.model.AlbumModel;
import com.tnb.app.garage7.domain.model.Car;
import com.tnb.app.garage7.domain.model.Constants;
import com.tnb.app.garage7.domain.model.PhotoItem;
import com.tnb.app.garage7.domain.model.PhotoModel;
import com.tnb.app.garage7.domain.model.request.ConcesionarioRequest;
import com.tnb.app.garage7.domain.presenter.FormPhotosImpl;
import com.tnb.app.garage7.domain.presenter.FormPhotosPresenter;
import com.tnb.app.garage7.data.view.FormPhotosView;
import com.tnb.app.garage7.presentation.Adapter.AlbumAdapter;
import com.tnb.app.garage7.presentation.Adapter.PhotoSelectorAdapter;
import com.tnb.app.garage7.presentation.Domain.PhotoSelectorDomain;
import com.tnb.app.garage7.presentation.activity.TutorialActivity;
import com.tnb.app.garage7.utils.CommonUtils;
import com.tnb.app.garage7.utils.PermissionManager;
import com.tnb.app.garage7.utils.PermissionTracker;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


public class FormPhotosFragment extends BaseFragment implements FormPhotosView,
        PhotoItem.onItemClickListener, PhotoItem.onPhotoItemCheckedListener, AdapterView.OnItemClickListener,
        View.OnClickListener, PermissionTracker {
    private static final String TAG = FormPhotosFragment.class.getSimpleName();
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.gv_photos_ar)
    GridView gvPhotos;
    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;
    @BindView(R.id.btn_tuto)
    Button btn_tuto;
    private static final int REQUEST_SELECT_PICTURE = 0x01;

    public static final String KEY_MAX = "key_max";
    private PhotoSelectorDomain photoSelectorDomain;
    private PhotoSelectorAdapter photoAdapter;
    private AlbumAdapter albumAdapter;
    public ArrayList<PhotoModel> selected;
    public ArrayList<String> pathOriginal;
    public ArrayList<String> fotos;
    String encodedImage;
    Activity activity;
    String[] afotos;
    StringBuilder sb;
    String[] myStringArray = new String[10];
    int cont = 0;
    int contPhotos, limitPhotos;
    String images;
    String fotosAdd;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "Garage7";

    private FormPhotosImpl formPhotos;
    private FormPhotosPresenter formPhotosPresenter;

    private PreferencesSessionManager preferencesSessionManager;
    private int type_publication;
    private Car userModelSingleton;
    private ConcesionarioRequest concesionarioRequestSingleton;
    public NextInterface listener;

    public FormPhotosFragment() {
        // Required empty public constructor
    }

    public static FormPhotosFragment newInstance(int type_publication) {
        FormPhotosFragment fragment = new FormPhotosFragment();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_form_photos;
    }


    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        photoSelectorDomain = new PhotoSelectorDomain(getContext());
        selected = new ArrayList<PhotoModel>();
        pathOriginal = new ArrayList<>();
        fotos = new ArrayList<>();
        afotos = new String[10];
        concesionarioRequestSingleton = ConcesionarioRequest.getInstance();

        type_publication = getArguments().getInt("type", 0);
        if (type_publication == 3) {
            btn_tuto.setVisibility(View.GONE);
            contPhotos = 0;
            images = "imagen";
            limitPhotos = 5;
        } else {
            contPhotos = 9;
            images = "imagenes";
            limitPhotos = 10;
        }

        photoAdapter = new PhotoSelectorAdapter(getContext(),
                new ArrayList<PhotoModel>(), CommonUtils.getWidthPixels(getActivity()),
                this, this, this);
        gvPhotos.setAdapter(photoAdapter);
        albumAdapter = new AlbumAdapter(getContext(),
                new ArrayList<AlbumModel>());

        formPhotosPresenter = new FormPhotosImpl(this);
        formPhotosPresenter.initImageLoader();
        preferencesSessionManager = new PreferencesSessionManager(getContext());

        verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

    }




    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onCheckedChanged(PhotoModel photoModel, CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            if (cont > contPhotos) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                Resources res = getResources();
                String title = res.getString(R.string.alerta);
                String message = res.getString(R.string.maximo);
                builder.setTitle(title);
                builder.setMessage(message);
                builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                buttonView.toggle();
            } else {
                if (!selected.contains(photoModel)) {
                    selected.add(photoModel);
                    pathOriginal.add(photoModel.getOriginalPath());
                }
                Uri u = Uri.parse(String.valueOf(Uri.fromFile(new File(photoModel.getOriginalPath()))));
                startCropActivity(u);
                photoModel.setChecked(true);

            }
        } else {
            photoModel.setChecked(false);
            selected.remove(photoModel);
            fotos.remove(cont - 1);
            cont--;
            Log.i(TAG, "unCheckedChanged: " + fotos.size());
        }
        tvNumber.setText("(" + selected.size() + ")");
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void publicar(Car car) {

    }

    @Override
    public Context getContextaAplication() {
        return getContext();
    }

    public interface OnLocalReccentListener {
        public void onPhotoLoaded(List<PhotoModel> photos);
    }

    public interface OnLocalAlbumListener {
        public void onAlbumLoaded(List<AlbumModel> albums);
    }

    private OnLocalAlbumListener albumListener = new OnLocalAlbumListener() {
        @Override
        public void onAlbumLoaded(List<AlbumModel> albums) {
            albumAdapter.update(albums);
        }
    };

    private OnLocalReccentListener reccentListener = new OnLocalReccentListener() {
        @Override
        public void onPhotoLoaded(List<PhotoModel> photos) {
            for (PhotoModel model : photos) {
                if (selected.contains(model)) {
                    model.setChecked(true);
                }
            }
            photoAdapter.update(photos);
            gvPhotos.smoothScrollToPosition(0);
            // reset(); //--keep selected photos
        }
    };

    @Override
    public void verifyPermission(String permission) {

        if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Crashlytics.log(Log.DEBUG,TAG, "--->Device is upper for external than 6.0");
                if (PermissionManager.getInstance().checkPermission(getActivity(), permission)) {
                    photoAdapter = new PhotoSelectorAdapter(getContext(),
                            new ArrayList<PhotoModel>(), CommonUtils.getWidthPixels(getActivity()),
                            this, this, this);
                    gvPhotos.setAdapter(photoAdapter);
                    albumAdapter = new AlbumAdapter(getContext(),
                            new ArrayList<AlbumModel>());
                    photoSelectorDomain.getReccent(reccentListener);
                    photoSelectorDomain.updateAlbum(albumListener);

                } else {
                    this.requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            } else {

                photoAdapter = new PhotoSelectorAdapter(getContext(),
                        new ArrayList<PhotoModel>(), CommonUtils.getWidthPixels(getActivity()),
                        this, this, this);
                gvPhotos.setAdapter(photoAdapter);
                albumAdapter = new AlbumAdapter(getContext(),
                        new ArrayList<AlbumModel>());

                photoSelectorDomain.getReccent(reccentListener);
                photoSelectorDomain.updateAlbum(albumListener);
            }
        }
    }

    @Override
    public void onResult(boolean result, int MY_PERMISSIONS_REQUEST) {

        if (MY_PERMISSIONS_REQUEST == Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {

            if (result) {
                photoSelectorDomain.getReccent(reccentListener);
                photoSelectorDomain.updateAlbum(albumListener);
            } else {
                String message = "Necesita permiso";
                new AlertDialog.Builder((getActivity()))
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                    dialog.cancel();
                                    PermissionManager permissionmanager = PermissionManager.getInstance();
                                    permissionmanager.dialogToSetting("necesitas ir a las configuraciones");
                                } else {
                                    verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                                }
                            }
                        })
                        .setNegativeButton(this.getString(R.string.exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                            }
                        })
                        .create()
                        .show();
            }
        }
    }

    @Override
    public void checkIfpermissionChanged() {

    }

    @OnClick(R.id.fa_next)
    public void Next(View view) {

        if (fotos.size() > contPhotos) {
            Log.i(TAG, "Next: " + contPhotos + "Photos size " + fotos.size());
            if (type_publication == 3) {
                getFragmentManager().popBackStack();
            }
            formato();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            Resources res = getResources();
            String title = res.getString(R.string.alert_limit_message);
            String message = res.getString(R.string.maximo_msj) +" "+limitPhotos +" "+images+" "+res.getString(R.string.maximo_msj_2);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @OnClick(R.id.btn_tuto)
    public void Tuto(View view) {
        if (type_publication == 1) {
            preferencesSessionManager.setUserTutoMoto(1);
            getTuto();
        } else {
            if (type_publication == 0) {
                preferencesSessionManager.setUserTutoCar(1);
                getTuto();
            }
        }
    }

    public void getTuto() {
        Intent intent = new Intent(getActivity(), TutorialActivity.class);
        intent.putExtra("type", type_publication);
        getActivity().startActivity(intent);
    }

    public void formato() {
        Log.i(TAG, "formato: " + fotos.size());
        sb = new StringBuilder("[");
        for (int i = 0; i < fotos.size(); i++) {
            Log.i(TAG, "formato: " + sb.append(fotos.get(i)));
            sb.append(fotos.get(i));
            if (i < fotos.size() - 1) {
                sb.append(",");
            }
        }
        sb.append("]").toString();
        fotosAdd = sb.toString();
        userModelSingleton = Car.getInstance();
        userModelSingleton.setPhotos(fotosAdd);
        if (type_publication == 3) {
            concesionarioRequestSingleton.setPhotoConcesionaria(fotosAdd);
            getFragmentManager().popBackStack();
        } else {
            listener.next(3);
        }
        Log.i(TAG, "formato: " + fotosAdd);
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            //Moto
            if (type_publication == 0) {
                if (preferencesSessionManager.getUserTutoCar() == 0) {
                    preferencesSessionManager.setUserTutoCar(1);
                    getTuto();
                }
            } else {
                if (preferencesSessionManager.getUserTutoMoto() == 0) {
                    preferencesSessionManager.setUserTutoMoto(1);
                    getTuto();
                }
            }

        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;

        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (NextInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    Log.i(TAG, "onActivityResult: ");
//                    startCropActivity(data.getData());
                } else {
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                Log.i(TAG, "onActivityResult: ");
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            Log.i(TAG, "onActivityResult: ");
//            handleCropError(data);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;

        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(50);
        options.setToolbarTitle("Editar imagen");
        options.setToolbarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(getContext(), R.color.color_toolbar));

        UCrop.of(uri, Uri.fromFile(new File(getContext().getCacheDir(), destinationFileName)))
                .withAspectRatio(18, 12)
                .withOptions(options)
                .start(getActivity(), FormPhotosFragment.this);


    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {

            Bitmap bm = BitmapFactory.decodeFile(String.valueOf(resultUri.getPath()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            //add IMAGES
            fotos.add(encodedImage);
            cont++;

        } else {
            Toast.makeText(getContext(), "toast_cannot_retrieve_cropped_image", Toast.LENGTH_SHORT).show();
        }
    }
}


