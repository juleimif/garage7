package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tnb.app.garage7.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class DetalleNegociacionDialogoFragment extends DialogFragment {
    public static final String TAG = DetalleNegociacionDialogoFragment.class.getSimpleName();
    public static DetalleNegociacionDialogoFragment newInstance(String personName, String personEmail, String base64Img) {
        DetalleNegociacionDialogoFragment f = new DetalleNegociacionDialogoFragment();
        Bundle args = new Bundle();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_detalle_negociacion);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);

        return dialogo;
    }


    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                dismiss();
                break;
        }
    }
}
