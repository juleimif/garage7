package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 18/09/2017.
 */

public class CearCitaRequest {
    @SerializedName("idpub")
    @Expose
    public String idpub;
    @SerializedName("date")
    @Expose
    public String date;

    public CearCitaRequest(String idpub, String date) {
        this.idpub = idpub;
        this.date = date;
    }

    public String getIdpub() {
        return idpub;
    }

    public void setIdpub(String idpub) {
        this.idpub = idpub;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
