package com.tnb.app.garage7.injector.component;

import com.tnb.app.garage7.injector.module.AppModule;
import com.tnb.app.garage7.injector.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Acer on 11/2/2018.
 */

//@Singleton
//@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {
    Retrofit retrofit();
}