package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Motorcycle;

import java.util.ArrayList;
import java.util.List;

public class MotorcycleListResult {

    @SerializedName(value = "data")
    private List<Motorcycle> results = new ArrayList<>();

    public List<Motorcycle> getResults() {
        return results;
    }
}
