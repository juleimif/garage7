package com.tnb.app.garage7.presentation.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.DataEstatusPublicacion;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;
import com.tnb.app.garage7.domain.model.response.PublicacionsResponse;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.PublicacionesAdapter;
import com.tnb.app.garage7.presentation.fragment.DetailsCarFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesActivasFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VentasFragment extends Fragment {
    public static final String TAG = PublicacionesActivasFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list) RecyclerView      recyclerView;
    @BindView(R.id.ly_list_save) CoordinatorLayout ly_list_save;
    @BindView(R.id.cont_publicactins)
    LinearLayout cont_publicactins;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.reload)
    ImageButton reload;
    private PublicacionesAdapter publicationesAdapter;
    private PreferencesSessionManager preferencesUserManager;
    GoDetails listener;


    public VentasFragment() {
        // Required empty public constructor
    }

    public static PublicacionesActivasFragment newInstance(String param1, String param2) {
        PublicacionesActivasFragment fragment = new PublicacionesActivasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ventas, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initViews();
                    }
                }, 0);
            }
        });

        initViews();
        return view;
    }

    private void initViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        getActivePublication();
    }

    public void getActivePublication(){

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        EstatusPublicacion body = new EstatusPublicacion("1","1");
        service.getMePublications(body).enqueue(new Callback<PublicacionsResponse>() {
            @Override
            public void onResponse(Call<PublicacionsResponse> call, Response<PublicacionsResponse> response) {
                if (response.code() == 200){
                    Log.i(TAG, "onResponse: ");
                    if(response.body().data.size()>0){
                        cont_publicactins.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        initPublicatons(response.body());
                    }
                    else {
                        cont_publicactins.setVisibility(View.VISIBLE);
                    }

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PublicacionsResponse> call, Throwable t) {

                Toast.makeText(getContext(),
                        getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();

                // If isn't refreshing
                if (!swipe_container.isRefreshing()) {
                    // Show ProgressBar
                    progressBar.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void initPublicatons(PublicacionsResponse data) {
        Log.i(TAG, "initPublicatons: " +  data.data.toString());
        publicationesAdapter = new PublicacionesAdapter(getContext(), data.data, new PublicacionesAdapter.PublicationItemClick() {

            @Override
            public void onBrokerClick(DataEstatusPublicacion clickedBroker) {
                Fragment fragmnet = new DetailsCarFragment();
                listener.goOtherFragmnet(fragmnet, TAG);
            }
        });
        hideLoading();
        recyclerView.setAdapter(publicationesAdapter);
    }

    private void respuestaServicio(String message) {
        Log.i(TAG, "respuestaServicio: " +message);

    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        initViews();
        reload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
