package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.freegeek.android.materialbanner.view.indicator.IconPageIndicator;
import com.freegeek.android.materialbanner.view.indicator.LinePageIndicator;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.Guardados;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;


public class DetailsCarFragment extends Fragment  {
    private static final String TAG = DetailsCarFragment.class.getSimpleName();

    @BindView(R.id.tv_brand) TextView             brand;
    @BindView(R.id.tv_model) TextView             model;
    @BindView(R.id.tv_año) TextView               year;
    @BindView(R.id.tv_km) TextView                km;
    @BindView(R.id.tv_desc) TextView              description;
    @BindView(R.id.tv_country) TextView          tv_location;
    @BindView(R.id.ly_list_car) CoordinatorLayout ly_list_car;
    @BindView(R.id.banner_information) BannerSlider banner_information;
    private CirclePageIndicator circlePageIndicator;
    private LinePageIndicator linePageIndicator;
    private IconPageIndicator iconPageIndicator;
    List<BannerData> list = new ArrayList<>();
    private TextView textView;
    List<Integer> icons = new ArrayList<>();
    private String[] images;
    private PreferencesSessionManager preferencesManager;
    private VehiculoCarro carDataModel;

    GoDetails listener;

    public DetailsCarFragment() {
        // Required empty public constructor
    }

    public static DetailsCarFragment newInstance(String param1, String param2) {
        DetailsCarFragment fragment = new DetailsCarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_car, container, false);
        ButterKnife.bind(this, view);
        carDataModel = VehiculoCarro.getInstance();
        preferencesManager = new PreferencesSessionManager(getContext());
        images = new String[10];
        setImagen();

//        materialBanner.setPages(
//                new ViewHolderCreator<ImageHolderView>() {
//                    @Override
//                    public ImageHolderView createHolder() {
//                        return new ImageHolderView();
//                    }
//                }, list)
//                .setIndicator(circlePageIndicator);
//
//
//        materialBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                Log.i(TAG, "onPageSelected: " + position);            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//        materialBanner.getAdapter().setIcons(icons);
//
//        materialBanner.setOnItemClickListener(new MaterialBanner.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                Log.i(TAG, "onItemClick: ");
//            }
//        });
        showInf();
        return view;
    }

    private void setImagen() {

        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto1()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto2()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto3()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto4()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto5()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto6()));
        banner_information.addBanner(new RemoteBanner(carDataModel.getPhoto7()));
        banner_information.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//        Log.i(TAG, "setImagen: " + carDataModel.getPhoto1());
//            images[0] = carDataModel.getPhoto1();
//        images[0] = carDataModel.getPhoto1();
//        images[1] = carDataModel.getPhoto2();
//        images[2] = carDataModel.getPhoto3();
//        images[3] = carDataModel.getPhoto4();
//        images[4] = carDataModel.getPhoto5();
//        images[5] = carDataModel.getPhoto6();
//        images[6] = carDataModel.getPhoto7();
//        images[7] = carDataModel.getPhoto8();
//        images[8] = carDataModel.getPhoto9();
//        images[9] = carDataModel.getPhoto10();



    }




    public void showInf(){
        brand.setText(carDataModel.getBrandC());
        model.setText(carDataModel.getModelC());
        year.setText(carDataModel.getYearC());
        km.setText(carDataModel.getMileageC());
        tv_location.setText(carDataModel.getLocationC());
        description.setText(carDataModel.getDescriptionsC());

    }



    @OnClick({R.id.iv_save, R.id.btn_ofertar,R.id.btn_solicitar_precio, R.id.iv_exit})
    public void save(View view) {
        switch (view.getId()) {
            case R.id.iv_save:
                savePublicacion();
                break;
            case R.id.iv_exit:
                getFragmentManager().popBackStack();
                getActivity().overridePendingTransition(R.anim.right_out, R.anim.right_in);

                break;
            case R.id.btn_ofertar:
                listener.goMainActivity();

                break;
            case R.id.btn_solicitar_precio:

                Utils.showSnackBar(getContext(), getResources().getString(R.string.opción), ly_list_car);

                break;
        }
    }


    public void savePublicacion(){

        Guardados guardados = new Guardados(preferencesManager.getUserId(),carDataModel.getIdPublicacion(), "C");
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "savePublicacion: " + guardados.toString());
        service.setGuardados(guardados).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                if(response.code()==200){
                    responseServive(response.body());
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    private void respuestaServicio(String message) {
        Log.e(TAG, "respuestaServicio: " + message);
        Utils.showSnackBar(getContext(),message, ly_list_car);
    }

    private void responseServive(ResponseVehiculo body) {
        if(body.status.equals("true")){
            Utils.showSnackBar(getContext(),getResources().getString(R.string.guardado), ly_list_car);
        }
        else{
            Utils.showSnackBar(getContext(),body.message, ly_list_car);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
