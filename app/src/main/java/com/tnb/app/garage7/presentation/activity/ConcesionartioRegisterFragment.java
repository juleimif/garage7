package com.tnb.app.garage7.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;
import com.tnb.app.garage7.domain.model.request.ConcesionarioRequest;
import com.tnb.app.garage7.domain.presenter.ConcesionarioImpl;
import com.tnb.app.garage7.domain.presenter.ConcesionarioPresenter;
import com.tnb.app.garage7.domain.presenter.CreateAccountImpl;
import com.tnb.app.garage7.data.view.ConcesionarioView;
import com.tnb.app.garage7.data.view.CreateAccountView;
import com.tnb.app.garage7.presentation.Adapter.EstadosAdapter;
import com.tnb.app.garage7.presentation.fragment.BaseFragment;
import com.tnb.app.garage7.presentation.fragment.PhotosConsecionarioFragment;
import com.tnb.app.garage7.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ConcesionartioRegisterFragment extends BaseFragment implements TextWatcher, ConcesionarioView, CreateAccountView, Validator.ValidationListener {
    public static final String TAG = ConcesionartioRegisterFragment.class.getSimpleName();
    @NotEmpty(message = "Debe ingresar su nombre")
    @BindView(R.id.name) EditText name;
    @NotEmpty(message = "Debe ingresar su cédula")
    @BindView(R.id.n_identification) EditText n_identification;
    @NotEmpty(message = "Debe ingresar su dirección fiscal")
    @BindView(R.id.fiscal_address) EditText fiscal_address;
    @BindView(R.id.alternative_address) EditText alternative_address;
    @NotEmpty(message = "Debe ingresar un códígo postal")
    @BindView(R.id.postal_zone) EditText postal_zone;
    @NotEmpty(message = "Debe ingresar un telefono principal")
    @BindView(R.id.phone_principal) EditText phone_principal;
    @BindView(R.id.phone_movil) EditText phone_movil;
    @BindView(R.id.phone_alternative) EditText phone_alternative;
    @NotEmpty(message = "Debe ingresar un correo")
    @BindView(R.id.email) EditText email;
    @NotEmpty(message = "Debe ingresar un correo alternativo")
    @BindView(R.id.email_alternative) EditText email_alternative;
    @NotEmpty(message = "Debe ingresar una ciudad")
    @BindView(R.id.city) EditText city;
    @NotEmpty(message = "Debe ingresar una parroquia")
    @BindView(R.id.parish) EditText parish;
    @NotEmpty(message = "Debe ingresar un némero de calle")
    @BindView(R.id.street_address) EditText street_address;
    @NotEmpty(message = "Debe ingresar un número de edificio o casa")
    @BindView(R.id.building_number) EditText building_number;
    @BindView(R.id.add_images) TextView add_images;
    @BindView(R.id.ly_account) CoordinatorLayout ly_account;
    @BindView(R.id.rv_ciudades) RecyclerView rv_ciudades;
    @BindView(R.id.btn_register) Button btn_register;
    private ProgressDialog progress;
    private ConcesionarioPresenter concesionarioPresenter;
    private Validator validator;
    private String pais, ciudad;
    private CreateAccountImpl createAccountImpl;
    private EstadosAdapter estadosAdapter;
    public GoDetails listener;
    private ConcesionarioRequest concesionarioRequestSingleton;

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_create_account_concesionario;
    }

    public static ConcesionartioRegisterFragment newInstance(String country) {
        ConcesionartioRegisterFragment fragment = new ConcesionartioRegisterFragment();
        Bundle args = new Bundle();
        args.putString("country", country);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {


        pais = getArguments().getString("country", "");
        validator = new Validator(this);
        validator.setValidationListener(this);
        Utils.showMessage("Asegúrate de ingresar todos los datos",getActivity());
        concesionarioPresenter = new ConcesionarioImpl(this);
        createAccountImpl = new CreateAccountImpl(this);
        concesionarioRequestSingleton = ConcesionarioRequest.getInstance();
        street_address.addTextChangedListener(this);
        building_number.addTextChangedListener(this);
        name.addTextChangedListener(this);
        n_identification.addTextChangedListener(this);
        fiscal_address.addTextChangedListener(this);
        postal_zone.addTextChangedListener(this);
        phone_principal.addTextChangedListener(this);
        email.addTextChangedListener(this);
        email_alternative.addTextChangedListener(this);
        parish.addTextChangedListener(this);

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ciudad = String.valueOf(s);

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(ciudad.length()>2)
                    createAccountImpl.searchCity(ciudad, pais);

            }
        });
    }

    @OnClick({R.id.btn_register,R.id.add_images })
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                validator.validate();
                break;
            case R.id.add_images:
                Fragment fragment;
                fragment = new PhotosConsecionarioFragment();
                pushFragment(fragment, "Galeria");

                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }


    @Override
    public void onValidationSucceeded() {
        if (Utils.validateEmail(email.getText().toString()) == false)
            email.setError("Formato incorrecto");

        if (Utils.validateEmail(email_alternative.getText().toString()) == false)
            email_alternative.setError("Formato incorrecto");
        else {

            if (concesionarioRequestSingleton.getPhotoConcesionaria() != null) {
                ConcesionarioRequest concesionarioRequest = new ConcesionarioRequest(Utils.ucFirst(name.getText().toString()), Utils.ucFirst(n_identification.getText().toString()), Utils.ucFirst(fiscal_address.getText().toString()), alternative_address.getText().toString(), postal_zone.getText().toString(), phone_principal.getText().toString(), phone_movil.getText().toString(), phone_alternative.getText().toString(), email.getText().toString(), email_alternative.getText().toString(), pais, city.getText().toString(), parish.getText().toString(), street_address.getText().toString(), Utils.ucFirst(building_number.getText().toString()), concesionarioRequestSingleton.getPhotoConcesionaria(), "0");
                Log.i(TAG, "Data a registrar: " + concesionarioRequest.toString());
                concesionarioPresenter.createAccount(concesionarioRequest);
            } else {
                add_images.setError(getResources().getString(R.string.add_images));
            }
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public Context getContext() {
        return getActivity().getApplication();
    }

    @Override
    public void showCitys(List<AutoCompletarEstadosResponse> data) {
        Log.i(TAG, "showCitys: " + data.size());
        if(data.size()>0) {
            estadosAdapter = new EstadosAdapter(getContext(), data, new EstadosAdapter.BrandItemClick() {
                @Override
                public void onBrokerClick(AutoCompletarEstadosResponse clickedBroker) {
                    Log.i(TAG, "onBrokerClick: " + clickedBroker.getEstado());
                    city.setText(clickedBroker.getEstado());
                    city.setFocusable(false);
                    rv_ciudades.setVisibility(View.GONE);
                }
            });
            rv_ciudades.setVisibility(View.VISIBLE);
            rv_ciudades.setAdapter(estadosAdapter);
        }
        else {
            showToast("No hay coincidencias", 0);
        }

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Enviando solicitud");
        progress.setMessage("Por favor espere...");
        progress.show();
    }

    @Override
    public void hideLoading() {

        if(progress != null){
            progress.dismiss();
        }
    }

    @Override
    public void gotoMain() {

    }

    @Override
    public void showNotify(String s) {

    }


    @Override
    public void showToast(String mssg, int type) {
        if(type == 0) {
            Utils.showSnackBar(getActivity(), mssg, ly_account);
        }
        else {
        Utils.showMessage(mssg, getActivity());
        }
    }

    @Override
    public void goBack() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(!name.getText().toString().isEmpty() && !n_identification.getText().toString().isEmpty()
                && !fiscal_address.getText().toString().isEmpty() && !postal_zone.getText().toString().isEmpty()
                && !phone_principal.getText().toString().isEmpty() && !email.getText().toString().isEmpty()
                && !city.getText().toString().isEmpty() &&  !parish.getText().toString().isEmpty()
                && !building_number.getText().toString().isEmpty() &&  !street_address.getText().toString().isEmpty()
                && !email_alternative.getText().toString().isEmpty()){
            btn_register.setBackgroundResource(R.drawable.campo_registro);
        }
        else {
            btn_register.setBackgroundResource(R.drawable.button_background);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
