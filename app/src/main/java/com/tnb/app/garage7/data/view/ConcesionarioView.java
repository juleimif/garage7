package com.tnb.app.garage7.data.view;

import android.content.Context;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface ConcesionarioView {
    Context getContext();
    void showLoading();
    void hideLoading();
    void showToast(String mssg, int type);
    void goBack();

}
