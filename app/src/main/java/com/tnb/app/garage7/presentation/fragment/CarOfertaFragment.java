package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.decorations.BaseItemDecoration;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.response.Concesionario;
import com.tnb.app.garage7.domain.model.response.ConcesionarioResponse;
import com.tnb.app.garage7.domain.presenter.PerfilConcesionarioSignInImpl;
import com.tnb.app.garage7.domain.presenter.ProfileConcesionarioPresenter;
import com.tnb.app.garage7.data.view.ProfileConcesionarioView;
import com.tnb.app.garage7.presentation.Adapter.ConcesionarioAdapter;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;


public class CarOfertaFragment extends Fragment implements ProfileConcesionarioView {
    public static final String TAG = CarOfertaFragment.class.getSimpleName();
    @BindView(R.id.tv_brand) TextView brand;
    @BindView(R.id.tv_model) TextView model;
    @BindView(R.id.tv_año) TextView year;
    @BindView(R.id.tv_km) TextView km;
    @BindView(R.id.tv_desc) TextView description;
    @BindView(R.id.rv_concesionarios) RecyclerView rv_concesionarios;
    @BindView(R.id.banner_slider1) BannerSlider bannerSlider;
    private VehiculoCarro carDataModel;
    private Context context;
    private String idConsecionario;
    private ConcesionarioAdapter concesionarioAdapter;
    private PreferencesSessionManager preferencesManager;
    private ProfileConcesionarioPresenter profileConcesionarioPresenter;
    private Concesionario concesionarioSingl;
    GoDetails listener;

    public CarOfertaFragment() {
        // Required empty public constructor
    }

    public static CarOfertaFragment newInstance(VehiculoCarro data) {
        CarOfertaFragment fragment = new CarOfertaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_ofert, container, false);
        ButterKnife.bind(this, view);
        carDataModel = VehiculoCarro.getInstance();
        preferencesManager = new PreferencesSessionManager(getContext());
        context = view.getContext();
        showImages();
        showInf();
        showMessage("Importante", "Seleccione un concesionario para ver el vehículo");
        concesionarioSingl = Concesionario.getInstance();
        profileConcesionarioPresenter = new PerfilConcesionarioSignInImpl(this);
        profileConcesionarioPresenter.getConcesinario();

        return view;
    }

    private void getInitView(List<Concesionario> consecionarios) {
        rv_concesionarios.addItemDecoration(new BaseItemDecoration(getContext(), R.drawable.line_horizontal));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rv_concesionarios.setHasFixedSize(true);

        rv_concesionarios.setLayoutManager(layoutManager);

        concesionarioAdapter = new ConcesionarioAdapter(context, consecionarios, new ConcesionarioAdapter.BrandItemClick() {
            @Override
            public void onBrokerClick(Concesionario clickedBroker) {

                //seteamos la informacion a mostrar
                concesionarioSingl.setIdConcesionarias(clickedBroker.getIdConcesionarias());
                concesionarioSingl.setName(clickedBroker.getName());
                concesionarioSingl.setStreetAddress(clickedBroker.getStreetAddress());
                concesionarioSingl.setEmail(clickedBroker.getEmail());
                concesionarioSingl.setPhoneMovil(clickedBroker.getPhoneMovil());
                concesionarioSingl.setCountry(clickedBroker.getCountry());
                concesionarioSingl.setCity(clickedBroker.getCity());
                concesionarioSingl.setPhotoConcesionaria(clickedBroker.getPhotoConcesionaria());
                concesionarioSingl.setPhotoConcesionariaDos(clickedBroker.getPhotoConcesionariaDos());
                concesionarioSingl.setPhotoConcesionariaTres(clickedBroker.getPhotoConcesionariaTres());
                concesionarioSingl.setPhotoConcesionariaCuatro(clickedBroker.getPhotoConcesionariaCuatro());
                concesionarioSingl.setPhotoConcesionariaCinco(clickedBroker.getPhotoConcesionariaCinco());
                concesionarioSingl.setPuntuacion(clickedBroker.getPuntuacion());


                Log.i(TAG, "onBrokerClick: " + concesionarioSingl.toString());

                final MaterialStyledDialog.Builder dialog_concesionario = new MaterialStyledDialog.Builder(getContext())
                        .setStyle(Style.HEADER_WITH_TITLE)
                        //.setHeaderDrawable(R.drawable.ic_garage)
                        .withDialogAnimation(true)
                        .withDarkerOverlay(true)
                        .setTitle("Importante")
                        .setDescription("Slecciona una opción para continuar")
                        .setPositiveText("Ver perfil")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Fragment profileConcesionario = new ProfileConcesionarioFragment();
                                listener.goOtherFragmnet(profileConcesionario, TAG);
                                //Llamamos perfil del concesionario

//                                solicitarCita(    getResources().getString(R.string.alert_cita_message), getResources().getString(R.string.cita_msj));

                            }
                        })
                        .setNegativeText("Cancelar")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                            }
                        });
                dialog_concesionario.show();
            }

        });
        rv_concesionarios.setAdapter(concesionarioAdapter);
    }


    private void showMessage(String tittle, String message) {

        final MaterialStyledDialog.Builder dialog = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(tittle)
                .setDescription(message)
                .setPositiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                });

        dialog.show();
    }



    public void showInf() {
        brand.setText(carDataModel.getBrandC());
        model.setText(carDataModel.getModelC());
        year.setText(carDataModel.getYearC());
        km.setText(carDataModel.getMileageC());
        description.setText(carDataModel.getDescriptionsC());
    }

    public void showImages() {
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo1));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo2));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo3));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo4));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo5));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo6));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo7));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo8));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo9));
        bannerSlider.addBanner(new RemoteBanner(carDataModel.photo10));
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showInf(ConcesionarioResponse response) {
        getInitView(response.getData().getConsecionarios());
    }


}
