package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class AutocompletarModels {

    @SerializedName("modelo")
    @Expose
    public String modelo;

    public String getModelo() {
        return modelo;
    }
}
