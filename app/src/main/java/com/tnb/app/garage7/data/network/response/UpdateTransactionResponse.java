package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.TransactionUser;

public class UpdateTransactionResponse {

    @SerializedName("data")
    public final UpdateTransaction response;

    public UpdateTransactionResponse(UpdateTransaction response) {
        this.response = response;
    }

    public String state() {
        return response.state;
    }

    public TransactionUser seller() {
        return response.seller;
    }

    public TransactionUser buyer() {
        return response.buyer;
    }

    private class UpdateTransaction {

        public final String state;

        public final TransactionUser seller;

        public final TransactionUser buyer;

        public UpdateTransaction(String state, TransactionUser seller,
                                 TransactionUser buyer) {
            this.state = state;
            this.seller = seller;
            this.buyer = buyer;
        }
    }
}
