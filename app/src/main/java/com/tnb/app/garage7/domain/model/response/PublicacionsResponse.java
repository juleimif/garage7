package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.domain.model.DataEstatusPublicacion;

import java.util.List;

/**
 * Created by Juleimis on 02/10/2017.
 */

public class PublicacionsResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public List<DataEstatusPublicacion> data = null;

    public String getStatus() {
        return status;
    }

    public List<DataEstatusPublicacion> getData() {
        return data;
    }
}
