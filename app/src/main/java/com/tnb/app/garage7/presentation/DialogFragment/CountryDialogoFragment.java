package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.presentation.activity.ConcesionartioRegisterFragment;
import com.tnb.app.garage7.presentation.fragment.ProfileOpcionesFragment;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class CountryDialogoFragment extends DialogFragment {
    public static final String TAG = ProfileOpcionesFragment.class.getSimpleName();

    String[] PAIS = {"Colombia", "Panamá", "Venezuela"};
    private String pais = "";
    @BindView(R.id.sp_pais)
    MaterialBetterSpinner sp_pais;
    GoDetails listener;

    public static CountryDialogoFragment newInstance() {
        CountryDialogoFragment f = new CountryDialogoFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_choice_pais);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);

        ArrayAdapter<String> arrayAdapterPais = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, PAIS);
        sp_pais.setAdapter(arrayAdapterPais);

        sp_pais.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pais = adapterView.getItemAtPosition(i).toString();

            }
        });

        return dialogo;
    }

    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                if(pais.equals("")){
                    sp_pais.setError("Elija su país de origen");
                }
                else {
                    Fragment fragment =  ConcesionartioRegisterFragment.newInstance(pais);
                    listener.goOtherFragmnet(fragment, "Registro");
                    dismiss();
                }
                break;

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
