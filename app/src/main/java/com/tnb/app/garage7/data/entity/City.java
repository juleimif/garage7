package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class City {

    @NonNull
    public final String id;

    @NonNull
    public final String city;

    @NonNull
    public final String countryId;

    @NonNull
    public final String country;

    public City(@NonNull String id, @NonNull String city, @NonNull String countryId,
                @NonNull String country) {
        this.id = id;
        this.city = city;
        this.countryId = countryId;
        this.country = country;
    }

    public String toString() {
        return city;
    }
}
