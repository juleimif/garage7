package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Garage {

    @NonNull
    public final String id;

    public final String userId;

    public final String name;

    public final String idNumber;

    public final String fiscalAddress;

    public final String altAddress;

    public final String postalZone;

    public final String phone;

    public final String celphone;

    public final String altPhone;

    public final String email;

    public final String altEmail;

    public final String city;

    public final String parish;

    public final String street;

    public final String building;

    public final String buildingNumber;

    public final String isActive;

    public final String country;

    public final String countryId;

    public final String isApproved;

    public List<PhotoUrl> photosUrls = new ArrayList<>();

    public Garage(@NonNull String id, String userId, String name, String idNumber,
                  String fiscalAddress, String altAddress, String postalZone,
                  String phone, String celphone, String altPhone, String email,
                  String altEmail, String city, String parish, String street,
                  String building, String buildingNumber, String isActive,
                  String country, String countryId, String isApproved, List<PhotoUrl> photosUrls) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.idNumber = idNumber;
        this.fiscalAddress = fiscalAddress;
        this.altAddress = altAddress;
        this.postalZone = postalZone;
        this.phone = phone;
        this.celphone = celphone;
        this.altPhone = altPhone;
        this.email = email;
        this.altEmail = altEmail;
        this.city = city;
        this.parish = parish;
        this.street = street;
        this.building = building;
        this.buildingNumber = buildingNumber;
        this.isActive = isActive;
        this.country = country;
        this.countryId = countryId;
        this.isApproved = isApproved;
        this.photosUrls = photosUrls;
    }
}
