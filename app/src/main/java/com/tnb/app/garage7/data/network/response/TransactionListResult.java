package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionListResult {

    @SerializedName(value = "data")
    private List<Transaction> results = new ArrayList<>();

    public List<Transaction> getResults() {
        return results;
    }
}
