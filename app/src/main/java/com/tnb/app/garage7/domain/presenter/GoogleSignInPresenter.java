package com.tnb.app.garage7.domain.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.tnb.app.garage7.presentation.activity.BienvenidaActivity;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface GoogleSignInPresenter {
    void createGoogleClient (Context loginView);
    void onStart();
    void signIn(Activity loginView);
    void onActivityResult (Context loginView, int requestCode, int resultCode, Intent data);
    void onStop ();
    void onDestroy();
    void getCountry(String country);
}
