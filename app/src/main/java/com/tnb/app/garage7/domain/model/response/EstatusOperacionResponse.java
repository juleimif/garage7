package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 18/09/2017.
 */

public class EstatusOperacionResponse {

    @SerializedName("negociacion")
    @Expose
    public String negociacion;
    @SerializedName("publicacion")
    @Expose
    public String publicacion;
    @SerializedName("idusuario")
    @Expose
    public String idusuario;
    @SerializedName("consecionaria")
    @Expose
    public String consecionaria;
    @SerializedName("type_publication")
    @Expose
    public String typePublication;
    @SerializedName("transferencia")
    @Expose
    public String transferencia;
    @SerializedName("status_negociacion")
    @Expose
    public String statusNegociacion;
    @SerializedName("rating_comprador")
    @Expose
    public String ratingComprador;
    @SerializedName("rating_consecionario")
    @Expose
    public String ratingConsecionario;
    @SerializedName("rating_vendedor")
    @Expose
    public String ratingVendedor;

    public String getNegociacion() {
        return negociacion;
    }

    public void setNegociacion(String negociacion) {
        this.negociacion = negociacion;
    }

    public String getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(String publicacion) {
        this.publicacion = publicacion;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getConsecionaria() {
        return consecionaria;
    }

    public void setConsecionaria(String consecionaria) {
        this.consecionaria = consecionaria;
    }

    public String getTypePublication() {
        return typePublication;
    }

    public void setTypePublication(String typePublication) {
        this.typePublication = typePublication;
    }

    public String getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(String transferencia) {
        this.transferencia = transferencia;
    }

    public String getStatusNegociacion() {
        return statusNegociacion;
    }

    public void setStatusNegociacion(String statusNegociacion) {
        this.statusNegociacion = statusNegociacion;
    }

    public String getRatingComprador() {
        return ratingComprador;
    }

    public void setRatingComprador(String ratingComprador) {
        this.ratingComprador = ratingComprador;
    }

    public String getRatingConsecionario() {
        return ratingConsecionario;
    }

    public void setRatingConsecionario(String ratingConsecionario) {
        this.ratingConsecionario = ratingConsecionario;
    }

    public String getRatingVendedor() {
        return ratingVendedor;
    }

    public void setRatingVendedor(String ratingVendedor) {
        this.ratingVendedor = ratingVendedor;
    }
}
