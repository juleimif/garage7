package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.ResponseGuardados;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SaveAdapter extends RecyclerView.Adapter<SaveAdapter.ViewHolder> {

    private CarItemClick mItemClickListener;
    private  List<ResponseGuardados> saveList;
    private int temp;
    private Context contexto;
    private String idPersona;

    public SaveAdapter(Context applicationContext, List<ResponseGuardados> save, CarItemClick brokerItemClick) {
        this.saveList =  save;
        this.contexto = applicationContext;
        mItemClickListener = (CarItemClick) brokerItemClick;
    }


    @Override
    public SaveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_guardados, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ResponseGuardados currentItem = saveList.get(position);
        SaveAdapter.ViewHolder viewHolder = (SaveAdapter.ViewHolder) holder;
        viewHolder.tv_price.setText(currentItem.priceC);
        viewHolder.tv_brand.setText(currentItem.brandC);

        Display display =((WindowManager)contexto.getSystemService(contexto.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height=display.getHeight();


        Glide.with(contexto)
                .load(currentItem.getPhoto1())
                .apply(new RequestOptions()
                        .override((3*width)/10, (1*height)/5) // set exact size
                        .fitCenter() // keep memory usage low by fitting into (w x h) [optional]
                )
                .into(viewHolder.iv_car);
        /*

        Glide.with(mContext)
                .load(currentItem.getFeaturedImageResIdFav())
                .centerCrop()
                .into(brokerViewHolder.featuredImageFav);*/

    }

    @Override
    public int getItemCount() {

        return saveList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.tv_brand) TextView tv_brand;
        @BindView(R.id.tv_price) TextView tv_price;
        @BindView(R.id.iv_car)
        CircleImageView iv_car;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(0);
        }

    }

    public interface CarItemClick {
        void onBrokerClick(int clickedBroker);
    }

}
