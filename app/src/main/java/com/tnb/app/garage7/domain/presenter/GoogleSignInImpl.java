package com.tnb.app.garage7.domain.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.CheckEmail;
import com.tnb.app.garage7.domain.model.ResponseCheckEmail;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.data.view.LoginView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class GoogleSignInImpl implements GoogleSignInPresenter,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = GoogleSignInImpl.class.getSimpleName();

    private LoginView fieldLoginView;
    // Google client to communicate with Google
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN_G = 2017;
    private ResponseUser userModelSingleton;
    String personName;
    String personEmail;
    String personId;
    Uri personPhoto;
    String base64Img;
    String country;
    final PreferencesSessionManager preferencesUserManager;

    public GoogleSignInImpl(LoginView loginView) {
        this.fieldLoginView = loginView;
        preferencesUserManager = new PreferencesSessionManager(fieldLoginView.getContext());

    }

    @Override
    public void createGoogleClient(Context loginView) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        fieldLoginView.specifyGoogleSignIn(gso);
        mGoogleApiClient = new GoogleApiClient.Builder(loginView)
                .enableAutoManage((FragmentActivity) loginView /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
    }

    @Override
    public void signIn(Activity loginView) {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        loginView.startActivityForResult(signInIntent, RC_SIGN_IN_G);

    }

    @Override
    public void onActivityResult(Context loginView, int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN_G) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result, loginView);
        }
    }

    private void handleSignInResult(GoogleSignInResult result, Context loginView) {
        if (result.isSuccess()) {

            Log.d(TAG, "handleSignInResult:" + result.isSuccess());
            GoogleSignInAccount acct = result.getSignInAccount();
            personName = acct.getDisplayName();
            personEmail = acct.getEmail();
            personId = acct.getId();
            personPhoto = acct.getPhotoUrl();
            userModelSingleton = ResponseUser.getInstance();
            userModelSingleton.setName(personName);
            userModelSingleton.setEmail(personEmail);

            //setPrreference(acct);


            if(acct.getPhotoUrl()!=null) {
                userModelSingleton.setAvatarURL(acct.getPhotoUrl().toString());
                new CargarImagen(acct.getPhotoUrl().toString()).execute();
            }

        } else {
            Toast.makeText(fieldLoginView.getContext(),
                    fieldLoginView.getContext().getResources().getString(R.string.connection_error),
                    Toast.LENGTH_LONG)
                    .show();
            Log.i(TAG, "handleSignInResult error: ");

        }
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop: ");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    private class CargarImagen extends AsyncTask<Void, Void, Void> {
        private String foto_url;

        public CargarImagen(String foto_url) {
            this.foto_url = foto_url;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fieldLoginView.showLoading();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Bitmap bitmapFromserver = Utils.getBitmapFromURL(foto_url);
                base64Img = Utils.convertImageToStringForServer(bitmapFromserver);
                userModelSingleton.seturlAvatar(foto_url);
                Log.i(TAG, "doInBackground: " + foto_url);
                Log.i(TAG, "doInBackground: " + base64Img);
                Log.i(TAG, "doInBackground: " + userModelSingleton.getAvatarURL());


            } catch (Exception e) {
                e.printStackTrace();
            }
            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            comprabarRegistro();

            //fieldLoginView.hideLoading();
            //this method will be running on UI thread
        }
    }

    public void setPrreference() {

        //Guardamos la sesion
        preferencesUserManager.setServerToken("0");
        preferencesUserManager.setUserType("0");
        preferencesUserManager.setUserName(personName);
        preferencesUserManager.setUserEmail(personEmail);
        preferencesUserManager.setUserAvatar(userModelSingleton.getAvatarURL());

    }

    public void comprabarRegistro() {

        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();

        CheckEmail checkEmail = new CheckEmail(personEmail);
        inter.checkEmail(checkEmail).enqueue(new Callback<ResponseCheckEmail>() {
            @Override
            public void onResponse(Call<ResponseCheckEmail> call, Response<ResponseCheckEmail> response) {
                fieldLoginView.hideLoading();
                if (response.code() == 200) {
                    if (response.body().status.equals("true")) {
                        Log.i(TAG, "id user " + response.body().getIduser().get(0).getIdusuarios());
                        setPrreference();
                        preferencesUserManager.setUserId(response.body().getIduser().get(0).getIdusuarios());
                        preferencesUserManager.setUserConce(response.body().getIduser().get(0).getIsconsecionario());
                        preferencesUserManager.setUserIdConcesionario(response.body().getIdconse());
                        fieldLoginView.startProfileActivity();

                    } else {
                        Log.i(TAG, "onResponse voy a registrar: " + personName + personEmail + base64Img);
                        fieldLoginView.choiceCountry(personName, personEmail, base64Img);
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseCheckEmail> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

                Toast.makeText(fieldLoginView.getContext(),
                        fieldLoginView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                fieldLoginView.hideLoading();
                fieldLoginView.desconectar();
            }
        });

    }

    private void respuestaServicio(String message) {
        Log.e(TAG, "respuestaServicio: " + message);

    }

    @Override
    public void onDestroy() {
        fieldLoginView = null;
    }

    @Override
    public void getCountry(String country) {
        this.country = country;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

