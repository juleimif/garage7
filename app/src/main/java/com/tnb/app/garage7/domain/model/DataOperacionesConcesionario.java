package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;

import java.util.List;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class DataOperacionesConcesionario {
    @SerializedName("publicaciones")
    @Expose
    public List<OperacionesConcesionarioResponse> publicaciones = null;

    public List<OperacionesConcesionarioResponse> getPublicaciones() {
        return publicaciones;
    }
}

