package com.tnb.app.garage7.common.country;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.data.vo.Resource;

import java.util.List;

import javax.inject.Inject;

public class CountryDialogViewModel extends ViewModel {

    private LiveData<Resource<List<Country>>> countriesLiveData;

    @Inject
    public CountryDialogViewModel(@NonNull AppRepository repository) {
        countriesLiveData = repository.getCountries();
    }

    public LiveData<Resource<List<Country>>> countriesLiveData() {
        return countriesLiveData;
    }

}
