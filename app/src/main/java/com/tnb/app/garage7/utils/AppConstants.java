package com.tnb.app.garage7.utils;

public class AppConstants {

    /**
     * Connection timeout duration
     */
    public static final int CONNECT_TIMEOUT = 60 * 1000;
    /**
     * Connection Read timeout duration
     */
    public static final int READ_TIMEOUT = 60 * 1000;
    /**
     * Connection write timeout duration
     */
    public static final int WRITE_TIMEOUT = 60 * 1000;
    /**
     * Endpoint
     */
    public static final String BASE_URL = "http://garage7.jesusrivas.com.ve/";

    public static final String STORAGE_BASE_URL = "http://garage7.jesusrivas.com.ve/storage/";

    public static final String SHARED_PREFERENCES_FILE_NAME = "Garage7SP";

    public static final String SPK_ACCESS_TOKEN = "SPK_ACCESS_TOKEN";
}
