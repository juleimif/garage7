package com.tnb.app.garage7.data.view;

import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;

import java.util.List;

/**
 * Created by Acer on 11/2/2018.
 */

public interface CarListContract {

    interface View {
        void showCar(List<VehiculoCarro> posts);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadListCar();
    }
}

