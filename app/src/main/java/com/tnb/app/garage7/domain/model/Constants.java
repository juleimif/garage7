package com.tnb.app.garage7.domain.model;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */
public class Constants {
    public static final String BASE_URL = "https://www.garage7.net/api_restful/";

    public static class Network {

        public static final int MAX_TIMEOUT_SECONDS = 10;
        public static final String WS_UPDATE_CALIFICACION_CONCE = "addconse";

    }

    //login_status
    public final static int ONLINE = 1;
    public final static int OFFLINE = 0;
    public final static int IN_GALLERY = 3;

    //login_status
    public static String LOGIN_STATUS = "login_status";
    public final static int LOGIN_IN = 1;
    public final static int LOGIN_OUT = 0;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 6920;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 6921;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 6922;
    public static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 6923;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 6924;

    public class ConcesionarioCons {

        public static final int CITA_CONCESIONARIO = 2;
        public static final int COMPRA_CONFIRMADA_VENDEDOR = 3;
        public static final int PAGO_CONFIRMADO_VENDEDOR = 4;
        public static final int ENTREGA_DE_VEHICULO_VENDEDOR = 5;
        public static final int COMPLETADA_VENDEDOR = 6;

    }

    //Proceso de compra y venta
    public static final String CITA_CONCESIONARIO = "CITA_CONCESIONARIO";
    public static final String SOLICITANDO_CITA = "SOLICITANDO CITA";
    public static final String CITA_ACEPTADA = "CITA ACEPTADA";
    public static final String COMPRA_CONFIRMADA = "COMPRA CONFIRMADA";
    public static final String REGISTRO_DE_PAGO = "PAGO RECIBIDO";
    public static final String PAGO_EN_PROCESO = "PAGO EN PROCESO";
    public static final String ENTREGA_DE_VEHICULO = "CONFIRMACION DE ENTREGA";
    public static final String COMPLETADA = "COMPLETADA";
    public static final String CAMNCELADA = "RECHAZADA";
    public static final String REVISION = "REVISION DEL VEHICULO";


}
