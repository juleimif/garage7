package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Banner;

import java.util.List;

public class BannerListResponse {

    @SerializedName(value = "data")
    private List<Banner> results;

    public BannerListResponse(List<Banner> results) {
        this.results = results;
    }

    public List<Banner> getResults() {
        return results;
    }
}
