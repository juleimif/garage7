package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 10/09/2017.
 */

public class MotoRequest {
    @SerializedName("usuario")
    @Expose
    public String usuario;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("cilindradas")
    @Expose
    public String cilindradas;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("mileage")
    @Expose
    public String mileage;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("descriptions")
    @Expose
    public String descriptions;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("photos")
    @Expose
    public String photos;

    public MotoRequest(String usuario, String brand, String cilindradas, String year, String mileage, String color, String descriptions, String location, String price, String photos) {
        this.usuario = usuario;
        this.brand = brand;
        this.cilindradas = cilindradas;
        this.year = year;
        this.mileage = mileage;
        this.color = color;
        this.descriptions = descriptions;
        this.location = location;
        this.price = price;
        this.photos = photos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCilindradas() {
        return cilindradas;
    }

    public void setCilindradas(String cilindradas) {
        this.cilindradas = cilindradas;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "MotoRequest{" +
                "usuario='" + usuario + '\'' +
                ", brand='" + brand + '\'' +
                ", cilindradas='" + cilindradas + '\'' +
                ", year='" + year + '\'' +
                ", mileage='" + mileage + '\'' +
                ", color='" + color + '\'' +
                ", descriptions='" + descriptions + '\'' +
                ", location='" + location + '\'' +
                ", price='" + price + '\'' +
                ", photos='" + photos + '\'' +
                '}';
    }
}
