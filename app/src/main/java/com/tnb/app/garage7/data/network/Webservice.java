package com.tnb.app.garage7.data.network;

import android.arch.lifecycle.LiveData;

import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.network.request.*;
import com.tnb.app.garage7.data.network.response.*;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Webservice {

    @GET("api/brands")
    LiveData<ApiResponse<BrandListResult>> getBrands(@QueryMap Map<String, String> options);

    @GET("api/brands/{brandId}/models")
    LiveData<ApiResponse<ModelListResult>> getModels(@Path("brandId") String brandId, @QueryMap Map<String, String> options);

    @GET("api/cars")
    LiveData<ApiResponse<CarListResult>> getCars(@QueryMap Map<String, String> options);

    @GET("api/cars/{carId}")
    LiveData<ApiResponse<CarResult>> getCar(@Path("carId") String carId);

    @Multipart
    @POST("api/cars/{carId}/photos")
    LiveData<ApiResponse<UploadImageResult>> uploadCarPhoto(@Part("photo") RequestBody photo, @Path("carId") String carId);

    @GET("api/cars/{carId}/photos")
    LiveData<ApiResponse<PhotoUrlListResult>> getCarPhotoUrls(@Path("carId") String carId);

    @DELETE("api/cars/{carId}/photos/{photoId}")
    LiveData<ApiResponse<DeletedResult>> deleteCarPhoto(@Path("carId") String carId, @Path("photoId") String photoId);

    @Multipart
    @PATCH("api/cars/{carId}/photos/{photoId}")
    LiveData<ApiResponse<UpdatedResult>> updateCarPhoto(@Part("photo") RequestBody photo, @Path("carId") String carId, @Path("photoId") String photoId);

    @GET("api/colors")
    LiveData<ApiResponse<ColorListResult>> getColors(@QueryMap Map<String, String> options);

    @GET("api/countries")
    LiveData<ApiResponse<CountryListResult>> getCountries(@QueryMap Map<String, String> options);

    @GET("api/countries/{countryId}/cities")
    LiveData<ApiResponse<CityListResult>> getCities(@Path("countryId") String countryId, @QueryMap Map<String, String> options);

    @GET("api/displacements")
    LiveData<ApiResponse<DisplacementListResult>> getDisplacements(@QueryMap Map<String, String> options);

    @GET("api/garages")
    LiveData<ApiResponse<GarageListResult>> getGarages(@QueryMap Map<String, String> options);

    @GET("api/garages/{garageId}")
    LiveData<ApiResponse<GarageResponse>> getGarage(@Path("garageId") String garageId);

    @Multipart
    @POST("api/garages/{garageId}/photos")
    LiveData<ApiResponse<UploadImageResult>> uploadGaragePhoto(@Part("photo") RequestBody photo, @Path("garageId") String garageId);

    @GET("api/garages/{garageId}/photos")
    LiveData<ApiResponse<PhotoUrlListResult>> getGaragePhotoUrls(@Path("garageId") String garageId);

    @DELETE("api/garages/{garageId}/photos/{photoId}")
    LiveData<ApiResponse<DeletedResult>> deleteGaragePhoto(@Path("garageId") String garageId, @Path("photoId") String photoId);

    @Multipart
    @PATCH("api/garages/{garageId}/photos/{photoId}")
    LiveData<ApiResponse<UpdatedResult>> updateGaragePhoto(@Part("photo") RequestBody photo, @Path("garageId") String garageId, @Path("photoId") String photoId);

    @GET("api/motorcycles")
    LiveData<ApiResponse<MotorcycleListResult>> getMotorcycles(@QueryMap Map<String, String> options);

    @GET("api/motorcycles/{motorcycleId}")
    LiveData<ApiResponse<MotorcycleResult>> getMotorcycle(@Path("motorcycleId") String motorcycleId);

    @Multipart
    @POST("api/motorcycles/{motorcycleId}/photos")
    LiveData<ApiResponse<UploadImageResult>> uploadMotorcyclePhoto(@Part("photo") RequestBody photo, @Path("motorcycleId") String motorcycleId);

    @GET("api/motorcycles/{motorcycleId}/photos")
    LiveData<ApiResponse<PhotoUrlListResult>> getMotorcyclePhotoUrls(@Path("motorcycleId") String motorcycleId);

    @DELETE("api/motorcycles/{motorcycleId}/photos/{photoId}")
    LiveData<ApiResponse<DeletedResult>> deleteMotorcyclePhoto(@Path("motorcycleId") String motorcycleId, @Path("photoId") String photoId);

    @Multipart
    @PATCH("api/motorcycles/{motorcycleId}/photos/{photoId}")
    LiveData<ApiResponse<UpdatedResult>> updateMotorcyclePhoto(@Part("photo") RequestBody photo, @Path("motorcycleId") String motorcycleId, @Path("photoId") String photoId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("api/users")
    LiveData<ApiResponse<UserResult>> createUser(@Body UserRequest userRequest);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("api/users")
    Call<UserResult> createNewUser(@Body UserRequest userRequest);

    @PATCH("api/users/{userId}")
    LiveData<ApiResponse<UpdatedResult>> updateUser(@Path("userId") String userId, @Body UserRequest userRequest);

    @GET("api/users/{userId}")
    LiveData<ApiResponse<UserResult>> getUserInfo(@Path("userId") String userId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("api/user")
    LiveData<ApiResponse<UserResult>> getUserInfoByemail(@Query("email") String email);

    @POST("api/users/{userId}/cars")
    LiveData<ApiResponse<CarResult>> postNewCar(@Path("userId") String userId, @Body CarRequest carRequest);

    @PATCH("api/users/{userId}/cars/{carId}")
    LiveData<ApiResponse<UpdatedResult>> updatePostedCar(@Path("userId") String userId, @Path("carId") String carId, @Body CarRequest carRequest);

    @DELETE("api/users/{userId}/cars/{carId}")
    LiveData<ApiResponse<DeletedResult>> deletePostedCar(@Path("userId") String userId, @Path("carId") String carId);

    @POST("api/users/{userId}/favorites")
    LiveData<ApiResponse<FavoriteResult>> createFavorite(@Path("userId") String userId, @Body FavoriteRequest favorite);

    @GET("api/users/{userId}/favorites")
    LiveData<ApiResponse<FavoriteCarsResponse>> getFavoriteCars(@Path("userId") String userId, @QueryMap Map<String, String> options);

    @GET("api/users/{userId}/favorites")
    LiveData<ApiResponse<FavoriteMotorcyclesResponse>> getFavoriteMotorcycles(@Path("userId") String userId, @QueryMap Map<String, String> options);

    @DELETE("api/users/{userId}/favorites/{favoriteId}")
    LiveData<ApiResponse<DeletedResult>> deleteFavorite(@Path("userId") String userId, @Path("favoriteId") String favoriteId);

    @GET("api/users/{userId}/garages")
    LiveData<ApiResponse<GarageListResult>> getUserGarages(@Path("userId") String userId);

    @POST("api/users/{userId}/garages")
    LiveData<ApiResponse<GarageResponse>> createGarage(@Path("userId") String userId, @Body GarageRequest garage);

    @GET("api/users/{userId}/garages/{garageId}")
    LiveData<ApiResponse<GarageResponse>> getUserGarage(@Path("userId") String userId, @Path("garageId") String garageId);

    @DELETE("api/users/{userId}/garages/{garageId}")
    LiveData<ApiResponse<DeletedResult>> deleteGarage(@Path("userId") String userId, @Path("garageId") String garageId);

    @POST("api/users/{userId}/motorcycles")
    LiveData<ApiResponse<MotorcycleResult>> postNewMotorcycle(@Path("userId") String userId, @Body MotorcycleRequest motorcycleRequest);

    @PATCH("api/users/{userId}/motorcycles/{motorcycleId}")
    LiveData<ApiResponse<UpdatedResult>> updatePostedMotorcycle(@Path("userId") String userId, @Path("motorcycleId") String motorcycleId, @Body MotorcycleRequest motorcycleRequest);

    @DELETE("api/users/{userId}/motorcycles/{motorcycleId}")
    LiveData<ApiResponse<DeletedResult>> deletePostedMotorcycle(@Path("userId") String userId, @Path("motorcycleId") String motorcycleId);

    @POST("api/users/{userId}/transactions")
    LiveData<ApiResponse<TransactionResponse>> createTransaction(@Path("userId") String userId, @Body TransactionRequest transaction);

    @GET("api/users/{userId}/transactions")
    LiveData<ApiResponse<TransactionListResult>> getTransactions(@Path("userId") String userId, @QueryMap Map<String, String> options);

    @PATCH("api/users/{userId}/transactions/{transactionId}")
    LiveData<ApiResponse<UpdateTransactionResponse>> updateTransaction(@Path("userId") String userId, @Path("transactionId") String transactionId, @Body UpdateTransactionRequest state);

    @PATCH("api/users/{userId}/transactions/{transactionId}")
    LiveData<ApiResponse<StateResponse>> updateTransactionState(@Path("userId") String userId, @Path("transactionId") String transactionId, @Body UpdateTransactionRequest state);

    @GET("api/users/{userId}/transactions/{transactionId}")
    LiveData<ApiResponse<TransactionResponse>> getTransaction(@Path("userId") String userId, @Path("transactionId") String transactionId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("oauth/token")
    Call<Token> getAccessToken(@Body TokenRequest request);

    @GET("api/users/{userId}/cars")
    LiveData<ApiResponse<CarListResult>> getUserCars(@Path("userId") String userId, @QueryMap Map<String, String> options);

    @GET("api/users/{userId}/motorcycles")
    LiveData<ApiResponse<MotorcycleListResult>> getUserMotorcycles(@Path("userId") String userId, @QueryMap Map<String, String> options);

    @GET("api/banner/images")
    LiveData<ApiResponse<BannerListResponse>> getBanerImages();
}
