package com.tnb.app.garage7.common.country;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.injector.Injectable;
import com.tnb.app.garage7.data.vo.Resource;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CountryDialogFragment extends DialogFragment implements Injectable  {

    private static final String TAG = CountryDialogFragment.class.getSimpleName();

    @BindView(R.id.sp_pais)
    MaterialBetterSpinner countriesSpinner;

    @BindView(R.id.countryProgressIndicator)
    View progressIndicator;

    @BindView(R.id.errorMessage)
    View errorMessage;

    @BindView(R.id.btn_ok)
    View okButton;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    private CountryDialogViewModel viewModel;

    private CountryAdapter adapter;

    private OkButtonListener listener;

    private Country country;

    public CountryDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        return configDialog();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (OkButtonListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement OkButtonListener");
        }
    }

    @OnClick(R.id.btn_ok)
    public void onClick(View view) {
        if (country == null && adapter.getCount() > 0) {
            countriesSpinner.setError("Elija un país");
            return;
        }
        listener.onClick(country);
        getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);
        dismiss();
    }

    private Dialog configDialog() {
        adapter = new CountryAdapter(getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                new ArrayList<>());
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CountryDialogViewModel.class);
        viewModel.countriesLiveData().observe(this, this::handleResponse);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choice_pais, null);
        ButterKnife.bind(this, view);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        countriesSpinner.setAdapter(adapter);
        countriesSpinner.setOnItemClickListener(countrySelectedListener);

        return builder.create();
    }

    private void handleResponse(Resource<List<Country>> listResource) {
        if (listResource != null) {
            switch(listResource.status) {
                case SUCCESS:
                    if (listResource.data.isEmpty()) {
                        errorMessage.setVisibility(View.VISIBLE);
                        progressIndicator.setVisibility(View.GONE);
                        countriesSpinner.setVisibility(View.GONE);
                        return;
                    }
                    progressIndicator.setVisibility(View.GONE);
                    adapter.replaceData(listResource.data);
                    //countriesSpinner.setVisibility(View.VISIBLE);
                    break;
                case LOADING:
                    //countriesSpinner.setVisibility(View.GONE);
                    progressIndicator.setVisibility(View.VISIBLE);
                    break;
                case ERROR: break;
            }
        }
    }

    private AdapterView.OnItemClickListener countrySelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            country = adapter.getItem(position);
        }
    };

    public interface OkButtonListener {
        void onClick(Country country);
    }
}
