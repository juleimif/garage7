package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.SerializedName;

public class CarRequest {

    public final String model;

    public final String brand;

    public final String fuel;

    public final String color;

    public final String city;

    public final String country;

    @SerializedName("country_id")
    public final String countryId;

    public final String description;

    public final String price;

    public final String doors;

    public final String year;

    public final String mileage;

    public final String transmission;

    @SerializedName("is_active")
    public final boolean isActive;

    public CarRequest(String model, String brand, String fuel, String color, String city,
                      String country, String countryId, String description, String price,
                      String doors, String year, String mileage, String transmission,
                      boolean isActive) {
        this.model = model;
        this.brand = brand;
        this.fuel = fuel;
        this.color = color;
        this.city = city;
        this.country = country;
        this.countryId = countryId;
        this.description = description;
        this.price = price;
        this.doors = doors;
        this.year = year;
        this.mileage = mileage;
        this.transmission = transmission;
        this.isActive = isActive;
    }
}
