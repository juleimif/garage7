package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class CheckEmail {
    @SerializedName("email")
    @Expose
    public String email;

    public CheckEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CheckEmail{" +
                "email='" + email + '\'' +
                '}';
    }
}