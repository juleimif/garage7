package com.tnb.app.garage7.presentation.publicaciones.views;

import android.content.Context;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface PublicacionesView {

    void geyInfPublicacion();
    Context getContext();
    void showToast(String mssg);
}
