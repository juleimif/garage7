package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Displacement;

import java.util.ArrayList;
import java.util.List;

public class DisplacementListResult {

    @SerializedName(value = "data")
    private List<Displacement> results = new ArrayList<>();

    public List<Displacement> getResults() {
        return results;
    }
}
