package com.tnb.app.garage7.data.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.tnb.app.garage7.data.network.response.InputError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

import retrofit2.Response;

public class ApiResponse<T> {

    private static final Pattern LINK_PATTERN = Pattern
            .compile("<([^>]*)>[\\s]*;[\\s]*rel=\"([a-zA-Z0-9]+)\"");
    private static final Pattern PAGE_PATTERN = Pattern.compile("\\bpage=(\\d+)");
    private static final String NEXT_LINK = "next";

    public final int code;

    @Nullable
    public final T body;

    @Nullable
    public final InputError inputErrors;

    @Nullable
    public final String errorMessage;

    @NonNull
    public final Map<String, String> links;

    public ApiResponse(Throwable error) {
        code = 500;
        body = null;
        inputErrors = null;
        errorMessage = error.getMessage();
        links = Collections.emptyMap();
        Log.e("ApiResponse", "error  " + error.getMessage());
    }

    public ApiResponse(Response<T> response) {
        code = response.code();
        Log.e("ApiResponse", "response  " + code);
        links = Collections.emptyMap();
        if (isSuccessful()) {

            body = response.body();
            inputErrors = null;
            errorMessage = null;
        } else {
            errorMessage = "Error";

            body = null;
            Gson gson = new Gson();
            JSONObject jObjError;
            String jsonString = null;
            try {
                jObjError = new JSONObject(response.errorBody().string());
                jsonString = jObjError.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            inputErrors = gson.fromJson(jsonString, InputError.class);

        }

    }

    public boolean isSuccessful() {
        return code >= 200 && code < 300;
    }

}
