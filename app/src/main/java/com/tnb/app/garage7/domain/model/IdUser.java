package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class IdUser {

    @SerializedName("idusuarios")
    @Expose
    public String idusuarios;
    @SerializedName("isconsecionario")
    @Expose
    public String isconsecionario;

    public IdUser(String idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(String idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getIsconsecionario() {
        return isconsecionario;
    }

    public void setIsconsecionario(String isconsecionario) {
        this.isconsecionario = isconsecionario;
    }
}

