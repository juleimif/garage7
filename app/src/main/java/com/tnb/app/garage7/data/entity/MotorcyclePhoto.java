package com.tnb.app.garage7.data.entity;

import com.google.gson.annotations.SerializedName;

public class MotorcyclePhoto {

    public final String id;

    public final String photoUrl;

    @SerializedName(value = "photoable_id")
    public final String motorcycleId;

    public MotorcyclePhoto(String id, String photoUrl, String motorcycleId) {
        this.id = id;
        this.photoUrl = photoUrl;
        this.motorcycleId = motorcycleId;
    }
}
