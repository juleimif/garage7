package com.tnb.app.garage7.injector.component;

import android.app.Application;

import com.tnb.app.garage7.GarageAplication;
import com.tnb.app.garage7.injector.module.AppModule;
import com.tnb.app.garage7.injector.module.RoomModule;
import com.tnb.app.garage7.injector.module.ActivityModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityModule.class,
        RoomModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();

        Builder appModule(AppModule appModule);
    }

    void inject(GarageAplication app);
}
