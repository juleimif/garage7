package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class ConcesionarioCar {
    @SerializedName("marca")
    @Expose
    public String marca;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
