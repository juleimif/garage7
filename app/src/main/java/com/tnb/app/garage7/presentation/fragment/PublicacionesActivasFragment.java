package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnb.app.garage7.GarageAplication;
import com.tnb.app.garage7.R;
/*import com.tnb.app.garage7.injector.component.DaggerCarListComponent;
import com.tnb.app.garage7.injector.component.DaggerPublicationsComponent;
import com.tnb.app.garage7.injector.module.CarsListModule;
import com.tnb.app.garage7.injector.module.PublicactionsModule;*/
import com.tnb.app.garage7.data.view.CarListContract;
import com.tnb.app.garage7.data.view.PublicationsContract;
import com.tnb.app.garage7.domain.presenter.CarListPresenteImp;
import com.tnb.app.garage7.domain.presenter.PublicationsImp;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.DataEstatusPublicacion;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;
import com.tnb.app.garage7.domain.model.response.PublicacionsResponse;
import com.tnb.app.garage7.data.view.PublicacionesView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.PublicacionesAdapter;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PublicacionesActivasFragment extends Fragment implements PublicationsContract.View {
    public static final String TAG = PublicacionesActivasFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list) RecyclerView           recyclerView;
    @BindView(R.id.ly_list_save) CoordinatorLayout      ly_list_save;
    @BindView(R.id.cont_publicactins) LinearLayout      cont_publicactins;
    @BindView(R.id.swipe_container) SwipeRefreshLayout  swipe_container;
    @BindView(R.id.progressBar) ProgressBar             progressBar;
    @BindView(R.id.reload) ImageButton                  reload;
    private PublicacionesAdapter publicationesAdapter;
    private PreferencesSessionManager preferencesUserManager;
    GoDetails listener;
    //@Inject
    PublicationsImp publicationsImp;

    public PublicacionesActivasFragment() {
        // Required empty public constructor
    }

    public static PublicacionesActivasFragment newInstance(String param1, String param2) {
        PublicacionesActivasFragment fragment = new PublicacionesActivasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_publicaciones_activas, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivePublication();

                    }
                }, 0);
            }
        });

        getActivePublication();


        return view;
    }

    private void initViews() {

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    public void getActivePublication() {

        EstatusPublicacion body = new EstatusPublicacion(preferencesUserManager.getUserId(), "0");
        Log.i(TAG, "getActivePublication: " + body.toString());

        /*DaggerPublicationsComponent.builder()
                .networkComponent(((GarageAplication) getActivity().getApplication()).getNetComponent())
                .publicactionsModule(new PublicactionsModule(this))
                .build().inject(this);*/
        publicationsImp.loadPublicatonsByStatus(body);

    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        getActivePublication();
        reload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }


    @Override
    public void showPublicaions(List<DataEstatusPublicacion> posts) {
        initViews();

        publicationesAdapter = new PublicacionesAdapter(getContext(), posts, new PublicacionesAdapter.PublicationItemClick() {

            @Override
            public void onBrokerClick(DataEstatusPublicacion clickedBroker) {
                Fragment fragmnet = UpdatePublicacionFragment.newInstance(clickedBroker.getIdpublicacion());
                listener.goOtherFragmnet(fragmnet, "Editar publicación");
            }
        });
        hideLoading();
        recyclerView.setAdapter(publicationesAdapter);

    }

    @Override
    public void showError(String message) {
        hideLoading();
        reload.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(),
                getContext().getResources().getString(R.string.connection_error),
                Toast.LENGTH_LONG)
                .show();

    }

    @Override
    public void showComplete() {

    }
}
