package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.RegisterResponse;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.data.view.LoginView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class GoogleDialogoFragment extends DialogFragment implements Validator.ValidationListener{
    public static final String TAG = GoogleDialogoFragment.class.getSimpleName();

    @NotEmpty(message = "Debe ingresar su cédula")
    @BindView(R.id.input_cedula) TextInputEditText input_cedula;
    @BindView(R.id.spinner_pais) Spinner spinner_pais;
    @BindView(R.id.input_nombre) TextInputEditText input_nombre;
    @BindView(R.id.input_correo) TextInputEditText input_correo;
    @BindView(R.id.iv_profile) CircleImageView iv_profile;

    private ResponseUser userDataModel;
    private ArrayAdapter<String> spinnerArrayAdapter;
    private Activity activity;
    private Validator validator;


    @Override
    public void onValidationSucceeded() {
        User user = new User(input_nombre.getText().toString(),"",
                "","","","",
                "".toString(), "","",input_correo.getText().toString(),
                "0", "", "");
        Registrer(user);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    public interface OkDialogListener {
        void onOkButtonClick();// Eventos Botón ok para seleccionar el objetivo
        void onDestroyAc();
    }

    OkDialogListener listener;
    LoginView fieldLoginView;


    public static GoogleDialogoFragment newInstance() {
        GoogleDialogoFragment f = new GoogleDialogoFragment();
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialogo_google);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this,dialogo);

        validator = new Validator(this);
        validator.setValidationListener(this);

        userDataModel = ResponseUser.getInstance();
        input_nombre.setText(userDataModel.getName());
        input_correo.setText(userDataModel.getEmail());

        // Initializing a String Array
        String[] paises = new String[]{
                "Pais",
                "Colombia",
                "Panamá",
                "Venezuela"
        };

        final List<String> paisesList = new ArrayList<>(Arrays.asList(paises));
        setAdapterPaises(paisesList);


        spinner_pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){
                    // Notify the selected item text

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cargarImagen();
        return dialogo;

    }

    public void setAdapterPaises(List<String> paisesList){
        // Initializing an ArrayAdapter
        spinnerArrayAdapter = new ArrayAdapter<String>(activity,R.layout.spinner_item,paisesList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }

        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner_pais.setAdapter(spinnerArrayAdapter);
    }

    private void cargarImagen() {

        String foto= userDataModel.getAvatarURL();
        final byte[] decodedBytes = Base64.decode(foto, Base64.DEFAULT);
        Glide.with(getActivity())
                .load(foto)
                .into(iv_profile);
        }

    @OnClick({R.id.iv_salir, R.id.btn_singIn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_salir:
                fieldLoginView.desconectar();
                dismiss();
                break;
            case R.id.btn_singIn:
                validator.validate();
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity=activity;
        try {
            listener = (OkDialogListener) activity;
            fieldLoginView = (LoginView) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(
                    activity.toString() +
                            " no implementó OkDialogListener");

        }
    }

    public void Registrer(User user){
        final ProgressDialog progress;
        progress = new ProgressDialog(activity);
        progress.setTitle("Cargando");
        progress.setMessage("Por favor espere...");
        progress.show();


        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        inter.setRegistreUser(user).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(progress != null){
                    progress.dismiss();
                }
                Log.e(TAG, "onResponse: " + response.code() );
                if (response.code() == 200){
                    Log.i(TAG, "onResponse: ");
                    listener.onOkButtonClick();
                    responseLogin(response.body());

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void respuestaServicio(String message) {

    }

    private void responseLogin(RegisterResponse body) {
        if(body.status.equals("true")){
        }
        else{
        }
    }
}
