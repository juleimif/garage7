package com.tnb.app.garage7.signup;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.common.city.CityFilterAdapter;
import com.tnb.app.garage7.data.entity.City;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.network.request.UserRequest;
import com.tnb.app.garage7.data.network.response.UserResult;
import com.tnb.app.garage7.data.vo.Resource;
import com.tnb.app.garage7.main.MainActivity;
import com.tnb.app.garage7.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

import static com.tnb.app.garage7.login.LoginActivity.USER_EMAIL;
import static com.tnb.app.garage7.login.LoginActivity.USER_PASSWORD;
import static com.tnb.app.garage7.welcome.WelcomeActivity.COUNTRY_ID;

public class SignupActivity extends AppCompatActivity implements Validator.ValidationListener{

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    SignupViewModel signupViewModel;

    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;

    @NotEmpty(message = "Debe ingresar su nombre")
    @BindView(R.id.input_nombre)
    EditText input_nombre;

    @NotEmpty(message = "Debe ingresar su apellido")
    @BindView(R.id.input_apellido)
    EditText input_apellido;

    @Length(min = 3, max = 16, message = "Debe ingresar su cédula, Máx. 16 dígitos")
    @BindView(R.id.input_cedula)
    EditText input_cedula;

    @NotEmpty(message = "Debe ingresar su ciudad")
    @BindView(R.id.input_ciudad)
    AutoCompleteTextView input_ciudad;

    @Length(min = 10, max = 16, message = "Su teléfono debe tener 10 o más dígitos")
    @BindView(R.id.input_telefono)
    EditText input_telefono;

    @Email(message = "Debe ingresar un correo válido")
    @BindView(R.id.input_correo)
    EditText input_correo;

    @BindView(R.id.progressIndicator)
    View progressIndicator;

    @BindView(R.id.btn_sign_up)
    View signupButton;

    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC_SYMBOLS,
            message = "Debe ingresar su contraseña, con letras, números y símbolos")
    @BindView(R.id.input_password)
    EditText input_password;

    @ConfirmPassword(message = "Debe confirmar su contraseña")
    @BindView(R.id.input_password_conf)
    EditText input_password_conf;

    @NotEmpty(message = "Debe seleccionar un género")
    @BindView(R.id.sp_sexo)
    MaterialBetterSpinner materialDesignSpinner;

    private String genero, cityId;

    private Validator validator;

    String[] GENDER = {"Femenino", "Masculino"};

    List<City> cities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);

        initGenderSpinner();
        initViewModel();

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initViewModel() {
        signupViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SignupViewModel.class);
        Intent i = getIntent();
        signupViewModel.init(i.getStringExtra(COUNTRY_ID));
        signupViewModel.getCitiesLiveData().observe(this, this::handleCitiesResponse);
    }

    private void initGenderSpinner() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, GENDER);

        materialDesignSpinner.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setOnItemClickListener((adapterView, view, i, l) ->
                genero = adapterView.getItemAtPosition(i).toString());
    }

    @OnClick(R.id.btn_sign_up)
    public void signUp() {
        showProgress(true);
        validator.validate();
    }

    private boolean isCityValid() {
        boolean result = false;
        Log.e("signup", input_ciudad.getText().toString());
        for(City c: cities) {
            Log.e("cities", c.city);
            if (c.city.equals(input_ciudad.getText().toString())) {
                result = true;
                break;
            }
        }
        return result;
    }

    private void showProgress(boolean show) {
        progressIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        signupButton.setVisibility(!show ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.iv_salir)
    public void salir(View view) {
        onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    private void handleCitiesResponse(Resource<List<City>> citiesResource) {
        if (citiesResource != null) {
            switch (citiesResource.status) {
                case ERROR: break;
                case LOADING: break;
                case SUCCESS:
                    prepareCitiesInput(citiesResource.data);
                    break;
            }
        }
    }

    private void prepareCitiesInput(List<City> cities) {
        if (cities != null) {
            this.cities = cities;
            CityFilterAdapter adapter = new CityFilterAdapter(this,
                    android.R.layout.select_dialog_item,
                    cities);
            input_ciudad.setThreshold(2);
            input_ciudad.setAdapter(adapter);
            input_ciudad.setOnItemClickListener((parent, view, position, id) ->
                    cityId = Objects.requireNonNull(adapter.getItem(position)).id);
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (isCityValid()) {
            Intent i = getIntent();
            UserRequest userRequest = new UserRequest(
                Utils.ucFirst(input_nombre.getText().toString()),
                Utils.ucFirst(input_apellido.getText().toString()),
                genero,
                cityId,
                i.getStringExtra(COUNTRY_ID),
                input_telefono.getText().toString(),
                input_correo.getText().toString(),
                input_password.getText().toString(),
                input_password_conf.getText().toString(),
                input_cedula.getText().toString()
            );
            signupViewModel.createNewUser(userRequest).observe(this, this::handleNewUserResponse);
        } else {
            showProgress(false);
            input_ciudad.setError("Debe seleccionar una ciudad válida");
        }
    }

    private void handleNewUserResponse(UserResult userResource) {
        if (userResource != null) {
            requestToken();
        }
    }

    private void requestToken() {
        signupViewModel.requestToken(input_correo.getText().toString(),
                input_password.getText().toString())
                .observe(this, this::handleResponse);
    }

    private void handleResponse(Token token) {
        if (token != null) {
            goToMainScreen();
        }
    }

    private void goToMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(USER_EMAIL, input_correo.getText().toString());
        intent.putExtra(USER_PASSWORD, input_password.getText().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );

        startActivity(intent);
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
        finish();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        showProgress(false);
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
            else{
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        if (!isCityValid()) {
            input_ciudad.setError("Debe seleccionar una ciudad válida");
        }
    }

}
