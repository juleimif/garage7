package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.DataEstatusPublicacion;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComprasAdapter extends RecyclerView.Adapter<ComprasAdapter.ViewHolder> {

    private PublicationItemClick mItemClickListener;
    private  List<DataEstatusPublicacion> publicacionsList;
    private Context context;

    public ComprasAdapter(Context applicationContext, List<DataEstatusPublicacion> publicacionsList, PublicationItemClick brokerItemClick) {
        this.publicacionsList =  publicacionsList;
        mItemClickListener = (PublicationItemClick) brokerItemClick;
        this.context = applicationContext;
        Log.i("jule", "PublicacionesAdapter: " +publicacionsList.size() );
    }


    @Override
    public ComprasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_publication, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataEstatusPublicacion currentItem = publicacionsList.get(position);
        ComprasAdapter.ViewHolder viewHolder = (ComprasAdapter.ViewHolder) holder;
    }

    @Override
    public int getItemCount() {

        return publicacionsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_desc) TextView tv_desc;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(publicacionsList.get(position));
        }

    }

    public interface PublicationItemClick {
        void onBrokerClick(DataEstatusPublicacion clickedBroker);
    }


}
