package com.tnb.app.garage7.presentation.listeners;

/**
 * Created by Luisana Abache on 09/07/2017.
 */

public interface NextInterface {
    void next(int num);
}
