package com.tnb.app.garage7.data.network.response;

public class StateResponse {

    public final String state;

    public StateResponse(String state) {
        this.state = state;
    }
}
