package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SolicitudesAdapter extends RecyclerView.Adapter<SolicitudesAdapter.ViewHolder> {

    private PublicationItemClick mItemClickListener;
    private  List<OperacionesConcesionarioResponse> solicitudesList;
    private Context context;

    public SolicitudesAdapter(Context applicationContext, List<OperacionesConcesionarioResponse> solicitudesList, PublicationItemClick brokerItemClick) {
        this.solicitudesList =  solicitudesList;
        mItemClickListener = (PublicationItemClick) brokerItemClick;
        this.context = applicationContext;
    }


    @Override
    public SolicitudesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_solicitudes, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OperacionesConcesionarioResponse currentItem = solicitudesList.get(position);
        SolicitudesAdapter.ViewHolder viewHolder = (SolicitudesAdapter.ViewHolder) holder;
        viewHolder.tv_name.setText(currentItem.getUsername());
        viewHolder.tv_name_car.setText(currentItem.getBrandC() + " - " +currentItem.getModelC());
        viewHolder.tv_price.setText(currentItem.getPriceC() + " VEF");


        Glide.with(context)
                .load(currentItem.getPhoto1())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(viewHolder.iv_car);

    }

    @Override
    public int getItemCount() {

        return solicitudesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_price) TextView tv_price;
        @BindView(R.id.tv_name_car)    TextView tv_name_car;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mItemClickListener.onBrokerClick(solicitudesList.get(position));
        }

    }

    public interface PublicationItemClick {
        void onBrokerClick(OperacionesConcesionarioResponse clickedBroker);
    }

}
