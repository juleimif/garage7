package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class Concesionario {
    @SerializedName("idConcesionarias")
    @Expose
    public String idConcesionarias;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("n_identification")
    @Expose
    public String nIdentification;
    @SerializedName("fiscal_address")
    @Expose
    public String fiscalAddress;
    @SerializedName("alternative_address")
    @Expose
    public String alternativeAddress;
    @SerializedName("postal_zone")
    @Expose
    public String postalZone;
    @SerializedName("phone_principal")
    @Expose
    public String phonePrincipal;
    @SerializedName("phone_movil")
    @Expose
    public String phoneMovil;
    @SerializedName("phone_alternative")
    @Expose
    public String phoneAlternative;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("email_alternative")
    @Expose
    public String emailAlternative;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("parish")
    @Expose
    public String parish;
    @SerializedName("street_address")
    @Expose
    public String streetAddress;
    @SerializedName("building_number")
    @Expose
    public String buildingNumber;
    @SerializedName("photo_concesionaria")
    @Expose
    public String photoConcesionaria;
    @SerializedName("photo_concesionaria_dos")
    @Expose
    public String photoConcesionariaDos;
    @SerializedName("photo_concesionaria_tres")
    @Expose
    public String photoConcesionariaTres;
    @SerializedName("photo_concesionaria_cuatro")
    @Expose
    public String photoConcesionariaCuatro;
    @SerializedName("photo_concesionaria_cinco")
    @Expose
    public String photoConcesionariaCinco;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("puntuacion")
    @Expose
    public String puntuacion;


    private static Concesionario mInstance = null;

    public static Concesionario getInstance(){
        if(mInstance == null)
        {
            mInstance = new Concesionario();
        }
        return mInstance;
    }

    public String getIdConcesionarias() {
        return idConcesionarias;
    }

    public void setIdConcesionarias(String idConcesionarias) {
        this.idConcesionarias = idConcesionarias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getnIdentification() {
        return nIdentification;
    }

    public void setnIdentification(String nIdentification) {
        this.nIdentification = nIdentification;
    }

    public String getFiscalAddress() {
        return fiscalAddress;
    }

    public void setFiscalAddress(String fiscalAddress) {
        this.fiscalAddress = fiscalAddress;
    }

    public String getAlternativeAddress() {
        return alternativeAddress;
    }

    public void setAlternativeAddress(String alternativeAddress) {
        this.alternativeAddress = alternativeAddress;
    }

    public String getPostalZone() {
        return postalZone;
    }

    public void setPostalZone(String postalZone) {
        this.postalZone = postalZone;
    }

    public String getPhonePrincipal() {
        return phonePrincipal;
    }

    public void setPhonePrincipal(String phonePrincipal) {
        this.phonePrincipal = phonePrincipal;
    }

    public String getPhoneMovil() {
        return phoneMovil;
    }

    public void setPhoneMovil(String phoneMovil) {
        this.phoneMovil = phoneMovil;
    }

    public String getPhoneAlternative() {
        return phoneAlternative;
    }

    public void setPhoneAlternative(String phoneAlternative) {
        this.phoneAlternative = phoneAlternative;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailAlternative() {
        return emailAlternative;
    }

    public void setEmailAlternative(String emailAlternative) {
        this.emailAlternative = emailAlternative;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getParish() {
        return parish;
    }

    public void setParish(String parish) {
        this.parish = parish;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getPhotoConcesionaria() {
        return photoConcesionaria;
    }

    public void setPhotoConcesionaria(String photoConcesionaria) {
        this.photoConcesionaria = photoConcesionaria;
    }

    public String getPhotoConcesionariaDos() {
        return photoConcesionariaDos;
    }

    public void setPhotoConcesionariaDos(String photoConcesionariaDos) {
        this.photoConcesionariaDos = photoConcesionariaDos;
    }

    public String getPhotoConcesionariaTres() {
        return photoConcesionariaTres;
    }

    public void setPhotoConcesionariaTres(String photoConcesionariaTres) {
        this.photoConcesionariaTres = photoConcesionariaTres;
    }

    public String getPhotoConcesionariaCuatro() {
        return photoConcesionariaCuatro;
    }

    public void setPhotoConcesionariaCuatro(String photoConcesionariaCuatro) {
        this.photoConcesionariaCuatro = photoConcesionariaCuatro;
    }

    public String getPhotoConcesionariaCinco() {
        return photoConcesionariaCinco;
    }

    public void setPhotoConcesionariaCinco(String photoConcesionariaCinco) {
        this.photoConcesionariaCinco = photoConcesionariaCinco;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Override
    public String toString() {
        return "ConcesionarioCar{" +
                "idConcesionarias='" + idConcesionarias + '\'' +
                ", name='" + name + '\'' +
                ", nIdentification='" + nIdentification + '\'' +
                ", fiscalAddress='" + fiscalAddress + '\'' +
                ", alternativeAddress='" + alternativeAddress + '\'' +
                ", postalZone='" + postalZone + '\'' +
                ", phonePrincipal='" + phonePrincipal + '\'' +
                ", phoneMovil='" + phoneMovil + '\'' +
                ", phoneAlternative='" + phoneAlternative + '\'' +
                ", email='" + email + '\'' +
                ", emailAlternative='" + emailAlternative + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", parish='" + parish + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", buildingNumber='" + buildingNumber + '\'' +
                ", photoConcesionaria='" + photoConcesionaria + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
