package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Color;

import java.util.ArrayList;
import java.util.List;

public class ColorListResult {

    @SerializedName(value = "data")
    private List<Color> results = new ArrayList<>();

    public List<Color> getResults() {
        return results;
    }
}
