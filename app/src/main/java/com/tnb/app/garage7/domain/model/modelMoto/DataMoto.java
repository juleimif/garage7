package com.tnb.app.garage7.domain.model.modelMoto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class DataMoto {

    @SerializedName("vehiculos")
    @Expose
    public List<VehiculoMoto> vehiculos;

}
