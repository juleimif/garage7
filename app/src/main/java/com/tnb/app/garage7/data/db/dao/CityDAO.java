package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.City;

import java.util.List;

@Dao
public interface CityDAO {

    @Query("SELECT * FROM City where countryId = :countryId")
    LiveData<List<City>> findAll(String countryId);

    @Query("SELECT * FROM City where countryId = :countryId and city like '%' || :city || '%' ")
    LiveData<List<City>> findByName(String countryId, String city);

    @Query("SELECT * FROM City where id = :id")
    LiveData<City> searchCityById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(City... city);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCities(List<City> cityList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createCityIfNotExists(City city);

    @Query("DELETE FROM City")
    void deleteAll();
}
