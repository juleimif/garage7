package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.response.ConcesionarioResponse;
import com.tnb.app.garage7.data.view.ProfileConcesionarioView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class PerfilConcesionarioSignInImpl implements ProfileConcesionarioPresenter {
    public static final String TAG = PerfilConcesionarioSignInImpl.class.getSimpleName();
    ProfileConcesionarioView profileConcesionarioView;

    public PerfilConcesionarioSignInImpl(ProfileConcesionarioView profileConcesionarioView) {
        this.profileConcesionarioView = profileConcesionarioView;
    }

    private void respuestaServicio(String message) {
        Log.e(TAG, "respuestaServicio: " + message);
    }

    @Override
    public void getConcesinario() {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getConcesionarios().enqueue(new Callback<ConcesionarioResponse>() {
            @Override
            public void onResponse(Call<ConcesionarioResponse> call, Response<ConcesionarioResponse> response) {
                if (response.code() == 200) {
                    responseConcesionario(response.body());
                    Log.i(TAG, "onResponse: ");
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConcesionarioResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(profileConcesionarioView.getContext(),
                        profileConcesionarioView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    private void responseConcesionario(ConcesionarioResponse body) {
        profileConcesionarioView.showInf(body);
    }
}

