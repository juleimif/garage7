package com.tnb.app.garage7.domain.model.publicacionesDestacadas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 19/1/2018.
 */

public class Vehiculo {
    @SerializedName("idpublicacion")
    @Expose
    public String idpublicacion;
    @SerializedName("type_post_c")
    @Expose
    public String typePostC;
    @SerializedName("date_publication_c")
    @Expose
    public String datePublicationC;
    @SerializedName("usuario_c")
    @Expose
    public String usuarioC;
    @SerializedName("year_c")
    @Expose
    public String yearC;
    @SerializedName("brand_c")
    @Expose
    public String brandC;
    @SerializedName("model_c")
    @Expose
    public String modelC;
    @SerializedName("color_c")
    @Expose
    public String colorC;
    @SerializedName("location_c")
    @Expose
    public String locationC;
    @SerializedName("descriptions_c")
    @Expose
    public String descriptionsC;
    @SerializedName("price_c")
    @Expose
    public String priceC;
    @SerializedName("estatus_c")
    @Expose
    public String estatusC;
    @SerializedName("photo1")
    @Expose
    public String photo1;
    @SerializedName("photo2")
    @Expose
    public String photo2;
    @SerializedName("photo3")
    @Expose
    public String photo3;
    @SerializedName("photo4")
    @Expose
    public String photo4;
    @SerializedName("photo5")
    @Expose
    public String photo5;
    @SerializedName("photo6")
    @Expose
    public String photo6;
    @SerializedName("photo7")
    @Expose
    public String photo7;
    @SerializedName("photo8")
    @Expose
    public String photo8;
    @SerializedName("photo9")
    @Expose
    public String photo9;

    public String getIdpublicacion() {
        return idpublicacion;
    }

    public String getTypePostC() {
        return typePostC;
    }

    public String getDatePublicationC() {
        return datePublicationC;
    }

    public String getUsuarioC() {
        return usuarioC;
    }

    public String getYearC() {
        return yearC;
    }

    public String getBrandC() {
        return brandC;
    }

    public String getModelC() {
        return modelC;
    }

    public String getColorC() {
        return colorC;
    }

    public String getLocationC() {
        return locationC;
    }

    public String getDescriptionsC() {
        return descriptionsC;
    }

    public String getPriceC() {
        return priceC;
    }

    public String getEstatusC() {
        return estatusC;
    }

    public String getPhoto1() {
        return photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "idpublicacion='" + idpublicacion + '\'' +
                ", typePostC='" + typePostC + '\'' +
                ", datePublicationC='" + datePublicationC + '\'' +
                ", usuarioC='" + usuarioC + '\'' +
                ", yearC='" + yearC + '\'' +
                ", brandC='" + brandC + '\'' +
                ", modelC='" + modelC + '\'' +
                ", colorC='" + colorC + '\'' +
                ", locationC='" + locationC + '\'' +
                ", descriptionsC='" + descriptionsC + '\'' +
                ", priceC='" + priceC + '\'' +
                ", estatusC='" + estatusC + '\'' +
                ", photo1='" + photo1 + '\'' +
                ", photo2='" + photo2 + '\'' +
                ", photo3='" + photo3 + '\'' +
                ", photo4='" + photo4 + '\'' +
                ", photo5='" + photo5 + '\'' +
                ", photo6='" + photo6 + '\'' +
                ", photo7='" + photo7 + '\'' +
                ", photo8='" + photo8 + '\'' +
                ", photo9='" + photo9 + '\'' +
                '}';
    }


}
