package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.utils.PreferencesSessionRemember;
import com.tnb.app.garage7.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileFragment extends Fragment implements Validator.ValidationListener {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;
    @NotEmpty(message = "Debe ingresar su nombre")
    @BindView(R.id.input_nombre)
    EditText input_nombre;
    @NotEmpty(message = "Debe ingresar su apellido")
    @BindView(R.id.input_apellido) EditText input_apellido;
    @NotEmpty(message = "Debe ingresar su cedula")
    @BindView(R.id.input_cedula) EditText input_cedula;
    @NotEmpty(message = "Debe ingresar su dirección")
    @BindView(R.id.input_dirrecion) EditText input_dirrecion;
    @NotEmpty(message = "Debe ingresar su telefono")
    @BindView(R.id.input_telefono) EditText input_telefono;
    @NotEmpty(message = "Debe ingresar su correo")
    @BindView(R.id.input_correo) EditText input_correo;
    @NotEmpty(message = "Debe ingresar su contraseña")
    @BindView(R.id.input_password) EditText input_contraseña;
    @NotEmpty(message = "Debe confirmar su contraseña")
    @BindView(R.id.input_password_conf) EditText input_contraseña_conf;

    private Validator validator;
    private String genero, pais;
    private ResponseUser userDataModel;
    private PreferencesSessionRemember preferencesManager;

    String[] GENERO = {"Femenino", "Masculino"};
    String[] PAIS = {"Colombia", "Panamà", "Venezuela"};

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        preferencesManager = new PreferencesSessionRemember(getContext());

        if(!preferencesManager.getUserEmail().equals("") && !preferencesManager.gettUserPassword().equals("")) {
            input_contraseña.setText(preferencesManager.gettUserPassword());
            input_contraseña_conf.setText(preferencesManager.gettUserPassword());

        }
        userDataModel = ResponseUser.getInstance();
        input_nombre.setText(userDataModel.getName());
        input_correo.setText(userDataModel.getEmail());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, GENERO);
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, PAIS);

        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                view.findViewById(R.id.sp_sexo);
        materialDesignSpinner.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        MaterialBetterSpinner materialDesignSpinner2 = (MaterialBetterSpinner)
                view.findViewById(R.id.sp_pais);

        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner2.setAdapter(arrayAdapter2);
        materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                genero = adapterView.getItemAtPosition(i).toString();
            }
        });
        materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pais = adapterView.getItemAtPosition(i).toString();

            }
        });

        return view;
    }

    @OnClick(R.id.btn_acept)
    public void aceptar(View view) {
        if(validatePasworld(input_contraseña.getText().toString(), input_contraseña_conf.getText().toString()))
            validator.validate();
    }

    @Override
    public void onValidationSucceeded() {


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean validatePasworld(String p1, String p2)
    {
        if (p1.equals(p2))
            return true;
        else {
            Utils.showSnackBar(getContext(),"Las contraseñas no coinciden",ly_account);
            return false;
        }
    }


}
