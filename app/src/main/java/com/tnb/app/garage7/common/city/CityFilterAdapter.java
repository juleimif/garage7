package com.tnb.app.garage7.common.city;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.City;

import java.util.ArrayList;
import java.util.List;

public class CityFilterAdapter extends ArrayAdapter<City> {

    private final Context mContext;

    private final List<City> mCities;

    private final List<City> mCitiesAll;

    private final int mLayoutResourceId;

    public CityFilterAdapter(@NonNull Context context, int resource, @NonNull List<City> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mCities = new ArrayList<>(objects);
        this.mCitiesAll = new ArrayList<>(objects);
        this.mLayoutResourceId = resource;
    }

    public int getCount() {
        return mCities.size();
    }

    public City getItem(int position) {
        return mCities.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(mLayoutResourceId, parent, false);
            }
            City city = getItem(position);
            TextView name = (TextView) convertView.findViewById(android.R.id.text1);
            name.setText(city.city);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((City) resultValue).city;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<City> citiesSuggestion = new ArrayList<>();
                if (constraint != null) {
                    for (City city : mCitiesAll) {
                        if (city.city.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            citiesSuggestion.add(city);
                        }
                    }
                    filterResults.values = citiesSuggestion;
                    filterResults.count = citiesSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mCities.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                    for (Object object : (List<?>) results.values) {
                        if (object instanceof City) {
                            mCities.add((City) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mCities.addAll(mCitiesAll);
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}
