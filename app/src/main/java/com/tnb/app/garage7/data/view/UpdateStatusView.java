package com.tnb.app.garage7.data.view;

import android.content.Context;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface UpdateStatusView {
    void showLoading();
    void hideLoading();
    void showNotify(String s);
    Context getContext();

}
