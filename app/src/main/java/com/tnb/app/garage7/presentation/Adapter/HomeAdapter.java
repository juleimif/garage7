package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Vehiculo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private HomeAdapter.CarItemClick mItemClickListener;
    private Context contexto;
    private List<Vehiculo> listDestacadaos;


    public HomeAdapter(Context context, List<Vehiculo> body, CarItemClick carItemClick) {
        this.contexto=context;
        this.listDestacadaos = body;
        mItemClickListener = (HomeAdapter.CarItemClick) carItemClick;
    }


    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_home, parent, false);
                return new HomeAdapter.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {

        Vehiculo currentItem = listDestacadaos.get(position);
        HomeAdapter.ViewHolder viewHolder = (HomeAdapter.ViewHolder) holder;
        viewHolder.tv_name.setText(currentItem.getBrandC() + " - " + currentItem.getModelC());
        viewHolder.tv_desc.setText(currentItem.getYearC() + " - " + currentItem.getLocationC());

        Glide.with(contexto)
                .load(currentItem.getPhoto1())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(viewHolder.iv_car);
    }

    @Override
    public int getItemCount() {

        return listDestacadaos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_desc) TextView tv_desc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mItemClickListener.onBrokerClick(listDestacadaos.get(position));
        }
    }

    public interface CarItemClick {
        void onBrokerClick(Vehiculo vehiculo);
    }
}
