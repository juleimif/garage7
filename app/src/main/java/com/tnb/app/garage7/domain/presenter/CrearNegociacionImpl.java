package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.request.NegociacionRequest;
import com.tnb.app.garage7.domain.model.request.UpdateStatusCarRequest;
import com.tnb.app.garage7.data.view.CrearNegociacionView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class CrearNegociacionImpl implements CrearNegociacionPresenter {
    public static final String TAG = CrearNegociacionImpl.class.getSimpleName();
    public CrearNegociacionView crearNegociacionView;

    public CrearNegociacionImpl(CrearNegociacionView crearNegociacionView){
        this.crearNegociacionView = crearNegociacionView;
    }

    private void respuestaServicio(String message) {
        crearNegociacionView.showMessage(message, "", "");

    }


    @Override
    public void crearNegociacion(final NegociacionRequest negociacionRequest) {
        Log.i(TAG, "crearNegociacion: " + negociacionRequest.toString());
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.setNegociacion(negociacionRequest).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {

                if (response.code() == 200) {
                    upDatePublication(negociacionRequest.getIdpub());
                    Log.i(TAG, "cree una negociacion, debo esperar la confirmacion: ");
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(crearNegociacionView.getContext(),
                        crearNegociacionView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

    }

    public void upDatePublication(String idPublication){
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        UpdateStatusCarRequest data = new UpdateStatusCarRequest(idPublication, "1");
        service.setEstatusCar(data).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                if (response.code() == 200) {
                    crearNegociacionView.showMessage("¡Felicitaciones!", crearNegociacionView.getContext().getResources().getString(R.string.cita_confirmacion), crearNegociacionView.getContext().getResources().getString(R.string.cita_confirmacion_conca));

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });

    }
}
