package com.tnb.app.garage7.domain.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface UpdateStatusPresenter {
    void UpdateStatus(String idNegociacion, String status);
}
