package com.tnb.app.garage7.domain.model.modelCar;

//import org.parceler.Parcel;

//import lombok.Data;
//import lombok.EqualsAndHashCode;

/**
 * Created by Juleimis on 14/07/2017.
 */

//@EqualsAndHashCode(callSuper = false)
//@Parcel
public
//@Data
class VehiculoCarro {

    public String idpostCars;

    public String typePostC;

    public String usuarioC;

    public String datePublicationC;

    public String brandC;

    public String modelC;

    public String yearC;

    public String mileageC;

    public String transmitionC;

    public String gasC;

    public String colorC;

    public String doorNumberC;

    public String descriptionsC;

    public String locationC;

    public String priceC;

    public String priceCommissionC;

    public String estatusC;

    public String idPublicacion;

    public String photo1;

    public String photo2;

    public String photo3;

    public String photo4;

    public String photo5;

    public String photo6;

    public String photo7;

    public String photo8;

    public String photo9;

    public String photo10;

    public String photos;


    //Singleton
    private static VehiculoCarro mInstance = null;

    public static VehiculoCarro getInstance() {
        if (mInstance == null) {
            mInstance = new VehiculoCarro();
        }
        return mInstance;
    }

    public VehiculoCarro() {
        super();
    }

    public VehiculoCarro(String idpostCars, String typePostC, String usuarioC, String datePublicationC, String brandC, String modelC, String yearC, String mileageC, String transmitionC, String gasC, String colorC, String doorNumberC, String descriptionsC, String locationC, String priceC, String priceCommissionC, String estatusC, String idPublicacion, String photo1, String photo2, String photo3, String photo4, String photo5, String photo6, String photo7, String photo8, String photo9, String photo10) {
        this.idpostCars = idpostCars;
        this.typePostC = typePostC;
        this.usuarioC = usuarioC;
        this.datePublicationC = datePublicationC;
        this.brandC = brandC;
        this.modelC = modelC;
        this.yearC = yearC;
        this.mileageC = mileageC;
        this.transmitionC = transmitionC;
        this.gasC = gasC;
        this.colorC = colorC;
        this.doorNumberC = doorNumberC;
        this.descriptionsC = descriptionsC;
        this.locationC = locationC;
        this.priceC = priceC;
        this.priceCommissionC = priceCommissionC;
        this.estatusC = estatusC;
        this.idPublicacion = idPublicacion;
        this.photo1 = photo1;
        this.photo2 = photo2;
        this.photo3 = photo3;
        this.photo4 = photo4;
        this.photo5 = photo5;
        this.photo6 = photo6;
        this.photo7 = photo7;
        this.photo8 = photo8;
        this.photo9 = photo9;
        this.photo10 = photo10;
    }

    @Override
    public String toString() {
        return "VehiculoCarro{" +
                "idpostCars='" + idpostCars + '\'' +
                ", typePostC='" + typePostC + '\'' +
                ", usuarioC='" + usuarioC + '\'' +
                ", datePublicationC='" + datePublicationC + '\'' +
                ", brandC='" + brandC + '\'' +
                ", modelC='" + modelC + '\'' +
                ", yearC='" + yearC + '\'' +
                ", mileageC='" + mileageC + '\'' +
                ", transmitionC='" + transmitionC + '\'' +
                ", gasC='" + gasC + '\'' +
                ", colorC='" + colorC + '\'' +
                ", doorNumberC='" + doorNumberC + '\'' +
                ", descriptionsC='" + descriptionsC + '\'' +
                ", locationC='" + locationC + '\'' +
                ", priceC='" + priceC + '\'' +
                ", priceCommissionC='" + priceCommissionC + '\'' +
                ", estatusC='" + estatusC + '\'' +
                ", idPublicacion='" + idPublicacion + '\'' +
                ", photo1='" + photo1 + '\'' +
                ", photo2='" + photo2 + '\'' +
                ", photo3='" + photo3 + '\'' +
                ", photo4='" + photo4 + '\'' +
                ", photo5='" + photo5 + '\'' +
                ", photo6='" + photo6 + '\'' +
                ", photo7='" + photo7 + '\'' +
                ", photo8='" + photo8 + '\'' +
                ", photo9='" + photo9 + '\'' +
                ", photo10='" + photo10 + '\'' +
                ", photos='" + photos + '\'' +
                '}';
    }

    public String getIdpostCars() {
        return idpostCars;
    }

    public String getTypePostC() {
        return typePostC;
    }

    public String getUsuarioC() {
        return usuarioC;
    }

    public String getDatePublicationC() {
        return datePublicationC;
    }

    public String getBrandC() {
        return brandC;
    }

    public String getModelC() {
        return modelC;
    }

    public String getYearC() {
        return yearC;
    }

    public String getMileageC() {
        return mileageC;
    }

    public String getTransmitionC() {
        return transmitionC;
    }

    public String getGasC() {
        return gasC;
    }

    public String getColorC() {
        return colorC;
    }

    public String getDoorNumberC() {
        return doorNumberC;
    }

    public String getDescriptionsC() {
        return descriptionsC;
    }

    public String getLocationC() {
        return locationC;
    }

    public String getPriceC() {
        return priceC;
    }

    public String getPriceCommissionC() {
        return priceCommissionC;
    }

    public String getEstatusC() {
        return estatusC;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public String getPhoto1() {
        return photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    public String getPhoto10() {
        return photo10;
    }

    public String getPhotos() {
        return photos;
    }

    /*public void setIdpostCars(String idpostCars) {
        this.idpostCars = idpostCars;
    }*/

    public void setTypePostC(String typePostC) {
        this.typePostC = typePostC;
    }

    public void setUsuarioC(String usuarioC) {
        this.usuarioC = usuarioC;
    }

    /*public void setDatePublicationC(String datePublicationC) {
        this.datePublicationC = datePublicationC;
    }*/

    public void setBrandC(String brandC) {
        this.brandC = brandC;
    }

    public void setModelC(String modelC) {
        this.modelC = modelC;
    }

    public void setYearC(String yearC) {
        this.yearC = yearC;
    }

    public void setMileageC(String mileageC) {
        this.mileageC = mileageC;
    }

    public void setTransmitionC(String transmitionC) {
        this.transmitionC = transmitionC;
    }

    public void setGasC(String gasC) {
        this.gasC = gasC;
    }

    public void setColorC(String colorC) {
        this.colorC = colorC;
    }

    public void setDoorNumberC(String doorNumberC) {
        this.doorNumberC = doorNumberC;
    }

    public void setDescriptionsC(String descriptionsC) {
        this.descriptionsC = descriptionsC;
    }

    public void setLocationC(String locationC) {
        this.locationC = locationC;
    }

    public void setPriceC(String priceC) {
        this.priceC = priceC;
    }

    /*public void setPriceCommissionC(String priceCommissionC) {
        this.priceCommissionC = priceCommissionC;
    }

    /*public void setEstatusC(String estatusC) {
        this.estatusC = estatusC;
    }*/

    public void setIdPublicacion(String idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public void setPhoto6(String photo6) {
        this.photo6 = photo6;
    }

    public void setPhoto7(String photo7) {
        this.photo7 = photo7;
    }

    public void setPhoto8(String photo8) {
        this.photo8 = photo8;
    }

    public void setPhoto9(String photo9) {
        this.photo9 = photo9;
    }

    public void setPhoto10(String photo10) {
        this.photo10 = photo10;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
