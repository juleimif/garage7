package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class AutocompletarMarcasResponse {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public List<AutocompletarMarcas> data = null;

    public String getStatus() {
        return status;
    }

    public List<AutocompletarMarcas> getData() {
        return data;
    }
}
