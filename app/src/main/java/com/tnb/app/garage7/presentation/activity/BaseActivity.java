package com.tnb.app.garage7.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

//import com.crashlytics.android.Crashlytics;
import com.tnb.app.garage7.R;

import butterknife.ButterKnife;
//import io.fabric.sdk.android.Fabric;

/**
 * Created by Juleimis on 27/06/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getName();
    public ProgressDialog progress;
    abstract public int getLayout();
    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        //Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);
        onCreateView(savedInstanceState);
    }


    public void pushFragment(Fragment fragment, String TAG){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.left_out, R.anim.left_in, R.anim.right_in, R.anim.right_out).replace(R.id.contenedorPrincipalFragmento, fragment, TAG).addToBackStack(null).commit();
    }


    public void setchangeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void showProgressDialogB(Context context){
        Log.i(TAG, "showProgressDialogB: " + context);
        progress = new ProgressDialog(context);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();

    }

    public void dismissDialog(){
        if(progress != null){
            progress.dismiss();
        }
    }
}
