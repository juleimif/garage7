package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Transaction;

public class TransactionResponse {

    @SerializedName(value = "data")
    private Transaction transaction;

    public Transaction transaction() {
        return transaction;
    }
}
