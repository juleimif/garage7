package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Brand {

    @NonNull
    public final String id;

    @NonNull
    public final String brand;

    public Brand(@NonNull String id, @NonNull String brand) {
        this.id = id;
        this.brand = brand;
    }

}
