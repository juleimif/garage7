package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseGuardados;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.SaveAdapter;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SavesFragment extends Fragment {
    public static final String TAG = SavesFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list) RecyclerView          recyclerView;
    @BindView(R.id.ly_list_save) CoordinatorLayout     ly_list_save;
    @BindView(R.id.ly_cont) LinearLayout               ly_cont;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar) ProgressBar            progressBar;
    @BindView(R.id.reload) ImageButton                 reload;

    private SaveAdapter saveAdapter;
    private PreferencesSessionManager preferencesUserManager;

    public SavesFragment() {
        // Required empty public constructor
    }

    public static SavesFragment newInstance(String param1, String param2) {
        SavesFragment fragment = new SavesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guardados, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAllSaves();
                    }
                }, 0);
            }
        });

        initViews();
        return view;
    }

    private void initViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        getAllSaves();
    }

    public void getAllSaves(){

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "getAllSaves: " + service.getSaves(preferencesUserManager.getUserId()).request().url());
        service.getSaves(preferencesUserManager.getUserId()).enqueue(new Callback<List<ResponseGuardados>>() {
            @Override
            public void onResponse(Call<List<ResponseGuardados>> call, Response<List<ResponseGuardados>> response) {
                if (response.code() == 200){
                    Log.i(TAG, "onResponse: " + response.code());
                    if(response.body().size()>0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        saveAdapter = new SaveAdapter(getContext(), response.body(), new SaveAdapter.CarItemClick() {
                            @Override
                            public void onBrokerClick(int clickedBroker) {

                            }
                        });
                        recyclerView.setAdapter(saveAdapter);
                    }
                    else {
                        ly_cont.setVisibility(View.VISIBLE);
                        Utils.showSnackBar(getContext(), "No existen publicaciones guardadas", ly_list_save);
                    }
                    hideLoading();

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ResponseGuardados>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                hideLoading();
                reload.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),
                        getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();

            }
        });

        // If isn't refreshing
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    private void respuestaServicio(String message) {
        Log.i(TAG, "respuestaServicio: " +message);

    }

}
