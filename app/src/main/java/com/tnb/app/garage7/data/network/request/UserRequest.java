package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRequest {

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("id_number")
    @Expose
    public String idNumber;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("city_id")
    @Expose
    public String cityId;

    @SerializedName("country_id")
    @Expose
    public String countryId;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("password_confirmation")
    @Expose
    public String passwordConfirmation;

    public UserRequest(String firstName, String lastName, String gender, String cityId,
                       String countryId, String phone, String email, String password,
                       String passwordConfirmation, String idNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.cityId = cityId;
        this.countryId = countryId;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.idNumber = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
