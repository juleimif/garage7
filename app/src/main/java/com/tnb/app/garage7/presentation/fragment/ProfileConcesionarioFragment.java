package com.tnb.app.garage7.presentation.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.request.NegociacionRequest;
import com.tnb.app.garage7.domain.model.response.Concesionario;
import com.tnb.app.garage7.domain.presenter.CrearNegociacionImpl;
import com.tnb.app.garage7.domain.presenter.CrearNegociacionPresenter;
import com.tnb.app.garage7.data.view.CrearNegociacionView;
import com.tnb.app.garage7.presentation.activity.MainActivity;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.techery.properratingbar.ProperRatingBar;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

public class ProfileConcesionarioFragment extends Fragment implements CrearNegociacionView {


    public static final String TAG = ProfileConcesionarioFragment.class.getSimpleName();
    private View view;
    private Context context;
    @BindView(R.id.tv_country) TextView tv_country;
    @BindView(R.id.tv_city) TextView tv_estado;
    @BindView(R.id.tv_ubicacion) TextView tv_ubicacion;
    @BindView(R.id.tv_tlf) TextView tv_tlf;
    @BindView(R.id.tv_email) TextView tv_email;
    @BindView(R.id.tv_name) TextView tv_name;
    @BindView(R.id.banner_slider1) BannerSlider bannerSlider;
    @BindView(R.id.lowerRatingBar)
    ProperRatingBar lowerRatingBar;

    private PreferencesSessionManager preferencesSessionManager;
    private Concesionario concesionarioSingl;
    private VehiculoCarro carDataModel;
    private CrearNegociacionPresenter crearNegociacionPresenter;


    public ProfileConcesionarioFragment() {
        // Required empty public constructor
    }

    public static ProfileConcesionarioFragment newInstance(String idConcesionario) {
        ProfileConcesionarioFragment fragment = new ProfileConcesionarioFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_profile_concesionario, container, false);
        ButterKnife.bind(this, view);
        concesionarioSingl = Concesionario.getInstance();
        carDataModel = VehiculoCarro.getInstance();
        preferencesSessionManager = new PreferencesSessionManager(getContext());
        crearNegociacionPresenter = new CrearNegociacionImpl(this);
         initView();
        return view;
    }

    private void initView() {

        Log.i(TAG, "initView: " + concesionarioSingl.toString());
        tv_name.setText(concesionarioSingl.getName().toString());
        tv_country.setText(concesionarioSingl.getCountry().toString());
        tv_estado.setText(concesionarioSingl.getCity().toString());
        tv_ubicacion.setText(Html.fromHtml("<b>Ubicación:</b>  "+concesionarioSingl.getStreetAddress().toString()));
        tv_tlf.setText(Html.fromHtml("<b>Tlf:</b>  "+concesionarioSingl.getPhoneMovil().toString()));
        tv_email.setText(Html.fromHtml("<b>Correo:</b> "+concesionarioSingl.getEmail().toString()));

        bannerSlider.addBanner(new RemoteBanner(concesionarioSingl.getPhotoConcesionaria()));
        bannerSlider.addBanner(new RemoteBanner(concesionarioSingl.getPhotoConcesionariaDos()));
        bannerSlider.addBanner(new RemoteBanner(concesionarioSingl.getPhotoConcesionariaTres()));
        bannerSlider.addBanner(new RemoteBanner(concesionarioSingl.getPhotoConcesionariaCuatro()));
        bannerSlider.addBanner(new RemoteBanner(concesionarioSingl.getPhotoConcesionariaCinco()));

        int a;
        try {
            a = Integer.parseInt(concesionarioSingl.getPuntuacion());
            lowerRatingBar.setRating(a);

        } catch (NumberFormatException nfe) {
            // Handle the condition when str is not a number.
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    @OnClick({R.id.iv_exit, R.id.btn_aceptar})
    public void save(View view) {
        switch (view.getId()) {
            case R.id.iv_exit:
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_aceptar:
                starNegociacion(getResources().getString(R.string.alert_cita_message), getResources().getString(R.string.cita_msj));
                break;
        }
    }

    private void starNegociacion(String tittle, String maessage) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            Resources res = getResources();
            String title = tittle;
            String message = maessage;
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    NegociacionRequest negociacionRequest = new NegociacionRequest(carDataModel.getIdPublicacion(), preferencesSessionManager.getUserId(), carDataModel.getUsuarioC(), concesionarioSingl.getIdConcesionarias(), carDataModel.getTypePostC()) ;

                    Log.i(TAG, "data car: " + negociacionRequest.toString());
                    crearNegociacionPresenter.crearNegociacion(negociacionRequest);
                }
            })
                    .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();

                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

    private void respuestaServicio(String message) {
        Log.i(TAG, "respuestaServicio: " + message);

    }

    @Override
    public void showMessage(String tittle, String message, String conca) {

        final MaterialStyledDialog.Builder dialog = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(tittle)
                .setDescription(message + " " +concesionarioSingl.getName()+". "+ conca)
                .setPositiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        startActivity(new Intent(getContext(), MainActivity.class));
                        getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);

                    }
                });

        dialog.show();
    }

    @Override
    public void CrearNegociacion() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showRelod() {

    }
}
