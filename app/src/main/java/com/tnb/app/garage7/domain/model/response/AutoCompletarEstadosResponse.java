package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class AutoCompletarEstadosResponse {
    @SerializedName("estado")
    @Expose
    public String estado;

    public String getEstado() {
        return estado;
    }
}
