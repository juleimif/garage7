package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class OperacionesUsuario {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public List<OperacionesUsuarioResponse> data = null;

    public String getStatus() {
        return status;
    }

    public List<OperacionesUsuarioResponse> getData() {
        return data;
    }
}

