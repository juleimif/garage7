package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Garage;

import java.util.ArrayList;
import java.util.List;

public class GarageListResult {

    @SerializedName(value = "data")
    private List<Garage> results = new ArrayList<>();

    public List<Garage> getResults() {
        return results;
    }
}
