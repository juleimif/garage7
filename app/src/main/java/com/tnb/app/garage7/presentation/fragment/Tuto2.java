package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tnb.app.garage7.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Tuto2 extends Fragment{
    @BindView(R.id.iv_tuto)
    ImageView iv_tuto;
    private int typeCar = 0;

    public Tuto2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public static Tuto2 newInstance(int  type_publication) {
        Tuto2 fragment = new Tuto2();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tuto1, container, false);
        ButterKnife.bind(this, view);
        typeCar = getArguments().getInt("type", 0);

        if(typeCar == 0) {
            iv_tuto.setBackground(getResources().getDrawable(R.drawable.tutorial2));
        }
        else {
          //  iv_tuto.setBackground(getResources().getDrawable(R.drawable.vista_lateral));
        }

        return view;
    }
}
