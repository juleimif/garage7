package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 7/2/2018.
 */

public class IduserRegister {

    @SerializedName("idusuarios")
    @Expose
    public String idusuarios;
    @SerializedName("idconse")
    @Expose
    public Integer idconse;

    public String getIdusuarios() {
        return idusuarios;
    }

    public Integer getIdconse() {
        return idconse;
    }
}
