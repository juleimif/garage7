package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(foreignKeys = @ForeignKey(entity = Car.class,
        parentColumns = "id",
        childColumns = "carId"))
public class CarPhoto {

    @PrimaryKey
    @NonNull
    public final String id;

    public final String photoUrl;

    @SerializedName(value = "photoable_id")
    public final String carId;

    public CarPhoto(String id, String photoUrl, String carId) {
        this.photoUrl = photoUrl;
        this.id = id;
        this.carId = carId;
    }
}
