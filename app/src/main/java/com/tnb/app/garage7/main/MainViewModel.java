package com.tnb.app.garage7.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.Banner;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.vo.Resource;
import com.tnb.app.garage7.utils.AbsentLiveData;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {

    private AppRepository repository;

    private LiveData<Resource<User>> userLiveData;

    private final LiveData<Resource<List<Banner>>> bannerLiveData;

    @Inject
    public MainViewModel(AppRepository repository) {
        this.repository = repository;
        bannerLiveData = repository.getBannerImagesNames();
    }

    public void init(String email) {
        userLiveData = repository.getUserByEmail(email);
    }

    public LiveData<Resource<User>> getUserLiveData() {
        if (userLiveData == null) {
            return AbsentLiveData.create();
        }
        return userLiveData;
    }

    public void deleteAllUsers() {
        repository.deleteAllUsers();
    }

    public LiveData<Resource<List<Banner>>> getBannerLiveData() {
        return bannerLiveData;
    }
}
