package com.tnb.app.garage7.domain.presenter;

import android.util.Log;

import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.request.UpdateStatusCarRequest;
import com.tnb.app.garage7.data.view.ChangeStatusPubView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class ChangeStatusPubImpl implements ChangeStatusPubPresenter {
    public static final String TAG = ChangeStatusPubImpl.class.getSimpleName();
    public ChangeStatusPubView changeStatusPubView;

    public ChangeStatusPubImpl(ChangeStatusPubView changeStatusPubView) {
        this.changeStatusPubView = changeStatusPubView;
    }


    @Override
    public void changeStatus(UpdateStatusCarRequest updateStatusCarRequest) {

        changeStatusPubView.showLoading();
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "changeStatus: " + updateStatusCarRequest.toString());
        service.setEstatusCar(updateStatusCarRequest).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                changeStatusPubView.hideLoading();
                if (response.code() == 200) {
                    response(response.body());
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                changeStatusPubView.hideLoading();
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });

    }

    private void respuestaServicio(String message) {
        changeStatusPubView.showToast(message,0);
        Log.e(TAG, "respuestaServicio: " + message );
    }

    private void response(ResponseVehiculo body) {
        if (body.status.equals("true")) {
            changeStatusPubView.showToast(body.message, 1);
            changeStatusPubView.goBack();

        } else {
            changeStatusPubView.showToast(body.message, 0);
            Log.e(TAG, "false: " + body.message );

        }
    }
}
