package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juleimis on 16/06/2017.
 */

public class ResponseCheckEmail {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("iduser")
    @Expose
    public List<IdUser> iduser = null;
    public String idconse;

    public String getIdconse() {
        return idconse;
    }

    @SerializedName("idconse")


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<IdUser> getIduser() {
        return iduser;
    }

    public void setIduser(List<IdUser> iduser) {
        this.iduser = iduser;
    }
}
