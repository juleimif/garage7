package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Car;

import java.util.ArrayList;
import java.util.List;

public class CarListResult {

    @SerializedName(value = "data")
    private List<Car> results = new ArrayList<>();

    public List<Car> getResults() {
        return results;
    }
}
