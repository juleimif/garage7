package com.tnb.app.garage7.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tnb.app.garage7.BuildConfig;
import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.network.request.TokenRequest;
import com.tnb.app.garage7.utils.AbsentLiveData;

import javax.inject.Inject;

public class LoginViewModel extends ViewModel {

    private AppRepository repository;

    @Inject
    public LoginViewModel(@NonNull AppRepository repository) {
        this.repository = repository;
    }

    public LiveData<Token> login(String email, String password) {
        TokenRequest tokenRequest = new TokenRequest("password",
                BuildConfig.GARAGE7_API_CLIENT_ID,
                BuildConfig.GARAGE7_API_CLIENT_SECRET,
                email, password, "*");
        return repository.requestToken(tokenRequest);
    }

}
