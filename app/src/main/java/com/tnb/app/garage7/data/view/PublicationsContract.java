package com.tnb.app.garage7.data.view;

import com.tnb.app.garage7.domain.model.DataEstatusPublicacion;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;

import java.util.List;

/**
 * Created by Acer on 11/2/2018.
 */

public interface PublicationsContract {

    interface View {
        void showPublicaions(List<DataEstatusPublicacion> posts);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadPublicatonsByStatus(EstatusPublicacion body);
    }
}

