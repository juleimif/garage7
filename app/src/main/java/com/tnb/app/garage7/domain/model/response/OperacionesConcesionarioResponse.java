package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class OperacionesConcesionarioResponse {
    @SerializedName("idpost_cars")
    @Expose
    public String idpostCars;
    @SerializedName("type_post_c")
    @Expose
    public String typePostC;
    @SerializedName("usuario_c")
    @Expose
    public String usuarioC;
    @SerializedName("price_c")
    @Expose
    public String priceC;
    @SerializedName("brand_c")
    @Expose
    public String brandC;
    @SerializedName("model_c")
    @Expose
    public String modelC;
    @SerializedName("idvendedor")
    @Expose
    public String idvendedor;
    @SerializedName("rating_vendedor")
    @Expose
    public String ratingVendedor;
    @SerializedName("negociacion")
    @Expose
    public String negociacion;
    @SerializedName("rating_comprador")
    @Expose
    public String ratingComprador;
    @SerializedName("status_negociacion")
    @Expose
    public String statusNegociacion;
    @SerializedName("photo1")
    @Expose
    public String photo1;
    @SerializedName("username")
    @Expose
    public String username;

    public OperacionesConcesionarioResponse(String idpostCars, String typePostC, String usuarioC, String priceC, String brandC, String modelC, String idvendedor, String ratingVendedor, String negociacion, String ratingComprador, String statusNegociacion, String photo1, String username) {
        this.idpostCars = idpostCars;
        this.typePostC = typePostC;
        this.usuarioC = usuarioC;
        this.priceC = priceC;
        this.brandC = brandC;
        this.modelC = modelC;
        this.idvendedor = idvendedor;
        this.ratingVendedor = ratingVendedor;
        this.negociacion = negociacion;
        this.ratingComprador = ratingComprador;
        this.statusNegociacion = statusNegociacion;
        this.photo1 = photo1;
        this.username = username;
    }

    public String getIdpostCars() {
        return idpostCars;
    }

    public String getTypePostC() {
        return typePostC;
    }

    public String getUsuarioC() {
        return usuarioC;
    }

    public String getPriceC() {
        return priceC;
    }

    public String getBrandC() {
        return brandC;
    }

    public String getModelC() {
        return modelC;
    }

    public String getIdvendedor() {
        return idvendedor;
    }

    public String getRatingVendedor() {
        return ratingVendedor;
    }

    public String getNegociacion() {
        return negociacion;
    }

    public String getRatingComprador() {
        return ratingComprador;
    }

    public String getStatusNegociacion() {
        return statusNegociacion;
    }

    public String getPhoto1() {
        return photo1;
    }

    public String getUsername() {
        return username;
    }
}

