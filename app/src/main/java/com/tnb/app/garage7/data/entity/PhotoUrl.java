package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class PhotoUrl {

    public final String id;

    public final String photoUrl;

    public PhotoUrl(String id, String photoUrl) {
        this.photoUrl = photoUrl;
        this.id = id;
    }
}
