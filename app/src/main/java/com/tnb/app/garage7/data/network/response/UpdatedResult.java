package com.tnb.app.garage7.data.network.response;

public class UpdatedResult {

    private boolean updated;

    public boolean isUpdated() {
        return updated;
    }
}
