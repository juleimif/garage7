package com.tnb.app.garage7.domain.model;

/**
 * Created by Luisana Abache on 16/07/2017.
 */

public class ResponseSession {

    public Integer idusuarios;
    public String name;
    public String lastname;
    public String sex;
    public String country;
    public String city;
    public String address;
    public String N_indetification;
    public String phone;
    public String email;
    public String status;
    public String password;
    public String profile_photo;
    public String isconsecionario;
    public String idconse;

    public ResponseSession(Integer idusuarios, String name, String lastname, String sex, String country, String city, String address, String n_indetification, String phone, String email, String status, String password, String profile_photo, String isconsecionario, String idconse) {
        this.idusuarios = idusuarios;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.country = country;
        this.city = city;
        this.address = address;
        N_indetification = n_indetification;
        this.phone = phone;
        this.email = email;
        this.status = status;
        this.password = password;
        this.profile_photo = profile_photo;
        this.isconsecionario = isconsecionario;
        this.idconse = idconse;
    }

    public Integer getIdusuarios() {
        return idusuarios;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getSex() {
        return sex;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getN_indetification() {
        return N_indetification;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getPassword() {
        return password;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public String getIsconsecionario() {
        return isconsecionario;
    }

    public String getIdconse() {
        return idconse;
    }

    @Override
    public String toString() {
        return "ResponseSession{" +
                "idusuarios=" + idusuarios +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", sex='" + sex + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", N_indetification='" + N_indetification + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                ", password='" + password + '\'' +
                ", profile_photo='" + profile_photo + '\'' +
                ", isconsecionario='" + isconsecionario + '\'' +
                ", idconse='" + idconse + '\'' +
                '}';
    }
}
