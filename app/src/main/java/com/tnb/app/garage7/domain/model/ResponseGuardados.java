package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class ResponseGuardados {
    @SerializedName("idpublicaciones_carros")
    @Expose
    public String idpublicacionesCarros;
    @SerializedName("type_publication_c")
    @Expose
    public String typePublicationC;
    @SerializedName("usuario_c")
    @Expose
    public String usuarioC;
    @SerializedName("date_publication_c")
    @Expose
    public String datePublicationC;
    @SerializedName("brand_c")
    @Expose
    public String brandC;
    @SerializedName("model_c")
    @Expose
    public String modelC;
    @SerializedName("year_c")
    @Expose
    public String yearC;
    @SerializedName("mileage_c")
    @Expose
    public String mileageC;
    @SerializedName("transmition_c")
    @Expose
    public String transmitionC;
    @SerializedName("color_c")
    @Expose
    public String colorC;
    @SerializedName("door_number_c")
    @Expose
    public String doorNumberC;
    @SerializedName("descriptions_c")
    @Expose
    public String descriptionsC;
    @SerializedName("location_c")
    @Expose
    public String locationC;
    @SerializedName("price_c")
    @Expose
    public String priceC;
    @SerializedName("price_commission_c")
    @Expose
    public String priceCommissionC;
    @SerializedName("estatus_c")
    @Expose
    public String estatusC;
    @SerializedName("id_publicacion")
    @Expose
    public String idPublicacion;
    @SerializedName("photo1")
    @Expose
    public String photo1;
    @SerializedName("photo2")
    @Expose
    public String photo2;
    @SerializedName("photo3")
    @Expose
    public String photo3;
    @SerializedName("photo4")
    @Expose
    public String photo4;
    @SerializedName("photo5")
    @Expose
    public String photo5;
    @SerializedName("photo6")
    @Expose
    public String photo6;
    @SerializedName("photo7")
    @Expose
    public String photo7;
    @SerializedName("photo8")
    @Expose
    public String photo8;
    @SerializedName("photo9")
    @Expose
    public String photo9;
    @SerializedName("photo10")
    @Expose
    public String photo10;
    @SerializedName("id_user")
    @Expose
    public String idUser;
    @SerializedName("type")
    @Expose
    public String type;

    public ResponseGuardados(String idpublicacionesCarros, String typePublicationC, String usuarioC, String datePublicationC, String brandC, String modelC, String yearC, String mileageC, String transmitionC, String colorC, String doorNumberC, String descriptionsC, String locationC, String priceC, String priceCommissionC, String estatusC, String idPublicacion, String photo1, String photo2, String photo3, String photo4, String photo5, String photo6, String photo7, String photo8, String photo9, String photo10, String idUser, String type) {
        this.idpublicacionesCarros = idpublicacionesCarros;
        this.typePublicationC = typePublicationC;
        this.usuarioC = usuarioC;
        this.datePublicationC = datePublicationC;
        this.brandC = brandC;
        this.modelC = modelC;
        this.yearC = yearC;
        this.mileageC = mileageC;
        this.transmitionC = transmitionC;
        this.colorC = colorC;
        this.doorNumberC = doorNumberC;
        this.descriptionsC = descriptionsC;
        this.locationC = locationC;
        this.priceC = priceC;
        this.priceCommissionC = priceCommissionC;
        this.estatusC = estatusC;
        this.idPublicacion = idPublicacion;
        this.photo1 = photo1;
        this.photo2 = photo2;
        this.photo3 = photo3;
        this.photo4 = photo4;
        this.photo5 = photo5;
        this.photo6 = photo6;
        this.photo7 = photo7;
        this.photo8 = photo8;
        this.photo9 = photo9;
        this.photo10 = photo10;
        this.idUser = idUser;
        this.type = type;
    }

    public String getIdpublicacionesCarros() {
        return idpublicacionesCarros;
    }

    public void setIdpublicacionesCarros(String idpublicacionesCarros) {
        this.idpublicacionesCarros = idpublicacionesCarros;
    }

    public String getTypePublicationC() {
        return typePublicationC;
    }

    public void setTypePublicationC(String typePublicationC) {
        this.typePublicationC = typePublicationC;
    }

    public String getUsuarioC() {
        return usuarioC;
    }

    public void setUsuarioC(String usuarioC) {
        this.usuarioC = usuarioC;
    }

    public String getDatePublicationC() {
        return datePublicationC;
    }

    public void setDatePublicationC(String datePublicationC) {
        this.datePublicationC = datePublicationC;
    }

    public String getBrandC() {
        return brandC;
    }

    public void setBrandC(String brandC) {
        this.brandC = brandC;
    }

    public String getModelC() {
        return modelC;
    }

    public void setModelC(String modelC) {
        this.modelC = modelC;
    }

    public String getYearC() {
        return yearC;
    }

    public void setYearC(String yearC) {
        this.yearC = yearC;
    }

    public String getMileageC() {
        return mileageC;
    }

    public void setMileageC(String mileageC) {
        this.mileageC = mileageC;
    }

    public String getTransmitionC() {
        return transmitionC;
    }

    public void setTransmitionC(String transmitionC) {
        this.transmitionC = transmitionC;
    }

    public String getColorC() {
        return colorC;
    }

    public void setColorC(String colorC) {
        this.colorC = colorC;
    }

    public String getDoorNumberC() {
        return doorNumberC;
    }

    public void setDoorNumberC(String doorNumberC) {
        this.doorNumberC = doorNumberC;
    }

    public String getDescriptionsC() {
        return descriptionsC;
    }

    public void setDescriptionsC(String descriptionsC) {
        this.descriptionsC = descriptionsC;
    }

    public String getLocationC() {
        return locationC;
    }

    public void setLocationC(String locationC) {
        this.locationC = locationC;
    }

    public String getPriceC() {
        return priceC;
    }

    public void setPriceC(String priceC) {
        this.priceC = priceC;
    }

    public String getPriceCommissionC() {
        return priceCommissionC;
    }

    public void setPriceCommissionC(String priceCommissionC) {
        this.priceCommissionC = priceCommissionC;
    }

    public String getEstatusC() {
        return estatusC;
    }

    public void setEstatusC(String estatusC) {
        this.estatusC = estatusC;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(String idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public void setPhoto6(String photo6) {
        this.photo6 = photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public void setPhoto7(String photo7) {
        this.photo7 = photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public void setPhoto8(String photo8) {
        this.photo8 = photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    public void setPhoto9(String photo9) {
        this.photo9 = photo9;
    }

    public String getPhoto10() {
        return photo10;
    }

    public void setPhoto10(String photo10) {
        this.photo10 = photo10;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
