package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Transaction {

    private String id;
    private String buyerId;
    private String sellerId;
    private String garageId;
    private String vehicleId;
    private String type;
    private String state;
    private String price;
    private String formattedPrice;
    private String commission;
    private String transactionNumber;
    private String paymentNumber;
    private String paymentAmount;
    private String sellerGarageRating;
    private String sellerBuyerRating;
    private String buyerGarageRating;
    private String buyerSellerRating;
    private String garageBuyerRating;
    private String isActive;

    public Transaction(String id, String buyerId, String sellerId, String garageId,
                       String vehicleId, String type, String state, String price,
                       String formattedPrice, String commission, String transactionNumber,
                       String paymentNumber, String paymentAmount, String sellerGarageRating,
                       String sellerBuyerRating, String buyerGarageRating,
                       String buyerSellerRating, String garageBuyerRating, String isActive) {
        this.id = id;
        this.buyerId = buyerId;
        this.sellerId = sellerId;
        this.garageId = garageId;
        this.vehicleId = vehicleId;
        this.type = type;
        this.state = state;
        this.price = price;
        this.formattedPrice = formattedPrice;
        this.commission = commission;
        this.transactionNumber = transactionNumber;
        this.paymentNumber = paymentNumber;
        this.paymentAmount = paymentAmount;
        this.sellerGarageRating = sellerGarageRating;
        this.sellerBuyerRating = sellerBuyerRating;
        this.buyerGarageRating = buyerGarageRating;
        this.buyerSellerRating = buyerSellerRating;
        this.garageBuyerRating = garageBuyerRating;
        this.isActive = isActive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getGarageId() {
        return garageId;
    }

    public void setGarageId(String garageId) {
        this.garageId = garageId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFormattedPrice() {
        return formattedPrice;
    }

    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getSellerGarageRating() {
        return sellerGarageRating;
    }

    public void setSellerGarageRating(String sellerGarageRating) {
        this.sellerGarageRating = sellerGarageRating;
    }

    public String getSellerBuyerRating() {
        return sellerBuyerRating;
    }

    public void setSellerBuyerRating(String sellerBuyerRating) {
        this.sellerBuyerRating = sellerBuyerRating;
    }

    public String getBuyerGarageRating() {
        return buyerGarageRating;
    }

    public void setBuyerGarageRating(String buyerGarageRating) {
        this.buyerGarageRating = buyerGarageRating;
    }

    public String getBuyerSellerRating() {
        return buyerSellerRating;
    }

    public void setBuyerSellerRating(String buyerSellerRating) {
        this.buyerSellerRating = buyerSellerRating;
    }

    public String getGarageBuyerRating() {
        return garageBuyerRating;
    }

    public void setGarageBuyerRating(String garageBuyerRating) {
        this.garageBuyerRating = garageBuyerRating;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
