package com.tnb.app.garage7.presentation.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.domain.model.CheckEmail;
import com.tnb.app.garage7.domain.model.response.CheckEmailResponse;
import com.tnb.app.garage7.presentation.activity.ProcessPublicationActivity;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TipoPublicacionFragment extends Fragment {
    public static final String TAG = TipoPublicacionFragment.class.getSimpleName();
    Intent intent;
    PreferencesSessionManager preferencesSessionManager;
    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;

    public TipoPublicacionFragment() {
        // Required empty public constructor
    }

    public static TipoPublicacionFragment newInstance(String param1, String param2) {
        TipoPublicacionFragment fragment = new TipoPublicacionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tipo_publicacion, container, false);
        ButterKnife.bind(this, view);
        preferencesSessionManager = new PreferencesSessionManager(getContext());
        return view;
    }

    public void comprabarRegistro(final int i) {

        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        Log.i(TAG, "comprabarRegistro: ");
        CheckEmail checkEmail = new CheckEmail(preferencesSessionManager.getUsereEmail());
        inter.checkByEmail(checkEmail).enqueue(new Callback<List<CheckEmailResponse>>() {
            @Override
            public void onResponse(Call<List<CheckEmailResponse>> call, Response<List<CheckEmailResponse>> response) {
                if (response.body().get(0).getnIndetification().equals("")) {
                    Utils.showSnackBar(getContext(), "Complete su información de perfil", ly_account);
                } else {
                    switch (i) {
                        case 0:
                            //Carro
                            intent = new Intent(getContext(), ProcessPublicationActivity.class);
                            intent.putExtra("type", 0);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);

                            break;
                        case 1:

                            //Moto
                            intent = new Intent(getContext(), ProcessPublicationActivity.class);
                            intent.putExtra("type", 1);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);

                            break;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<CheckEmailResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure: ");
            }
        });

    }


    @OnClick({R.id.iv_car, R.id.iv_moto})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_car:
                comprabarRegistro(0);
                break;
            case R.id.iv_moto:
                comprabarRegistro(1);
                break;

        }
    }

}
