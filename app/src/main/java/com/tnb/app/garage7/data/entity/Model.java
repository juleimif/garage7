package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Model {

    @NonNull
    public final String id;

    @NonNull
    public final String model;

    @NonNull
    public final String brandId;

    @NonNull
    public final String brand;

    public Model(@NonNull String id, String model, String brandId, String brand) {
        this.id = id;
        this.model = model;
        this.brandId = brandId;
        this.brand = brand;
    }
}
