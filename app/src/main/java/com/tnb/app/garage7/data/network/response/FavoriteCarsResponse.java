package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.FavoriteCar;

import java.util.List;

public class FavoriteCarsResponse {

    @SerializedName(value = "data")
    private List<FavoriteCar> favoriteCars;

    public List<FavoriteCar> favoriteCars() {
        return favoriteCars;
    }
}
