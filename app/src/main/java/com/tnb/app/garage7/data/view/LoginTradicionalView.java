package com.tnb.app.garage7.data.view;

import android.content.Context;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface LoginTradicionalView {
    void showLoading();
    void hideLoading();
    void gotoMain();
    void showNotify(String s);
    Context getContext();

}
