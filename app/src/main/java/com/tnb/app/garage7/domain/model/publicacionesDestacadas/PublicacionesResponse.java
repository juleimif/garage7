package com.tnb.app.garage7.domain.model.publicacionesDestacadas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Acer on 19/1/2018.
 */

public class PublicacionesResponse {
    @SerializedName("vehiculos")
    @Expose
    public List<Vehiculo> vehiculos = null;

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }
}

