package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 10/09/2017.
 */

public class NegociacionRequest {
    @SerializedName("idpub")
    @Expose
    public String idpub;
    @SerializedName("idcomprador")
    @Expose
    public String idcomprador;
    @SerializedName("idvendedor")
    @Expose
    public String idvendedor;
    @SerializedName("idconse")
    @Expose
    public String idconse;
    @SerializedName("type")
    @Expose
    public String type;

    public NegociacionRequest(String idpub, String idcomprador, String idvendedor, String idconse, String type) {
        this.idpub = idpub;
        this.idcomprador = idcomprador;
        this.idvendedor = idvendedor;
        this.idconse = idconse;
        this.type = type;
    }

    public String getIdpub() {
        return idpub;
    }

    public String getIdcomprador() {
        return idcomprador;
    }

    public String getIdvendedor() {
        return idvendedor;
    }

    public String getIdconse() {
        return idconse;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "NegociacionRequest{" +
                "idpub='" + idpub + '\'' +
                ", idcomprador='" + idcomprador + '\'' +
                ", idvendedor='" + idvendedor + '\'' +
                ", idconse='" + idconse + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
