package com.tnb.app.garage7.injector.component;

import com.tnb.app.garage7.injector.module.CarsListModule;
import com.tnb.app.garage7.injector.module.PublicactionsModule;
import com.tnb.app.garage7.injector.scope.CustomScope;
import com.tnb.app.garage7.presentation.fragment.CarListFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesActivasFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesFinalizadasFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesPausadasFragment;

import dagger.Component;

/**
 * Created by Acer on 11/2/2018.
 */

//@CustomScope
//@Component(dependencies = NetworkComponent.class, modules = PublicactionsModule.class)
public interface PublicationsComponent {
    void inject(PublicacionesActivasFragment fragment);

    void inject(PublicacionesFinalizadasFragment fragment);

    void inject(PublicacionesPausadasFragment fragment);

}

