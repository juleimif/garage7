package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.response.OperacionesUsuario;
import com.tnb.app.garage7.data.view.OperacionesUserView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class OperacionesUserImpl implements OperacionesUserPresenter {
    public static final String TAG = OperacionesUserImpl.class.getSimpleName();
    public OperacionesUserView operacionesUserView;

    public OperacionesUserImpl(OperacionesUserView operacionesUserView){
        this.operacionesUserView = operacionesUserView;
    }

    private void respuestaServicio(String message) {
    }


    @Override
    public void getOperaciones(String userId) {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getOpreracionesFromUusuario(userId).enqueue(new Callback<OperacionesUsuario>() {
            @Override
            public void onResponse(Call<OperacionesUsuario> call, Response<OperacionesUsuario> response) {
                operacionesUserView.showProgress();
                if(response.code()==200){
                    if(response.body().getData().size()>0){
                        operacionesUserView.showOperaciones(response.body().getData());
                    }
                    else
                    {
                        operacionesUserView.showRelod();

                    }

                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }


            @Override
            public void onFailure(Call<OperacionesUsuario> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                operacionesUserView.hideProgress();
                operacionesUserView.showRelod();
                Toast.makeText(operacionesUserView.getContext(),
                        operacionesUserView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();

            }
        });

        operacionesUserView.showProgress();


    }
}
