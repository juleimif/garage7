package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 02/10/2017.
 */

public class EstatusPublicacion {
    @SerializedName("usuario")
    @Expose
    public String usuario;
    @SerializedName("status")
    @Expose
    public String status;

    public EstatusPublicacion(String usuario, String status) {
        this.usuario = usuario;
        this.status = status;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EstatusPublicacion{" +
                "usuario='" + usuario + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
