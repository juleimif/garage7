package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tnb.app.garage7.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class TerminosDialogoFragment extends DialogFragment {

    public static TerminosDialogoFragment newInstance() {
        TerminosDialogoFragment f = new TerminosDialogoFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialogo_terminos);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this, dialogo);

        return dialogo;
    }

    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                dismiss();
                break;
        }
    }
}
