package com.tnb.app.garage7.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.domain.model.CheckEmail;
import com.tnb.app.garage7.domain.model.ResponseCheckEmail;
import com.tnb.app.garage7.domain.model.response.CheckEmailResponse;
import com.tnb.app.garage7.presentation.DialogFragment.ChoiceCountryDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.GoogleDialogoFragment;
import com.tnb.app.garage7.presentation.NonSwipeableViewPager;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.NextInterface;
import com.tnb.app.garage7.presentation.Adapter.ScreenSlidePagerAdapter;
import com.tnb.app.garage7.presentation.fragment.FormInfCarFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessPublicationActivity extends BaseActivity  implements FormInfCarFragment.goMain, NextInterface, ViewPager.OnPageChangeListener {
    public static final String TAG = ProcessPublicationActivity.class.getSimpleName();
    @BindView(R.id.viewPagerOpciones)
    NonSwipeableViewPager pager;
    @BindView(R.id.ly_account)             CoordinatorLayout ly_account;
    @BindView(R.id.usage_stateprogressbar) StateProgressBar stateprogressbar;
    private int num;
    public int type ;

    @Override
    public int getLayout() {
        return R.layout.activity_process_publication_;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        type = getIntent().getExtras().getInt("type");
        Log.i(TAG, "type: " + type);

        pager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager(),
                ProcessPublicationActivity.this, type));
        stateprogressbar.enableAnimationToCurrentState(false);
        pager.setPagingEnabled(false);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.i(TAG, "onPageScrolled: " + position);
    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                break;
            case 1:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                break;
            case 2:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                break;
            case 3:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.i(TAG, "onPageScrollStateChanged: " + state);
    }



    @Override
    public void next(int num) {
        this.num = num;
        pager.setCurrentItem(num);
    }

    @Override
    public void onBackPressed() {
        Log.d("Tipo de publicacion","onBackPressed()");
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);

    }

    @Override
    public void goMain() {
        startActivity(new Intent(this, MainActivity.class));
    }


}

