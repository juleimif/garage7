package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juleimis on 03/10/2017.
 */

public class ConcesionarioData {

    @SerializedName("consecionarios")
    @Expose
    public List<Concesionario> consecionarios = null;

    public List<Concesionario> getConsecionarios() {
        return consecionarios;
    }
}
