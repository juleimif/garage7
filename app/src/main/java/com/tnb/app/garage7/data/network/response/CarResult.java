package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Car;

public class CarResult {

    @SerializedName(value = "data")
    private Car car;

    public CarResult(Car car) {
        this.car = car;
    }

    public Car car() {
        return car;
    }

    public void setCar(Car car) { this.car = car;}
}
