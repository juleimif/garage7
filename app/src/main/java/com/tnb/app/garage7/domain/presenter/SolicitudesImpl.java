package com.tnb.app.garage7.domain.presenter;

import android.util.Log;

import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.OperacionesConcesionario;
import com.tnb.app.garage7.data.view.SolicitudesView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class SolicitudesImpl implements SolicitudesPresenter {
    public static final String TAG = SolicitudesImpl.class.getSimpleName();
    public SolicitudesView solicitudesView;

    public SolicitudesImpl(SolicitudesView solicitudesView){
        this.solicitudesView = solicitudesView;
    }

    private void respuestaServicio(String message) {

    }

    @Override
    public void getSolicitudes(String idConcesionario) {

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getSolicitudes(idConcesionario).enqueue(new Callback<OperacionesConcesionario>() {
            @Override
            public void onResponse(Call<OperacionesConcesionario> call, Response<OperacionesConcesionario> response) {
                solicitudesView.showProgress();
                if(response.code()==200){
                    if (response.body().getData().getPublicaciones().size()>0)
                    solicitudesView.showSolicitudes(response.body().data.getPublicaciones());
                }
//                else {
//                    publicacionesView.showRelod();
//                    try {
//                        JSONObject jObjError = new JSONObject(response.errorBody().string());
//                        respuestaServicio(jObjError.getString("message"));
//                        Log.e("respuestaServicio", jObjError.getString("message"));
//                    } catch (Exception e) {
//                        respuestaServicio(e.getMessage());
//                    }
//                }
                solicitudesView.hideProgress();
            }

            @Override
            public void onFailure(Call<OperacionesConcesionario> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                solicitudesView.hideProgress();
                solicitudesView.showRelod();
            }
        });

    }
}
