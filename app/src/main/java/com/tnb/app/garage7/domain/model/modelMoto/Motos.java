package com.tnb.app.garage7.domain.model.modelMoto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 16/07/2017.
 */

public class Motos {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public DataMoto data;
}
