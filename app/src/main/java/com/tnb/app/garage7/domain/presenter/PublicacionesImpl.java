package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;
import com.tnb.app.garage7.domain.model.response.PublicacionsResponse;
import com.tnb.app.garage7.data.view.PublicacionesView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class PublicacionesImpl implements PublicacionesPresenter {
    public static final String TAG = PublicacionesImpl.class.getSimpleName();
    public PublicacionesView publicacionesView;

    public PublicacionesImpl(PublicacionesView publicacionesView){
        this.publicacionesView = publicacionesView;
    }

    private void respuestaServicio(String message) {

    }


    @Override
    public void getPublicaciones(String idUser) {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        EstatusPublicacion body = new EstatusPublicacion(idUser,"1");
        Log.i(TAG, "getFinishPublication: "+ body.toString());
        service.getMePublications(body).enqueue(new Callback<PublicacionsResponse>() {
            @Override
            public void onResponse(Call<PublicacionsResponse> call, Response<PublicacionsResponse> response) {
                if (response.code() == 200){
                    Log.i(TAG, "onResponse: ");
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PublicacionsResponse> call, Throwable t) {
                Toast.makeText(publicacionesView.getContext(),
                        publicacionesView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });
    }
}
