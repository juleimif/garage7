package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;

import java.util.List;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface CreateAccountView {
    void showLoading();
    void hideLoading();
    void gotoMain();
    void showNotify(String s);
    Context getContext();
    void showCitys(List<AutoCompletarEstadosResponse> data);
}
