package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Brand;

import java.util.ArrayList;
import java.util.List;

public class BrandListResult {

    @SerializedName(value = "data")
    private List<Brand> results;

    public BrandListResult(List<Brand> list) {
        results = list;
    }

    public List<Brand> getResults() {
        return results;
    }
}
