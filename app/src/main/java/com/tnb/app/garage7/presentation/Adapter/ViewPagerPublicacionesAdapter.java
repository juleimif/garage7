package com.tnb.app.garage7.presentation.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tnb.app.garage7.presentation.fragment.PublicacionesActivasFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesFinalizadasFragment;
import com.tnb.app.garage7.presentation.fragment.PublicacionesPausadasFragment;

/**
 * Created by Juleimis on 30/06/2017.
 */

public class ViewPagerPublicacionesAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[];
    public ViewPagerPublicacionesAdapter(FragmentManager fm, String[] tabTitles) {
        super(fm);
        this.tabTitles = tabTitles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PublicacionesActivasFragment();
            case 1:
                return new PublicacionesPausadasFragment();
            case 2:
                return new PublicacionesFinalizadasFragment();

        }
        return null;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }



}
