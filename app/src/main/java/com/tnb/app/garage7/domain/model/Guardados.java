package com.tnb.app.garage7.domain.model;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class Guardados {
    public String userid;
    public String pubid;
    public String type;

    public Guardados(String userid, String pubid, String type) {
        this.userid = userid;
        this.pubid = pubid;
        this.type = type;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPubid() {
        return pubid;
    }

    public void setPubid(String pubid) {
        this.pubid = pubid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Guardados{" +
                "userid='" + userid + '\'' +
                ", pubid='" + pubid + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
