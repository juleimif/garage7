package com.tnb.app.garage7.presentation.fragment;

/**
 * Created by Juleimis on 15/11/2017.
 */

public class BannerData {

    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
