package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.AutocompletarModels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModelsAdapter extends RecyclerView.Adapter<ModelsAdapter.ViewHolder> {

    private BrandItemClick mItemClickListener;
    private  List<AutocompletarModels> modelsList;
    private Context contexto;

    public ModelsAdapter(Context applicationContext, List<AutocompletarModels> brandsList, BrandItemClick brokerItemClick) {
        this.modelsList =  brandsList;
        this.contexto = applicationContext;
        mItemClickListener = (BrandItemClick) brokerItemClick;
    }


    @Override
    public ModelsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_brands, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        AutocompletarModels currentItem = modelsList.get(position);
        ModelsAdapter.ViewHolder viewHolder = (ModelsAdapter.ViewHolder) holder;
        if(currentItem.getModelo().equals("")){
            viewHolder.tv_brand.setError("No coincide su búsqueda");
        }
        else
        viewHolder.tv_brand.setText(currentItem.getModelo());

    }

    @Override
    public int getItemCount() {

        return modelsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.tv_brand) TextView tv_brand;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(modelsList.get(position));
        }

    }

    public interface BrandItemClick {
        void onBrokerClick(AutocompletarModels clickedBroker);
    }

}
