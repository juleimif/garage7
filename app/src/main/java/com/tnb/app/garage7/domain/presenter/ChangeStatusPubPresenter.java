package com.tnb.app.garage7.domain.presenter;

import com.tnb.app.garage7.domain.model.request.UpdateStatusCarRequest;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface ChangeStatusPubPresenter {
    void changeStatus(UpdateStatusCarRequest updateStatusCarRequest);

}
