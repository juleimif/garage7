package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.NextInterface;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.modelMoto.VehiculoMoto;
import com.tnb.app.garage7.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CreatePublicactionFragment2 extends Fragment implements Validator.ValidationListener {

    private static final String TAG = CreatePublicactionFragment2.class.getSimpleName();
    String[] PUERTAS = {"2","3","4","5"};
    String[] TRANSMISION = {"Automática","Sincrónica"};
    String[] COLOR = {"Azul","Blanco","Negro","Plateado","Rojo","Otros"};
    @NotEmpty(message = "Debe ingresar la ubicación el vehículo")

    @BindView(R.id.input_localidad) EditText input_localidad;
    @NotEmpty(message = "Debe ingresar una descripción")

    @BindView(R.id.input_desc) EditText input_desc;
    @NotEmpty(message = "Debe ingresar una transmisión")

    @BindView(R.id.sp_trans) MaterialBetterSpinner sp_transmision;
    @NotEmpty(message = "Debe ingresar los campos puertas")

    @BindView(R.id.sp_door) MaterialBetterSpinner sp_puertas;
    @NotEmpty(message = "Debe ingresar un color")

    @BindView(R.id.sp_color) MaterialBetterSpinner sp_color;

    private String color,transmision,puertas;
    private View view;
    public int type_publication;

    private Validator validator;

    private VehiculoCarro carroModelSingleton;
    private VehiculoMoto motoModelSingleton;

    NextInterface listener;

    public CreatePublicactionFragment2() {
        // Required empty public constructor
    }

    public static CreatePublicactionFragment2 newInstance(int type_publication) {
        CreatePublicactionFragment2 fragment = new CreatePublicactionFragment2();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_publication_2, container, false);
        ButterKnife.bind(this,view);

        validator = new Validator(this);
        validator.setValidationListener(this);

        //Class singleton
        carroModelSingleton = VehiculoCarro.getInstance();
        motoModelSingleton = VehiculoMoto.getInstance();

        //Obtenemos tipo de publicacion
        type_publication = getArguments().getInt("type", 0);
        Log.i(TAG, "publicando: " + type_publication);

        if (type_publication==1){
            //MOTO
            sp_transmision.setVisibility(View.GONE);
            sp_puertas.setVisibility(View.GONE);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_dropdown_item_1line, TRANSMISION);

                sp_transmision.setAdapter(arrayAdapter);
                sp_transmision.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                        transmision = parent.getItemAtPosition(position).toString();
            }
        });

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, COLOR);

        sp_color.setAdapter(arrayAdapter2);
        sp_color.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                color = parent.getItemAtPosition(position).toString();
            }
        });

        ArrayAdapter<String> arrayAdapter3 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, PUERTAS);

        sp_puertas.setAdapter(arrayAdapter3);
        sp_puertas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                puertas = parent.getItemAtPosition(position).toString();
            }
        });

        input_desc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cont = String.valueOf(s.length());
                input_desc.setError("Minimo 80 caracteres");
                if (s.length()>1)
                input_desc.setError(cont);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    @OnClick(R.id.fl_next)
    public void Next(View view) {
        validator.validate();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (NextInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");

        }
    }

    public void setTypePublicaton(int type){
        if(type==1) //moto
        {
            motoModelSingleton.setColor(color);
            motoModelSingleton.setLocation(Utils.ucFirst(input_localidad.getText().toString()));
            motoModelSingleton.setDescriptionsM(Utils.ucFirst(input_desc.getText().toString()));

            listener.next(2);

        }
        else // carro
        {
            Log.i(TAG, "setTypePublicaton 2: " + transmision + color);
            carroModelSingleton.setTransmitionC(transmision);
            carroModelSingleton.setColorC(color);
            carroModelSingleton.setDoorNumberC(puertas);
            carroModelSingleton.setLocationC(Utils.ucFirst(input_localidad.getText().toString()));
            carroModelSingleton.setDescriptionsC(Utils.ucFirst(input_desc.getText().toString()));
            listener.next(2);

        }
    }

    @Override
    public void onValidationSucceeded() {
        setTypePublicaton(type_publication);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

