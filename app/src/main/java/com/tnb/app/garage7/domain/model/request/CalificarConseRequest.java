package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 10/09/2017.
 */

public class CalificarConseRequest {

    @SerializedName("idnegociacion")
    @Expose
    public String idnegociacion;
    @SerializedName("ratingconse")
    @Expose
    public String ratingconse;

    public CalificarConseRequest(String idnegociacion, String ratingconse) {
        this.idnegociacion = idnegociacion;
        this.ratingconse = ratingconse;
    }

    @Override
    public String toString() {
        return "CalificarConseRequest{" +
                "idnegociacion='" + idnegociacion + '\'' +
                ", ratingconse='" + ratingconse + '\'' +
                '}';
    }
}
