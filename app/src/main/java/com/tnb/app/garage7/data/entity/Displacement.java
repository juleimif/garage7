package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Displacement {

    @NonNull
    public final String id;

    public final String displacement;

    public Displacement(@NonNull String id, String displacement) {
        this.id = id;
        this.displacement = displacement;
    }
}
