package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class User {

    @NonNull
    private String id;
    
    private String firstName;
    
    private String lastName;
    
    private String gender;
    
    private String cityId;
    
    private String countryId;
    
    private String city;
    
    private String country;
    
    private String phone;
    
    private String email;

    private String idNumber;

    private String isActive;

    private String isDealer;

    private String acceptTerms;
    
    private String avatarUrl;
    
    private String createdAt;
    
    private String updatedAt;
    
    private String url;

    public User(String id, String firstName, String lastName, String gender,
                String cityId, String countryId, String city, String country, 
                String phone, String email, String idNumber, String isActive, 
                String isDealer, String acceptTerms, String avatarUrl, 
                String createdAt, String updatedAt, String url) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.cityId = cityId;
        this.countryId = countryId;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.email = email;
        this.idNumber = idNumber;
        this.isActive = isActive;
        this.isDealer = isDealer;
        this.acceptTerms = acceptTerms;
        this.avatarUrl = avatarUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public String getIsActive() {
        return isActive;
    }

    public String getIsDealer() {
        return isDealer;
    }

    public String getAcceptTerms() {
        return acceptTerms;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUrl() {
        return url;
    }

    public String toString() {
        return firstName;
    }
}
