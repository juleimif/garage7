package com.tnb.app.garage7.presentation.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tnb.app.garage7.presentation.fragment.CarListFragment;
import com.tnb.app.garage7.presentation.fragment.MotoListFragment;

/**
 * Created by Juleimis on 30/06/2017.
 */

public class ViewPagerOpcionesAdapter extends FragmentStatePagerAdapter {

    Context context;
    String model, year, km, locale, disel, tramition;
    int price;


    public ViewPagerOpcionesAdapter(FragmentManager supportFragmentManager, MainActivity mainActivity, String model, String year, String km, String locale, String disel, String trasmition, int price) {
        super(supportFragmentManager);
        this.context = context;
        this.model = model;
        this.year = year;
        this.km = km;
        this.locale = locale;
        this.disel = disel;
        this.tramition = trasmition;
        this.price = price;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CarListFragment.newInstance(model, year, km, locale, disel, tramition, price);
            case 1:
                return new MotoListFragment();

        }
        return null;
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
       switch (position) {
            case 0:
                return "Carros";
            case 1:
                return "Motos";

       }
        return null;
    }



}
