package com.tnb.app.garage7.injector.component;

import com.tnb.app.garage7.injector.module.CarsListModule;
import com.tnb.app.garage7.injector.scope.CustomScope;
import com.tnb.app.garage7.presentation.fragment.CarListFragment;

import dagger.Component;

/**
 * Created by Acer on 11/2/2018.
 */

    //@CustomScope
    //@Component(dependencies = NetworkComponent.class, modules = CarsListModule.class)
    public interface CarListComponent {
        void inject(CarListFragment fragment);
    }

