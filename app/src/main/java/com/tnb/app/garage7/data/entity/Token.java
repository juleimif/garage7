package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity(
        primaryKeys = {"accessToken"})
public class Token {

    @SerializedName("token_type")
    public final String tokenType;

    @SerializedName("expires_in")
    public final String expiresIn;

    @SerializedName("access_token")
    public final String accessToken;

    @SerializedName("refresh_token")
    public final String refreshToken;

    public Token(String tokenType, String expiresIn, String accessToken, String refreshToken) {
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

}
