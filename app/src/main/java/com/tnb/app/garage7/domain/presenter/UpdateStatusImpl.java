package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.request.UpdateEstatusNegociacion;
import com.tnb.app.garage7.data.view.UpdateStatusView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class UpdateStatusImpl implements UpdateStatusPresenter {
    public static final String TAG = UpdateStatusImpl.class.getSimpleName();
    public UpdateStatusView updateStatusView;

    public UpdateStatusImpl(UpdateStatusView updateStatusView){
        this.updateStatusView = updateStatusView;
    }


    @Override
    public void UpdateStatus(String idNegociacion, String status) {

        UpdateEstatusNegociacion update = new UpdateEstatusNegociacion(idNegociacion, status);
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "UpdateStatus: "+ service.updateNegociacion(update).request().url());
        service.updateNegociacion(update).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                if(response.code()==200){
                    Log.i(TAG, "onResponse UpdateStatus: ");
                    responseLogin(response.body());
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                Toast.makeText(updateStatusView.getContext(),
                        updateStatusView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });

    }

    private void respuestaServicio(String message) {
        updateStatusView.showNotify(message);
    }

    private void responseLogin(ResponseVehiculo body) {
        if(body.status.equals("true")){
            updateStatusView.showNotify("Exito");
        }
        else{
            updateStatusView.showNotify(body.message);
        }
    }
}
