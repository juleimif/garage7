package com.tnb.app.garage7.injector.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.tnb.app.garage7.common.country.CountryDialogViewModel;
import com.tnb.app.garage7.injector.qualifires.ViewModelKey;
import com.tnb.app.garage7.login.LoginViewModel;
import com.tnb.app.garage7.main.MainViewModel;
import com.tnb.app.garage7.signup.SignupViewModel;
import com.tnb.app.garage7.splash.SplashViewModel;
import com.tnb.app.garage7.common.ProjectViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel bindSplashViewModel(SplashViewModel splashViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CountryDialogViewModel.class)
    abstract ViewModel bindCountryDialogViewModel(CountryDialogViewModel countryDialogViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SignupViewModel.class)
    abstract ViewModel bindSignupViewModel(SignupViewModel signupViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ProjectViewModelFactory projectViewModelFactory);

}
