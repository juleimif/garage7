package com.tnb.app.garage7.login;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.main.MainActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

import static com.tnb.app.garage7.welcome.WelcomeActivity.LOGIN_WITH_GOOGLE;
import static com.tnb.app.garage7.welcome.WelcomeActivity.SIGN_IN_CODE;

public class LoginActivity
        extends AppCompatActivity
        implements Validator.ValidationListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    public static final String USER_EMAIL = "USER_EMAIL";

    public static final String USER_PASSWORD = "USER_PASSWORD";

    @Email(message = "Debe ingresar su correo")
    @BindView(R.id.input_user)
    TextInputEditText input_user;

    @NotEmpty(message = "Debe ingresar su contraseña")
    @BindView(R.id.input_password)
    TextInputEditText input_password;

    @BindView(R.id.recordar)
    CheckBox ch_recordar;

    private Validator validator;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    LoginViewModel loginViewModel;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        initGoogleApiClient();

        loginViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(LoginViewModel.class);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onValidationSucceeded() {
        loginViewModel.login(input_user.getText().toString(),
                input_password.getText().toString())
                .observe(this, this::handleResponse);
    }

    private void handleResponse(Token token) {
        if (token != null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(USER_EMAIL, input_user.getText().toString());
            startActivity(intent);
            overridePendingTransition(R.anim.left_out, R.anim.left_in);
            finish();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.recordar)
    public void remember(View view) {
        if(!ch_recordar.isChecked()){
            Log.i(TAG, "remember: ");
            //preferencesManager.clearPreferences();
        }
    }

    @OnClick(R.id.tv_sign_google)
    public void signInGoogle(View view) {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, SIGN_IN_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(LOGIN_WITH_GOOGLE, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.left_out, R.anim.left_in);
            finish();
        } else {
            Toast.makeText(this, R.string.not_log_in, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.iv_salir)
    public void salir(View view) {
        onBackPressed();
        overridePendingTransition(R.anim.right_out, R.anim.right_in);
    }

    @OnClick(R.id.btn_singIn)
    public void registar(View view) {
        validator.validate();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}
