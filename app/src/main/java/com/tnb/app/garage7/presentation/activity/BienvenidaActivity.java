package com.tnb.app.garage7.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.presenter.CreateAccountImpl;
import com.tnb.app.garage7.domain.presenter.GoogleSignInImpl;
import com.tnb.app.garage7.domain.presenter.GoogleSignInPresenter;
import com.tnb.app.garage7.data.view.LoginView;
import com.tnb.app.garage7.login.LoginActivity;
import com.tnb.app.garage7.presentation.DialogFragment.ChoiceCountryDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.GoogleDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.TerminosDialogoFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class BienvenidaActivity extends BaseActivity implements LoginView,
        GoogleDialogoFragment.OkDialogListener {

    public static final String TAG = BienvenidaActivity.class.getSimpleName();
    @BindView(R.id.ly_bienvenida) LinearLayout ly_bienvenida;
    private CreateAccountImpl createAccountImpl;
    private GoogleSignInPresenter signInGooglePresenter;
    private PreferencesSessionManager preferencesUserManager;

    @Override
    public int getLayout() {
        return R.layout.activity_bienvenida;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        if(!isOnline(this))
            showToast("Compruebe su conexión");
    }

    @OnClick(R.id.btn_singGoogle)
    public void signInGoogle(View view) {

        Log.i(TAG, "onClick: ");
        //Implementing MVP for Login
        signInGooglePresenter = new GoogleSignInImpl(this);
        signInGooglePresenter.createGoogleClient(view.getContext());
        signInGooglePresenter.signIn(this);

    }


    @OnClick({R.id.btn_singUp, R.id.btn_singIn, R.id.terminos, R.id.iv_salir})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_singUp:
                //seleccionamos el pais de origen
                ChoiceCountryDialogoFragment newFragment = new ChoiceCountryDialogoFragment().newInstance(0, "","","");
                newFragment.show(getSupportFragmentManager(), "dialog");
//                startActivity(new Intent(this, CreateAccountActivity.class));
//                overridePendingTransition(R.anim.left_out, R.anim.left_in);
                break;
            case R.id.btn_singIn:
                startActivity(new Intent(this, LoginActivity.class));
                overridePendingTransition(R.anim.left_out, R.anim.left_in);
                break;
            case R.id.terminos:
                callTerminos();
                break;
            case R.id.iv_salir:
                finish();
                break;
        }
    }

    private void callTerminos() {
        TerminosDialogoFragment newFragment = new TerminosDialogoFragment();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void specifyGoogleSignIn(GoogleSignInOptions gso) {
    }

    @Override
    public void startProfileActivity() {
        Log.i(TAG, "startProfileActivity: ");
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
        finish();
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(this);
        progress.setTitle("Cargando");
        progress.setMessage("Por favor espere...");
        progress.show();
    }

    @Override
    public void hideLoading() {
        if(progress != null){
            progress.dismiss();
        }
    }

    @Override
    public void goToGallery() {

    }

    @Override
    public void showToast(String mssg) {
        Utils.showSnackBar(getApplicationContext(),mssg,ly_bienvenida);
    }

    @Override
    public void desconectar() {
        signInGooglePresenter.onDestroy();
    }

    @Override
    public void choiceCountry(String personName, String personEmail, String base64Img) {
        Log.i(TAG, "choiceCountry: " + personName +personEmail +base64Img );
        ChoiceCountryDialogoFragment newFragment = new ChoiceCountryDialogoFragment().newInstance(1, personName,personEmail,base64Img);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void callFromVKSignIn(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("google", "onActivityResult: ");
        signInGooglePresenter.onActivityResult(getContext(), requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    @Override
    public void onOkButtonClick() {

        preferencesUserManager = new PreferencesSessionManager(this);
        preferencesUserManager.setUserId("0");
        preferencesUserManager.setServerToken("0");
        preferencesUserManager.setUserType("0");

        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
        finish();
    }

    @Override
    public void onDestroyAc() {
        Log.i(TAG, "onDestroyAc: ");
        signInGooglePresenter.onStop();
    }
}

