package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Banner {

    @NonNull
    public final String id;
    @NonNull
    public final String name;

    public Banner(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
