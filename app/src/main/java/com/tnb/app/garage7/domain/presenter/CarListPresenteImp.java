package com.tnb.app.garage7.domain.presenter;

import com.tnb.app.garage7.data.view.CarListContract;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.modelCar.Carros;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Acer on 11/2/2018.
 */

public class CarListPresenteImp implements CarListContract.Presenter {
    Retrofit retrofit;
    CarListContract.View mView;

    @Inject
    public CarListPresenteImp(Retrofit retrofit, CarListContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }

    @Override
    public void loadListCar() {
        retrofit.create(ApiInterface.class).getAllCars().enqueue(new Callback<Carros>() {
            @Override
            public void onResponse(Call<Carros> call, Response<Carros> response) {
                mView.showCar(response.body().getData().getVehiculos());
            }

            @Override
            public void onFailure(Call<Carros> call, Throwable t) {

            }
        });

    }
}