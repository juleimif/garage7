package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.Brand;

import java.util.List;

@Dao
public interface BrandDAO {

    @Query("SELECT * FROM Brand")
    LiveData<List<Brand>> findAll();

    @Query("SELECT * FROM Brand where brand like '%' || :brand || '%' ")
    LiveData<List<Brand>> findByName(String brand);

    @Query("SELECT * FROM Brand where id = :id")
    LiveData<Brand> searchBrandById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Brand... brand);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBrands(List<Brand> brandList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createBrandIfNotExists(Brand brand);

    @Query("DELETE FROM Brand")
    void deleteAll();
}
