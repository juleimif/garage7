package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.Car;

public interface FormPhotosView {
    void publicar(Car car);
    Context getContextaAplication();
}
