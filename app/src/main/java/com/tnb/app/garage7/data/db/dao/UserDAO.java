package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.User;

import java.util.List;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM User")
    LiveData<List<User>> findAll();

    @Query("SELECT * FROM User where id = :id")
    LiveData<User> searchUserById(String id);

    @Query("SELECT * FROM User limit 1")
    LiveData<User> findFisrt();

    @Query("SELECT * FROM User where email = :email")
    LiveData<User> searchUserByEmail(String email);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User... user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsers(List<User> userList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createUserIfNotExists(User user);

    @Query("DELETE FROM User")
    void deleteAll();
}
