package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;

import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.domain.presenter.UpdateStatusImpl;
import com.tnb.app.garage7.data.view.UpdateStatusView;

import com.tnb.app.garage7.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.techery.properratingbar.ProperRatingBar;


/**
 * Created by Juleimis on 19/06/2017.
 */

public class DetailSolicitudeDialogoFragment extends DialogFragment implements UpdateStatusView {
    public static final String TAG = DetailSolicitudeDialogoFragment.class.getSimpleName();
    @BindView(R.id.iv_profile) ImageView iv_profile;
    @BindView(R.id.tv_name) TextView tv_name;
    @BindView(R.id.tv_name_car) TextView tv_name_car;
    @BindView(R.id.tv_price) TextView tv_price;
    @BindView(R.id.lowerRatingBar) ProperRatingBar lowerRatingBar;
    @BindView(R.id.containerDialogDetalle) LinearLayout containerDialogDetalle;
    private String photo,name, brand, model, ratingVendedor, price, idNegociacion;
    private UpdateStatusImpl updateStatusPresenter;

    private static final String DESCRIBABLE_KEY = "describable_key";
    private User mDescribable;

    public interface EditNameDialogListener {
        void onFinishEditDialog();
    }


    public static DetailSolicitudeDialogoFragment newInstance(String idNegociacion,String name, String photo1, String ratingVendedor, String brandC, String modelCr, String price) {

        DetailSolicitudeDialogoFragment fragment = new DetailSolicitudeDialogoFragment();
        Bundle args = new Bundle();

        //rgs.putSerializable(DESCRIBABLE_KEY, mDescribable);
        args.putString("idNegociacion", idNegociacion);
        args.putString("photo", photo1);
        args.putString("name", name);
        args.putString("ratingVendedor", ratingVendedor);
        args.putString("brandC", brandC);
        args.putString("modelCr", modelCr);
        args.putString("price", price);

        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_detail_solicitude);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);
        photo = getArguments().getString("photo", "");
        name = getArguments().getString("name", "");
        ratingVendedor = getArguments().getString("ratingVendedor", "");
        model = getArguments().getString("modelCr", "");
        brand = getArguments().getString("brandC", "");
        price = getArguments().getString("price", "");
        idNegociacion = getArguments().getString("idNegociacion", "");
        Log.i(TAG, "onCreateDialog: " + idNegociacion);

        mDescribable = (User) getArguments().getSerializable(
                DESCRIBABLE_KEY);
        updateStatusPresenter = new UpdateStatusImpl(this);
        initView();

        return dialogo;
    }
    public void initView(){

        tv_name.setText(name + " ha solicitado una cita a tu concesionario");
        tv_name_car.setText(model + " - " + brand);
        tv_price.setText(price + " VEF");

        lowerRatingBar.setRating(Integer.parseInt(String.valueOf(ratingVendedor)));

        Glide.with(getContext())
                .load(photo)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(iv_profile);

    }

    @OnClick({R.id.iv_exit,R.id.btn_acept,R.id.btn_rechazar})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_exit:
                dismiss();
                break;
            case R.id.btn_acept:
                updateStatusPresenter.UpdateStatus(idNegociacion,"2"); //estaus en proceso
                sendBackResult();
                break;
            case R.id.btn_rechazar:
                showAlert( "¿ Está seguro que desea rechazar esta solicitud ?", "Si");
                break;
        }
    }

    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        EditNameDialogListener listener = (EditNameDialogListener) getTargetFragment();
        listener.onFinishEditDialog();
        dismiss();
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showNotify(String s) {
        Utils.showSnackBar(getContext(),s,containerDialogDetalle);
    }

    private void showAlert(String message, String tvConfirm) {

        new AlertDialog.Builder((getActivity()))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(tvConfirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        updateStatusPresenter.UpdateStatus(idNegociacion,"10"); //estausrechazada
                        sendBackResult();
                    }
                })
                .setNegativeButton(this.getString(R.string.declinar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }



}
