package com.tnb.app.garage7.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tnb.app.garage7.data.db.GarageDb;
import com.tnb.app.garage7.data.db.dao.BannerDAO;
import com.tnb.app.garage7.data.db.dao.BrandDAO;
import com.tnb.app.garage7.data.db.dao.CityDAO;
import com.tnb.app.garage7.data.db.dao.CountryDAO;
import com.tnb.app.garage7.data.db.dao.UserDAO;
import com.tnb.app.garage7.data.entity.Banner;
import com.tnb.app.garage7.data.entity.Brand;
import com.tnb.app.garage7.data.entity.City;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.network.request.TokenRequest;
import com.tnb.app.garage7.data.network.request.UserRequest;
import com.tnb.app.garage7.data.network.ApiResponse;
import com.tnb.app.garage7.data.network.response.BannerListResponse;
import com.tnb.app.garage7.data.network.response.BrandListResult;
import com.tnb.app.garage7.data.network.response.CityListResult;
import com.tnb.app.garage7.data.network.response.CountryListResult;
import com.tnb.app.garage7.data.network.response.UserResult;
import com.tnb.app.garage7.data.network.Webservice;
import com.tnb.app.garage7.data.util.AppExecutors;
import com.tnb.app.garage7.data.util.LiveDataCallAdapterFactory;
import com.tnb.app.garage7.data.util.NetworkBoundResource;
import com.tnb.app.garage7.data.vo.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.tnb.app.garage7.utils.AppConstants.SPK_ACCESS_TOKEN;

@Singleton
public class AppRepository {

    private Webservice webservice;


    private final AppExecutors mAppExecutors;

    private CountryDAO countryDao;

    private CityDAO cityDao;

    private UserDAO userDao;

    private BrandDAO brandDAO;

    private BannerDAO bannerDAO;

    private SharedPreferences preferences;

    @Inject
    public AppRepository(Webservice webservice, AppExecutors mAppExecutors,
                         CountryDAO countryDao, CityDAO cityDao, UserDAO userDao,
                         BrandDAO brandDAO, SharedPreferences preferences, BannerDAO bannerDao) {
        this.webservice = webservice;

        this.mAppExecutors = mAppExecutors;
        this.countryDao = countryDao;
        this.cityDao = cityDao;
        this.userDao = userDao;
        this.preferences = preferences;
        this.brandDAO = brandDAO;
        this.bannerDAO = bannerDao;
    }

    public void deleteAllUsers() {
        userDao.deleteAll();
    }

    public LiveData<Resource<User>> getUserByEmail(String email) {

        return new NetworkBoundResource<User, UserResult>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull UserResult item) {
                userDao.insert(item.user());
            }

            @Override
            protected boolean shouldFetch(@Nullable User data) {
                return data == null;
            }

            @NonNull
            @Override
            protected LiveData<User> loadFromDb() {
                return userDao.searchUserByEmail(email);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<UserResult>> createCall() {
                return webservice.getUserInfoByemail(email);
            }

            @Override
            protected boolean isPost() {
                return false;
            }
        }.asLiveData();
    }

    public LiveData<Resource<User>> getUser() {
        return new NetworkBoundResource<User, UserResult>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull UserResult item) {
                userDao.insert(item.user());
            }

            @Override
            protected boolean shouldFetch(@Nullable User data) {
                return false;
            }

            @NonNull
            @Override
            protected LiveData<User> loadFromDb() {
                return userDao.findFisrt();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<UserResult>> createCall() {
                return null;
            }
            @Override
            protected boolean isPost() {
                return false;
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<Banner>>> getBannerImagesNames() {
        return new NetworkBoundResource<List<Banner>, BannerListResponse>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull BannerListResponse item) {
                bannerDAO.insertList(item.getResults());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Banner> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<Banner>> loadFromDb() {
                return bannerDAO.findAll();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BannerListResponse>> createCall() {
                return webservice.getBanerImages();
            }

            @Override
            protected boolean isPost() {
                return false;
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<Country>>> getCountries() {
        return new NetworkBoundResource<List<Country>, CountryListResult>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull CountryListResult item) {
                countryDao.insertCountries(item.getResults());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Country> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<Country>> loadFromDb() {
                return countryDao.findAll();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<CountryListResult>> createCall() {
                Map<String, String> params = new HashMap<>();
                return webservice.getCountries(params);
            }
            @Override
            protected boolean isPost() {
                return false;
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<City>>> getCitiesByName(String countryId, String name) {
        return new NetworkBoundResource<List<City>, CityListResult>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull CityListResult item) {
                cityDao.insertCities(item.getResults());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<City> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<City>> loadFromDb() {
                return cityDao.findByName(countryId, name);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<CityListResult>> createCall() {
                Map<String, String> params = new HashMap<>();
                params.put("city", name);
                return webservice.getCities(countryId, params);
            }
            @Override
            protected boolean isPost() {
                return false;
            }
        }.asLiveData();
    }

    public LiveData<Token> requestToken(TokenRequest request) {
        final MutableLiveData<Token> data = new MutableLiveData<>();
        webservice.getAccessToken(request).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.code() == 200) {
                    saveAccesstoken(response.body());
                    data.setValue(response.body());
                    return;
                }
                data.setValue(null);
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                return;
            }
        });

        return data;
    }

    private void saveAccesstoken(Token tokenData) {
        String token = tokenData.tokenType + " " + tokenData.accessToken;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SPK_ACCESS_TOKEN, token).commit();
    }

    public LiveData<UserResult> signUp(UserRequest userRequest) {
        final MutableLiveData<UserResult> data = new MutableLiveData<>();
        webservice.createNewUser(userRequest).enqueue(new Callback<UserResult>() {
            @Override
            public void onResponse(Call<UserResult> call, Response<UserResult> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<UserResult> call, Throwable t) {
                return;
            }
        });

        return data;
    }

    public LiveData<Resource<List<Brand>>> getBrands() {
        return new NetworkBoundResource<List<Brand>, BrandListResult>(mAppExecutors) {
            @Override
            protected void saveCallResult(@NonNull BrandListResult item) {
                brandDAO.insertBrands(item.getResults());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Brand> data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<Brand>> loadFromDb() {
                return brandDAO.findAll();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BrandListResult>> createCall() {
                Map<String, String> params = new HashMap<>();
                return webservice.getBrands(params);
            }

            @Override
            protected boolean isPost() {
                return true;
            }
        }.asLiveData();
    }
}
