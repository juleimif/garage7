package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Country {

    @NonNull
    public final String id;

    @NonNull
    public final String country;

    @NonNull
    public final String locale;

    @NonNull
    public final String currency;

    @NonNull
    public final String distanceSymbol;

    @NonNull
    //@SerializedName("thousandsSep")
    public final String thousandsSep;

    @NonNull
    public final String decPoint;

    public Country(@NonNull String id, String country, String locale, String currency,
                   String distanceSymbol, String thousandsSep, String decPoint) {
        this.id = id;
        this.country = country;
        this.locale = locale;
        this.currency = currency;
        this.distanceSymbol = distanceSymbol;
        this.thousandsSep = thousandsSep;
        this.decPoint = decPoint;
    }

    public String toString() {
        return country;
    }
}
