package com.tnb.app.garage7.injector.module;

import com.tnb.app.garage7.login.LoginActivity;
import com.tnb.app.garage7.main.MainActivity;
import com.tnb.app.garage7.signup.SignupActivity;
import com.tnb.app.garage7.splash.SplashActivity;
import com.tnb.app.garage7.welcome.WelcomeActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SplashActivity contributeSplashActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract WelcomeActivity contributeBienvenidaActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract SignupActivity contributeSignupActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();
}
