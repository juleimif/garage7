package com.tnb.app.garage7.data.entity;

public enum TransactionState {

    OPERATION_STARTED,
    DATE_SCHEDULED,
    PURCHASE_ACCEPTED,
    PAYMENT_MADE,
    VEHICLE_DELIVERED,
    VEHICLE_RECEIVED,
    OPERATION_FINISHED,
    CANCELED_BY_BUYER,
    CANCELED_BY_GARAGE;

    public String toString() {
        String state = "Operación iniciada";
        switch(this) {
            case OPERATION_STARTED: state = "Operación iniciada"; break;
            case DATE_SCHEDULED: state = "Cita concesionario"; break;
            case PURCHASE_ACCEPTED: state = "Compra aceptada"; break;
            case PAYMENT_MADE: state = "Pago realizado"; break;
            case VEHICLE_DELIVERED: state = "Vehículo entregado"; break;
            case VEHICLE_RECEIVED: state = "Vehículo recibido"; break;
            case OPERATION_FINISHED: state = "Operación finalizada"; break;
            case CANCELED_BY_BUYER: state = "Cancelada por comprador"; break;
            case CANCELED_BY_GARAGE: state = "Cancelada por concesionario"; break;
        }
        return state;
    }
}
