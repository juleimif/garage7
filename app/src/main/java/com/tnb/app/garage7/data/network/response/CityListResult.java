package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.City;

import java.util.ArrayList;
import java.util.List;

public class CityListResult {

    @SerializedName(value = "data")
    private List<City> results = new ArrayList<>();

    public CityListResult(List<City> list) {
        results = list;
    }

    public List<City> getResults() {
        return results;
    }
}
