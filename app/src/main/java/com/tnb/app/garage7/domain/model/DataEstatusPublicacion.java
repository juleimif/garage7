package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//import org.parceler.Parcel;

//import lombok.Data;
//import lombok.EqualsAndHashCode;

/**
 * Created by Juleimis on 02/10/2017.
 */
public
class DataEstatusPublicacion {

    @SerializedName("idpublicacion")
    @Expose
    public String idpublicacion;
    @SerializedName("type_post")
    @Expose
    public String typePost;
    @SerializedName("date_publicacion")
    @Expose
    public String datePublicacion;
    @SerializedName("estatus")
    @Expose
    public String estatus;
    @SerializedName("ubicacion")
    @Expose
    public String ubicacion;
    @SerializedName("precio")
    @Expose
    public String precio;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("id_publicacion")
    @Expose
    public String idPublicacion;
    @SerializedName("photo1")
    @Expose
    public String photo1;
    @SerializedName("photo2")
    @Expose
    public String photo2;
    @SerializedName("photo3")
    @Expose
    public String photo3;
    @SerializedName("photo4")
    @Expose
    public String photo4;
    @SerializedName("photo5")
    @Expose
    public String photo5;
    @SerializedName("photo6")
    @Expose
    public String photo6;
    @SerializedName("photo7")
    @Expose
    public String photo7;
    @SerializedName("photo8")
    @Expose
    public String photo8;
    @SerializedName("photo9")
    @Expose
    public String photo9;
    @SerializedName("photo10")
    @Expose
    public String photo10;

    public String getIdpublicacion() {
        return idpublicacion;
    }

    public String getTypePost() {
        return typePost;
    }

    public String getDatePublicacion() {
        return datePublicacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getPrecio() {
        return precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public String getPhoto1() {
        return photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    public String getPhoto10() {
        return photo10;
    }

    @Override
    public String toString() {
        return "DataEstatusPublicacion{" +
                "idpublicacion='" + idpublicacion + '\'' +
                ", typePost='" + typePost + '\'' +
                ", datePublicacion='" + datePublicacion + '\'' +
                ", estatus='" + estatus + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                ", precio='" + precio + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", idPublicacion='" + idPublicacion + '\'' +
                ", photo1='" + photo1 + '\'' +
                ", photo2='" + photo2 + '\'' +
                ", photo3='" + photo3 + '\'' +
                ", photo4='" + photo4 + '\'' +
                ", photo5='" + photo5 + '\'' +
                ", photo6='" + photo6 + '\'' +
                ", photo7='" + photo7 + '\'' +
                ", photo8='" + photo8 + '\'' +
                ", photo9='" + photo9 + '\'' +
                ", photo10='" + photo10 + '\'' +
                '}';
    }
}
