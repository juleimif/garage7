package com.tnb.app.garage7.data.view;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface LoginView {
    void specifyGoogleSignIn(GoogleSignInOptions gso);
    void startProfileActivity();
    Context getContext();
    void showLoading();
    void hideLoading();
    void goToGallery ();
    void showToast(String mssg);
    void desconectar();
    void choiceCountry(String personName, String personEmail, String base64Img);
    void callFromVKSignIn(int requestCode, int resultCode, Intent data);
}
