package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Error {

    @Expose
    @SerializedName(value = "first_name")
    private List<String> firstNameErrors;

    @Expose
    @SerializedName(value = "last_name")
    private List<String> lastNameErrors;

    @Expose
    @SerializedName(value = "gender")
    private List<String> genderErrors;

    @Expose
    @SerializedName(value = "id_number")
    private List<String> idNumberErrors;

    @Expose
    @SerializedName(value = "phone")
    private List<String> phoneErrors;

    @Expose
    @SerializedName(value = "email")
    private List<String> emailErrors;

    @Expose
    @SerializedName(value = "password")
    private List<String> passwordErrors;

    @Expose
    @SerializedName(value = "city_id")
    private List<String> cityIdErrors;

    @Expose
    @SerializedName(value = "country_id")
    private List<String> countryIdErrors;

    @SerializedName(value = "model")
    private List<String> modelErrors;

    @SerializedName(value = "brand")
    private List<String> brandErrors;

    @SerializedName(value = "fuel")
    private List<String> fuelErrors;

    @SerializedName(value = "color")
    private List<String> colorErrors;

    @SerializedName(value = "city")
    private List<String> cityErrors;

    @SerializedName(value = "country")
    private List<String> countryErrors;

    @SerializedName(value = "description")
    private List<String> descriptionErrors;

    @SerializedName(value = "price")
    private List<String> priceErrors;

    @SerializedName(value = "doors")
    private List<String> doorsErrors;

    @SerializedName(value = "year")
    private List<String> yearErrors;

    @SerializedName(value = "mileage")
    private List<String> mileageErrors;

    @SerializedName(value = "transmission")
    private List<String> transmissionErrors;

    @SerializedName(value = "displacement")
    private List<String> displacementErrors;

    @SerializedName(value = "is_active")
    private List<String> isActiveErrors;

    public List<String> firstNameErrors() { return firstNameErrors; }

    public List<String> lastNameErrors() { return lastNameErrors; }

    public List<String> genderErrors() { return genderErrors; }

    public List<String> idNumberErrors() { return idNumberErrors; }

    public List<String> phoneErrors() { return phoneErrors; }

    public List<String> emailErrors() { return emailErrors; }

    public List<String> passwordErrors() { return passwordErrors; }

    public List<String> cityIdErrors() { return cityIdErrors; }

    public List<String> countryIdErrors() { return countryIdErrors; }

    public List<String> modelErrors() {
        return modelErrors;
    }

    public List<String> brandErrors() {
        return brandErrors;
    }

    public List<String> fuelErrors() {
        return fuelErrors;
    }

    public List<String> colorErrors() {
        return colorErrors;
    }

    public List<String> cityErrors() {
        return cityErrors;
    }

    public List<String> countryErrors() {
        return countryErrors;
    }

    public List<String> descriptionErrors() {
        return descriptionErrors;
    }

    public List<String> priceErrors() {
        return priceErrors;
    }

    public List<String> doorsErrors() {
        return doorsErrors;
    }

    public List<String> yearErrors() {
        return yearErrors;
    }

    public List<String> mileageErrors() {
        return mileageErrors;
    }

    public List<String> transmissionErrors() {
        return transmissionErrors;
    }

    public List<String> isActiveErrors() {
        return isActiveErrors;
    }

    public List<String> displacementErrors() {
        return displacementErrors;
    }
}
