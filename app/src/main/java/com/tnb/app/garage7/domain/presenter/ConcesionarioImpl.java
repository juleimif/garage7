package com.tnb.app.garage7.domain.presenter;

import android.util.Log;

import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.request.ConcesionarioRequest;
import com.tnb.app.garage7.data.view.ConcesionarioView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class ConcesionarioImpl implements ConcesionarioPresenter {
    public static final String TAG = ConcesionarioImpl.class.getSimpleName();
    public ConcesionarioView concesionarioView;
    PreferencesSessionManager preferencesUserManager;

    public ConcesionarioImpl(ConcesionarioView concesionarioView) {
        this.concesionarioView = concesionarioView;
        preferencesUserManager = new PreferencesSessionManager(concesionarioView.getContext());

    }

    @Override
    public void createAccount(final ConcesionarioRequest concesionarioRequest) {
        concesionarioView.showLoading();
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.setConcesionario(concesionarioRequest).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                concesionarioView.hideLoading();
                if (response.code() == 200) {
                    response(response.body());
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                concesionarioView.hideLoading();
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }


    private void respuestaServicio(String message) {
        concesionarioView.showToast(message,0);
        Log.e(TAG, "respuestaServicio: " + message );
    }

    private void response(ResponseVehiculo body) {
        if (body.status.equals("true")) {
            preferencesUserManager.setUserConce("1");
            concesionarioView.showToast(body.message, 1);
            concesionarioView.goBack();

        } else {
            concesionarioView.showToast(body.message, 0);
            Log.e(TAG, "false: " + body.message );

        }
    }

}
