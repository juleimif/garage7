package com.tnb.app.garage7.data.network.response;

public class UploadImageResult {

    private boolean created;

    private String path;

    public UploadImageResult(boolean created, String path) {
        this.created = created;
        this.path = path;
    }

    public boolean isCreated() {
        return created;
    }

    public String getPath() {
        return path;
    }
}
