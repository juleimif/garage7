package com.tnb.app.garage7.domain.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 02/10/2017.
 */

public class UpdateStatusCarRequest {

    @SerializedName("idpub")
    @Expose
    public String idpub;
    @SerializedName("newstatus")
    @Expose
    public String newstatus;

    public UpdateStatusCarRequest(String idpub, String newstatus) {
        this.idpub = idpub;
        this.newstatus = newstatus;
    }

    public String getIdpub() {
        return idpub;
    }

    public void setIdpub(String idpub) {
        this.idpub = idpub;
    }

    public String getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(String newstatus) {
        this.newstatus = newstatus;
    }

    @Override
    public String toString() {
        return "UpdateStatusCarRequest{" +
                "idpub='" + idpub + '\'' +
                ", newstatus='" + newstatus + '\'' +
                '}';
    }
}
