package com.tnb.app.garage7.domain.presenter;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface PublicacionesPresenter {
    void getPublicaciones(String idConcesionario);
}
