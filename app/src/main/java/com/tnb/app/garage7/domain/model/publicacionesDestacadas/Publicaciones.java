package com.tnb.app.garage7.domain.model.publicacionesDestacadas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 19/1/2018.
 */

public class Publicaciones {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public PublicacionesResponse data;

    public String getStatus() {
        return status;
    }

    public PublicacionesResponse getData() {
        return data;
    }
}
