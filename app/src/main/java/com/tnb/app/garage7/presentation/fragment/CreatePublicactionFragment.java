package com.tnb.app.garage7.presentation.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;
import com.tnb.app.garage7.presentation.Adapter.EstadosAdapter;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.NextInterface;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.modelMoto.VehiculoMoto;
import com.tnb.app.garage7.domain.model.response.AutocompletarMarcas;
import com.tnb.app.garage7.domain.model.response.AutocompletarMarcasResponse;
import com.tnb.app.garage7.domain.model.response.AutocompletarModels;
import com.tnb.app.garage7.domain.model.response.AutocompletarModelsResponse;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.BrandsAdapter;
import com.tnb.app.garage7.presentation.Adapter.ModelsAdapter;
import com.tnb.app.garage7.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreatePublicactionFragment extends Fragment implements TextWatcher, Validator.ValidationListener {
    private static final String TAG = CreatePublicactionFragment.class.getSimpleName();

    @BindView(R.id.fl_next)
    FloatingActionButton fl_next;
    @BindView(R.id.rv_brands)
    RecyclerView rv_brands;
    @BindView(R.id.rv_models)
    RecyclerView rv_models;
    @BindView(R.id.ly_account)
    CoordinatorLayout ly_account;
    @NotEmpty(message = "Debe ingresar una marca")
    @BindView(R.id.input_brand)
    EditText input_brand;
    @NotEmpty(message = "Debe ingresar un modelo")
    @BindView(R.id.input_model)
    EditText input_model;
    @NotEmpty(message = "Debe ingresar un año")
    @BindView(R.id.input_año)
    EditText input_año;
    @NotEmpty(message = "Debe el kilometraje")
    @BindView(R.id.input_km)
    EditText input_km;
    @NotEmpty(message = "Debe ingresar el combustible")
    @BindView(R.id.sp_disel)
    MaterialBetterSpinner sp_combustible;
    @NotEmpty(message = "Debe ingresar la cilindrada")
    @BindView(R.id.sp_cilindrada)
    MaterialBetterSpinner sp_cilindrada;
    @BindView(R.id.model)
    TextInputLayout model;

TextWatcher tw;
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!input_brand.getText().toString().isEmpty() && !input_model.getText().toString().isEmpty()
                && !input_año.getText().toString().isEmpty() && !input_km.getText().toString().isEmpty()
                && !combustible.isEmpty() && !cilindrada.isEmpty()) {
            //apto para pasar al siguiente formulario
            fl_next.setVisibility(View.VISIBLE);
        }
    }


    String[] CILINDRADA = {"M-01", "B-036"};
    String[] COMBUSTIBLE = {"Diesel", "Gasolina"};

    public NextInterface listener;
    public int type_publication;
    private BrandsAdapter brandsAdapter;
    private ModelsAdapter modelsAdapter;
    private String marca;
    private Validator validator;
    private Activity activity;
    private VehiculoCarro carroModelSingleton;
    private VehiculoMoto motorModelSingleton;
    private String modelo, tipo, combustible, cilindrada = "";

    public CreatePublicactionFragment() {
        // Required empty public constructor
    }

    public static CreatePublicactionFragment newInstance(int type_publication) {
        CreatePublicactionFragment fragment = new CreatePublicactionFragment();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_publication, container, false);
        ButterKnife.bind(this, view);
        validator = new Validator(this);
        validator.setValidationListener(this);
        fl_next.setVisibility(View.VISIBLE);

        //Obtenemos tipo de publicacion
        type_publication = getArguments().getInt("type", 0);

        //class singleton
        carroModelSingleton = VehiculoCarro.getInstance();
        motorModelSingleton = VehiculoMoto.getInstance();

        rv_brands.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_brands.setLayoutManager(layoutManager);


        //Moto
        if (type_publication == 1) {
            sp_combustible.setVisibility(View.GONE);
            sp_cilindrada.setVisibility(View.VISIBLE);
        }


        ArrayAdapter<String> arrayAdapter4 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, COMBUSTIBLE);
        sp_combustible.setAdapter(arrayAdapter4);
        sp_combustible.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                combustible = parent.getItemAtPosition(position).toString();
                if (!input_brand.getText().toString().isEmpty() && !input_model.getText().toString().isEmpty()
                        && !input_año.getText().toString().isEmpty() && !input_km.getText().toString().isEmpty()
                        && combustible != "" && cilindrada != "") {
                    //apto para pasar al siguiente formulario
                    fl_next.setVisibility(View.VISIBLE);
                }

            }
        });

        ArrayAdapter<String> arrayAdapter3 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, CILINDRADA);

        sp_cilindrada.setAdapter(arrayAdapter3);

        sp_cilindrada.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cilindrada = parent.getItemAtPosition(position).toString();
                if (!input_brand.getText().toString().isEmpty() && !input_model.getText().toString().isEmpty()
                        && !input_año.getText().toString().isEmpty() && !input_km.getText().toString().isEmpty()
                        && !combustible.isEmpty() && !cilindrada.isEmpty()) {
                    //apto para pasar al siguiente formulario
                    fl_next.setVisibility(View.VISIBLE);
                }

            }
        });

        input_año.addTextChangedListener(this);
        input_brand.addTextChangedListener(this);
        input_model.addTextChangedListener(this);


        input_año.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 4) {
                    input_año.setError("Formato incorrecto");
                    input_año.setText("");

                } else if (s.length() > 3) {
                    if (Integer.parseInt(String.valueOf(s)) < 1950 || Integer.parseInt(String.valueOf(s)) > 2018) {
                        input_año.setError("Ingrese un año válido");
                        input_año.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        input_brand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                marca = String.valueOf(charSequence);
                if (marca.length() > 2){
                    Log.i(TAG, "onTextChanged: " + input_brand.requestFocus());
                    if (input_brand.hasFocus()){
                        getBrandsCars(marca);
                    }

                }
                else if (marca.length() == 0) {
                    model.setVisibility(View.GONE);
                    input_model.setText("");
                    modelo = "";

                tw = this;

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        input_model.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                modelo = String.valueOf(charSequence);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (modelo.length() > 2){
                    if (input_brand.hasFocus()){
                        getModelCars(marca, modelo);
                    }
                }
            }
        });

        return view;
    }

    @OnClick(R.id.fl_next)
    public void Next(View view) {
        validator.validate();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (NextInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    activity.toString() +
                            " no implementó OkDialogListener");
        }
    }

    public void setTypePublicaton(int type) {
        Log.i(TAG, "set setearan los siguientes campos: " + type);
        if (type == 1) //Moto
        {
            motorModelSingleton.setBrand(input_brand.getText().toString());
            motorModelSingleton.setCilindradas(cilindrada);
            motorModelSingleton.setYear(input_año.getText().toString());
            motorModelSingleton.setMileage(input_año.getText().toString());
            listener.next(1);

        } else //Carro
        {
            Log.i(TAG, "setTypePublicaton: " + input_brand.getText().toString());
            carroModelSingleton.setBrandC(input_brand.getText().toString());
            carroModelSingleton.setModelC(modelo);
            carroModelSingleton.setYearC(input_año.getText().toString());
            carroModelSingleton.setMileageC(input_año.getText().toString());
            carroModelSingleton.setGasC(combustible);
            listener.next(1);
        }
    }

    @Override
    public void onValidationSucceeded() {
        setTypePublicaton(type_publication);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void getBrandsCars(String brand) {
        model.setVisibility(View.VISIBLE);
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        Log.i(TAG, "getBrandsCars: " + inter.getBrands(brand).request().url());
        inter.getBrands(brand).enqueue(new Callback<AutocompletarMarcasResponse>() {
            @Override
            public void onResponse(Call<AutocompletarMarcasResponse> call, Response<AutocompletarMarcasResponse> response) {
                if (response.code() == 200) {
                    brandsAdapter = new BrandsAdapter(getContext(), response.body().getData(), new BrandsAdapter.BrandItemClick() {
                        @Override
                        public void onBrokerClick(AutocompletarMarcas clickedBroker) {
                            Log.i(TAG, "onBrokerClick 1: ");
                            input_brand.setFocusableInTouchMode(false);
                            input_brand.setFocusable(false);

                            input_brand.setText(clickedBroker.getMarca());
                            rv_brands.setVisibility(View.GONE);

                            input_brand.setFocusableInTouchMode(true);
                            input_brand.setFocusable(true);
                        }
                    });
                    rv_brands.setAdapter(brandsAdapter);
                    rv_brands.setVisibility(View.VISIBLE);


                }
            }

            @Override
            public void onFailure(Call<AutocompletarMarcasResponse> call, Throwable t) {
                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

    }

    public void getModelCars(String brand, String model) {
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        Log.i(TAG, "getModelCars: " + brand + model);
        Log.i(TAG, "getModelCars: " + inter.getModels("toyota", model).request().url());
        inter.getModels(brand, model).enqueue(new Callback<AutocompletarModelsResponse>() {
            @Override
            public void onResponse(Call<AutocompletarModelsResponse> call, Response<AutocompletarModelsResponse> response) {
                if (response.code() == 200) {
                    if (response.body().getData().size() > 0) {

                        Log.i(TAG, "onResponse modelo: " + response.body().getData().size());
                        modelsAdapter = new ModelsAdapter(getContext(), response.body().getData(), new ModelsAdapter.BrandItemClick() {
                            @Override
                            public void onBrokerClick(AutocompletarModels clickedBroker) {
                                Log.i(TAG, "onBrokerClick: " + clickedBroker.getModelo());
                                input_model.setFocusableInTouchMode(false);
                                input_model.setFocusable(false);

                                input_model.setText(clickedBroker.getModelo());
                                rv_models.setVisibility(View.GONE);

                                input_model.setFocusableInTouchMode(true);
                                input_model.setFocusable(true);
                            }
                        });
                        rv_models.setAdapter(modelsAdapter);
                        rv_models.setVisibility(View.VISIBLE);
                    } else Utils.showSnackBar(getContext(), "No existen coincidencias", ly_account);

                }
            }

            @Override
            public void onFailure(Call<AutocompletarModelsResponse> call, Throwable t) {
                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }


    @Override
    public void afterTextChanged(Editable s) {

    }

    private void respuestaServicio(String message) {
        Log.e(TAG, "respuestaServicio: " + message);

    }

}

