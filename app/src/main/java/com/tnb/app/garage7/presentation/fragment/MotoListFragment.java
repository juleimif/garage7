package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.modelMoto.Motos;
import com.tnb.app.garage7.domain.model.modelMoto.VehiculoMoto;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.MotoAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MotoListFragment extends Fragment {
    public static final String TAG = MotoListFragment.class.getSimpleName();

    @BindView(R.id.rv_motos_list) RecyclerView rv_motos_list;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.reload) ImageButton reload;

    MotoAdapter motoAdapter;
    GoDetails listener;

    public MotoListFragment() {
        // Required empty public constructor
    }

    public static MotoListFragment newInstance(String param1, String param2) {
        MotoListFragment fragment = new MotoListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_motos_list, container, false);
        ButterKnife.bind(this, view);
        initViews();
        listener.showAjustes();
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAllMotos();
                    }
                }, 0);
            }
        });
        return view;
    }

    private void initViews() {
        rv_motos_list.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_motos_list.setLayoutManager(layoutManager);
        getAllMotos();
    }

    public void getAllMotos() {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getAllMotos().enqueue(new Callback<Motos>() {
            @Override
            public void onResponse(Call<Motos> call, Response<Motos> response) {
                if (response.code() == 200) {
                    if (response.body().data.vehiculos.size()>0) {
                        motoAdapter = new MotoAdapter(getContext(), response.body().data.vehiculos, new MotoAdapter.CarItemClick() {
                            @Override
                            public void onBrokerClick(VehiculoMoto clickedBroker) {
                            }
                        });
                    }
                    else {
                        reload.setVisibility(View.VISIBLE);
                    }
                }
                rv_motos_list.setAdapter(motoAdapter);
                hideLoading();
            }

            @Override
            public void onFailure(Call<Motos> call, Throwable t) {
                hideLoading();
                Log.e(TAG, "onFailure: " + t.getMessage());
                reload.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

        // If isn't refreshing
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        getAllMotos();
        reload.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}
