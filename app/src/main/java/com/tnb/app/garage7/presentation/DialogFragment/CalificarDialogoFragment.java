package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tnb.app.garage7.R;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class CalificarDialogoFragment extends DialogFragment {
    public static final String TAG = CalificarDialogoFragment.class.getSimpleName();

    @BindView(R.id.lowerRatingBar) ProperRatingBar lowerRatingBar;
    private String[] ticksArray = new String[] {"$", "★"};

    public interface pagoDialogListener {
        void onFinishVotageDialog();
    }

    public static CalificarDialogoFragment newInstance(String personName, String personEmail, String base64Img) {
        CalificarDialogoFragment f = new CalificarDialogoFragment();
        Bundle args = new Bundle();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_calificar);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);
        lowerRatingBar.setListener(ratingListener);

        return dialogo;
    }

    @OnClick(R.id.lowerRatingBar) void toggleTick() { // here goes shit-code. No time to do properly
        String tick;
        if (lowerRatingBar.getSymbolicTick().equals(ticksArray[0])) tick = ticksArray[1];
        else tick = ticksArray[1];
        lowerRatingBar.setSymbolicTick(tick);
    }

    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                sendBackResult();
                break;
        }
    }

    private RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatePicked(ProperRatingBar ratingBar) {

        }
    };



    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        CalificarDialogoFragment.pagoDialogListener listener = (CalificarDialogoFragment.pagoDialogListener) getTargetFragment();
        listener.onFinishVotageDialog();
        dismiss();
    }
}
