package com.tnb.app.garage7.presentation.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class ViewPagerAdapter extends FragmentPagerAdapter {


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
         switch (position) {
            case 0:
             case 1:
                 return  null;
              //   return new ProgressFragment();
             case 2:
                 return  null;

             //  return new MyGroupFragment();
             case 3:
                 return  null;

             //   return new SocialFragment();
         }
        return null;
    }


    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
       switch (position) {
            case 0:
                return "JoinUp";
            case 1:
                return "My groups";
            case 2:
                return "Progress";
            case 3:
                return "Pendiente menor";
        }
        return null;
    }



}
