package com.tnb.app.garage7.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.tnb.app.garage7.R;


/**
 * this generic class, for request all permissions android versions 6 or superior
 */
public class PermissionManager {

    public static PermissionManager  instance;
    private Activity activity;

    public final int MY_PERMISSIONS_REQUEST_CAMERA=6921;
    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE=6922;

    private int myRequestCode;
    private String[] myPermissions;
    private int[] myGrantResults;
    private static Context context;
    PermissionTracker inter;
    public boolean reinicioApp=true;

    /**
     * make a singletons instance
     * @return
     */
    public static PermissionManager getInstance(){
        if(instance==null)
            instance= new PermissionManager();
           // context= MainActivity.getContext();
        return instance;
    }

    /**
     * setters and getters
     * @return
     */
    public int getMyRequestCode() {
        return myRequestCode;
    }

    public void setMyRequestCode(int myRequestCode) {
        this.myRequestCode = myRequestCode;
    }

    public String[] getMyPermissions() {
        return myPermissions;
    }

    public void setMyPermissions(String[] myPermissions) {
        this.myPermissions = myPermissions;
    }

    public int[] getMyGrantResults() {
        return myGrantResults;
    }

    public void setMyGrantResults(int[] myGrantResults) {
        this.myGrantResults = myGrantResults;
    }

    /**
     * check if have pararm permission
     * @param activity
     * @param permission
     * @return
     */
    @TargetApi(Build.VERSION_CODES.M)
    public boolean checkPermission(Activity activity, String permission){
        this.activity=activity;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(permission.equals(Manifest.permission.CAMERA)){
                return isGetPermission(activity,permission,MY_PERMISSIONS_REQUEST_CAMERA);
            }else if(permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                return isGetPermission(activity,permission,MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
        return true;
    }
        /**
         * if get permission return true; else return false
         * @param activity
         * @param permission
         * @param MY_PERMISSIONS_REQUEST
         * @return
         */
        public boolean isGetPermission(Activity activity, String permission, int MY_PERMISSIONS_REQUEST){
            if (ContextCompat.checkSelfPermission(activity,permission) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        permission)) {
                    return false;
                } else {
                /*solicitando permisos*/
                    return false;
                }
            }
            return true;
        }

        /**
         *  get request permissions result
         * @param requestCode
         * @param permissions
         * @param grantResults
         */
        public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults, PermissionTracker inter) {
            setMyRequestCode(requestCode);
            setMyPermissions(permissions);
            setMyGrantResults(grantResults);
            this.inter=inter;
            boolean result = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
            inter.onResult(result,requestCode);
        }

    /**
     * show message before go to setting intent
     * @param message
     */
    public void dialogToSetting(String message){
        new AlertDialog.Builder(activity)
                .setTitle("Alerta")
                .setCancelable(false)
                .setPositiveButton("Ir a ajustes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog,
                                    final int id) {
                                final Intent i = new Intent();
                                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                i.addCategory(Intent.CATEGORY_DEFAULT);
                                i.setData(Uri.parse("package:" + context.getPackageName()));
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                context.startActivity(i);
                            }
                        })
                .setNegativeButton("Salir",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog,
                                    final int id) {
                                dialog.dismiss();
                                onRequestPermissionsResult(getMyRequestCode(),getMyPermissions(),getMyGrantResults(),inter);
                            }
                        })
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }




    }
