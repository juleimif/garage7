package com.tnb.app.garage7.presentation.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.Car;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.modelMoto.VehiculoMoto;
import com.tnb.app.garage7.domain.model.request.CarRequest;
import com.tnb.app.garage7.domain.model.request.MotoRequest;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.utils.CustomProgressUtils;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FormInfCarFragment extends Fragment implements Validator.ValidationListener {
    public static final String TAG = FormInfCarFragment.class.getSimpleName();

    @BindView(R.id.ly_account) CoordinatorLayout ly_account;
    @BindView(R.id.input_precio) EditText input_precio;
    @NotEmpty(message = "Debe ingresar un monto")
    @BindView(R.id.comision_conc) TextView comision_conc;
    @BindView(R.id.comision_app) TextView comision_app;
    @BindView(R.id.total) TextView total_monto;
    private Validator validator;
    private VehiculoCarro carDataModel;
    private VehiculoMoto motoDataModel;
    private Car userModelSingleton;
    private PreferencesSessionManager preferencesUserManager;
    ProgressDialog progress;
    String tv_total_monto;
    public int type_publication;


    public interface goMain{
        void goMain();
    }

    goMain goMain;

    public FormInfCarFragment() {
        // Required empty public constructor
    }

    public static FormInfCarFragment newInstance(int  type_publication) {
        FormInfCarFragment fragment = new FormInfCarFragment();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_about_car, container, false);
        ButterKnife.bind(this, view);
        carDataModel = VehiculoCarro.getInstance();
        motoDataModel = VehiculoMoto.getInstance();
        userModelSingleton = Car.getInstance();

        //Obtenemos tipo de publicacion
        type_publication = getArguments().getInt("type", 0);
        Log.i(TAG, "type_publication: " + type_publication);

        validator = new Validator(this);
        validator.setValidationListener(this);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        input_precio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    calculate();

                } else {
                    total_monto.setText("0" + " BsF");
                    comision_conc.setText("0" + " BsF");
                    comision_app.setText("0" + " BsF");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        Resources res = getResources();
        CustomProgressUtils customProgressShowProgress = (CustomProgressUtils) view.findViewById(R.id.customProgressShowProgress);
        customProgressShowProgress.setMaximumPercentage(1.0f);
        customProgressShowProgress.useRoundedRectangleShape(30.0f);
        customProgressShowProgress.setProgressColor(res.getColor(R.color.color_toolbar));
        customProgressShowProgress.setProgressBackgroundColor(res.getColor(R.color.color_toolbar));
        //customProgressShowProgress.setShowingPercentage(true);

        return view;
    }

    @OnClick(R.id.btn_publicar)
    public void Publicar(View view) {
        validator.validate();
    }

    /**
     * Metodo para calcular comisiones
     */
    public void calculate() {

        double monto = Integer.parseInt(input_precio.getText().toString());
        double calculate_conc = (monto * 5) / 100;
        double calculate_app = (monto * 3) / 100;
        String total = String.format("%,.2f", calculate_conc);
        String total2 = String.format("%,.2f", calculate_app);

        comision_conc.setText(total + " BsF");
        comision_app.setText(total2 + " BsF");
        Double monto_t = monto + calculate_conc + calculate_app;
        tv_total_monto = String.format("%,.2f", monto_t);
        total_monto.setText(tv_total_monto + " BsF");
    }

    @Override
    public void onValidationSucceeded() {
        new AlertDialog.Builder((getActivity()))
                .setMessage(getResources().getString(R.string.messaje))
                .setCancelable(false)
                .setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(TAG, "onClick: ");
                        if (type_publication == 0) {
                            Log.i(TAG, "onClick carro: ");
                            publicarCarro();
                        } else {
                            Log.i(TAG, "onClick moto: ");
                            publicarMoto();
                        }
                    }
                })
                .setNegativeButton(this.getString(R.string.exit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void publicarCarro() {

        progress = new ProgressDialog(getContext());
        progress.setTitle("Publicando");
        progress.setMessage("Por favor espere...");
        progress.show();

        carDataModel = VehiculoCarro.getInstance();
        carDataModel.setPriceC(total_monto.getText().toString());
        CarRequest car = new CarRequest(preferencesUserManager.getUserId(), carDataModel.getBrandC(), carDataModel.getModelC(), carDataModel.getYearC(), carDataModel.getMileageC(), carDataModel.getTransmitionC(), carDataModel.getGasC(), carDataModel.getColorC(), carDataModel.getDoorNumberC(), carDataModel.getDescriptionsC(), carDataModel.getLocationC(), tv_total_monto, userModelSingleton.getPhotos());
        ConnectClient connect = new ConnectClient();
        ApiInterface service = connect.setService();
        Log.i(TAG, "publicarCarro: " + service.setPublicaction(car).request().url());
        Log.i(TAG, "publicarCarro Data: " + car.toString());

        service.setPublicaction(car).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                if (progress != null) {
                    progress.dismiss();
                }
                if (response.code() == 200) {
                    showMessage();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {

                if (progress != null) {
                    progress.dismiss();
                }

                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void showMessage() {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle("¡Felicitaciones!")
                .setDescription("Has publicado de manera exitosa")
                .setPositiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        goMain.goMain();
                        dialog.dismiss();
                    }
                });

        dialogHeader_7.show();
    }

    private void respuestaServicio(String message) {
        Log.i(TAG, "respuestaServicio: " + message);
        Utils.showSnackBar(getContext(), message, ly_account);

    }


    public void publicarMoto() {
        progress = new ProgressDialog(getContext());
        progress.setTitle("Publicando");
        progress.setMessage("Por favor espere...");
        progress.show();

        motoDataModel = VehiculoMoto.getInstance();
        motoDataModel.setPrice(total_monto.getText().toString());
        MotoRequest moto = new MotoRequest(preferencesUserManager.getUserId(), motoDataModel.brand, motoDataModel.cilindradas, motoDataModel.year, motoDataModel.mileage, motoDataModel.color, motoDataModel.descriptionsM, motoDataModel.location, total_monto.getText().toString(), userModelSingleton.getPhotos());
        ConnectClient connect = new ConnectClient();
        ApiInterface service = connect.setService();
//        Log.i(TAG, "publicarMoto: " + moto.toString());

        service.setPublicactionMoto(moto).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {
                Log.i(TAG, "onResponse moto: " + response.code());
                if (progress != null) {
                    progress.dismiss();
                }
                if (response.code() == 200) {
                    Log.i(TAG, "onResponse moto: ");
                    showMessage();
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                if (progress != null) {
                    progress.dismiss();
                }

                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            goMain = (goMain) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }
}





