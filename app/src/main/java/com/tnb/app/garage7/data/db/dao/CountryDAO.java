package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.Country;

import java.util.List;

@Dao
public interface CountryDAO {

    @Query("SELECT * FROM Country")
    LiveData<List<Country>> findAll();

    @Query("SELECT * FROM Country where country like '%' || :country || '%' ")
    LiveData<List<Country>> findByName(String country);

    @Query("SELECT * FROM Country where id = :id")
    LiveData<Country> searchCountryById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Country... country);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountries(List<Country> countryList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createCountryIfNotExists(Country country);

    @Query("DELETE FROM Country")
    void deleteAll();
}
