package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryListResult {

    @SerializedName(value = "data")
    private List<Country> results = new ArrayList<>();

    public CountryListResult(List<Country> results) {
        this.results = results;
    }

    public List<Country> getResults() {
        return results;
    }
}
