package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 1/3/2018.
 */

public class CheckEmailResponse {

    @SerializedName("idusuarios")
    @Expose
    public String idusuarios;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("lastname")
    @Expose
    public String lastname;
    @SerializedName("sex")
    @Expose
    public String sex;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("N_indetification")
    @Expose
    public String nIndetification;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("profile_photo")
    @Expose
    public String profilePhoto;
    @SerializedName("terms")
    @Expose
    public String terms;
    @SerializedName("isconsecionario")
    @Expose
    public String isconsecionario;

    public String getIdusuarios() {
        return idusuarios;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getSex() {
        return sex;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getnIndetification() {
        return nIndetification;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getPassword() {
        return password;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public String getTerms() {
        return terms;
    }

    public String getIsconsecionario() {
        return isconsecionario;
    }

    @Override
    public String toString() {
        return "CheckEmailResponse{" +
                "idusuarios='" + idusuarios + '\'' +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", sex='" + sex + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", nIndetification='" + nIndetification + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                ", password='" + password + '\'' +
                ", profilePhoto='" + profilePhoto + '\'' +
                ", terms='" + terms + '\'' +
                ", isconsecionario='" + isconsecionario + '\'' +
                '}';
    }
}
