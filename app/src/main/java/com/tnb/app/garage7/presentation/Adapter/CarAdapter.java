package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Vehiculo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private CarItemClick mItemClickListener;
    private  List<VehiculoCarro> carList;
    private Context contexto;

    public CarAdapter(Context applicationContext, List<VehiculoCarro> car, CarItemClick brokerItemClick) {
        this.carList =  car;
        this.contexto = applicationContext;
        mItemClickListener = (CarItemClick) brokerItemClick;
    }

    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_cars, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VehiculoCarro currentItem = carList.get(position);
        CarAdapter.ViewHolder viewHolder = (CarAdapter.ViewHolder) holder;
        viewHolder.tv_name.setText(currentItem.getBrandC() + " - " + currentItem.getModelC());
        viewHolder.tv_desc.setText(currentItem.getYearC() + " - "+ currentItem.getLocationC());
        viewHolder.tv_price.setText(currentItem.getPriceC() + " VEF");

        Glide.with(contexto)
                .load(currentItem.getPhoto1())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(viewHolder.iv_car);
    }

    @Override
    public int getItemCount() {

        return carList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_desc) TextView tv_desc;
        @BindView(R.id.tv_ver_mas)    TextView tv_ver_mas;
        @BindView(R.id.tv_price)    TextView tv_price;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(carList.get(position));
        }
    }

    public interface CarItemClick {
        void onBrokerClick(VehiculoCarro clickedBroker);

    }


}
