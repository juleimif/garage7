package com.tnb.app.garage7.injector.module;

import com.tnb.app.garage7.data.view.CarListContract;
import com.tnb.app.garage7.injector.scope.CustomScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Acer on 11/2/2018.
 */
@Module
public class CarsListModule {

    private final CarListContract.View mView;


    public CarsListModule(CarListContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    CarListContract.View providesMainScreenContractView() {
        return mView;
    }

}
