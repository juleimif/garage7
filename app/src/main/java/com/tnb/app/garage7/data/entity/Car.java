package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import java.util.List;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Car  {

    public  String fuel;

    public  String doors;

    public  String transmission;

    @NonNull
    public   String id;

    public   String postedBy;

    public   String model;

    public   String brand;

    public   String color;

    public   String city;

    public   String country;

    public   String countryId;

    public   String description;

    public   String price;

    public   String formattedPrice;

    public   String currencySymbol;

    public   String year;

    public   String mileage;

    public   String mileageSymbol;

    public   String isActive;

    public   String createdAt;

    public   String updatedAt;

    public   String url;

    @Ignore // to insert in the Cars table
    public List<CarPhoto> photosUrls;

    public Car() {}
    
    public Car(@NonNull String id, String postedBy, String model, String brand,
               String color, String city, String country, String countryId,
               String fuel, String description, String price, String formattedPrice,
               String currencySymbol, String doors, String year, String mileage,
               String mileageSymbol, String transmission, String isActive,
               String createdAt, String updatedAt, String url, List<CarPhoto> photosUrls) {
        this.id = id;
        this.postedBy = postedBy;
        this.model = model;
        this.brand = brand;
        this.color = color;
        this.city = city;
        this.country = country;
        this.countryId = countryId;
        this.description = description;
        this.price = price;
        this.formattedPrice = formattedPrice;
        this.currencySymbol = currencySymbol;
        this.year = year;
        this.mileage = mileage;
        this.mileageSymbol = mileageSymbol;
        this.isActive = isActive;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.url = url;
        this.photosUrls = photosUrls;

        this.fuel = fuel;
        this.doors = doors;
        this.transmission = transmission;
    }
}
