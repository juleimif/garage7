package com.tnb.app.garage7.domain.presenter;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.Login;
import com.tnb.app.garage7.domain.model.ResponseLogin;
import com.tnb.app.garage7.domain.model.ResponseSession;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.data.view.LoginTradicionalView;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.activity.BaseActivity;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class LoginImpl extends BaseActivity implements LoginPresenter {
    public static final String TAG = LoginImpl.class.getSimpleName();
    public LoginTradicionalView loginTraView;
    private ResponseUser userModelSingleton;

    public LoginImpl(LoginTradicionalView loginView){
        this.loginTraView = loginView;
    }

    @Override
    public void login(Login user) {
        loginTraView.showLoading();
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "login: " + service.setLoginUser(user).request().url());
        service.setLoginUser(user).enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                loginTraView.hideLoading();
                if (response.code()==200){
                    responseLogin(response.body());
                    Log.i(TAG, "onResponse: ");
                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                loginTraView.hideLoading();
                Toast.makeText(loginTraView.getContext(),
                        loginTraView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();


            }
        });
    }

    private void respuestaServicio(String message) {
        loginTraView.showNotify(message);
    }

    private void responseLogin(ResponseLogin body) {
        if(body.status.equals("true")){
            getInfoUser(body.iduser);
        }
        else{
            loginTraView.showNotify(body.message);
        }
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

    }

    public void getInfoUser(String id_use){
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "login 2: " + service.getInfoUser(id_use).request().url());
        service.getInfoUser(id_use).enqueue(new Callback<List<ResponseSession>>() {
            @Override
            public void onResponse(Call<List<ResponseSession>> call, Response<List<ResponseSession>> response) {
                if(response.code()==200){
                    PreferencesSessionManager preferencesUserManager = new PreferencesSessionManager(loginTraView.getContext());
                    preferencesUserManager.setUserName(response.body().get(0).getName());
                    preferencesUserManager.setUserEmail(response.body().get(0).getEmail());
                    preferencesUserManager.setUserId(String.valueOf(response.body().get(0).getIdusuarios()));
                    preferencesUserManager.setUserCountry(response.body().get(0).getCountry());
                    preferencesUserManager.setUserAvatar(response.body().get(0).getProfile_photo());
                    preferencesUserManager.setUserConce(response.body().get(0).getIsconsecionario().toString());
                    preferencesUserManager.setUserIdConcesionario(response.body().get(0).getIdconse().toString());
                    loginTraView.gotoMain();
                    Log.d(TAG, "login data sessin : " + response.body().get(0).toString());
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ResponseSession>> call, Throwable t) {
                loginTraView.hideLoading();
                Toast.makeText(loginTraView.getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }
}
