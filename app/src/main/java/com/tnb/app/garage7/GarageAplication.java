package com.tnb.app.garage7;

import android.app.Activity;
import android.app.Application;
import android.support.multidex.MultiDexApplication;

//import com.crashlytics.android.Crashlytics;
import com.tnb.app.garage7.injector.AppInjector;
/*import com.tnb.app.garage7.injector.component.DaggerNetworkComponent;
import com.tnb.app.garage7.injector.module.AppModule;
import com.tnb.app.garage7.injector.component.NetworkComponent;
import com.tnb.app.garage7.injector.module.NetworkModule;
*/
import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
//import io.fabric.sdk.android.Fabric;

/**
 * Created by Acer on 11/2/2018.
 */

public class GarageAplication extends MultiDexApplication implements HasActivityInjector {

    //private NetworkComponent mNetComponent;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {

        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        AppInjector.init(this);
        /*mNetComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();*/
    }

    //public NetworkComponent getNetComponent() {
       // return mNetComponent;
    //}

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

}