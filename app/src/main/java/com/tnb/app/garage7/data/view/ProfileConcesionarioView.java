package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.response.ConcesionarioResponse;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface ProfileConcesionarioView {
    Context getContext();
    void hideLoading();
    void showInf(ConcesionarioResponse response);
}
