package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tnb.app.garage7.GarageAplication;
import com.tnb.app.garage7.data.view.CarListContract;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Vehiculo;
import com.tnb.app.garage7.domain.presenter.CarListPresenteImp;
//import com.tnb.app.garage7.injector.component.DaggerCarListComponent;
//import com.tnb.app.garage7.injector.module.CarsListModule;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.DialogFragment.FiltrosDialogoFragment;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.presentation.Adapter.CarAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.String.valueOf;


public class CarListFragment extends Fragment implements CarListContract.View {
    public static final String TAG = CarListFragment.class.getSimpleName();
    @BindView(R.id.rv_car_list)
    RecyclerView rv_car_list;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.reload)
    ImageButton reload;
    VehiculoCarro carModelSingleton;
    CarAdapter carAdapter;
    View view;
    GoDetails listener;
    List<VehiculoCarro> myListCar;
    List<String> listFilter;
    Context context;
    //@Inject
    CarListPresenteImp carListPresenteImp;
    String model, year, km, locale, disel, trasmition = "";
    int price;

    public CarListFragment() {
        // Required empty public constructor
    }

    public static CarListFragment newInstance(String model, String year, String km, String locale, String disel, String tramition, int price) {
        CarListFragment fragment = new CarListFragment();
        Bundle args = new Bundle();
        args.putString("model", model);
        args.putString("year", year);
        args.putString("km", km);
        args.putString("locale", locale);
        args.putString("disel", disel);
        args.putString("tramition", tramition);
        args.putInt("price", price);


        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cars_list, container, false);
        ButterKnife.bind(this, view);
        context = view.getContext();
        listener.showAjustes();
        model = getArguments().getString("model", "");
        year = getArguments().getString("year", "");
        km = getArguments().getString("km", "");
        trasmition = getArguments().getString("trasmition", "");
        locale = getArguments().getString("locale", "");
        disel = getArguments().getString("disel", "");
        price = getArguments().getInt("price", 0);
        if (!model.equals("")) {
            Log.i(TAG, "onCreateView2: ");
        }
        if (!year.equals("")) {
            Log.i(TAG, "onCreateView3: ");
        }

        Log.i(TAG, "onCreateView: " + model + "-" + year + "-" + price);

        carModelSingleton = VehiculoCarro.getInstance();
        myListCar = new ArrayList<VehiculoCarro>();
        listFilter = new ArrayList<String>();

        /*DaggerCarListComponent.builder()
                .networkComponent(((GarageAplication) getActivity().getApplication()).getNetComponent())
                .carsListModule(new CarsListModule(this))
                .build().inject(this);*/
        progressBar.setVisibility(View.VISIBLE);
        carListPresenteImp.loadListCar();

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        carListPresenteImp.loadListCar();
                    }
                }, 0);
            }
        });
        return view;
    }

    private void initViews() {
        myListCar.clear();
        listFilter.clear();
        rv_car_list.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rv_car_list.setLayoutManager(layoutManager);

    }

    @Override
    public void onResume() {
        super.onResume();
        carListPresenteImp.loadListCar();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        carListPresenteImp.loadListCar();
        reload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showCar(List<VehiculoCarro> posts) {


        carAdapter = new CarAdapter(getContext(), addList(posts), new CarAdapter.CarItemClick() {
            @Override
            public void onBrokerClick(VehiculoCarro clickedBroker) {

                Log.i(TAG, "onBrokerClick: " + clickedBroker.getPhoto1());
                carModelSingleton.setIdPublicacion(clickedBroker.getIdPublicacion());
                carModelSingleton.setUsuarioC(clickedBroker.getUsuarioC());
                carModelSingleton.setTypePostC(clickedBroker.getTypePostC());
                carModelSingleton.setBrandC(clickedBroker.getBrandC());
                carModelSingleton.setModelC(clickedBroker.getModelC());
                carModelSingleton.setYearC(clickedBroker.getYearC());
                carModelSingleton.setMileageC(clickedBroker.getMileageC());
                carModelSingleton.setDescriptionsC(clickedBroker.getDescriptionsC());
                carModelSingleton.setLocationC(clickedBroker.getLocationC());
                carModelSingleton.setPhoto1(clickedBroker.getPhoto1());
                carModelSingleton.setPhoto2(clickedBroker.getPhoto2());
                carModelSingleton.setPhoto3(clickedBroker.getPhoto3());
                carModelSingleton.setPhoto4(clickedBroker.getPhoto4());
                carModelSingleton.setPhoto5(clickedBroker.getPhoto5());
                carModelSingleton.setPhoto6(clickedBroker.getPhoto6());
                carModelSingleton.setPhoto7(clickedBroker.getPhoto7());
                carModelSingleton.setPhoto8(clickedBroker.getPhoto8());
                carModelSingleton.setPhoto9(clickedBroker.getPhoto9());
                carModelSingleton.setPhoto10(clickedBroker.getPhoto10());
                listener.details();
            }
        });
        rv_car_list.setAdapter(carAdapter);
        hideLoading();
    }

    @Override
    public void showError(String message) {

        hideLoading();
        reload.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(),
                getResources().getString(R.string.connection_error),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void showComplete() {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        listFilter.clear();
        myListCar.clear();
    }

    public List<VehiculoCarro> addList(List<VehiculoCarro> vehiculos) {
        initViews();

        Collections.reverse(vehiculos);


        if (!model.equals("")) {
            listFilter.add(model);
            Log.i(TAG, "addList m: ");
        }
        if (!year.equals("")) {
            listFilter.add(year);
            Log.i(TAG, "addList y: ");
        }
        if (!km.equals("")) {
            listFilter.add(km);
            Log.i(TAG, "addList k: ");
        }
        if (!locale.equals("")) {
            listFilter.add(locale);
            Log.i(TAG, "addList l: ");
        }
        if (!disel.equals("")) {
            listFilter.add(disel);
            Log.i(TAG, "addList d: ");
        }
        if (!trasmition.equals("")) {
            listFilter.add(trasmition);
            Log.i(TAG, "addList t: ");
        }

        VehiculoCarro data;
        if (listFilter.size() > 0) {
            Log.i(TAG, "addList sise: " + listFilter.size());

            switch (listFilter.size()) {
                case 1:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }

                    Log.i(TAG, "juleee: " + order(myListCar).size());

                    break;
                case 3:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year) && vehiculos.get(i).getMileageC().equals(km)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }
                    break;

                case 4:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year) && vehiculos.get(i).getMileageC().equals(km) && vehiculos.get(i).getLocationC().equals(locale)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }
                    break;

                case 5:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year) && vehiculos.get(i).getMileageC().equals(km) && vehiculos.get(i).getLocationC().equals(locale) && vehiculos.get(i).getGasC().equals(disel)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }
                    break;

                case 6:
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year) && vehiculos.get(i).getMileageC().equals(km) && vehiculos.get(i).getLocationC().equals(locale) && vehiculos.get(i).getGasC().equals(disel) && vehiculos.get(i).getTransmitionC().equals(trasmition)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }
                    break;

                case 7://Ordenamientp
                    for (int i = 0; i < vehiculos.size(); i++) {
                        if (vehiculos.get(i).getEstatusC().equals("0") && vehiculos.get(i).getBrandC().equals(model) && vehiculos.get(i).getYearC().equals(year) && vehiculos.get(i).getMileageC().equals(km) && vehiculos.get(i).getLocationC().equals(locale) && vehiculos.get(i).getGasC().equals(disel) && vehiculos.get(i).getTransmitionC().equals(trasmition)) {
                            data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                            myListCar.add(data);
                        }
                    }

                    break;
            }
            if (price != 0) {
                Log.i(TAG, "addList: esta activo lo de los precios 2 ");
                order(myListCar);
                return myListCar;
            }
            else
                return myListCar;

        }
         else {
            for (int i = 0; i < vehiculos.size(); i++) {

                if (vehiculos.get(i).getEstatusC().equals("0")) {//es una solicitud en proceso
                    data = new VehiculoCarro(vehiculos.get(i).getIdpostCars(), vehiculos.get(i).getTypePostC(), vehiculos.get(i).getUsuarioC(), vehiculos.get(i).getDatePublicationC(), vehiculos.get(i).getBrandC(), vehiculos.get(i).getModelC(), vehiculos.get(i).getYearC(), vehiculos.get(i).getMileageC(), vehiculos.get(i).getTransmitionC(), vehiculos.get(i).getGasC(), vehiculos.get(i).getColorC(), vehiculos.get(i).getDoorNumberC(), vehiculos.get(i).getDescriptionsC(), vehiculos.get(i).getLocationC(), vehiculos.get(i).getPriceC(), vehiculos.get(i).getPriceCommissionC(), vehiculos.get(i).getEstatusC(), vehiculos.get(i).getIdPublicacion(), vehiculos.get(i).getPhoto1(), vehiculos.get(i).getPhoto2(), vehiculos.get(i).getPhoto3(), vehiculos.get(i).getPhoto4(), vehiculos.get(i).getPhoto5(), vehiculos.get(i).getPhoto6(), vehiculos.get(i).getPhoto7(), vehiculos.get(i).getPhoto8(), vehiculos.get(i).getPhoto9(), vehiculos.get(i).getPhoto10());
                    myListCar.add(data);
                }
            }
            if (price != 0) {
                Log.i(TAG, "addList: esta activo lo de los precios ");
                order(myListCar);
                return myListCar;
            }
            else
            return myListCar;
        }
    }

    public List<VehiculoCarro> order(List<VehiculoCarro> myListCar) {
        Collections.sort(myListCar, new Comparator<VehiculoCarro>() {
            @Override
            public int compare(VehiculoCarro vehiculoCarro, VehiculoCarro t1) {
                if (price == 1) {
                    return vehiculoCarro.getPriceC().compareTo(valueOf(Integer.valueOf(t1.getPriceC())));
                } else
                    //menor a mayor
                    return Integer.valueOf(t1.getPriceC()).compareTo(Integer.valueOf(vehiculoCarro.getPriceC()));
            }
        });
        return myListCar;
    }
}
