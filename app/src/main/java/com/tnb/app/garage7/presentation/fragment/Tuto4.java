package com.tnb.app.garage7.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tnb.app.garage7.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Tuto4 extends Fragment{
    @BindView(R.id.iv_tuto)
    ImageView iv_tuto;
    private int typeCar = 0;


    public Tuto4() {
        // Required empty public constructor
    }

    public static Tuto4 newInstance(int  type_publication) {
        Tuto4 fragment = new Tuto4();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tuto1, container, false);
        ButterKnife.bind(this, view);
        typeCar = getArguments().getInt("type", 0);

        if(typeCar == 0) {
            iv_tuto.setBackground(getResources().getDrawable(R.drawable.tutorial4));
        }
        else {
         //   iv_tuto.setBackground(getResources().getDrawable(R.drawable.vista_kilometraje));
        }

        return view;
    }

}
