package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 7/2/2018.
 */

public class RegisterResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("iduser")
    @Expose
    public IduserRegister iduser;
    @SerializedName("idconse")
    @Expose
    public String idconse;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public IduserRegister getIduser() {
        return iduser;
    }

    public String getIdconse() {
        return idconse;
    }
}
