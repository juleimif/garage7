package com.tnb.app.garage7.injector.module;

import com.tnb.app.garage7.common.country.CountryDialogFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract CountryDialogFragment contributeCountryDialogFragment();


}