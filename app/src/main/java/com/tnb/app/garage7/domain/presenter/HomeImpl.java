package com.tnb.app.garage7.domain.presenter;

import android.util.Log;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ImagesHomeResponse;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Publicaciones;
import com.tnb.app.garage7.data.view.HomeView;
import com.tnb.app.garage7.data.rest.ConnectClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public class HomeImpl implements HomePresenter {
    public static final String TAG = HomeImpl.class.getSimpleName();
    public HomeView homeView;

    public HomeImpl(HomeView homeView){
        this.homeView = homeView;
    }

    private void respuestaServicio(String message) {

    }

    @Override
    public void getImages() {

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getHomeImages().enqueue(new Callback<ImagesHomeResponse>() {
            @Override
            public void onResponse(Call<ImagesHomeResponse> call, Response<ImagesHomeResponse> response) {
                if(response.code()==200){
                    if(response.body().getData().size()>0){
                        homeView.showImages(response.body().getData());
                        Log.i(TAG, "onResponse: " + response.body().getData().size() );
                    }
                    else
                    {
                        homeView.showRelod();

                    }
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ImagesHomeResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                homeView.hideProgress();
                homeView.showRelod();

            }
        });
    }

    @Override
    public void getDestacadas() {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.getImagesHome().enqueue(new Callback<Publicaciones>() {
            @Override
            public void onResponse(Call<Publicaciones> call, Response<Publicaciones> response) {
                if(response.code()==200){
                    if(response.body().getData().getVehiculos().size()>0){
                        homeView.showDestacadas(response.body().getData().getVehiculos());
                    }
                    else
                    {
                        homeView.showRelod();

                    }
                    Log.i(TAG, "onResponse: " + response.body().getData().getVehiculos().toString());
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<Publicaciones> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                homeView.hideProgress();
                homeView.showRelod();
                Toast.makeText(homeView.getContext(),
                        homeView.getContext().getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();
            }
        });


    }
}
