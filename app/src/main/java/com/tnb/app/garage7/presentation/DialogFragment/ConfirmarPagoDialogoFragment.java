package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.tnb.app.garage7.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class ConfirmarPagoDialogoFragment extends DialogFragment {
    public static final String TAG = ConfirmarPagoDialogoFragment.class.getSimpleName();
    String[] BANCOS = {"Banco 1", "Bnaco 2", "Banco 3"};
    @BindView(R.id.sp_banco)
    MaterialBetterSpinner sp_banco;
    private String banco;

    public interface pagoDialogListener {
        void onFinishPayDialog();
    }

    public static ConfirmarPagoDialogoFragment newInstance(String personName, String personEmail, String base64Img) {
        ConfirmarPagoDialogoFragment f = new ConfirmarPagoDialogoFragment();
        Bundle args = new Bundle();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_registrar_pago);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, BANCOS);
        sp_banco.setAdapter(arrayAdapter2);

        sp_banco.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                banco = parent.getItemAtPosition(position).toString();
            }
        });

        return dialogo;
    }

    @OnClick({R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                showAlert("¿ Seguro que desea registrar este pago ?", "Si");
                break;
        }
    }

    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        ConfirmarPagoDialogoFragment.pagoDialogListener listener = (ConfirmarPagoDialogoFragment.pagoDialogListener) getTargetFragment();
        listener.onFinishPayDialog();
        dismiss();
    }

    private void showAlert(String message, String tvConfirm) {

        new AlertDialog.Builder((getActivity()))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(tvConfirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sendBackResult();
                    }
                })
                .setNegativeButton(this.getString(R.string.declinar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }



}
