package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luisana Abache on 16/07/2017.
 */

public class MotoResponse {

    @SerializedName("idpublicaciones_motos")
    @Expose
    public String idpublicacionesMotos;
    @SerializedName("type_publication")
    @Expose
    public String typePublication;
    @SerializedName("usuario")
    @Expose
    public String usuario;
    @SerializedName("date_publication")
    @Expose
    public String datePublication;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("cilindradas")
    @Expose
    public String cilindradas;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("mileage")
    @Expose
    public String mileage;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("descriptions_m")
    @Expose
    public String descriptionsM;
    @SerializedName("price_commission")
    @Expose
    public String priceCommission;
    @SerializedName("estatus")
    @Expose
    public String estatus;
    @SerializedName("id_publicacion")
    @Expose
    public String idPublicacion;
    @SerializedName("photo1")
    @Expose
    public String photo1;
    @SerializedName("photo2")
    @Expose
    public String photo2;
    @SerializedName("photo3")
    @Expose
    public String photo3;
    @SerializedName("photo4")
    @Expose
    public String photo4;
    @SerializedName("photo5")
    @Expose
    public String photo5;
    @SerializedName("photo6")
    @Expose
    public String photo6;
    @SerializedName("photo7")
    @Expose
    public String photo7;
    @SerializedName("photo8")
    @Expose
    public String photo8;
    @SerializedName("photo9")
    @Expose
    public String photo9;
    @SerializedName("photo10")
    @Expose
    public String photo10;

    public String getIdpublicacionesMotos() {
        return idpublicacionesMotos;
    }

    public void setIdpublicacionesMotos(String idpublicacionesMotos) {
        this.idpublicacionesMotos = idpublicacionesMotos;
    }

    public String getTypePublication() {
        return typePublication;
    }

    public void setTypePublication(String typePublication) {
        this.typePublication = typePublication;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCilindradas() {
        return cilindradas;
    }

    public void setCilindradas(String cilindradas) {
        this.cilindradas = cilindradas;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescriptionsM() {
        return descriptionsM;
    }

    public void setDescriptionsM(String descriptionsM) {
        this.descriptionsM = descriptionsM;
    }

    public String getPriceCommission() {
        return priceCommission;
    }

    public void setPriceCommission(String priceCommission) {
        this.priceCommission = priceCommission;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(String idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public void setPhoto6(String photo6) {
        this.photo6 = photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public void setPhoto7(String photo7) {
        this.photo7 = photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public void setPhoto8(String photo8) {
        this.photo8 = photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    public void setPhoto9(String photo9) {
        this.photo9 = photo9;
    }

    public String getPhoto10() {
        return photo10;
    }

    public void setPhoto10(String photo10) {
        this.photo10 = photo10;
    }
}
