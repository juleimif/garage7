package com.tnb.app.garage7.presentation.activity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.Adapter.TutorialSlidePagerAdapter;

import butterknife.BindView;
import butterknife.OnClick;

public class TutorialActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    public static final String TAG = TutorialActivity.class.getSimpleName();

    @BindView(R.id.usage_stateprogressbar)
    StateProgressBar stateprogressbar;
    @BindView(R.id.viewPagerOpciones)
    ViewPager pager;
    public int type ;

    @Override
    public int getLayout() {
        return R.layout.activity_tutorial;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        type = getIntent().getExtras().getInt("type");
        Log.i(TAG, "type: " + type);

        pager.setAdapter(new TutorialSlidePagerAdapter(getSupportFragmentManager(), type));
        pager.setOnPageChangeListener(this);
        stateprogressbar.enableAnimationToCurrentState(false);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                break;
            case 1:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                break;
            case 2:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                break;
            case 3:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                break;
            case 4:
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FIVE);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick({R.id.iv_exit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_exit:
                finish();
                break;
        }
    }
}
