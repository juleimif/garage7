package com.tnb.app.garage7.presentation.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.tnb.app.garage7.presentation.fragment.Tuto1;
import com.tnb.app.garage7.presentation.fragment.Tuto2;
import com.tnb.app.garage7.presentation.fragment.Tuto3;
import com.tnb.app.garage7.presentation.fragment.Tuto4;
import com.tnb.app.garage7.presentation.fragment.Tuto5;

/**
 * Created by Luisana Abache on 09/07/2017.
 */

public class TutorialSlidePagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_PAGES = 5;
    public int type;

    public TutorialSlidePagerAdapter(FragmentManager fm, int type) {
        super(fm);
        this.type = type;
        Log.i("jule", "TutorialSlidePagerAdapter: ");
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Tuto1().newInstance(type);
            case 1:
                return new Tuto2().newInstance(type);
            case 2:
                return new Tuto3().newInstance(type);
            case 3:
                return new Tuto4().newInstance(type);
            case 4:
                return new Tuto5().newInstance(type);
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
