package com.tnb.app.garage7.presentation.activity;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseActivity {
    @BindView(R.id.input_nombre)
    EditText input_nombre;
    @BindView(R.id.input_correo)
    EditText input_correo;
    @BindView(R.id.iv_profile)
    CircleImageView iv_profile;
    @BindView(R.id.tv_name)
    TextView tv_name;
    PreferencesSessionManager preferencesSessionManager;
    String[] GENERO = {"Femenino", "Masculino"};
    private String genero;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    public int getLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        preferencesSessionManager = new PreferencesSessionManager(this);
        input_nombre.setText(preferencesSessionManager.getUsereName());
        input_correo.setText(preferencesSessionManager.getUsereEmail());
        tv_name.setText(preferencesSessionManager.getUsereEmail());
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(preferencesSessionManager.getUsereEmail());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Glide.with(this)
                .load(preferencesSessionManager.getUsereAvatar())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(iv_profile);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, GENERO);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.sp_sexo);
        materialDesignSpinner.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                genero = adapterView.getItemAtPosition(i).toString();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //do whatever
                onBackPressed();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @OnClick(R.id.iv_salir)
//    public void salir(View view) {
//        onBackPressed();
//        overridePendingTransition(R.anim.right_out, R.anim.right_in);
//
//    }
}
