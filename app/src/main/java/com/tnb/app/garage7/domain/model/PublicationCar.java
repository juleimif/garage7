package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class PublicationCar {

    public String idpublicaciones_carros;
    public String type_publication_c;
    public String usuario_c;
    public String date_publication_c;
    public String brand_c;
    public String model_c;
    public String year_c;
    public String mileage_c;
    public String transmition_c;
    public String color_c;
    public String door_number_c;
    public String descriptions_c;
    public String location_c;
    public String price_c;
    public String price_commission_c;
    public String estatus_c;
    public String photos_c;

    public String getIdpublicaciones_carros() {
        return idpublicaciones_carros;
    }

    public void setIdpublicaciones_carros(String idpublicaciones_carros) {
        this.idpublicaciones_carros = idpublicaciones_carros;
    }

    public String getType_publication_c() {
        return type_publication_c;
    }

    public void setType_publication_c(String type_publication_c) {
        this.type_publication_c = type_publication_c;
    }

    public String getUsuario_c() {
        return usuario_c;
    }

    public void setUsuario_c(String usuario_c) {
        this.usuario_c = usuario_c;
    }

    public String getDate_publication_c() {
        return date_publication_c;
    }

    public void setDate_publication_c(String date_publication_c) {
        this.date_publication_c = date_publication_c;
    }

    public String getBrand_c() {
        return brand_c;
    }

    public void setBrand_c(String brand_c) {
        this.brand_c = brand_c;
    }

    public String getModel_c() {
        return model_c;
    }

    public void setModel_c(String model_c) {
        this.model_c = model_c;
    }

    public String getYear_c() {
        return year_c;
    }

    public void setYear_c(String year_c) {
        this.year_c = year_c;
    }

    public String getMileage_c() {
        return mileage_c;
    }

    public void setMileage_c(String mileage_c) {
        this.mileage_c = mileage_c;
    }

    public String getTransmition_c() {
        return transmition_c;
    }

    public void setTransmition_c(String transmition_c) {
        this.transmition_c = transmition_c;
    }

    public String getColor_c() {
        return color_c;
    }

    public void setColor_c(String color_c) {
        this.color_c = color_c;
    }

    public String getDoor_number_c() {
        return door_number_c;
    }

    public void setDoor_number_c(String door_number_c) {
        this.door_number_c = door_number_c;
    }

    public String getDescriptions_c() {
        return descriptions_c;
    }

    public void setDescriptions_c(String descriptions_c) {
        this.descriptions_c = descriptions_c;
    }

    public String getLocation_c() {
        return location_c;
    }

    public void setLocation_c(String location_c) {
        this.location_c = location_c;
    }

    public String getPrice_c() {
        return price_c;
    }

    public void setPrice_c(String price_c) {
        this.price_c = price_c;
    }

    public String getPrice_commission_c() {
        return price_commission_c;
    }

    public void setPrice_commission_c(String price_commission_c) {
        this.price_commission_c = price_commission_c;
    }

    public String getEstatus_c() {
        return estatus_c;
    }

    public void setEstatus_c(String estatus_c) {
        this.estatus_c = estatus_c;
    }

    public String getPhotos_c() {
        return photos_c;
    }

    public void setPhotos_c(String photos_c) {
        this.photos_c = photos_c;
    }

    public PublicationCar(String idpublicaciones_carros, String type_publication_c, String usuario_c, String date_publication_c, String brand_c, String model_c, String year_c, String mileage_c, String transmition_c, String color_c, String door_number_c, String descriptions_c, String location_c, String price_c, String price_commission_c, String estatus_c, String photos_c) {
        this.idpublicaciones_carros = idpublicaciones_carros;
        this.type_publication_c = type_publication_c;
        this.usuario_c = usuario_c;
        this.date_publication_c = date_publication_c;
        this.brand_c = brand_c;
        this.model_c = model_c;
        this.year_c = year_c;
        this.mileage_c = mileage_c;
        this.transmition_c = transmition_c;
        this.color_c = color_c;
        this.door_number_c = door_number_c;
        this.descriptions_c = descriptions_c;
        this.location_c = location_c;
        this.price_c = price_c;
        this.price_commission_c = price_commission_c;
        this.estatus_c = estatus_c;
        this.photos_c = photos_c;


    }
}

