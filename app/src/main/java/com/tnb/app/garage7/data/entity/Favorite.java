package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

@Entity(indices = {@Index("id")},
        primaryKeys = {"id"})
public class Favorite {

    @NonNull
    public final String id;

    @NonNull
    public final String userId;

    @NonNull
    public final String favoriteId;

    @NonNull
    public final String type;

    public Favorite(String id, String userId, String favoriteId, String type) {
        this.id = id;
        this.userId = userId;
        this.favoriteId = favoriteId;
        this.type = type;
    }
}
