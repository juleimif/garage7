package com.tnb.app.garage7.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tnb.app.garage7.data.entity.Banner;
import com.tnb.app.garage7.data.entity.Brand;

import java.util.List;

@Dao
public interface BannerDAO {

    @Query("SELECT * FROM Banner")
    LiveData<List<Banner>> findAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Brand... brand);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertList(List<Banner> bannerList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long createIfNotExists(Banner banner);

    @Query("DELETE FROM Banner")
    void deleteAll();
}
