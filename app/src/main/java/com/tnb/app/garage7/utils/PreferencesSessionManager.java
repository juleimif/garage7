package com.tnb.app.garage7.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Juleimis on 23/06/2017.
 */

public class PreferencesSessionManager {
    private final String TAG = PreferencesSessionManager.class.getSimpleName();

    private static final String KEY_USER_ID = "USER_ID";
    private static final String KEY_USER_EMAIL = "USER_EMAIL";
    private static final String KEY_USER_NAME = "USER_NAME";
    private static final String KEY_USER_AVATAR = "USER_AVATAR";
    private static final String KEY_SERVER_TOKEN = "SERVER_TOKEN";
    private static final String KEY_USER_TYPE = "USER_TYPE";
    private static final String KEY_USER_COUNTRY = "USER_COUNTRY";
    private static final String KEY_TUTO_MOTOCYCLE = "KEY_TUTO_MOTOCYCLE";
    private static final String KEY_TUTO_CAR = "KEY_TUTO_CAR";
    private static final String KEY_CONCESIONARIO = "KEY_CONCESIONARIO";
    private static final String KEY_USER_ID_CONCESIONARIO = "USER_ID_CONCESIONARIO";



    private static final String PREFER_NAME = "ValidarNotificacionesPush";

    public SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Constructor
    public PreferencesSessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void clearPreferences(){
        setUserId("");
        setServerToken("");
        setUserType("");
        setUserCountry("");
        setUserAvatar("");
        setUserIdConcesionario("");


    }

    public String getUserConce() {return pref.getString(KEY_CONCESIONARIO, "");}
    public void setUserConce(String userType){
        editor.putString(KEY_CONCESIONARIO, userType);
        editor.apply();
        editor.commit();
    }

    public int getUserTutoCar() {return pref.getInt(KEY_TUTO_CAR, 0);}
    public void setUserTutoCar(int userType){
        editor.putInt(KEY_TUTO_CAR, userType);
        editor.apply();
        editor.commit();
    }

    public int getUserTutoMoto() {return pref.getInt(KEY_TUTO_MOTOCYCLE, 0);}
    public void setUserTutoMoto(int userType){
        editor.putInt(KEY_TUTO_MOTOCYCLE, userType);
        editor.apply();
        editor.commit();
    }


    public String getUserType() {return pref.getString(KEY_USER_TYPE, "null");}
    public void setUserType(String userType){
        editor.putString(KEY_USER_TYPE, userType);
        editor.apply();
        editor.commit();
    }

    public String getUserId() {return pref.getString(KEY_USER_ID, "null");}
    public void setUserId(String userId){
        editor.putString(KEY_USER_ID, userId);
        editor.apply();
        editor.commit();
    }

    public String getUsereEmail() {return pref.getString(KEY_USER_EMAIL, "null");}
    public void setUserEmail(String userEmail){
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.apply();
        editor.commit();
    }

    public String getUsereName() {return pref.getString(KEY_USER_NAME, "null");}
    public void setUserName(String userName){
        editor.putString(KEY_USER_NAME, userName);
        editor.apply();
        editor.commit();
    }

    public String getUsereAvatar() {return pref.getString(KEY_USER_AVATAR, "null");}
    public void setUserAvatar(String userAvatar){
        editor.putString(KEY_USER_AVATAR, userAvatar);
        editor.apply();
        editor.commit();
    }
    public String getServerToken() {return pref.getString(KEY_SERVER_TOKEN, "null");}
    public void setServerToken(String token){
        editor.putString(KEY_SERVER_TOKEN, token);
        editor.apply();
        editor.commit();
    }

    public String getUserCountry() {return pref.getString(KEY_USER_COUNTRY, "null");}
    public void setUserCountry(String country){
        editor.putString(KEY_USER_COUNTRY, country);
        editor.apply();
        editor.commit();
    }

    public String getUserConcesionario() {return pref.getString(KEY_USER_ID_CONCESIONARIO, "");}
    public void setUserIdConcesionario(String idConcesionario){
        editor.putString(KEY_USER_ID_CONCESIONARIO, idConcesionario);
        editor.apply();
        editor.commit();
    }








}
