package com.tnb.app.garage7.domain.model;

/**
 * Created by Juleimis on 23/06/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseUser {

    @SerializedName("idusuarios")
    @Expose
    private Integer idusuarios;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("N_indetification")
    @Expose
    private String nIndetification;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("sex")
    @Expose
    private String sex;
    private String avatar;
    private String urlAvatar;


    private int internet_status ;
    private int login_status;
    private ResponseUser user;
    private static ResponseUser mInstance = null;

    private ResponseUser() {
        internet_status = Constants.ONLINE;
    }

    public static ResponseUser getInstance(){
        if(mInstance == null)
        {
            mInstance = new ResponseUser();
        }
        return mInstance;
    }

    public void seturlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String geturlAvatar() {
        return urlAvatar;
    }

    public void setAvatarURL(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarURL() {
        return avatar;
    }


    public Integer getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(Integer idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNIndetification() {
        return nIndetification;
    }

    public void setNIndetification(String nIndetification) {
        this.nIndetification = nIndetification;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "ResponseUser{" +
                "idusuarios=" + idusuarios +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", address='" + address + '\'' +
                ", nIndetification='" + nIndetification + '\'' +
                ", phone='" + phone + '\'' +
                ", status='" + status + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", sex='" + sex + '\'' +
                ", internet_status=" + internet_status +
                ", login_status=" + login_status +
                ", user=" + user +
                '}';
    }

}


