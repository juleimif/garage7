package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.modelMoto.VehiculoMoto;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MotoAdapter extends RecyclerView.Adapter<MotoAdapter.ViewHolder> {

    private CarItemClick mItemClickListener;
    private List<VehiculoMoto> motoList;
    private Context contexto;

    public MotoAdapter(Context applicationContext, List<VehiculoMoto> car, CarItemClick brokerItemClick) {
        this.motoList =  car;
        this.contexto = applicationContext;
        mItemClickListener = (CarItemClick) brokerItemClick;
    }

    @Override
    public MotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_motos, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VehiculoMoto currentItem = motoList.get(position);
        MotoAdapter.ViewHolder viewHolder = (MotoAdapter.ViewHolder) holder;
        Log.i("jule", "onBindViewHolder: " + currentItem.toString());
        viewHolder.rl_contenedor.setVisibility(View.VISIBLE);
        viewHolder.tv_name.setText(currentItem.getBrand() + " - " + currentItem.getLocation());
        viewHolder.tv_desc.setText(currentItem.cilindradas);

        Glide.with(contexto)
                .load(currentItem.photo1)
                .into(viewHolder.iv_car);
    }

    @Override
    public int getItemCount() {
        return motoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_desc) TextView tv_desc;
        @BindView(R.id.rl_contenedor) RelativeLayout rl_contenedor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(motoList.get(position));
        }
    }

    public interface CarItemClick {
        void onBrokerClick(VehiculoMoto clickedBroker);
    }
}
