package com.tnb.app.garage7.domain.presenter;

import com.tnb.app.garage7.domain.model.User;

/**
 * Created by juleimis hostienda on 29/05/2017.
 */
public interface CreateAccountPresenter {
    void CreateAccount(User user);

    void searchCity(String ciudad, String pais);
}
