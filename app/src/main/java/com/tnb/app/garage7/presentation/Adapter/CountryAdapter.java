package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    private CarItemClick mItemClickListener;
    private  List<VehiculoCarro> carList;
    private int temp;
    private Context contexto;

    public CountryAdapter(Context applicationContext, List<VehiculoCarro> car, CarItemClick brokerItemClick) {
        Log.i("jule", "CarAdapter: " + car);
        this.carList =  car;
        this.contexto = applicationContext;
        mItemClickListener = (CarItemClick) brokerItemClick;
    }


    @Override
    public CountryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_pais, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {

        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.tv_name_pis) TextView tv_name_pis;
        @BindView(R.id.iv_pais) ImageView iv_pais;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            // mItemClickListener.onBrokerClick(carList.get(position));
        }

    }

    public interface CarItemClick {
        void onBrokerClick(VehiculoCarro clickedBroker);
    }


}
