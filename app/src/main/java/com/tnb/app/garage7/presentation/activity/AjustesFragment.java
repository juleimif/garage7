package com.tnb.app.garage7.presentation.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.fragment.PublicacionesActivasFragment;

import butterknife.ButterKnife;


public class AjustesFragment extends Fragment {
    public static final String TAG = PublicacionesActivasFragment.class.getSimpleName();


    public AjustesFragment() {
        // Required empty public constructor
    }

    public static PublicacionesActivasFragment newInstance(String param1, String param2) {
        PublicacionesActivasFragment fragment = new PublicacionesActivasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ajustes, container, false);
        ButterKnife.bind(this, view);

        return view;
    }
}
