package com.tnb.app.garage7.splash;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.vo.Resource;
import com.tnb.app.garage7.main.MainActivity;
import com.tnb.app.garage7.welcome.WelcomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

import static com.tnb.app.garage7.login.LoginActivity.USER_EMAIL;

public class SplashActivity
        extends AppCompatActivity{

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @BindView(R.id.progressBar)
    ProgressBar simpleProgressBar;

    private int progress = 0;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        SplashViewModel viewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel.class);
        viewModel.getUser().observe(this, this::handleResponse);
    }

    private void handleResponse(Resource<User> resource) {
        if (resource != null) {
            switch (resource.status) {
                case SUCCESS:
                    checkUserExists(resource);
                    break;
            }
        }
    }

    private void checkUserExists(Resource<User> resource) {
        if (resource.data != null) {
            startMainActivity(resource.data.getEmail());
        } else {
            setProgress();
        }
    }

    private void startMainActivity(String email) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(USER_EMAIL, email);
        startNextActivity(intent);
    }

    private void setProgress() {
        new Thread(() -> {
            while (progress < 100) {
                progress += 10;
                handler.post(() -> simpleProgressBar.setProgress(progress));
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            startWelcomeActivity();
        }).start();
    }

    private void startWelcomeActivity() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startNextActivity(intent);
    }

    private void startNextActivity(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
        startActivity(intent);
        overridePendingTransition(R.anim.left_out, R.anim.left_in);
        finish();
    }
}