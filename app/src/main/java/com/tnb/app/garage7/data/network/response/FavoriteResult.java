package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Favorite;

public class FavoriteResult {

    @SerializedName(value = "data")
    private Favorite favorite;

    public Favorite favorite() {
        return favorite;
    }
}
