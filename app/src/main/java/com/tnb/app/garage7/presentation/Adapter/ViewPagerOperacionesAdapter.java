package com.tnb.app.garage7.presentation.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tnb.app.garage7.presentation.fragment.HistorialFragment;
import com.tnb.app.garage7.presentation.fragment.ProcesosFragment;
import com.tnb.app.garage7.presentation.fragment.SolicitudesFragment;

/**
 * Created by Juleimis on 30/06/2017.
 */

public class ViewPagerOperacionesAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[];
    public ViewPagerOperacionesAdapter(FragmentManager fm, String[] tabTitles) {
        super(fm);
        this.tabTitles = tabTitles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SolicitudesFragment();
            case 1:
                return new ProcesosFragment();
            case 2:
                return new HistorialFragment();

        }
        return null;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }



}
