package com.tnb.app.garage7.presentation.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.NextInterface;
import com.tnb.app.garage7.domain.model.Car;
import com.tnb.app.garage7.domain.model.Constants;
import com.tnb.app.garage7.presentation.fragment.BaseFragment;
import com.tnb.app.garage7.utils.PermissionManager;
import com.tnb.app.garage7.utils.PermissionTracker;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Oleksii Shliama (https://github.com/shliama).
 */
public class AddImagesFragment extends BaseFragment implements PermissionTracker {

    private static final String TAG = "AddImagesFragment";

    private static final int REQUEST_SELECT_PICTURE = 0x01;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "Garage7";
    View view;
    @BindView(R.id.iv1)
    ImageView iv1;
    @BindView(R.id.iv2)
    ImageView iv2;
    @BindView(R.id.iv3)
    ImageView iv3;
    @BindView(R.id.iv4)
    ImageView iv4;
    @BindView(R.id.iv5)
    ImageView iv5;
    @BindView(R.id.iv6)
    ImageView iv6;
    @BindView(R.id.iv7)
    ImageView iv7;
    @BindView(R.id.iv8)
    ImageView iv8;
    @BindView(R.id.iv9)
    ImageView iv9;
    @BindView(R.id.iv10)
    ImageView iv10;
    @BindView(R.id.tv_number)
    TextView tvNumber;

    public int pos, contImages = 0;
    public ArrayList<String> fotos;
    public int contPhotos, type_publication, limitPhotos;
    public String imagesList;
    private Car userModelSingleton;

    NextInterface listener;
    private PreferencesSessionManager preferencesSessionManager;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_add_images;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        this.view = view;
        fotos = new ArrayList<>();
        preferencesSessionManager = new PreferencesSessionManager(getContext());
        setType();
        Log.i(TAG, "onViewReady: ");

    }

    private void setType() {
        type_publication = getArguments().getInt("type", 0);
    }

    public static AddImagesFragment newInstance(int type_publication) {
        AddImagesFragment fragment = new AddImagesFragment();
        Bundle args = new Bundle();
        args.putInt("type", type_publication);
        fragment.setArguments(args);
        return fragment;
    }


    @OnClick(R.id.fa_next)
    public void Next(View view) {

        Random random = new Random();
        int minSizePixels = 800;
        int maxSizePixels = 2400;
        startCropActivity(Uri.parse(String.format(Locale.getDefault(), "https://unsplash.it/%d/%d/?random",
                minSizePixels + random.nextInt(maxSizePixels - minSizePixels),
                minSizePixels + random.nextInt(maxSizePixels - minSizePixels))));

        Log.i(TAG, "Next: " + Uri.parse(String.format(Locale.getDefault(), "https://unsplash.it/%d/%d/?random",
                minSizePixels + random.nextInt(maxSizePixels - minSizePixels),
                minSizePixels + random.nextInt(maxSizePixels - minSizePixels))));

        Log.i(TAG, "Next: " + Locale.getDefault());
        if (fotos.size() >= 10) {
            formato();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            Resources res = getResources();
            String title = res.getString(R.string.alert_limit_message);
            String message = res.getString(R.string.maximo_msj);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @OnClick(R.id.btn_tuto)
    public void getTuto(View view) {
        preferencesSessionManager.setUserTutoMoto(1);
        preferencesSessionManager.setUserTutoCar(1);
        getTuto();

    }

    public void getTuto() {
        Intent intent = new Intent(getActivity(), TutorialActivity.class);
        intent.putExtra("type", type_publication);
        getActivity().startActivity(intent);
    }


    public void formato() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < fotos.size(); i++) {
            sb.append(fotos.get(i));
            if (i < fotos.size() - 1) {
                sb.append(",");
            }
        }
        sb.append("]").toString();
        userModelSingleton = Car.getInstance();
        userModelSingleton.setPhotos(sb.toString());
        listener.next(3);
        Log.i(TAG, "formato: " + sb.toString());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: " + requestCode);
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    Log.i(TAG, "onActivityResult startCropActivity: ");
                    startCropActivity(data.getData());
                } else {
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                Log.i(TAG, "onActivityResult handleCropResult: ");

                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        pickFromGallery();

//        switch (requestCode) {
//            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                }
//                break;
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
    }

    @SuppressWarnings("ConstantConditions")
    private void setupUI() {
        pickFromGallery();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (NextInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }


    private void pickFromGallery() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
//                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
//                    getString(R.string.permission_read_storage_rationale),
//                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
//        } else {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "label_select_picture"), REQUEST_SELECT_PICTURE);
//        }
    }


    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;

        UCrop.Options options = new UCrop.Options();
        options.setFreeStyleCropEnabled(false);
        options.setToolbarTitle("Editar imagen");
        options.setToolbarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(getContext(), R.color.color_toolbar));

        UCrop.of(uri, Uri.fromFile(new File(getContext().getCacheDir(), destinationFileName + "image" + pos)))
                .withAspectRatio(16, 9)
                .withOptions(options)
                .start(getActivity(), AddImagesFragment.this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
        try {
            trimCache(getContext());
            // Toast.makeText(this,"onDestroy " ,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }

    public void convertToBase64(String path) {
        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        //add imageBase64 to list
        fotos.add(encodedImage);
        Log.i(TAG, "convertToBase64: " + fotos.size() + " " + fotos.get(0));
    }

    @OnClick({R.id.iv1, R.id.iv2, R.id.iv3, R.id.iv4, R.id.iv5, R.id.iv6, R.id.iv7, R.id.iv8, R.id.iv9, R.id.iv10})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv1:
                pos = 0;
                pickFromGallery();

                break;
            case R.id.iv2:
                pos = 1;
                pickFromGallery();

                break;

            case R.id.iv3:
                pos = 2;
                pickFromGallery();

                break;

            case R.id.iv4:
                pos = 3;
                pickFromGallery();

                break;

            case R.id.iv5:
                pos = 4;
                pickFromGallery();

                break;

            case R.id.iv6:
                pos = 5;
                pickFromGallery();

                break;

            case R.id.iv7:
                pos = 6;
                pickFromGallery();

                break;

            case R.id.iv8:
                pos = 7;
                pickFromGallery();

                break;

            case R.id.iv9:
                pos = 8;
                pickFromGallery();

                break;

            case R.id.iv10:
                pos = 9;
                pickFromGallery();


                break;
        }
    }


    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        Log.i(TAG, "handleCropResult: " + resultUri.getPath() + "pos: " + pos);

        if (resultUri != null) {
            tvNumber.setText(fotos.size() + " de 10");
            switch (pos) {
                case 0:
                    Log.i(TAG, "handleCropResult 0: ");
                    convertToBase64(resultUri.getPath());
                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv1);
                    break;
                case 1:
                    Log.i(TAG, "handleCropResult 1: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv2);
                    break;
                case 2:
                    Log.i(TAG, "handleCropResult 2: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))

                            .into(iv3);
                    break;
                case 3:
                    Log.i(TAG, "handleCropResult 3: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))

                            .into(iv4);
                    break;
                case 4:
                    Log.i(TAG, "handleCropResult 4: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv5);
                    break;
                case 5:
                    Log.i(TAG, "handleCropResult 5: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv6);
                    break;
                case 6:
                    Log.i(TAG, "handleCropResult 6: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))

                            .into(iv7);
                    break;
                case 7:
                    Log.i(TAG, "handleCropResult 7: ");
                    convertToBase64(resultUri.getPath());

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv8);
                case 8:
                    Log.i(TAG, "handleCropResult 8: ");

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv9);
                case 9:
                    Log.i(TAG, "handleCropResult 9: ");

                    Glide.with(getContext())
                            .load(new File(resultUri.getPath()))
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.no_photo)
                                    .override(200, 150))
                            .into(iv10);
                    break;

            }
        } else {
            Toast.makeText(getContext(), "cannot_retrieve_cropped_image", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
        } else {
            Log.e(TAG, "handleCropError: ", cropError);
        }
    }


    @Override
    public void verifyPermission(String permission) {
        if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Crashlytics.log(Log.DEBUG,TAG, "--->Device is upper for external than 6.0");
                if (PermissionManager.getInstance().checkPermission(getActivity(), permission)) {
                    setupUI();
                } else {
                    this.requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            } else {
                setupUI();
            }
        }
    }

    @Override
    public void onResult(boolean result, int MY_PERMISSIONS_REQUEST) {

        if (MY_PERMISSIONS_REQUEST == Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {

            if (result) {
                setupUI();

            } else {
                String message = "Necesita permiso";
                new AlertDialog.Builder((getActivity()))
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                    dialog.cancel();
                                    PermissionManager permissionmanager = PermissionManager.getInstance();
                                    permissionmanager.dialogToSetting("necesitas ir a las configuraciones");
                                } else {
                                    verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                                }
                            }
                        })
                        .setNegativeButton(this.getString(R.string.exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                            }
                        })
                        .create()
                        .show();
            }
        }

    }

    @Override
    public void checkIfpermissionChanged() {

    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            Log.i(TAG, "setUserVisibleHint 1: ");
            verifyPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            //Moto
            if (type_publication == 0) {
                if (preferencesSessionManager.getUserTutoCar() == 0) {
                    preferencesSessionManager.setUserTutoCar(1);
                    getTuto();
                }
            } else {
                if (preferencesSessionManager.getUserTutoMoto() == 0) {
                    preferencesSessionManager.setUserTutoMoto(1);
                    getTuto();
                }
            }

        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
            Log.i(TAG, "setUserVisibleHint 2: ");

        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
            Log.i(TAG, "setUserVisibleHint 3: ");
        }
    }


}