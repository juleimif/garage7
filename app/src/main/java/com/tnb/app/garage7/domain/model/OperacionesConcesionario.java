package com.tnb.app.garage7.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class OperacionesConcesionario {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public DataOperacionesConcesionario data;

    public String getStatus() {
        return status;
    }

    public DataOperacionesConcesionario getData() {
        return data;
    }
}

