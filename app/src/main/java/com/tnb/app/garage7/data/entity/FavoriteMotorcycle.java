package com.tnb.app.garage7.data.entity;

import com.google.gson.annotations.SerializedName;

public class FavoriteMotorcycle extends Favorite {

    @SerializedName(value = "favorite")
    private Motorcycle motorcycle;

    public FavoriteMotorcycle(String id, String userId, String favoriteId, String type) {
        super(id, userId, favoriteId, type);
    }

    public Motorcycle motorcycle() {
        return motorcycle;
    }
}
