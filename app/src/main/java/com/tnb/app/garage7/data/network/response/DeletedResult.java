package com.tnb.app.garage7.data.network.response;

public class DeletedResult {

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }
}
