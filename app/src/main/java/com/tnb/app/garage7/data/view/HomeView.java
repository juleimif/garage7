package com.tnb.app.garage7.data.view;

import android.content.Context;

import com.tnb.app.garage7.domain.model.ImagesHome;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Vehiculo;

import java.util.List;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */public interface HomeView {
    void showImages(List<ImagesHome> body);
    Context getContext();
    void  showDestacadas(List<Vehiculo> body);
    void showProgress();
    void hideProgress();
    void showRelod();

}
