package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.FavoriteMotorcycle;

import java.util.List;

public class FavoriteMotorcyclesResponse {

    @SerializedName(value = "data")
    private List<FavoriteMotorcycle> favoriteCars;

    public List<FavoriteMotorcycle> favoriteMotorcycles() {
        return favoriteCars;
    }
}
