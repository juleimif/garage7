package com.tnb.app.garage7.domain.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.data.rest.ConnectClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Luisana Abache on 08/07/2017.
 */

public class UpPhotoAsyncTask extends AsyncTask<String,Void,Boolean> {
    public static final String TAG = UpPhotoAsyncTask.class.getSimpleName();

    private ProgressDialog progressDialog;
    AlertDialog.Builder builder;
    private Context context;

    /**Constructor de clase */
    public UpPhotoAsyncTask(Context context) {
        this.context = context;
        builder = new AlertDialog.Builder(context);
    }
    /**
     * Antes de comenzar la tarea muestra el progressDialog
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, "Por favor espere", "Subiendo...");
    }
    
    @Override
    protected Boolean doInBackground(String... strings) {
        Log.i(TAG, "doInBackground: ");
        final ConnectClient service = new ConnectClient();
        final ApiInterface inter = service.setService();
        inter.setPublicaction(null).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {

                Log.i(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {
                Log.i(TAG, "onFailure: ");

            }
        });
        return true;
    }

    @Override
    protected void onPostExecute(Boolean resul) {
        progressDialog.dismiss();
        if( resul )
        {
            builder.setMessage("Imagen subida al servidor")
                    .setTitle("JC le informa")
                    .setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.cancel();
                        }
                    }).create().show();
        }
        else
        {
            builder.setMessage("No se pudo subir la imagen")
                    .setTitle("JC le informa")
                    .setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.cancel();
                        }
                    }).create().show();
        }
    }

}
