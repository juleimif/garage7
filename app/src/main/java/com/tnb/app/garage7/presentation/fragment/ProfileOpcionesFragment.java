package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.activity.ProfileActivity;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.ResponseUser;
import com.tnb.app.garage7.presentation.DialogFragment.CountryDialogoFragment;
import com.tnb.app.garage7.presentation.activity.AjustesFragment;
import com.tnb.app.garage7.presentation.activity.ComprasFragment;
import com.tnb.app.garage7.presentation.activity.ConcesionartioRegisterFragment;
import com.tnb.app.garage7.presentation.activity.PublicacionesFragment;
import com.tnb.app.garage7.presentation.activity.VentasFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileOpcionesFragment extends BaseFragment implements GoDetails {
    public static final String TAG = ProfileOpcionesFragment.class.getSimpleName();
    private ResponseUser userDataModel;
    @BindView(R.id.tv_correo) TextView tv_correo;
    @BindView(R.id.tv_nombre) TextView tv_nombre;
    @BindView(R.id.iv_profile) CircleImageView iv_profile;
    @BindView(R.id.convertirme) TextView convertirme;
    @BindView(R.id.operaciones_concesionarios) TextView operaciones_concesionarios;

    private PreferencesSessionManager preferencesUserManager;
    GoDetails listener;

    public static ProfileOpcionesFragment newInstance(String param1, String param2) {
        ProfileOpcionesFragment fragment = new ProfileOpcionesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getFragmentLayout() {
        return  R.layout.fragment_profile_opciones;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        preferencesUserManager = new PreferencesSessionManager(getContext());
        Log.i(TAG, "onCreateView: " + preferencesUserManager.getUserConce());

        if(preferencesUserManager.getUserConce().equals("1")){
            convertirme.setVisibility(View.GONE);
            operaciones_concesionarios.setVisibility(View.VISIBLE);
        }
        else {
            operaciones_concesionarios.setVisibility(View.GONE);
            convertirme.setVisibility(View.VISIBLE);
        }

        if(preferencesUserManager.getUsereName() != null && preferencesUserManager.getUsereEmail()!= null){
            tv_nombre.setText("¡Hola " +preferencesUserManager.getUsereName()+"!");
            tv_correo.setText(preferencesUserManager.getUsereEmail());

            Glide.with(this)
                    .load(preferencesUserManager.getUsereAvatar())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.icon_user))
                    .into(iv_profile);
            return;

        }
    }

    @OnClick({R.id.publicaciones, R.id.compras, R.id.ventas, R.id.operaciones, R.id.convertirme,
            R.id.ajustes, R.id.ayuda, R.id.sobre_nosotros, R.id.operaciones_concesionarios, R.id.mis_datos})
    public void onClick(View view) {
        Fragment fragment;
        switch (view.getId()) {
            case R.id.publicaciones:
               Log.i(TAG, "onClick: ");
                fragment = new PublicacionesFragment();
                listener.goOtherFragmnet(fragment, "Publicaciones");

                break;
            case R.id.compras:
                fragment = new ComprasFragment();
                listener.goOtherFragmnet(fragment, "Compras");

                break;
            case R.id.ventas:
                fragment = new VentasFragment();
                listener.goOtherFragmnet(fragment, "Ventas");

                break;
            case R.id.operaciones:
                fragment = new OperacionesFragment();
                listener.goOtherFragmnet(fragment, "Operaciones");

                break;
            case R.id.ajustes:
                fragment = new AjustesFragment();
                listener.goOtherFragmnet(fragment, "Ajustes");

                break;
            case R.id.convertirme:
                CountryDialogoFragment newFragment = new CountryDialogoFragment();
                showDialogFragment(newFragment);
                break;

            case R.id.operaciones_concesionarios:
                Log.i(TAG, "onClick: ");
                fragment = new OperacionesConcesionarioFragment();
                listener.goOtherFragmnet(fragment, "Operaciones");

                break;

            case R.id.mis_datos:
               startActivity(new Intent(getContext(), ProfileActivity.class));
               getActivity().overridePendingTransition(R.anim.left_out, R.anim.left_in);

                break;

            case R.id.ayuda:
                break;
            case R.id.sobre_nosotros:
                break;
        }
    }

    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void details() {
    }

    @Override
    public void goMainActivity() {
        startActivity(new Intent(getContext(), ConcesionartioRegisterFragment.class));
    }

    @Override
    public void goOtherFragmnet(Fragment fragment, String tag) {

    }

    @Override
    public void showAjustes() {


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }


}



