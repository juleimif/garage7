package com.tnb.app.garage7.common.country;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.data.entity.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends ArrayAdapter<Country> {

    private List<Country> countries = new ArrayList<>();

    public CountryAdapter(@NonNull Context context, int resource,
                          @NonNull List<Country> countries) {
        super(context, resource, countries);
        setList(countries);
    }

    private void setList(List<Country> countries) {
        this.countries = countries;
    }

    public void replaceData(List<Country> countries) {
        setList(countries);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Country getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.country_list_item, parent, false);
            viewHolder.countryName = convertView.findViewById(R.id.countryName);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Country country = countries.get(position);
        viewHolder.countryName.setText(country.country);

        return convertView;
    }

    static class ViewHolder {
        TextView countryName;
    }
}
