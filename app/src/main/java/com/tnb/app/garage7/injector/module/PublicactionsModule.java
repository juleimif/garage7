package com.tnb.app.garage7.injector.module;

import com.tnb.app.garage7.injector.scope.CustomScope;
import com.tnb.app.garage7.data.view.PublicationsContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Acer on 11/2/2018.
 */
@Module
public class PublicactionsModule {

    private final PublicationsContract.View mView;

    public PublicactionsModule(PublicationsContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    PublicationsContract.View providesPublicationsContractView() {
        return mView;
    }

}
