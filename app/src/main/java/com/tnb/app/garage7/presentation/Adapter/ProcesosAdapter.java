package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.Constants;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProcesosAdapter extends RecyclerView.Adapter<ProcesosAdapter.ViewHolder> {

    private PublicationItemClick mItemClickListener;
    private  List<OperacionesConcesionarioResponse> procesoList;
    private Context context;

    public ProcesosAdapter(Context applicationContext, List<OperacionesConcesionarioResponse> procesoList, PublicationItemClick brokerItemClick) {
        this.procesoList =  procesoList;
        mItemClickListener = (PublicationItemClick) brokerItemClick;
        this.context = applicationContext;
    }


    @Override
    public ProcesosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_procesos, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OperacionesConcesionarioResponse currentItem = procesoList.get(position);
        ProcesosAdapter.ViewHolder viewHolder = (ProcesosAdapter.ViewHolder) holder;

        viewHolder.tv_name_car.setText(currentItem.getBrandC() + " - " +currentItem.getModelC());
        viewHolder.tv_price.setText(currentItem.getPriceC() + " VEF");
        viewHolder.tv_name.setText(currentItem.getUsername());

        Glide.with(context)
                .load(currentItem.getPhoto1())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.no_photo))
                .into(viewHolder.iv_car);


        switch (currentItem.getStatusNegociacion()){
            case "1":
                viewHolder.tv_status.setText(Constants.SOLICITANDO_CITA);
                viewHolder.tv_status.setTextColor(Color.BLUE);
                break;
            case "2":
                viewHolder.tv_status.setText(Constants.CITA_ACEPTADA);
                viewHolder.tv_status.setTextColor(Color.BLUE);

                break;
            case "3":
                viewHolder.tv_status.setText(Constants.COMPRA_CONFIRMADA);
                viewHolder.tv_status.setTextColor(Color.BLUE);

                break;
            case "4":
                viewHolder.tv_status.setText(Constants.REGISTRO_DE_PAGO);
                viewHolder.tv_status.setTextColor(Color.BLUE);

                break;
            case "5":
                viewHolder.tv_status.setText(Constants.ENTREGA_DE_VEHICULO);
                viewHolder.tv_status.setTextColor(Color.BLUE);

                break;
            case "6":
                viewHolder.tv_status.setText(Constants.ENTREGA_DE_VEHICULO);
                viewHolder.tv_status.setTextColor(Color.BLUE);

                break;
            case "10":
                viewHolder.tv_status.setText(Constants.CAMNCELADA);
                viewHolder.tv_status.setTextColor(Color.RED);

                break;
        }

    }

    @Override
    public int getItemCount() {

        return procesoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_status) TextView tv_status;
        @BindView(R.id.tv_price) TextView tv_price;
        @BindView(R.id.tv_name_car)    TextView tv_name_car;
        @BindView(R.id.tv_name)    TextView tv_name;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mItemClickListener.onBrokerClick(procesoList.get(position));
        }

    }

    public interface PublicationItemClick {
        void onBrokerClick(OperacionesConcesionarioResponse clickedBroker);
    }


}
