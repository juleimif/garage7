package com.tnb.app.garage7.data.network.request;

import com.google.gson.annotations.SerializedName;

public class GarageRequest {

    public final String name;

    @SerializedName("id_number")
    public final String idNumber;

    @SerializedName("fiscal_address")
    public final String fiscalAddress;

    @SerializedName("alt_address")
    public final String altAddress;

    @SerializedName("postal_zone")
    public final String postalZone;

    public final String phone;

    public final String celphone;

    @SerializedName("alt_phone")
    public final String altPhone;

    public final String email;

    @SerializedName("alt_email")
    public final String altEmail;

    public final String city;

    public final String parish;

    public final String street;

    public final String building;

    @SerializedName("building_number")
    public final String buildingNumber;

    public GarageRequest(String name, String idNumber, String fiscalAddress,
                         String altAddress, String postalZone, String phone,
                         String celphone, String altPhone, String email,
                         String altEmail, String city, String parish, String street,
                         String building, String buildingNumber) {
        this.name = name;
        this.idNumber = idNumber;
        this.fiscalAddress = fiscalAddress;
        this.altAddress = altAddress;
        this.postalZone = postalZone;
        this.phone = phone;
        this.celphone = celphone;
        this.altPhone = altPhone;
        this.email = email;
        this.altEmail = altEmail;
        this.city = city;
        this.parish = parish;
        this.street = street;
        this.building = building;
        this.buildingNumber = buildingNumber;
    }
}
