package com.tnb.app.garage7.data.entity;

import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehicle {

    @NonNull
    public final String id;

    public final String postedBy;

    public final String model;

    public final String brand;

    public final String color;

    public final String city;

    public final String country;

    public final String countryId;

    public final String description;

    public final String price;

    public final String formattedPrice;

    public final String currencySymbol;

    public final String year;

    public final String mileage;

    public final String mileageSymbol;

    public final String isActive;

    public final String createdAt;

    public final String updatedAt;

    public final String url;

    @Ignore
    public List<PhotoUrl> photosUrls = new ArrayList<>();

    public Vehicle(@NonNull String id, String postedBy, String model, String brand,
                   String color, String city, String country, String countryId,
                   String description, String price, String formattedPrice,
                   String currencySymbol, String year, String mileage,
                   String mileageSymbol, String isActive, String createdAt,
                   String updatedAt, String url, List<PhotoUrl> photosUrls) {
        this.id = id;
        this.postedBy = postedBy;
        this.model = model;
        this.brand = brand;
        this.color = color;
        this.city = city;
        this.country = country;
        this.countryId = countryId;
        this.description = description;
        this.price = price;
        this.formattedPrice = formattedPrice;
        this.currencySymbol = currencySymbol;
        this.year = year;
        this.mileage = mileage;
        this.mileageSymbol = mileageSymbol;
        this.isActive = isActive;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.url = url;
        this.photosUrls = photosUrls;
    }

}
