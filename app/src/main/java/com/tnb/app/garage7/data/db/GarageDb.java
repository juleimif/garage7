package com.tnb.app.garage7.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.tnb.app.garage7.data.db.dao.*;
import com.tnb.app.garage7.data.entity.*;

@Database(entities = {
        Country.class,
        City.class,
        Car.class,
        User.class,
        Brand.class,
        Banner.class
    }, version = 4, exportSchema = false)
public abstract class GarageDb extends RoomDatabase {

    abstract public CountryDAO countryDao();

    abstract public CityDAO cityDao();

    abstract public CarDAO carDao();

    abstract public UserDAO userDao();

    abstract public BrandDAO brandDao();

    abstract public BannerDAO bannerDao();
}
