package com.tnb.app.garage7.presentation.decorations;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by cyn  son 07/07/2016.
 */

public class BaseItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    Drawable rowSeparatorDrawable;

    /**
     * Default divider will be used
     */
    public BaseItemDecoration(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        rowSeparatorDrawable = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    /**
     * Custom divider will be used
     */
    public BaseItemDecoration(Context context, @DrawableRes int rowSeparatorDrawable) {
        this.rowSeparatorDrawable = ContextCompat.getDrawable(context, rowSeparatorDrawable);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        drawItemDecoration(c, parent);
    }

    protected void drawItemDecoration(Canvas c, RecyclerView parent) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + rowSeparatorDrawable.getIntrinsicHeight();

            rowSeparatorDrawable.setBounds(left, top, right, bottom);
            rowSeparatorDrawable.draw(c);
        }
    }


    /*
    * We need to provide offsets between list items so that we’re not drawing dividers on top of
    * our child views.
    * */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }

        //outRect.top = rowSeparatorDrawable.getIntrinsicHeight();
        outRect.set(0, 0, 0, rowSeparatorDrawable.getIntrinsicHeight());
    }
}