package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.ImagesHome;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Publicaciones;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Vehiculo;
import com.tnb.app.garage7.domain.presenter.HomeImpl;
import com.tnb.app.garage7.data.view.HomeView;
import com.tnb.app.garage7.presentation.Adapter.HomeAdapter;
import com.tnb.app.garage7.presentation.activity.MainActivity;
import com.tnb.app.garage7.presentation.listeners.GoDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;


public class HomeFragment extends Fragment implements HomeView {
    public static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.rv_horizontal_alpha)
    RecyclerView rv_horizontal_alpha;
    @BindView(R.id.banner_information)
    BannerSlider banner_information;
    @BindView(R.id.ly_cont)
    LinearLayout ly_cont;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.reload)
    ImageButton reload;
    HomeAdapter adapter;
    List<ImagesHome> listImagesHome;
    List<Publicaciones> listDestacadas;
    private static int firstVisibleInListview;
    private boolean hasMore;
    HomeImpl homePresenter;
    VehiculoCarro carModelSingleton;
    GoDetails goDetailsListener;

    View view;

    @Override
    public void showImages(List<ImagesHome> body) {
        ly_cont.setVisibility(View.VISIBLE);
        Log.i(TAG, "showImages: " + body.size());
        for (int i = 0; i < body.size(); i++) {
            banner_information.addBanner(new RemoteBanner(body.get(i).getUrl()));
        }
    }

    @Override
    public void showDestacadas(List<Vehiculo> body) {
        initViews();

        Log.i(TAG, "showDestacadas: " + body.size());

        adapter = new HomeAdapter(getContext(), body, new HomeAdapter.CarItemClick() {
            @Override
            public void onBrokerClick(Vehiculo clickedBroker) {

                Log.i(TAG, "onBrokerClick: " + clickedBroker.getPhoto1());
                carModelSingleton.setIdPublicacion(clickedBroker.getIdpublicacion());
                carModelSingleton.setUsuarioC(clickedBroker.getUsuarioC());
                carModelSingleton.setTypePostC(clickedBroker.getTypePostC());
                carModelSingleton.setBrandC(clickedBroker.getBrandC());
                carModelSingleton.setModelC(clickedBroker.getModelC());
                carModelSingleton.setYearC(clickedBroker.getYearC());
                carModelSingleton.setDescriptionsC(clickedBroker.getDescriptionsC());
                carModelSingleton.setLocationC(clickedBroker.getLocationC());
                carModelSingleton.setPhoto1(clickedBroker.getPhoto1());
                carModelSingleton.setPhoto2(clickedBroker.getPhoto2());
                carModelSingleton.setPhoto3(clickedBroker.getPhoto3());
                carModelSingleton.setPhoto4(clickedBroker.getPhoto4());
                carModelSingleton.setPhoto5(clickedBroker.getPhoto5());
                carModelSingleton.setPhoto6(clickedBroker.getPhoto6());
                carModelSingleton.setPhoto7(clickedBroker.getPhoto7());
                carModelSingleton.setPhoto8(clickedBroker.getPhoto8());
                carModelSingleton.setPhoto9(clickedBroker.getPhoto9());
                goDetailsListener.details();

            }
        });
        rv_horizontal_alpha.setAdapter(adapter);
        hideLoading();

    }

    @Override
    public void showProgress() {
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRelod() {
        reload.setVisibility(View.VISIBLE);

    }


    public interface goOpcion {
        void go(int opcion);
    }

    WelcomePublicationFragment.goOpcion listener;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        Context context = view.getContext();
        homePresenter = new HomeImpl(this);
        homePresenter.getImages();
        homePresenter.getDestacadas();
        carModelSingleton = VehiculoCarro.getInstance();

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        homePresenter.getDestacadas();
                        homePresenter.getImages();


                    }
                }, 0);
            }
        });


        return view;
    }

    private void initViews() {
        rv_horizontal_alpha.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        rv_horizontal_alpha.setLayoutManager(layoutManager);

    }


    @OnClick({R.id.tv_car, R.id.tv_moto})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_car:
                listener.go(0);
                break;

            case R.id.tv_moto:
                listener.go(1);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (WelcomePublicationFragment.goOpcion) context;
            goDetailsListener = (GoDetails) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        homePresenter.getDestacadas();
        homePresenter.getImages();
        reload.setVisibility(View.INVISIBLE);
    }
}
