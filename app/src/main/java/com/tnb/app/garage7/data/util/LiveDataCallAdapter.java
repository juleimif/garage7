package com.tnb.app.garage7.data.util;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.tnb.app.garage7.data.network.ApiResponse;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveDataCallAdapter<R> implements CallAdapter<R, LiveData<ApiResponse<R>>> {

    private final Type responseType;

    public LiveDataCallAdapter(Type responseType) {
        this.responseType = responseType;
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public LiveData<ApiResponse<R>> adapt(final Call<R> call) {
        Log.e("LiveDataCallAdapter", "adapt  " + call.isExecuted() );

        return new LiveData<ApiResponse<R>>() {

            AtomicBoolean started = new AtomicBoolean(false);

            @Override
            protected void onActive() {
                super.onActive();
                Log.e("LiveDataCallAdapter", "onActive  " );
                if (started.compareAndSet(false, true)) {
                    call.enqueue(new Callback<R>() {
                        @Override
                        public void onResponse(Call<R> call, Response<R> response) {
                            Log.e("LiveDataCallAdapter", "onResponse  " + call.request().header("Authorization"));
                            postValue(new ApiResponse<>(response));
                        }

                        @Override
                        public void onFailure(Call<R> call, Throwable throwable) {
                            Log.e("LiveDataCallAdapter", "onFailure  " + throwable.getMessage());

                            postValue(new ApiResponse<R>(throwable));
                        }
                    });
                }
            }
        };
    }
}
