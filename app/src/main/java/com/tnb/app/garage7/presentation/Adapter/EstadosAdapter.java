package com.tnb.app.garage7.presentation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.response.AutoCompletarEstadosResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EstadosAdapter extends RecyclerView.Adapter<EstadosAdapter.ViewHolder> {

    private BrandItemClick mItemClickListener;
    private List<AutoCompletarEstadosResponse> brandsList;
    private Context contexto;

    public EstadosAdapter(Context applicationContext, List<AutoCompletarEstadosResponse> brandsList, BrandItemClick brokerItemClick) {
        this.brandsList = brandsList;
        this.contexto = applicationContext;
        mItemClickListener = (BrandItemClick) brokerItemClick;
    }


    @Override
    public EstadosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_brands, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        AutoCompletarEstadosResponse currentItem = brandsList.get(position);
        EstadosAdapter.ViewHolder viewHolder = (EstadosAdapter.ViewHolder) holder;
            Log.i("ciudades", "onBindViewHolder: ");
            viewHolder.tv_brand.setText(currentItem.getEstado());
    }

    @Override
    public int getItemCount() {

        return brandsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        @BindView(R.id.tv_brand)
        TextView tv_brand;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mItemClickListener.onBrokerClick(brandsList.get(position));
        }

    }

    public interface BrandItemClick {
        void onBrokerClick(AutoCompletarEstadosResponse clickedBroker);
    }

}
