package com.tnb.app.garage7.presentation.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tnb.app.garage7.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juleimis on 19/06/2017.
 */

public class CitaMessageDialogoFragment extends DialogFragment {
    public static final String TAG = CitaMessageDialogoFragment.class.getSimpleName();
    @BindView(R.id.tv_titulo) TextView tv_titulo;
    @BindView(R.id.tv_mensage) TextView tv_mensage;


    public static CitaMessageDialogoFragment newInstance(String personName, String personEmail, String base64Img) {
        CitaMessageDialogoFragment f = new CitaMessageDialogoFragment();
        Bundle args = new Bundle();

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialogo = super.onCreateDialog(savedInstanceState);
        dialogo.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.dialog_cita_messages);
        dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this, dialogo);


        return dialogo;
    }

    @OnClick({R.id.btn_ok, R.id.btn_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                break;
            case R.id.btn_cancel:
                break;
        }
    }
}
