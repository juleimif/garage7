package com.tnb.app.garage7.presentation.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.AlbumModel;
import com.tnb.app.garage7.domain.model.Car;
import com.tnb.app.garage7.domain.model.PhotoItem;
import com.tnb.app.garage7.domain.model.PhotoModel;
import com.tnb.app.garage7.domain.model.request.ConcesionarioRequest;
import com.tnb.app.garage7.domain.presenter.FormPhotosImpl;
import com.tnb.app.garage7.domain.presenter.FormPhotosPresenter;
import com.tnb.app.garage7.data.view.FormPhotosView;
import com.tnb.app.garage7.presentation.Adapter.AlbumAdapter;
import com.tnb.app.garage7.presentation.Adapter.PhotoSelectorAdapter;
import com.tnb.app.garage7.presentation.Domain.PhotoSelectorDomain;
import com.tnb.app.garage7.utils.CommonUtils;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


public class PhotosConsecionarioFragment extends BaseFragment implements FormPhotosView, PhotoItem.onItemClickListener, PhotoItem.onPhotoItemCheckedListener, AdapterView.OnItemClickListener,
        View.OnClickListener {

    private static final String TAG = FormPhotosFragment.class.getSimpleName();
    @BindView(R.id.tv_number) TextView tvNumber;
    @BindView(R.id.gv_photos_ar) GridView gvPhotos;
    @BindView(R.id.ly_account) CoordinatorLayout ly_account;
    @BindView(R.id.btn_tuto) Button btn_tuto;
    private static final int REQUEST_SELECT_PICTURE = 0x01;

    private PhotoSelectorDomain photoSelectorDomain;
    private PhotoSelectorAdapter photoAdapter;
    private AlbumAdapter albumAdapter;
    public ArrayList<PhotoModel> selected;
    public ArrayList<String> pathOriginal;
    public ArrayList<String> fotos;
    String encodedImage;
    StringBuilder sb;
    int cont = 0;
    int contPhotos, limitPhotos;
    String images;
    String fotosAdd;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "Garage7";
    private FormPhotosPresenter formPhotosPresenter;
    private PreferencesSessionManager preferencesSessionManager;
    private ConcesionarioRequest concesionarioRequestSingleton;

    public PhotosConsecionarioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_form_photos;
    }


    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

        photoSelectorDomain = new PhotoSelectorDomain(getContext());
        selected = new ArrayList<PhotoModel>();
        pathOriginal = new ArrayList<>();
        fotos = new ArrayList<>();
        concesionarioRequestSingleton = ConcesionarioRequest.getInstance();
        formPhotosPresenter = new FormPhotosImpl(this);


        formPhotosPresenter.initImageLoader();
        preferencesSessionManager = new PreferencesSessionManager(getContext());
        btn_tuto.setVisibility(View.INVISIBLE);

        photoAdapter = new PhotoSelectorAdapter(getContext(),
                new ArrayList<PhotoModel>(), CommonUtils.getWidthPixels(getActivity()),
                this, this, this);
        gvPhotos.setAdapter(photoAdapter);
        albumAdapter = new AlbumAdapter(getContext(),
                new ArrayList<AlbumModel>());
        photoSelectorDomain.getReccent(reccentListener);
        photoSelectorDomain.updateAlbum(albumListener);


    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onCheckedChanged(PhotoModel photoModel, CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            Log.i(TAG, "onCheckedChanged: " + cont);
            if (cont > 6) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                Resources res = getResources();
                String title = res.getString(R.string.alerta);
                String message = res.getString(R.string.maximo);
                builder.setTitle(title);
                builder.setMessage(message);
                builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                buttonView.toggle();
            } else {
                if (!selected.contains(photoModel)) {
                    selected.add(photoModel);
                    pathOriginal.add(photoModel.getOriginalPath());
                }
                Uri u = Uri.parse(String.valueOf(Uri.fromFile(new File(photoModel.getOriginalPath()))));
                startCropActivity(u);
                photoModel.setChecked(true);

            }
        } else {
            photoModel.setChecked(false);
            selected.remove(photoModel);
            fotos.remove(cont - 1);
            cont--;
            Log.i(TAG, "unCheckedChanged: " + fotos.size());
        }
        tvNumber.setText("(" + selected.size() + ")");

    }

    public interface OnLocalReccentListener {
        public void onPhotoLoaded(List<PhotoModel> photos);
    }

    public interface OnLocalAlbumListener {
        public void onAlbumLoaded(List<AlbumModel> albums);
    }

    private FormPhotosFragment.OnLocalAlbumListener albumListener = new FormPhotosFragment.OnLocalAlbumListener() {
        @Override
        public void onAlbumLoaded(List<AlbumModel> albums) {
            albumAdapter.update(albums);
        }
    };

    private FormPhotosFragment.OnLocalReccentListener reccentListener = new FormPhotosFragment.OnLocalReccentListener() {
        @Override
        public void onPhotoLoaded(List<PhotoModel> photos) {
            for (PhotoModel model : photos) {
                if (selected.contains(model)) {
                    model.setChecked(true);
                }
            }
            photoAdapter.update(photos);
            gvPhotos.smoothScrollToPosition(0);
            // reset(); //--keep selected photos
        }
    };

    @OnClick(R.id.fa_next)
    public void Next(View view) {

        if (fotos.size() > 6) {
            Log.i(TAG, "Next: " + contPhotos + "Photos size " + fotos.size());
            formato();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            Resources res = getResources();
            String title = res.getString(R.string.alert_limit_message);
            String message = res.getString(R.string.maximo_msj)  + " 5 imagenes " + res.getString(R.string.maximo_msj_2);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void formato() {
        Log.i(TAG, "formato: " + fotos.size());
        sb = new StringBuilder("[");
        for (int i = 0; i < fotos.size(); i++) {
            Log.i(TAG, "formato: " + sb.append(fotos.get(i)));
            sb.append(fotos.get(i));
            if (i < fotos.size() - 1) {
                sb.append(",");
            }
        }
        sb.append("]").toString();
        fotosAdd = sb.toString();
        concesionarioRequestSingleton.setPhotoConcesionaria(fotosAdd);
        getFragmentManager().popBackStack();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    Log.i(TAG, "onActivityResult: ");
//                    startCropActivity(data.getData());
                } else {
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                Log.i(TAG, "onActivityResult: ");
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            Log.i(TAG, "onActivityResult: ");
//            handleCropError(data);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;

        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(50);
        options.setToolbarTitle("Editar imagen");
        options.setToolbarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(getContext(), R.color.color_toolbar));

        UCrop.of(uri, Uri.fromFile(new File(getContext().getCacheDir(), destinationFileName)))
                .withAspectRatio(16, 12)
                .withOptions(options)
                .start(getActivity(), PhotosConsecionarioFragment.this);


    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {

            Bitmap bm = BitmapFactory.decodeFile(String.valueOf(resultUri.getPath()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            //add IMAGES
            fotos.add(encodedImage);
            cont++;

        } else {
            Toast.makeText(getContext(), "toast_cannot_retrieve_cropped_image", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void publicar(Car car) {

    }

    @Override
    public Context getContextaAplication() {
        return getContext();
    }
}


