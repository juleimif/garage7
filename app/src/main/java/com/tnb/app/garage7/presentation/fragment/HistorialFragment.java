package com.tnb.app.garage7.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.response.OperacionesConcesionarioResponse;
import com.tnb.app.garage7.domain.presenter.SolicitudesImpl;
import com.tnb.app.garage7.data.view.SolicitudesView;
import com.tnb.app.garage7.presentation.Adapter.HistorialAdapter;
import com.tnb.app.garage7.presentation.DialogFragment.DetalleNegociacionDialogoFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistorialFragment extends Fragment implements SolicitudesView {
    public static final String TAG = HistorialFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list) RecyclerView      recyclerView;
    @BindView(R.id.ly_list_save) CoordinatorLayout ly_list_save;
    @BindView(R.id.cont_publicactiones) LinearLayout cont_publicactins;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.reload) ImageButton reload;
    private HistorialAdapter historialAdapter;
    private PreferencesSessionManager preferencesUserManager;
    GoDetails listener;
    private SolicitudesImpl solicitudesPresenter;
    private List<OperacionesConcesionarioResponse> myListSolicitudes;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    public HistorialFragment() {
        // Required empty public constructor
    }

    public static HistorialFragment newInstance(String param1, String param2) {
        HistorialFragment fragment = new HistorialFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_operaciones, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
                    }
                }, 0);
            }
        });

        solicitudesPresenter = new SolicitudesImpl(this);
        solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
        myListSolicitudes = new ArrayList<OperacionesConcesionarioResponse>();
        return view;
    }

    private void initViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }


    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
        reload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (GoDetails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() +
                            " no implementó OkDialogListener");
        }
    }

    @Override
    public void showSolicitudes(List<OperacionesConcesionarioResponse> body) {
        initViews();

        for (int i = 0; i <body.size(); i++) {
            if (body.get(i).getStatusNegociacion().equals("7")||body.get(i).getStatusNegociacion().equals("10")) {//es una solicitud

                OperacionesConcesionarioResponse dates =new OperacionesConcesionarioResponse(body.get(i).getIdpostCars(),body.get(i).getTypePostC(),body.get(i).getUsuarioC(),
                        body.get(i).getPriceC(),body.get(i).getBrandC(),body.get(i).getModelC(),body.get(i).getIdvendedor(),body.get(i).getRatingVendedor(),body.get(i).getNegociacion(),
                        body.get(i).getRatingComprador(),body.get(i).getStatusNegociacion(),body.get(i).getPhoto1(),body.get(i).getUsername());
                myListSolicitudes.add(dates);
            }
        }

        if(myListSolicitudes.size()>0){
            cont_publicactins.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            historialAdapter = new HistorialAdapter(getContext(), myListSolicitudes, new HistorialAdapter.PublicationItemClick() {
                @Override
                public void onBrokerClick(OperacionesConcesionarioResponse clickedBroker) {

                    DetalleNegociacionDialogoFragment newFragment = new DetalleNegociacionDialogoFragment();
                    showDialogFragment(newFragment);
                }
            });
            recyclerView.setAdapter(historialAdapter);
        }
        else
        {
            cont_publicactins.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            Log.i(TAG, "esto e suna solicitud 2: " + myListSolicitudes.size());
        }


    }

    @Override
    public void showProgress() {
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRelod() {
        reload.setVisibility(View.VISIBLE);

    }



    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }


    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            myListSolicitudes.clear();
            solicitudesPresenter.getSolicitudes(preferencesUserManager.getUserConcesionario());
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;

        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

}
