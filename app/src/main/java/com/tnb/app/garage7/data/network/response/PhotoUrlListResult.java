package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.PhotoUrl;

import java.util.ArrayList;
import java.util.List;

public class PhotoUrlListResult {

    @SerializedName(value = "data")
    private PhotoUrls photoUrls;

    public PhotoUrls getPhotoUrls() {
        return photoUrls;
    }

    public List<PhotoUrl> getResults() {
        return photoUrls.getResults();
    }

    private class PhotoUrls {
        @SerializedName(value = "photosUrls")
        private List<PhotoUrl> results = new ArrayList<>();

        public List<PhotoUrl> getResults() {
            return results;
        }
    }
}
