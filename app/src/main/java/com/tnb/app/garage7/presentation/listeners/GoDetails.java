package com.tnb.app.garage7.presentation.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by Juleimis on 31/08/2017.
 */

public interface GoDetails {
    void details();
    void goMainActivity();
    void goOtherFragmnet(Fragment fragment, String tag);
    void showAjustes();
}
