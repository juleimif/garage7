package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.Motorcycle;

public class MotorcycleResult {

    @SerializedName(value = "data")
    private Motorcycle motorcycle;

    public MotorcycleResult(Motorcycle motorcycle) {
        this.motorcycle = motorcycle;
    }

    public Motorcycle motorcycle() {
        return motorcycle;
    }

    public void setMotorcycle(Motorcycle motorcycle) { this.motorcycle = motorcycle;}
}
