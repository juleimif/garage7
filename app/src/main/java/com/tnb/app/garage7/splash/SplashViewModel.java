package com.tnb.app.garage7.splash;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.vo.Resource;

import java.util.List;

import javax.inject.Inject;

public class SplashViewModel extends ViewModel{

    LiveData<Resource<User>> users;

    @Inject
    public SplashViewModel(@NonNull AppRepository repository) {
        users = repository.getUser();
    }

    public LiveData<Resource<User>> getUser() {
        return users;
    }
}
