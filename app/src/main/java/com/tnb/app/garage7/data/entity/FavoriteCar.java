package com.tnb.app.garage7.data.entity;

import com.google.gson.annotations.SerializedName;

public class FavoriteCar extends Favorite {

    @SerializedName(value = "favorite")
    private Car car;

    public FavoriteCar(String id, String userId, String favoriteId, String type, Car car) {
        super(id, userId, favoriteId, type);
        this.car = car;
    }

    public Car car() {
        return car;
    }
}
