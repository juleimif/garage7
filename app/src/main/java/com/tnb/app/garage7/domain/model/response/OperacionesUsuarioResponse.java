package com.tnb.app.garage7.domain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 16/06/2017.
 */
public class OperacionesUsuarioResponse {
    @SerializedName("negociacion")
    @Expose
    public String negociacion;
    @SerializedName("publicacion")
    @Expose
    public String publicacion;
    @SerializedName("idconsecionaria")
    @Expose
    public String idconsecionaria;
    @SerializedName("status_negociacion")
    @Expose
    public String statusNegociacion;
    @SerializedName("type_post_c")
    @Expose
    public String typePostC;
    @SerializedName("brand_c")
    @Expose
    public String brandC;
    @SerializedName("model_c")
    @Expose
    public String modelC;
    @SerializedName("price_c")
    @Expose
    public String priceC;
    @SerializedName("conse_name")
    @Expose
    public String conseName;
    @SerializedName("is")
    @Expose
    public Integer is;

    public String getNegociacion() {
        return negociacion;
    }

    public String getPublicacion() {
        return publicacion;
    }

    public String getIdconsecionaria() {
        return idconsecionaria;
    }

    public String getStatusNegociacion() {
        return statusNegociacion;
    }

    public String getTypePostC() {
        return typePostC;
    }

    public String getBrandC() {
        return brandC;
    }

    public String getModelC() {
        return modelC;
    }

    public String getPriceC() {
        return priceC;
    }

    public String getConseName() {
        return conseName;
    }

    public Integer getIs() {
        return is;
    }
}

