package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;

public class InputError {

    @SerializedName(value = "errors")
    private Error errors;

    public Error errors() {
        return errors;
    }
}
