package com.tnb.app.garage7.injector.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.tnb.app.garage7.data.db.GarageDb;
import com.tnb.app.garage7.data.db.dao.BannerDAO;
import com.tnb.app.garage7.data.db.dao.BrandDAO;
import com.tnb.app.garage7.data.db.dao.CityDAO;
import com.tnb.app.garage7.data.db.dao.CountryDAO;
import com.tnb.app.garage7.data.db.dao.UserDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    @Singleton
    @Provides
    GarageDb providesRoomDatabase(Application app) {
        return Room.databaseBuilder(app, GarageDb.class, "garage_db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    CountryDAO provideCountryDao(GarageDb db){
        return db.countryDao();
    }

    @Singleton
    @Provides
    BrandDAO provideBrandDao(GarageDb db){
        return db.brandDao();
    }

    @Singleton
    @Provides
    CityDAO provideCityDao(GarageDb db){
        return db.cityDao();
    }

    @Singleton
    @Provides
    UserDAO provideUserDao(GarageDb db){
        return db.userDao();
    }

    @Singleton
    @Provides
    BannerDAO provideBannerDao(GarageDb db){
        return db.bannerDao();
    }
}
