package com.tnb.app.garage7.domain.model.modelCar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juleimis on 14/07/2017.
 */

public class Carros {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public DataCarro data;

    public String getStatus() {
        return status;
    }

    public DataCarro getData() {
        return data;
    }
}
