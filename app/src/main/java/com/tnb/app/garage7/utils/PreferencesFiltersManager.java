package com.tnb.app.garage7.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Juleimis on 23/06/2017.
 */

public class PreferencesFiltersManager {
    private final String TAG = PreferencesFiltersManager.class.getSimpleName();

    private static final String MODEL = "MODEL";
    private static final String KM = "KM";
    private static final String YEAR = "YEAR";
    private static final String LOCATION = "LOCATION";
    private static final String DISEL = "DISEL";
    private static final String TRASMITION = "TRASMITION";
    private static final String PRISE = "PRICE";
    
    private static final String PREFER_NAME = "ValidarNotificacionesPush";

    public SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Constructor
    public PreferencesFiltersManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void clearPreferences(){
        setModel("");
        setKm("");
        setLocation("");
        setYear("");
        setDisel("");
        setTrasmition("");
        setPrice("");
    }


    public String getLocation() {return pref.getString(LOCATION, "");}
    public void setLocation(String location){
        editor.putString(LOCATION, location);
        editor.apply();
        editor.commit();
    }

    public String getModel() {return pref.getString(MODEL, "");}
    public void setModel(String model){
        editor.putString(MODEL, model);
        editor.apply();
        editor.commit();
    }

    public String getPrice() {return pref.getString(PRISE, "");}
    public void setPrice(String price){
        editor.putString(PRISE, price);
        editor.apply();
        editor.commit();
    }


    public String getDisel() {return pref.getString(DISEL, "");}
    public void setDisel(String disel){
        editor.putString(DISEL, disel);
        editor.apply();
        editor.commit();
    }
    public String getKm() {return pref.getString(KM, "");}
    public void setKm(String km){
        editor.putString(KM, km);
        editor.apply();
        editor.commit();
    }

    public String getYear() {return pref.getString(YEAR, "");}
    public void setYear(String year){
        editor.putString(YEAR, year);
        editor.apply();
        editor.commit();
    }

    public String getTrasmition() {return pref.getString(TRASMITION, "");}
    public void setTrasmition(String trasmition){
        editor.putString(TRASMITION, trasmition);
        editor.apply();
        editor.commit();
    }








}
