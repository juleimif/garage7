package com.tnb.app.garage7.data.entity;

public class TransactionUser {

    public final String name;

    public final String phone;

    public TransactionUser(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
