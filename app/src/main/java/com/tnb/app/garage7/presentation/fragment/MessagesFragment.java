package com.tnb.app.garage7.presentation.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.ApiInterface;
import com.tnb.app.garage7.domain.model.ResponseGuardados;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.request.UpdateEstatusNegociacion;
import com.tnb.app.garage7.domain.model.response.EstatusOperacionResponse;
import com.tnb.app.garage7.data.rest.ConnectClient;
import com.tnb.app.garage7.presentation.Adapter.SaveAdapter;
import com.tnb.app.garage7.presentation.DialogFragment.CalificarDialogoFragment;
import com.tnb.app.garage7.presentation.DialogFragment.ConfirmarPagoDialogoFragment;
import com.tnb.app.garage7.utils.PreferencesSessionManager;
import com.tnb.app.garage7.utils.Utils;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MessagesFragment extends Fragment {
    public static final String TAG = MessagesFragment.class.getSimpleName();

    @BindView(R.id.rv_save_list) RecyclerView      recyclerView;
    @BindView(R.id.ly_list_save) CoordinatorLayout ly_list_save;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.reload) ImageButton reload;
    @BindView(R.id.ly_content_message) LinearLayout ly_content_message;

    private SaveAdapter saveAdapter;
    private PreferencesSessionManager preferencesUserManager;

    public MessagesFragment() {
        // Required empty public constructor
    }

    public static MessagesFragment newInstance(String param1, String param2) {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, view);
        preferencesUserManager = new PreferencesSessionManager(getContext());

        initViews();

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getEstatus();
                    }
                }, 0);
            }
        });

        return view;
    }

    private void initViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
      //  getAllSaves();
        getEstatus();
    }

    private void getEstatus() {
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "getEstatus: " + service.getEstatusOpreacion("1").request().url());
        service.getEstatusOpreacion("1").enqueue(new Callback<List<EstatusOperacionResponse>>() {
            @Override
            public void onResponse(Call<List<EstatusOperacionResponse>> call, Response<List<EstatusOperacionResponse>> response) {
                if (response.code() == 200){
                    if(response.body().size()>0) {
                        ly_content_message.setVisibility(View.GONE);
                        Log.i(TAG, "onResponse: " + response.body().get(0).getStatusNegociacion());
                        responseEstatus(response.body().get(0));
                    }
                    else {
                        ly_content_message.setVisibility(View.VISIBLE);
                    }

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
                hideLoading();
            }

            @Override
            public void onFailure(Call<List<EstatusOperacionResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                hideLoading();
                reload.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),
                        getResources().getString(R.string.connection_error),
                        Toast.LENGTH_LONG)
                        .show();

            }
        });


        // If isn't refreshing
        if (!swipe_container.isRefreshing()) {
            // Show ProgressBar
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void responseEstatus(EstatusOperacionResponse body) {
        String status = body.getStatusNegociacion().toString();
        Log.i(TAG, "responseEstatus: " + body.getStatusNegociacion());

        if(status.equals("0")){ // ya mi soliciud ha sido aceptada
            ly_content_message.setVisibility(View.VISIBLE);
        }
        if(status.equals("1")){ // ya mi soliciud ha sido aceptada
            showConfirmacion("Solicitud de cita","Tu solicitud aún no ha sido aceptada, espera la confirmación de disponibilidad", "Ok");
        }

        if(status.equals("2")){ // ya mi soliciud ha sido aceptada
            showMessage(getResources().getString(R.string.cita_message));
        }
        else
        if(status.equals("3")){ // ya mi soliciud ha sido aceptada
            showConfirmacion("Confirmación de transferencia", "Tu pago esta siendo procesado, por favor espera", "Ok");
        }
        else
        if(status.equals("4")){ // ya mi soliciud ha sido aceptada
            showConfirmacion("Confirmación de transferencia", "El concesionario ha confirmado la recepción del pago, ya puedes retirar el vehículo, espera una llamada del concesionario para concretar la cita de entrega. Una vez recibas el vehiculo confirma para finalizar la operación", "Confirmar entrega");
        }
        else
            //Confirmacion de entrega del vehiculo
        if(status.equals("5")){ // ya mi soliciud ha sido aceptada
            showConfirmacion("Confirmación de entrega", "El vendedor ha confirmado la entrega del vehículo Chevrolet Cruze 2012, número de matricula: HAL282D. Confirma si efectivamente recibiste en vehículo", "Confirmar");
        }
        else
        if(status.equals("6")){ // ya mi soliciud ha sido aceptada
            showCalificar("Confirmación de entrega", "¿ Has recibido el vehiculo ? Estas a punto de confirmar la recepción del vehículo. Recuerda que una vez confirmada la entrega se realizar el pago al vendedor", "Si lo recibí");
        }
    }

    private void showMessage(String message) {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle("Cita concesionario")
                .setDescription(message)
                .setPositiveText("Comprar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        showAlert();

                    }
                })
                .setNegativeText("Declinar");
        dialogHeader_7.show();
    }

    private void showAlert() {

        new AlertDialog.Builder((getActivity()))
                .setMessage(getResources().getString(R.string.cita_concesionario))
                .setCancelable(false)
                .setPositiveButton(this.getString(R.string.confirmar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showConfirmacionPay("Compra confirmada", getResources().getString(R.string.compra_confirmada), "Registrar pago");
                    }
                })
                .setNegativeButton(this.getString(R.string.declinar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }


    private void showConfirmacion(String titulo,String mensaje,String opcionOk) {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                 .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(titulo)
                .setDescription(mensaje)
                .setPositiveText(opcionOk)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                       // ConfirmarPagoDialogoFragment newFragment = new ConfirmarPagoDialogoFragment();
                      //  showDialogFragment(newFragment);

                    }
                });


        dialogHeader_7.show();
    }


    private void showConfirmacionPay(String titulo,String mensaje,String opcionOk) {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(titulo)
                .setDescription(mensaje)
                .setPositiveText(opcionOk)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        ConfirmarPagoDialogoFragment newFragment = new ConfirmarPagoDialogoFragment();
                        showDialogFragment(newFragment);

                    }
                });


        dialogHeader_7.show();
    }

    private void showCalificar(String titulo,String mensaje,String opcionOk) {

        final MaterialStyledDialog.Builder dialogHeader_7 = new MaterialStyledDialog.Builder(getContext())
                .setStyle(Style.HEADER_WITH_TITLE)
                //.setHeaderDrawable(R.drawable.ic_garage)
                .withDialogAnimation(true)
                .withDarkerOverlay(true)
                .setTitle(titulo)
                .setDescription(mensaje)
                .setPositiveText(opcionOk)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        showDialogCalificar();

                    }
                })
                .setNegativeText("No lo he recibido");


        dialogHeader_7.show();
    }

    private void showDialogFragment(DialogFragment dialogFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(dialogFragment, dialogFragment.getTag());
        ft.commitAllowingStateLoss();
    }

    public void showDialogCalificar(){
        //Llamamos al tutorial de como tomar fotos
        CalificarDialogoFragment newFragment = new CalificarDialogoFragment();
        showDialogFragment(newFragment);
    }

    public void getAllSaves(){

        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        Log.i(TAG, "getAllSaves: " + service.getSaves("1").request().url());
        service.getSaves(preferencesUserManager.getUserId()).enqueue(new Callback<List<ResponseGuardados>>() {
            @Override
            public void onResponse(Call<List<ResponseGuardados>> call, Response<List<ResponseGuardados>> response) {
                if (response.code() == 200){

                    Log.i(TAG, "onResponse: " + response.code());
                    if(response.body().size()>0) {
                        saveAdapter = new SaveAdapter(getContext(), response.body(), new SaveAdapter.CarItemClick() {
                            @Override
                            public void onBrokerClick(int clickedBroker) {

                            }
                        });

                        recyclerView.setAdapter(saveAdapter);
                    }
                    else
                        Utils.showSnackBar(getContext(), "No existen publicaciones guardadas", ly_list_save);

                }else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        respuestaServicio(jObjError.getString("message"));
                        Log.e("respuestaServicio", jObjError.getString("message"));
                    } catch (Exception e) {
                        respuestaServicio(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ResponseGuardados>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });

    }

    private void respuestaServicio(String message) {
        Log.i(TAG, "respuestaServicio: " +message);

    }

    public void updateStatus(String idPub, String newStatus){

        UpdateEstatusNegociacion update = new UpdateEstatusNegociacion(idPub, newStatus);
        ConnectClient teacher = new ConnectClient();
        ApiInterface service = teacher.setService();
        service.updateNegociacion(update).enqueue(new Callback<ResponseVehiculo>() {
            @Override
            public void onResponse(Call<ResponseVehiculo> call, Response<ResponseVehiculo> response) {

            }

            @Override
            public void onFailure(Call<ResponseVehiculo> call, Throwable t) {

            }
        });

    }
    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.reload)
    public void reload(View v) {
        getEstatus();
        reload.setVisibility(View.INVISIBLE);
    }

}
