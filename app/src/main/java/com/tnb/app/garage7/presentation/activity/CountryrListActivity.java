package com.tnb.app.garage7.presentation.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.presentation.listeners.GoDetails;
import com.tnb.app.garage7.domain.model.modelCar.VehiculoCarro;
import com.tnb.app.garage7.presentation.Adapter.CountryAdapter;

import butterknife.BindView;
import butterknife.OnClick;


public class CountryrListActivity extends BaseActivity {
    public static final String TAG = CountryrListActivity.class.getSimpleName();
    @BindView(R.id.rv_country_list) RecyclerView rv_country_list;
    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.reload) ImageButton reload;
    CountryAdapter countryAdapter;

    GoDetails listener;

    @Override
    public int getLayout() {
        return R.layout.activity_country_list;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        rv_country_list.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_country_list.setLayoutManager(layoutManager);

        countryAdapter = new CountryAdapter(this, null, new CountryAdapter.CarItemClick() {
            @Override
            public void onBrokerClick(VehiculoCarro clickedBroker) {

            }
        });
        rv_country_list.setAdapter(countryAdapter);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    }
                }, 0);
            }
        });
    }

    /**
     * hideLoading
     * Hide loading icon
     */
    private void hideLoading() {
        // If is refreshing
        if (swipe_container.isRefreshing()) {
            // Hide refreshing
            swipe_container.setRefreshing(false);
        }
        // If isn't refreshing
        else {
            // Hide ProgressBar
            progressBar.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.reload)
    public void reload(View v) {
        reload.setVisibility(View.INVISIBLE);
    }
}
