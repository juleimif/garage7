package com.tnb.app.garage7.presentation.listeners;

import com.tnb.app.garage7.domain.model.CheckEmail;
import com.tnb.app.garage7.domain.model.Guardados;
import com.tnb.app.garage7.domain.model.ImagesHomeResponse;
import com.tnb.app.garage7.domain.model.Login;
import com.tnb.app.garage7.domain.model.OperacionesConcesionario;
import com.tnb.app.garage7.domain.model.RegisterResponse;
import com.tnb.app.garage7.domain.model.ResponseCheckEmail;
import com.tnb.app.garage7.domain.model.ResponseGuardados;
import com.tnb.app.garage7.domain.model.ResponseLogin;
import com.tnb.app.garage7.domain.model.ResponseSession;
import com.tnb.app.garage7.domain.model.ResponseVehiculo;
import com.tnb.app.garage7.domain.model.User;
import com.tnb.app.garage7.domain.model.modelCar.Carros;
import com.tnb.app.garage7.domain.model.modelMoto.Motos;
import com.tnb.app.garage7.domain.model.publicacionesDestacadas.Publicaciones;
import com.tnb.app.garage7.domain.model.request.CalificarConseRequest;
import com.tnb.app.garage7.domain.model.request.CarRequest;
import com.tnb.app.garage7.domain.model.request.CearCitaRequest;
import com.tnb.app.garage7.domain.model.request.ConcesionarioRequest;
import com.tnb.app.garage7.domain.model.request.EstatusPublicacion;
import com.tnb.app.garage7.domain.model.request.MotoRequest;
import com.tnb.app.garage7.domain.model.request.NegociacionRequest;
import com.tnb.app.garage7.domain.model.request.UpdateEstatusNegociacion;
import com.tnb.app.garage7.domain.model.request.UpdateStatusCarRequest;
import com.tnb.app.garage7.domain.model.request.UpdateStatusMotoRequest;
import com.tnb.app.garage7.domain.model.response.AutocompletarEstados;
import com.tnb.app.garage7.domain.model.response.AutocompletarMarcasResponse;
import com.tnb.app.garage7.domain.model.response.AutocompletarModelsResponse;
import com.tnb.app.garage7.domain.model.response.CheckEmailResponse;
import com.tnb.app.garage7.domain.model.response.ConcesionarioResponse;
import com.tnb.app.garage7.domain.model.response.EstatusOperacionResponse;
import com.tnb.app.garage7.domain.model.response.OperacionesUsuario;
import com.tnb.app.garage7.domain.model.response.PublicacionsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by juleimis hostienda on 01/06/2017.
 */
public interface ApiInterface {

    //actualzar usario
    @PUT("usuarios")
    Call<RegisterResponse> updateUser(@Body User resquest);

    //calificamos a un concesionario
    @POST("clientebymail")
    Call<List<CheckEmailResponse>> checkByEmail(@Body CheckEmail resquest);

    //calificamos a un concesionario
    @POST("addconse")
    Call<ResponseVehiculo> setCalificarConse(@Body CalificarConseRequest resquest);

    //registramos un concesionario
    @POST("addconse")
    Call<ResponseVehiculo> setConcesionario(@Body ConcesionarioRequest resquest);

    //Obtener publicaciones por usuario
    @POST("getpubactiveuser")
    Call<PublicacionsResponse> getMePublications(@Body EstatusPublicacion resquest);

    //Modificamos status de un carro
    @POST("updatestatuscar")
    Call<ResponseVehiculo> setEstatusCar(@Body UpdateStatusCarRequest resquest);

    //Modificamos status de una moto
    @POST("updatestatusmot")
    Call<ResponseVehiculo> setEstatusMoto(@Body UpdateStatusMotoRequest resquest);

    //Guardar una publicacion
    @POST("checkmail")
    Call<ResponseCheckEmail> checkEmail(@Body CheckEmail resquest);

    //Creamos una negociacion
    @POST("updatenegociacion")
    Call<ResponseVehiculo> updateNegociacion(@Body UpdateEstatusNegociacion resquest);

    //Creamos una negociacion
    @POST("createnegociacion")
    Call<ResponseVehiculo> setNegociacion(@Body NegociacionRequest resquest);

    //Creamos una cita
    @POST("crearcita")
    Call<ResponseVehiculo> setCita(@Body CearCitaRequest resquest);


    //Guardar una publicacion
    @POST("savefavpub")
    Call<ResponseVehiculo> setGuardados(@Body Guardados resquest);

    //Resgistrar un nuevo usuario
    @POST("registrar")
    Call<RegisterResponse> setRegistreUser(@Body User resquest);

    //Resgistrar una nueva publicacion
    @POST("addcar")
    Call<ResponseVehiculo> setPublicaction(@Body CarRequest resquest);

    //Resgistrar una nueva publicacion
    @POST("addmot")
    Call<ResponseVehiculo> setPublicactionMoto(@Body MotoRequest resquest);

    //Iniciar sesióon usuario
    @POST("login")
    Call<ResponseLogin> setLoginUser(@Body Login resquest);

    //Obtenemos información de un usuario
    @GET("clientes/{user_id}")
    Call<List<ResponseSession>> getInfoUser(@Path("user_id") String user_id);

    //Obtenemos información de carros
    @GET("publicacionescarros")
    Call<Carros> getAllCars();

    //Obtenemos información de motos
    @GET("publicacionesmotos")
    Call<Motos> getAllMotos();

    //Obtenemos información de un carro
    @GET("publicacionescarros/{car_id}")
    Call<Carros> getInfCar(@Path("car_id") int car_id);

    //Obtenemos publicaciones guardadas
    @GET("getsavedpub/{user_id}")
    Call<List<ResponseGuardados>> getSaves(@Path("user_id") String user_id);

    //Obtenemos autocompletar de las marcas de carros
    @GET("autocompletebrandcar")
    Call<AutocompletarMarcasResponse> getBrands(@Query("nombre") String nombre);

    //Obtenemos autocompletar de los estados VENEZUELA
    @GET("autocompleteedosvzla")
    Call<AutocompletarEstados> getEstadosVenezuela(@Query("nombre") String nombre);

    //Obtenemos autocompletar de los estados VENEZUELA
    @GET("autocompleteedoscolo")
    Call<AutocompletarEstados> getEstadosColombia(@Query("nombre") String nombre);

    //Obtenemos autocompletar de los estados VENEZUELA
    @GET("autocompleteedospana")
    Call<AutocompletarEstados> getEstadosPanama(@Query("nombre") String nombre);

    //Obtenemos información de una operacin
    @GET("getnegociacion/{id}")
    Call<List<EstatusOperacionResponse>> getEstatusOpreacion(@Path("id") String id);

    //Obtenemos listados de concewsionarios
    @GET("getconse")
    Call<ConcesionarioResponse> getConcesionarios();

    //Obtenemos imagenes del home
    @GET("getimageshomeslider")
    Call<ImagesHomeResponse> getHomeImages();

    //Obtenemos las solicitudes relacionadas a un concesionario
    @GET("operacionesdeconse/{id}")
    Call <OperacionesConcesionario> getSolicitudes(@Path("id") String id);

    //Obtenemos las operaciones d eun usuario
    @GET("getnegociacionfromuser/{id}")
    Call <OperacionesUsuario> getOpreracionesFromUusuario(@Path("id") String id);

    //obtenemos publicaciones destacadas
    @GET("getall")
    Call <Publicaciones> getImagesHome();

    //obtenemos autocompletar modelos a partir de la marca
    @GET("autocompletemodelcar")
    Call<AutocompletarModelsResponse> getModels(@Query("brand") String brand, @Query("model") String model);

}
