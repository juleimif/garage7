package com.tnb.app.garage7.signup;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tnb.app.garage7.BuildConfig;
import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.City;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.network.request.TokenRequest;
import com.tnb.app.garage7.data.network.request.UserRequest;
import com.tnb.app.garage7.data.network.response.UserResult;
import com.tnb.app.garage7.data.vo.Resource;
import com.tnb.app.garage7.utils.AbsentLiveData;

import java.util.List;

import javax.inject.Inject;

public class SignupViewModel extends ViewModel {

    private AppRepository repository;

    private LiveData<Resource<List<City>>> citiesLiveData;

    private LiveData<UserResult> signedUp;

    private LiveData<Token> token;

    @Inject
    public SignupViewModel(@NonNull AppRepository repository) {
        this.repository = repository;
    }

    public void init(String countryId) {
        citiesLiveData = repository.getCitiesByName(countryId, "");
    }

    public LiveData<Resource<List<City>>> getCitiesLiveData() {
        return citiesLiveData;
    }

    public LiveData<UserResult> createNewUser(UserRequest userRequest) {
       signedUp = repository.signUp(userRequest);
       return signedUp;
    }

    public LiveData<Token> requestToken(String email, String password) {
        TokenRequest tokenRequest = new TokenRequest("password",
                BuildConfig.GARAGE7_API_CLIENT_ID,
                BuildConfig.GARAGE7_API_CLIENT_SECRET,
                email, password, "*");
        token = repository.requestToken(tokenRequest);
        return token;
    }

    public LiveData<Token> getToken() {
        if (token == null) {
            return AbsentLiveData.create();
        }
        return token;
    }

}