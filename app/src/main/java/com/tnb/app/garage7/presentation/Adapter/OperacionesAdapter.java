package com.tnb.app.garage7.presentation.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tnb.app.garage7.R;
import com.tnb.app.garage7.domain.model.Constants;
import com.tnb.app.garage7.domain.model.response.OperacionesUsuarioResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OperacionesAdapter extends RecyclerView.Adapter<OperacionesAdapter.ViewHolder> {

    private PublicationItemClick mItemClickListener;
    private  List<OperacionesUsuarioResponse> operacionesList;
    private Context context;

    public OperacionesAdapter(Context applicationContext, List<OperacionesUsuarioResponse> publicacionsList, PublicationItemClick brokerItemClick) {
        this.operacionesList =  publicacionsList;
        mItemClickListener = (PublicationItemClick) brokerItemClick;
        this.context = applicationContext;
    }


    @Override
    public OperacionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.card_view_operaciones, parent, false);
                return new ViewHolder(view);
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OperacionesUsuarioResponse currentItem = operacionesList.get(position);
        OperacionesAdapter.ViewHolder viewHolder = (OperacionesAdapter.ViewHolder) holder;
        viewHolder.tv_name_concesionario.setText(currentItem.getConseName());
        viewHolder.tv_name_car.setText(currentItem.getBrandC());
        viewHolder.tv_price.setText(currentItem.getPriceC() + " VEF");


        switch (currentItem.getIs()){
            case 1:
                viewHolder.tv_type_operation.setText("COMPRA");
                switch (currentItem.getStatusNegociacion()){
                    case "1":
                        viewHolder.tv_status_operation.setText(Constants.SOLICITANDO_CITA);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);
                        break;
                    case "2":
                        viewHolder.tv_status_operation.setText(Constants.CITA_ACEPTADA);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "3":
                        viewHolder.tv_status_operation.setText(Constants.COMPRA_CONFIRMADA);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "4":
                        viewHolder.tv_status_operation.setText(Constants.PAGO_EN_PROCESO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);
                        break;
                    case "5":
                        viewHolder.tv_status_operation.setText(Constants.REGISTRO_DE_PAGO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "6":
                        viewHolder.tv_status_operation.setText(Constants.ENTREGA_DE_VEHICULO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "7":
                        viewHolder.tv_status_operation.setText(Constants.COMPLETADA);
                        viewHolder.tv_status_operation.setTextColor(Color.GREEN);

                        break;
                    case "10":
                        viewHolder.tv_status_operation.setText(Constants.CAMNCELADA);
                        viewHolder.tv_status_operation.setTextColor(Color.RED);

                        break;
                }
                break;
            case 2:
                viewHolder.tv_type_operation.setText("VENTA");
                switch (currentItem.getStatusNegociacion()){
                    case "1":
                        viewHolder.tv_status_operation.setText(Constants.CITA_CONCESIONARIO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);
                        break;
                    case "2":
                        viewHolder.tv_status_operation.setText(Constants.REVISION);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);
                        break;
                    case "3":
                        viewHolder.tv_status_operation.setText(Constants.COMPRA_CONFIRMADA);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "4":
                        viewHolder.tv_status_operation.setText(Constants.COMPRA_CONFIRMADA);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "5":
                        viewHolder.tv_status_operation.setText(Constants.REGISTRO_DE_PAGO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "6":
                        viewHolder.tv_status_operation.setText(Constants.ENTREGA_DE_VEHICULO);
                        viewHolder.tv_status_operation.setTextColor(Color.BLUE);

                        break;
                    case "7":
                        viewHolder.tv_status_operation.setText(Constants.COMPLETADA);
                        viewHolder.tv_status_operation.setTextColor(Color.GREEN);

                        break;
                    case "10":
                        viewHolder.tv_status_operation.setText(Constants.CAMNCELADA);
                        viewHolder.tv_status_operation.setTextColor(Color.RED);

                        break;
                }
                break;

        }
    }

    @Override
    public int getItemCount() {

        return operacionesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        @BindView(R.id.iv_car) ImageView iv_car;
        @BindView(R.id.tv_type_operation) TextView tv_type_operation;
        @BindView(R.id.tv_name_car) TextView tv_name_car;
        @BindView(R.id.tv_price) TextView tv_price;
        @BindView(R.id.tv_name_concesionario) TextView tv_name_concesionario;
        @BindView(R.id.tv_status_operation) TextView tv_status_operation;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
             mItemClickListener.onBrokerClick(operacionesList.get(position));
        }

    }

    public interface PublicationItemClick {
        void onBrokerClick(OperacionesUsuarioResponse clickedBroker);
    }


}
