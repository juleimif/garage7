package com.tnb.app.garage7.data.network.response;

import com.google.gson.annotations.SerializedName;
import com.tnb.app.garage7.data.entity.User;

public class UserResult {

    @SerializedName(value = "data")
    private User user;

    public UserResult(User user) {
        this.user = user;
    }

    public User user() {
        return user;
    }

}
