package com.tnb.app.garage7.api;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.tnb.app.garage7.data.network.ApiResponse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Test de respuestas genericas de la api
 */
@RunWith(JUnit4.class)
public class ApiResponseTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();
    @Test
    public void exception() {
        Exception exception = new Exception("foo");
        ApiResponse<String> apiResponse = new ApiResponse<>(exception);
        assertThat(apiResponse.body, nullValue());
        assertThat(apiResponse.code, is(500));

    }

    @Test
    public void success() {
        ApiResponse<String> apiResponse = new ApiResponse<>(Response.success("foo"));
        assertThat(apiResponse.code, is(200));
        assertThat(apiResponse.body, is("foo"));

    }


}
