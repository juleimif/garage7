package com.tnb.app.garage7.api;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.tnb.app.garage7.data.entity.Brand;
import com.tnb.app.garage7.data.entity.Car;
import com.tnb.app.garage7.data.entity.City;
import com.tnb.app.garage7.data.entity.Color;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.data.entity.Displacement;
import com.tnb.app.garage7.data.entity.Favorite;
import com.tnb.app.garage7.data.entity.FavoriteCar;
import com.tnb.app.garage7.data.entity.FavoriteMotorcycle;
import com.tnb.app.garage7.data.entity.Garage;
import com.tnb.app.garage7.data.entity.Model;
import com.tnb.app.garage7.data.entity.Motorcycle;
import com.tnb.app.garage7.data.entity.PhotoUrl;
import com.tnb.app.garage7.data.entity.Token;
import com.tnb.app.garage7.data.entity.Transaction;
import com.tnb.app.garage7.data.entity.TransactionState;
import com.tnb.app.garage7.data.entity.VehicleType;
import com.tnb.app.garage7.data.network.request.GarageRequest;
import com.tnb.app.garage7.data.network.request.TransactionRequest;
import com.tnb.app.garage7.data.network.request.UpdateTransactionRequest;
import com.tnb.app.garage7.data.network.request.UserRequest;
import com.tnb.app.garage7.data.network.response.CarResult;
import com.tnb.app.garage7.data.network.response.DeletedResult;
import com.tnb.app.garage7.data.network.response.FavoriteCarsResponse;
import com.tnb.app.garage7.data.network.response.FavoriteMotorcyclesResponse;
import com.tnb.app.garage7.data.network.response.FavoriteResult;
import com.tnb.app.garage7.data.network.response.GarageListResult;
import com.tnb.app.garage7.data.network.response.GarageResponse;
import com.tnb.app.garage7.data.network.response.MotorcycleListResult;
import com.tnb.app.garage7.data.network.response.MotorcycleResult;
import com.tnb.app.garage7.data.network.response.PhotoUrlListResult;
import com.tnb.app.garage7.data.network.response.StateResponse;
import com.tnb.app.garage7.data.network.response.TransactionListResult;
import com.tnb.app.garage7.data.network.response.TransactionResponse;
import com.tnb.app.garage7.data.network.response.UpdateTransactionResponse;
import com.tnb.app.garage7.data.network.response.UpdatedResult;
import com.tnb.app.garage7.data.network.response.UploadImageResult;
import com.tnb.app.garage7.data.network.request.CarRequest;
import com.tnb.app.garage7.data.network.request.FavoriteRequest;
import com.tnb.app.garage7.data.network.request.MotorcycleRequest;
import com.tnb.app.garage7.data.entity.User;
import com.tnb.app.garage7.data.network.response.BrandListResult;
import com.tnb.app.garage7.data.network.response.CarListResult;
import com.tnb.app.garage7.data.network.response.CityListResult;
import com.tnb.app.garage7.data.network.response.ColorListResult;
import com.tnb.app.garage7.data.network.response.CountryListResult;

import com.tnb.app.garage7.data.network.response.DisplacementListResult;
import com.tnb.app.garage7.data.network.response.InputError;
import com.tnb.app.garage7.data.network.response.ModelListResult;
import com.tnb.app.garage7.data.network.response.UserResult;
import com.tnb.app.garage7.data.network.Webservice;
import com.tnb.app.garage7.data.network.request.TokenRequest;
import com.tnb.app.garage7.data.util.LiveDataCallAdapterFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import okio.BufferedSource;
import okio.Okio;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tnb.app.garage7.utils.AppConstants.*;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static com.tnb.app.garage7.api.util.LiveDataTestUtil.getValue;

@RunWith(JUnit4.class)
public class WebserviceTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();



    private Webservice service;
    private MockWebServer mockWebServer;

    @Before
    public void createdService()  {
        mockWebServer = new MockWebServer();
        service = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(Webservice.class);
    }

    @After
    public void stopService() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void testGetCountries()  throws IOException, InterruptedException {
        enqueueResponse("countries.json", 200);
        Map<String, String> params = new HashMap<>();
        CountryListResult countries = getValue(service.getCountries(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/countries"));

        assertNotNull(countries);

        List<Country> countryList = countries.getResults();
        assertTrue(countryList.size() > 0);
        assertEquals("French Polynesia", countryList.get(0).country);
        assertEquals("Peru", countryList.get(1).country);
    }

    @Test
    public void testGetBrands()  throws IOException, InterruptedException {
        enqueueResponse("brands.json", 200);
        Map<String, String> params = new HashMap<>();
        BrandListResult brands = getValue(service.getBrands(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/brands"));

        assertNotNull(brands);

        List<Brand> brandList = brands.getResults();
        assertTrue(brandList.size() > 0);
        assertEquals("Bednar and Sons", brandList.get(0).brand);
        assertEquals("Keebler PLC", brandList.get(1).brand);
        assertEquals("Stanton PLC", brandList.get(2).brand);
    }

    @Test
    public void testGetBrandsByName()  throws IOException, InterruptedException {
        enqueueResponse("brandsByName.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("brand", "Bed");
        BrandListResult brands = getValue(service.getBrands(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/brands?brand=Bed"));

        assertNotNull(brands);

        List<Brand> brandList = brands.getResults();
        assertTrue(brandList.size() == 1);
        assertEquals("Bednar and Sons", brandList.get(0).brand);
    }

    @Test
    public void testGetColor()  throws IOException, InterruptedException {
        enqueueResponse("colors.json", 200);
        Map<String, String> params = new HashMap<>();
        ColorListResult colors = getValue(service.getColors(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/colors"));

        assertNotNull(colors);

        List<Color> colorList = colors.getResults();
        assertTrue(colorList.size() > 0);
        assertEquals("Aquamarine", colorList.get(0).color);
        assertEquals("Aquamarine light", colorList.get(1).color);
        assertEquals("DarkKhaki", colorList.get(2).color);
    }

    @Test
    public void testGetColorByName()  throws IOException, InterruptedException {
        enqueueResponse("colorsByName.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("color", "Aqua");
        ColorListResult colors = getValue(service.getColors(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/colors?color=Aqua"));

        assertNotNull(colors);

        List<Color> colorList = colors.getResults();
        assertTrue(colorList.size() == 2);
        assertEquals("Aquamarine", colorList.get(0).color);
        assertEquals("Aquamarine light", colorList.get(1).color);
    }

    @Test
    public void testGetDisplacement()  throws IOException, InterruptedException {
        enqueueResponse("displacements.json", 200);
        Map<String, String> params = new HashMap<>();
        DisplacementListResult displacements = getValue(service.getDisplacements(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/displacements"));

        assertNotNull(displacements);

        List<Displacement> displacementsList = displacements.getResults();
        assertTrue(displacementsList.size() > 0);
        assertEquals("125 cc", displacementsList.get(0).displacement);
        assertEquals("175 cc", displacementsList.get(1).displacement);
        assertEquals("250 cc", displacementsList.get(2).displacement);
    }

    @Test
    public void testGetDisplacementByName()  throws IOException, InterruptedException {
        enqueueResponse("displacementsByName.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("displacement", "1");
        DisplacementListResult displacements = getValue(service.getDisplacements(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/displacements?displacement=1"));

        assertNotNull(displacements);

        List<Displacement> displacementsList = displacements.getResults();
        assertTrue(displacementsList.size() == 2);
        assertEquals("125 cc", displacementsList.get(0).displacement);
        assertEquals("175 cc", displacementsList.get(1).displacement);
    }

    @Test
    public void testGetCities() throws IOException, InterruptedException {
        enqueueResponse("cities.json", 200);
        String countryId = "2";
        Map<String, String> params = new HashMap<>();
        CityListResult cities = getValue(service.getCities(countryId, params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/countries/2/cities"));

        assertNotNull(cities);

        List<City> cityList = cities.getResults();
        assertTrue(cityList.size() > 0);
        assertEquals("Alenaton", cityList.get(0).city);
        assertEquals("Donnellystad", cityList.get(1).city);
    }

    @Test
    public void testGetCitiesByName() throws IOException, InterruptedException {
        enqueueResponse("citiesByName.json", 200);
        String countryId = "2";
        Map<String, String> params = new HashMap<>();
        params.put("city", "Lake");

        CityListResult cities = getValue(service.getCities(countryId, params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/countries/2/cities?city=Lake"));

        assertNotNull(cities);

        List<City> cityList = cities.getResults();
        assertTrue(cityList.size() > 0);
        assertEquals("Lake Edd", cityList.get(0).city);
        assertEquals("Lake Kenyattafort", cityList.get(1).city);
    }

    @Test
    public void testGetModels() throws IOException, InterruptedException {
        enqueueResponse("models.json", 200);
        String brandId = "1";
        Map<String, String> params = new HashMap<>();
        ModelListResult models = getValue(service.getModels(brandId, params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/brands/1/models"));

        assertNotNull(models);

        List<Model> modelList = models.getResults();
        assertTrue(modelList.size() > 0);
        assertEquals("Annamaehaven8967", modelList.get(0).model);
        assertEquals("Armstrongstad4", modelList.get(1).model);
    }

    @Test
    public void testGetModelsByName() throws IOException, InterruptedException {
        enqueueResponse("modelsByName.json", 200);
        String brandId = "1";
        Map<String, String> params = new HashMap<>();
        params.put("model", "Kieraborough");
        ModelListResult models = getValue(service.getModels(brandId, params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/brands/1/models?model=Kieraborough"));

        assertNotNull(models);

        List<Model> modelList = models.getResults();
        assertTrue(modelList.size() == 1);
        assertEquals("Kieraborough", modelList.get(0).model);
    }

    @Test
    public void testGetCars() throws IOException, InterruptedException {
        enqueueResponse("cars.json", 200);

        Map<String, String> params = new HashMap<>();
        CarListResult cars = getValue(service.getCars(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars"));

        assertNotNull(cars);

        List<Car> carList = cars.getResults();
        assertTrue(carList.size() > 0);
        assertEquals("1", carList.get(0).id);
        assertEquals("1", carList.get(0).postedBy);
        assertEquals("modelo", carList.get(0).model);
        assertEquals("marca", carList.get(0).brand);
    }

    @Test
    public void testGetUSerCars() throws IOException, InterruptedException {
        enqueueResponse("cars.json", 200);

        Map<String, String> params = new HashMap<>();
        CarListResult cars = getValue(service.getUserCars("1", params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/cars"));

        assertNotNull(cars);

        List<Car> carList = cars.getResults();
        assertTrue(carList.size() > 0);
        assertEquals("1", carList.get(0).id);
        assertEquals("1", carList.get(0).postedBy);
        assertEquals("modelo", carList.get(0).model);
        assertEquals("marca", carList.get(0).brand);
    }

    @Test
    public void testGetCar() throws IOException, InterruptedException {
        enqueueResponse("car.json", 200);
        String carId = "1";
        CarResult carResult = getValue(service.getCar(carId)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars/1"));

        assertNotNull(carResult);

        Car car = carResult.car();

        assertEquals("1", car.id);
        assertEquals("1", car.postedBy);
        assertEquals("modelo", car.model);
        assertEquals("marca", car.brand);
    }

    @Test
    public void testGetCarPhotoUrls() throws IOException, InterruptedException {
        enqueueResponse("carPhotoUrls.json", 200);
        String carId = "1";
        PhotoUrlListResult carPhotoUrlsResult = getValue(service.getCarPhotoUrls(carId)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars/1/photos"));

        assertNotNull(carPhotoUrlsResult);

        List<PhotoUrl> urls = carPhotoUrlsResult.getResults();
        assertTrue(urls.size() == 1);
        assertEquals("users1/cars1/p6KojuI9W3ThHaZFueMYLBdKBold0cm4lJNxRwog.png", urls.get(0).photoUrl);
    }

    @Test
    public void testGetGarages() throws IOException, InterruptedException {
        enqueueResponse("garages.json", 200);
        Map<String, String> params = new HashMap<>();
        GarageListResult garages = getValue(service.getGarages(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages"));

        assertNotNull(garages);

        List<Garage> garageList = garages.getResults();
        assertTrue(garageList.size() == 2);
        assertEquals("4", garageList.get(0).id);
        assertEquals("1", garageList.get(0).userId);
        assertEquals("62114-6050", garageList.get(0).postalZone);
        assertEquals("Rosellastad", garageList.get(0).city);
    }

    @Test
    public void testGetUserGarages() throws IOException, InterruptedException {
        enqueueResponse("garages.json", 200);

        GarageListResult garages = getValue(service.getUserGarages("1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/garages"));

        assertNotNull(garages);

        List<Garage> garageList = garages.getResults();
        assertTrue(garageList.size() == 2);
        assertEquals("4", garageList.get(0).id);
        assertEquals("1", garageList.get(0).userId);
        assertEquals("62114-6050", garageList.get(0).postalZone);
        assertEquals("Rosellastad", garageList.get(0).city);
    }

    @Test
    public void testGetGaragesByName() throws IOException, InterruptedException {
        enqueueResponse("garageByName.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("name", "Moore");
        GarageListResult garages = getValue(service.getGarages(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages?name=Moore"));

        assertNotNull(garages);

        List<Garage> garageList = garages.getResults();
        assertTrue(garageList.size() == 1);
        assertEquals("4", garageList.get(0).id);
        assertEquals("1", garageList.get(0).userId);
        assertEquals("Altenwerth, Moore and Bahringer", garageList.get(0).name);
        assertEquals("Rosellastad", garageList.get(0).city);
    }

    @Test
    public void testGetGarage() throws IOException, InterruptedException {
        enqueueResponse("garage.json", 200);

        GarageResponse garageResponse = getValue(service.getGarage("1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages/1"));

        assertNotNull(garageResponse);

        Garage garage = garageResponse.garage();

        assertEquals("1", garage.id);
        assertEquals("1", garage.userId);
        assertEquals("Conn PLC", garage.name);
        assertEquals("Ericamouth", garage.city);
    }

    @Test
    public void testGetUserGarage() throws IOException, InterruptedException {
        enqueueResponse("garage.json", 200);

        GarageResponse garageResponse = getValue(service.getUserGarage("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/garages/1"));

        assertNotNull(garageResponse);

        Garage garage = garageResponse.garage();

        assertEquals("1", garage.id);
        assertEquals("1", garage.userId);
        assertEquals("Conn PLC", garage.name);
        assertEquals("Ericamouth", garage.city);
    }

    @Test
    public void testGetGaragePhotoUrls() throws IOException, InterruptedException {
        enqueueResponse("carPhotoUrls.json", 200);
        String garageId = "1";
        PhotoUrlListResult garagePhotoUrlsResult = getValue(service.getGaragePhotoUrls(garageId)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages/1/photos"));

        assertNotNull(garagePhotoUrlsResult);

        List<PhotoUrl> urls = garagePhotoUrlsResult.getResults();
        assertTrue(urls.size() == 1);
        assertEquals("3", urls.get(0).id);
        assertEquals("users1/cars1/p6KojuI9W3ThHaZFueMYLBdKBold0cm4lJNxRwog.png", urls.get(0).photoUrl);
    }

    @Test
    public void testGetCarsByBrand() throws IOException, InterruptedException {
        enqueueResponse("carsByBrand.json", 200);

        Map<String, String> params = new HashMap<>();
        params.put("brand", "fiat");

        CarListResult cars = getValue(service.getCars(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars?brand=fiat"));

        assertNotNull(cars);

        List<Car> carList = cars.getResults();
        assertTrue(carList.size() > 0);
        assertEquals("fiat", carList.get(0).brand);
        assertEquals("fiat", carList.get(1).brand);
    }

    @Test
    public void testGetCarsByModel() throws IOException, InterruptedException {
        enqueueResponse("carsByModel.json", 200);

        Map<String, String> params = new HashMap<>();
        params.put("model", "uno");

        CarListResult cars = getValue(service.getCars(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars?model=uno"));

        assertNotNull(cars);

        List<Car> carList = cars.getResults();
        assertTrue(carList.size() > 0);
        assertEquals("uno", carList.get(0).model);
        assertEquals("uno", carList.get(1).model);
    }



    @Test
    public void testCreateUser() throws IOException, InterruptedException {
        enqueueResponse("newUser.json", 200);
        UserRequest userRequest = new UserRequest("Jesus", "Rivas", "Masculino",
                "1", "1", "01234567890", "jrivas@correo.com",
                "secret", "secret", "123456789");

        UserResult userResult = getValue(service.createUser(userRequest)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users"));

        User user = userResult.user();
        assertEquals("Jesus", user.getFirstName());
    }

    @Test
    public void testUpdateUser() throws IOException, InterruptedException {
        enqueueResponse("updated.json", 200);
        UserRequest userRequest = new UserRequest("Jesus", "Rivas", "Masculino",
                "1", "1", "01234567890", "jrivas@correo.com",
                "", "", "");
        UpdatedResult result = getValue(service.updateUser("1", userRequest)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1"));

        assertEquals(true, result.isUpdated());
    }

    @Test
    public void testCreateNewFavorite() throws IOException, InterruptedException {
        enqueueResponse("favoriteCarResult.json", 200);
        FavoriteRequest favoriteRequest = new FavoriteRequest("200", "carro");
        FavoriteResult favoriteResult = getValue(service.createFavorite("1", favoriteRequest)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/favorites"));

        Favorite favorite = favoriteResult.favorite();

        assertEquals("200", favorite.favoriteId);
        assertEquals("Carro", favorite.type);
    }

    @Test
    public void testGetCarFavorites() throws IOException, InterruptedException {
        enqueueResponse("favorites.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("offset", "0");
        params.put("limit", "10");
        params.put("type", "carro");

        FavoriteCarsResponse cars = getValue(service.getFavoriteCars("1", params)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/favorites?offset=0&limit=10&type=carro"));

        assertNotNull(cars);

        List<FavoriteCar> carList = cars.favoriteCars();
        assertTrue(carList.size() > 0);
        assertEquals("modelo", carList.get(0).car().model);
        assertEquals("Id et officia odit eligendi.", carList.get(0).car().description);
    }


    @Test
    public void testGetMotorcycleFavorites() throws IOException, InterruptedException {
        enqueueResponse("favoritesMotorcycles.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("offset", "0");
        params.put("limit", "10");
        params.put("type", "moto");

        FavoriteMotorcyclesResponse motorcycles = getValue(service.getFavoriteMotorcycles("1", params)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/favorites?offset=0&limit=10&type=moto"));

        assertNotNull(motorcycles);

        List<FavoriteMotorcycle> motorcycleList = motorcycles.favoriteMotorcycles();
        assertTrue(motorcycleList.size() > 0);
        assertEquals("modelo", motorcycleList.get(0).motorcycle().model);
        assertEquals("Id et officia odit eligendi.", motorcycleList.get(0).motorcycle().description);
    }

    @Test
    public void testPostNewCarErrors() throws IOException, InterruptedException {
        enqueueResponse("postCarErrors.json", 422);
        CarRequest carRequest = new CarRequest("", "", "", "", "",
                "", "", "", "", "", "", "",
                "", false);
        InputError inputErrors = getValue(service.postNewCar("1", carRequest)).inputErrors;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/cars"));

        assertEquals("El campo modelo es obligatorio.",
                inputErrors.errors().modelErrors().get(0));
        assertEquals("El campo combustible es obligatorio.",
                inputErrors.errors().fuelErrors().get(0));
        assertEquals("El campo número de puertas es obligatorio.",
                inputErrors.errors().doorsErrors().get(0));
    }

    @Test
    public void testCreateUserErrorMessages() throws IOException, InterruptedException {
        enqueueResponse("newUserFirstNameErrors.json", 422);
        UserRequest userRequest = new UserRequest("", "", "",
                "", "", "", "",
                "", "", "");

        InputError inputErrors = getValue(service.createUser(userRequest)).inputErrors;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users"));

        assertEquals("El campo nombre es obligatorio.",
                inputErrors.errors().firstNameErrors().get(0));
        assertEquals("El campo apellido es obligatorio.",
                inputErrors.errors().lastNameErrors().get(0));
        assertEquals("El campo género es obligatorio.",
                inputErrors.errors().genderErrors().get(0));
        assertEquals("El campo cédula es obligatorio.",
                inputErrors.errors().idNumberErrors().get(0));
        assertEquals("El campo teléfono es obligatorio.",
                inputErrors.errors().phoneErrors().get(0));
        assertEquals("El campo correo electrónico es obligatorio.",
                inputErrors.errors().emailErrors().get(0));
        assertEquals("El campo contraseña es obligatorio.",
                inputErrors.errors().passwordErrors().get(0));
        assertEquals("El campo ciudad es obligatorio.",
                inputErrors.errors().cityIdErrors().get(0));
        assertEquals("El campo país es obligatorio.",
                inputErrors.errors().countryIdErrors().get(0));
    }

    @Test
    public void testGetUserInfo() throws IOException, InterruptedException {
        enqueueResponse("user.json", 200);
        String userId = "1";
        UserResult userResult = getValue(service.getUserInfo(userId)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1"));

        assertNotNull(userResult);

        User user = userResult.user();

        assertEquals("Cornelius Hyatt", user.getFirstName());
        assertEquals("Michelle Hettinger", user.getLastName());
        assertEquals("1234567890", user.getIdNumber());
        assertEquals("Masculino", user.getGender());
    }

    @Test
    public void testGetMotorcycles() throws IOException, InterruptedException {
        enqueueResponse("motorcycles.json", 200);

        Map<String, String> params = new HashMap<>();
        MotorcycleListResult motorcycles = getValue(service.getMotorcycles(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles"));

        assertNotNull(motorcycles);

        List<Motorcycle> motorcycleList = motorcycles.getResults();
        assertTrue(motorcycleList.size() > 0);
        assertEquals("1", motorcycleList.get(0).id);
        assertEquals("2", motorcycleList.get(0).postedBy);
        assertEquals("modelo", motorcycleList.get(0).model);
        assertEquals("marca", motorcycleList.get(0).brand);
    }

    @Test
    public void testGetUserMotorcycles() throws IOException, InterruptedException {
        enqueueResponse("motorcycles.json", 200);

        Map<String, String> params = new HashMap<>();
        MotorcycleListResult motorcycles = getValue(service.getUserMotorcycles("1", params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/motorcycles"));

        assertNotNull(motorcycles);

        List<Motorcycle> motorcycleList = motorcycles.getResults();
        assertTrue(motorcycleList.size() > 0);
        assertEquals("1", motorcycleList.get(0).id);
        assertEquals("2", motorcycleList.get(0).postedBy);
        assertEquals("modelo", motorcycleList.get(0).model);
        assertEquals("marca", motorcycleList.get(0).brand);
    }

    @Test
    public void testGetMotorcyclesByBrand() throws IOException, InterruptedException {
        enqueueResponse("motorcyclesByBrand.json", 200);

        Map<String, String> params = new HashMap<>();
        params.put("brand", "Suzuki");
        MotorcycleListResult motorcycles = getValue(service.getMotorcycles(params)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles?brand=Suzuki"));

        assertNotNull(motorcycles);

        List<Motorcycle> motorcycleList = motorcycles.getResults();
        assertTrue(motorcycleList.size() == 2);
        assertEquals("1", motorcycleList.get(0).id);
        assertEquals("2", motorcycleList.get(0).postedBy);
        assertEquals("modelo", motorcycleList.get(0).model);
        assertEquals("Suzuki", motorcycleList.get(0).brand);
        assertEquals("Suzuki", motorcycleList.get(1).brand);
    }

    @Test
    public void testGetMotorcycle() throws IOException, InterruptedException {
        enqueueResponse("motorcycle.json", 200);

        MotorcycleResult motorcycleResult = getValue(service.getMotorcycle("3")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles/3"));

        assertNotNull(motorcycleResult);

        Motorcycle motorcycle = motorcycleResult.motorcycle();

        assertEquals("3", motorcycle.id);
        assertEquals("2", motorcycle.postedBy);
        assertEquals("modelo", motorcycle.model);
        assertEquals("marca", motorcycle.brand);
    }

    @Test
    public void testGetMotorcyclePhotoUrls() throws IOException, InterruptedException {
        enqueueResponse("carPhotoUrls.json", 200);
        String motorcycleId = "1";
        PhotoUrlListResult motorcyclePhotoUrlsResult = getValue(service.getMotorcyclePhotoUrls(motorcycleId)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles/1/photos"));

        assertNotNull(motorcyclePhotoUrlsResult);

        List<PhotoUrl> urls = motorcyclePhotoUrlsResult.getResults();
        assertTrue(urls.size() == 1);
        assertEquals("3", urls.get(0).id);
        assertEquals("users1/cars1/p6KojuI9W3ThHaZFueMYLBdKBold0cm4lJNxRwog.png", urls.get(0).photoUrl);
    }

    @Test
    public void testGetTransactionAsSeller() throws IOException, InterruptedException {
        enqueueResponse("transactions.json", 200);
        Map<String, String> params = new HashMap<>();
        params.put("role", "seller");

        TransactionListResult listResult = getValue(service.getTransactions("1", params)).body;


        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/transactions?role=seller"));
        assertNotNull(listResult);

        List<Transaction> transactions = listResult.getResults();
        assertTrue(transactions.size() == 1);
        assertEquals("Cita concesionario", transactions.get(0).getState());
    }

    @Test
    public void testGetTransaction() throws IOException, InterruptedException {
        enqueueResponse("transaction.json", 200);
        TransactionResponse transactionResponse = getValue(service.getTransaction("1", "1")).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(transactionResponse);
        Transaction transaction = transactionResponse.transaction();
        assertEquals("Cita concesionario", transaction.getState());
    }

    @Test
    public void testPostNewCar() throws IOException, InterruptedException {
        enqueueResponse("car.json", 201);
        CarRequest carRequest = new CarRequest("modelo" , "marca", "Gasolina", "color",
                "ciudad", "country", "1", "Architecto dicta iusto expedita id.",
                "1000", "4", "2004", "20000", "Manual",
                true);
        CarResult carResult = getValue(service.postNewCar("1", carRequest)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/cars"));
        assertNotNull(carResult);
        Car car = carResult.car();

        assertEquals("1", car.id);
        assertEquals("1", car.postedBy);
        assertEquals("modelo", car.model);
        assertEquals("marca", car.brand);
    }

    @Test
    public void testUpdatePostedCar() throws IOException, InterruptedException {
        enqueueResponse("updated.json", 200);
        CarRequest carRequest = new CarRequest("modelo" , "marca", "Gasolina", "color",
                "ciudad", "country", "1", "Architecto dicta iusto expedita id.",
                "1000", "4", "2004", "20000", "Manual",
                true);
        UpdatedResult updatedResult = getValue(service.updatePostedCar("1", "1", carRequest)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/cars/1"));

        assertTrue(updatedResult.isUpdated());
    }

    @Test
    public void testDeletePostedCar() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deletedResult = getValue(service.deletePostedCar("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/cars/1"));

        assertTrue(deletedResult.isDeleted());
    }

    @Test
    public void testCreateGarage() throws IOException, InterruptedException {
        enqueueResponse("garage.json", 200);
        GarageRequest garageRequest = new GarageRequest("concecionario", "1",
                "Caracas", "Caracas", "1221", "123456789",
                "123456789", "12456789", "jesus@correo.com", "",
                "Caracas", "Libertador", "principal", "1", "1");
        GarageResponse garageResponse = getValue(service.createGarage("1", garageRequest)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/garages"));
        assertNotNull(garageResponse);
        Garage garage = garageResponse.garage();
        assertEquals("1", garage.id);
        assertEquals("1234567890", garage.celphone);
        assertEquals("Ahmed Unions", garage.street);
    }

    @Test
    public void testDeleteFavorite() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deletedResult = getValue(service.deleteFavorite("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/favorites/1"));

        assertTrue(deletedResult.isDeleted());
    }

    @Test
    public void testDeleteGarage() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deletedResult = getValue(service.deleteGarage("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/garages/1"));

        assertTrue(deletedResult.isDeleted());
    }

    @Test
    public void testCreateTransaction() throws IOException, InterruptedException {
        enqueueResponse("transaction.json", 200);
        TransactionRequest transactionRequest = new TransactionRequest("1", "2", "1", VehicleType.MOTORCYCLE);
        TransactionResponse transactionResponse = getValue(service.createTransaction("1", transactionRequest)).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/transactions"));

        assertNotNull(transactionResponse);

        Transaction transaction = transactionResponse.transaction();
        assertEquals("1", transaction.getId());
        assertEquals("Cita concesionario", transaction.getState());
    }

    @Test
    public void testUpdatePostedMotorcycle() throws IOException, InterruptedException {
        enqueueResponse("updated.json", 201);
        MotorcycleRequest motorcycleRequest = new MotorcycleRequest("modelo", "marca", "color",
                "ciudad", "pais", "1",
                "Fuga omnis perspiciatis voluptates quo dolorum.", "1000", "2004",
                "20000", "displacement", true);
        UpdatedResult updatedResult = getValue(service.updatePostedMotorcycle("1", "1", motorcycleRequest)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/motorcycles/1"));

        assertTrue(updatedResult.isUpdated());
    }

    @Test
    public void testPostNewMotorcycle()  throws IOException, InterruptedException {
        enqueueResponse("motorcycle.json", 201);
        MotorcycleRequest motorcycleRequest = new MotorcycleRequest("modelo", "marca", "color",
                "ciudad", "pais", "1",
                "Fuga omnis perspiciatis voluptates quo dolorum.", "1000", "2004",
                "20000", "displacement", true);
        MotorcycleResult motorcycleResult = getValue(service.postNewMotorcycle("1", motorcycleRequest)).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/motorcycles"));
        Motorcycle motorcycle = motorcycleResult.motorcycle();
        assertEquals("3", motorcycle.id);
        assertEquals("2", motorcycle.postedBy);
        assertEquals("modelo", motorcycle.model);
        assertEquals("marca", motorcycle.brand);
    }

    @Test
    public void testUpdateTransactionDATE_SCHEDULED() throws IOException, InterruptedException {
        enqueueResponse("dateScheduled.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.DATE_SCHEDULED.toString());

        UpdateTransactionResponse update = getValue(service.updateTransaction("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Cita concesionario", update.state());
        assertEquals("nombre vendedor", update.seller().name);
        assertEquals("123456789", update.seller().phone);

        assertEquals("nombre comprador", update.buyer().name);
        assertEquals("987654321", update.buyer().phone);

    }

    @Test
    public void testUpdateTransactionPURCHASE_ACCEPTED() throws IOException, InterruptedException {
        enqueueResponse("purchaseAccepted.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.PURCHASE_ACCEPTED.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Compra aceptada", update.state);

    }

    @Test
    public void testUpdateTransactionPAYMENT_MADE() throws IOException, InterruptedException {
        enqueueResponse("paymentMade.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.PAYMENT_MADE.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Pago realizado", update.state);

    }

    @Test
    public void testUpdateTransactionVEHICLE_DELIVERED() throws IOException, InterruptedException {
        enqueueResponse("vehicleDelivered.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.VEHICLE_DELIVERED.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Vehículo entregado", update.state);

    }

    @Test
    public void testUpdateTransactionVEHICLE_RECEIVED() throws IOException, InterruptedException {
        enqueueResponse("vehicleReceived.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.VEHICLE_RECEIVED.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Vehículo recibido", update.state);

    }

    @Test
    public void testUpdateTransactionOPERATION_FINISHED() throws IOException, InterruptedException {
        enqueueResponse("operationFinished.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.OPERATION_FINISHED.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Operación finalizada", update.state);

    }

    @Test
    public void testUpdateTransactionCANCELED_BY_BUYER() throws IOException, InterruptedException {
        enqueueResponse("canceledByBuyer.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.CANCELED_BY_BUYER.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Cancelada por comprador", update.state);

    }

    @Test
    public void testUpdateTransactionCANCELED_BY_GARAGE() throws IOException, InterruptedException {
        enqueueResponse("canceledByGarage.json", 200);
        UpdateTransactionRequest state = new UpdateTransactionRequest(TransactionState.CANCELED_BY_GARAGE.toString());

        StateResponse update = getValue(service.updateTransactionState("1", "1", state)).body;
        RecordedRequest request = mockWebServer.takeRequest();

        assertThat(request.getPath(), is("/api/users/1/transactions/1"));
        assertNotNull(update);
        assertEquals("Cancelada por concesionario", update.state);

    }

    @Test
    public void testPostNewMotorcycleErrors() throws IOException, InterruptedException  {
        enqueueResponse("postMotorcycleErrors.json", 422);
        MotorcycleRequest motorcycleRequest = new MotorcycleRequest("", "", "",
                "", "", "",
                "", "", "",
                "", "", true);
        InputError inputErrors = getValue(service.postNewMotorcycle("1", motorcycleRequest)).inputErrors;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/motorcycles"));

        assertEquals("El campo cilindrada es obligatorio.", inputErrors.errors().displacementErrors().get(0));

    }

    @Test
    public void testDeletePostedMotorcycle() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deletedResult = getValue(service.deletePostedMotorcycle("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/users/1/motorcycles/1"));

        assertTrue(deletedResult.isDeleted());
    }

    @Test
    public void testUploadCarPhoto() throws IOException, InterruptedException {
        enqueueResponse("carPhotoCreated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());



        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UploadImageResult uir = getValue(service.uploadCarPhoto(mFile, "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars/1/photos"));

        assertEquals(true, uir.isCreated());
        assertEquals("users1/cars1/2ZlSS2dBrpGsNDsQR2rFmHpMeWW8y1AUNujJTyJm.png", uir.getPath());
    }

    @Test
    public void testUploadGaragePhoto() throws IOException, InterruptedException {
        enqueueResponse("carPhotoCreated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UploadImageResult uir = getValue(service.uploadGaragePhoto(mFile, "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages/1/photos"));

        assertEquals(true, uir.isCreated());
        assertEquals("users1/cars1/2ZlSS2dBrpGsNDsQR2rFmHpMeWW8y1AUNujJTyJm.png", uir.getPath());
    }

    @Test
    public void testUploadMotorcyclePhoto() throws IOException, InterruptedException {
        enqueueResponse("carPhotoCreated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UploadImageResult uir = getValue(service.uploadMotorcyclePhoto(mFile, "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles/1/photos"));

        assertEquals(true, uir.isCreated());
        assertEquals("users1/cars1/2ZlSS2dBrpGsNDsQR2rFmHpMeWW8y1AUNujJTyJm.png", uir.getPath());
    }

    @Test
    public void testDeleteCarPhoto() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deleted = getValue(service.deleteCarPhoto("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars/1/photos/1"));
        assertEquals(true, deleted.isDeleted());
    }

    @Test
    public void testDeleteGaragePhoto() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deleted = getValue(service.deleteGaragePhoto("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages/1/photos/1"));
        assertEquals(true, deleted.isDeleted());
    }

    @Test
    public void testDeleteMotorcyclePhoto() throws IOException, InterruptedException {
        enqueueResponse("deleted.json", 200);
        DeletedResult deleted = getValue(service.deleteMotorcyclePhoto("1", "1")).body;

        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles/1/photos/1"));
        assertEquals(true, deleted.isDeleted());
    }

    @Test
    public void testUpdateCarPhoto()  throws IOException, InterruptedException  {
        enqueueResponse("updated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UpdatedResult updated = getValue(service.updateCarPhoto(mFile, "1", "1")).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/cars/1/photos/1"));

        assertEquals(true, updated.isUpdated());
    }

    @Test
    public void testUpdateGaragePhoto()  throws IOException, InterruptedException  {
        enqueueResponse("updated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UpdatedResult updated = getValue(service.updateGaragePhoto(mFile, "1", "1")).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/garages/1/photos/1"));

        assertEquals(true, updated.isUpdated());
    }

    @Test
    public void testUpdateMotorcyclePhoto()  throws IOException, InterruptedException  {
        enqueueResponse("updated.json", 200);
        File file = new File(getClass().getClassLoader().getResource("images/default.png").getFile());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

        UpdatedResult updated = getValue(service.updateMotorcyclePhoto(mFile, "1", "1")).body;
        RecordedRequest request = mockWebServer.takeRequest();
        assertThat(request.getPath(), is("/api/motorcycles/1/photos/1"));

        assertEquals(true, updated.isUpdated());
    }

    private void enqueueResponse(String fileName, int responseCode) throws IOException {
        enqueueResponse(fileName, Collections.<String, String>emptyMap(), responseCode);
    }

    private void enqueueResponse(String fileName, Map<String, String> headers, int responseCode) throws IOException {
        InputStream inputStream = getClass().getClassLoader()
                .getResourceAsStream("api-response/" + fileName);

        BufferedSource source = Okio.buffer(Okio.source(inputStream));
        MockResponse mockResponse = new MockResponse();
        mockResponse.setResponseCode(responseCode);
        for (Map.Entry<String, String> header : headers.entrySet()) {
            mockResponse.addHeader(header.getKey(), header.getValue());
        }

        mockWebServer.enqueue(mockResponse
                .setBody(source.readString(StandardCharsets.UTF_8)));
    }

    private OkHttpClient getHttpClient(){
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }
}
