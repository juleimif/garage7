package com.tnb.app.garage7.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import com.tnb.app.garage7.data.AppRepository;
import com.tnb.app.garage7.data.entity.Country;
import com.tnb.app.garage7.data.network.ApiResponse;
import com.tnb.app.garage7.data.network.response.CountryListResult;
import com.tnb.app.garage7.signup.SignupViewModel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SignupViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Test
    public void testGetCountriesList() {
        /*AppRepository repository = Mockito.mock(AppRepository.class);
        SignupViewModel viewModel = new SignupViewModel(repository);
        Observer<ApiResponse<CountryListResult>> observer = Mockito.mock(Observer.class);

        MutableLiveData<ApiResponse<CountryListResult>> response = new MutableLiveData<>();

        List<Country> list = new ArrayList<>();
        list.add(new Country("1", "Venezuela", "es_VE", "BS.",
                "Km", ".", ","));
        list.add(new Country("2", "Colombia", "es_CO", "COP",
                "Km", ".", ","));
        Response<CountryListResult> rc = Response.success(new CountryListResult(list));
        ApiResponse apiResponse = new ApiResponse(rc);
        when(repository.getCountries()).thenReturn(response);


        viewModel.getCountries().observeForever(observer);
        viewModel.init();
        verify(observer).onChanged(apiResponse);*/
    }
}
