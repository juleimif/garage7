package com.tnb.app.garage7.util;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

import com.tnb.app.garage7.TestApp;

public class Garage7TestRunner extends AndroidJUnitRunner {
    @Override
    public Application newApplication(ClassLoader cl, String className,
                                      Context context)
            throws InstantiationException, IllegalAccessException,
            ClassNotFoundException {
        return super.newApplication(cl, TestApp.class.getName(), context);
    }
}
