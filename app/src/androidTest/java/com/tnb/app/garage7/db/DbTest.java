package com.tnb.app.garage7.db;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import com.tnb.app.garage7.data.db.GarageDb;

import org.junit.After;
import org.junit.Before;

public abstract class DbTest {

    protected GarageDb garageDb;

    @Before
    public void initDb() {
        garageDb = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(),
                GarageDb.class
        ).build();
    }

    @After
    public void close(){
        garageDb.close();
    }
}
