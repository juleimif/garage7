package com.tnb.app.garage7.db;

import com.tnb.app.garage7.data.entity.Country;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tnb.app.garage7.util.LiveDataTestUtil.getValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class CountryDaoTest extends DbTest {

    @Test
    public void testInsertAndRead() throws InterruptedException {
        Country country = new Country("1", "Venezuela", "es_VE", "Bs.",
                "Km", ".", ",");

        garageDb.countryDao().insert(country);
        List<Country> list = getValue(garageDb.countryDao().findAll());
        assertThat(list, notNullValue());
        assertThat(list.get(0).country, is("Venezuela"));

        list = getValue(garageDb.countryDao().findByName("ezue"));
        assertThat(list, notNullValue());
        assertThat(list.get(0).country, is("Venezuela"));
    }

    @Test
    public void testCreateIfNotExists_exists() throws InterruptedException {
        Country country = new Country("1", "Venezuela", "es_VE", "Bs.",
                "Km", ".", ",");
        garageDb.countryDao().insert(country);
        assertThat(garageDb.countryDao().createCountryIfNotExists(country), is( -1L));
    }

    @Test
    public void testCreateIfNotExists_doesNotExists() throws InterruptedException {
        Country country = new Country("1", "Venezuela", "es_VE", "Bs.",
                "Km", ".", ",");
        assertThat(garageDb.countryDao().createCountryIfNotExists(country), is( 1L));
    }

    @Test
    public void testDeleteAll() throws InterruptedException {
        Country country = new Country("1", "Venezuela", "es_VE", "Bs.",
                "Km", ".", ",");
        garageDb.countryDao().insert(country);
        garageDb.countryDao().deleteAll();
        List<Country> list = getValue(garageDb.countryDao().findAll());
        assertThat(list.size(), is(0));
    }

    @Test
    public void testInsertList() throws InterruptedException {
        Country country1 = new Country("1", "Venezuela", "es_VE", "Bs.",
                "Km", ".", ",");
        Country country2 = new Country("2", "Colombia", "es_CO", "Bs.",
                "Km", ".", ",");
        List<Country> toInsert = new ArrayList<>();
        toInsert.add(country1);
        toInsert.add(country2);
        garageDb.countryDao().insertCountries(toInsert);
        List<Country> list = getValue(garageDb.countryDao().findAll());
        assertThat(list.size(), is(2));
    }
}
