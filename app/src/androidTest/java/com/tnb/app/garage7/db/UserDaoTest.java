package com.tnb.app.garage7.db;

import android.support.annotation.NonNull;

import com.tnb.app.garage7.data.entity.User;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tnb.app.garage7.util.LiveDataTestUtil.getValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class UserDaoTest extends DbTest {

    @Test
    public void testInsertAndRead() throws InterruptedException {
        User user = getUser("1");
        garageDb.userDao().insert(user);
        User findById = getValue(garageDb.userDao().searchUserById("1"));
        assertThat(findById, notNullValue());
        assertThat(findById.getCity(), is("Caracas"));
        assertThat(findById.getCountry(), is("Venezuela"));

        User findByEmail = getValue(garageDb.userDao().searchUserByEmail("jesus@correo.com"));
        assertThat(findByEmail, notNullValue());
        assertThat(findById.getFirstName(), is("Jesus"));
        assertThat(findById.getEmail(), is("jesus@correo.com"));

    }

    @Test
    public void testCreateIfNotExists_exists() throws InterruptedException {
        User user = getUser("1");
        garageDb.userDao().insert(user);
        assertThat(garageDb.userDao().createUserIfNotExists(user), is( -1L));
    }

    @Test
    public void testCreateIfNotExists_doesNotExists() throws InterruptedException {
        User user = getUser("1");
        assertThat(garageDb.userDao().createUserIfNotExists(user), is( 1L));
    }

    @Test
    public void testDeleteAll() throws InterruptedException {
        User user = getUser("1");
        garageDb.userDao().insert(user);
        garageDb.userDao().deleteAll();
        List<User> list = getValue(garageDb.userDao().findAll());
        assertThat(list.size(), is(0));
    }

    @Test
    public void testInsertList() throws InterruptedException {
        User user = getUser("1");
        User user2 = getUser("2");

        List<User> toInsert = new ArrayList<>();
        toInsert.add(user);
        toInsert.add(user2);
        garageDb.userDao().insertUsers(toInsert);
        List<User> list = getValue(garageDb.userDao().findAll());
        assertThat(list.size(), is(2));
    }

    @NonNull
    private User getUser(String id) {
        return new User(id, "Jesus", "Rivas",
                    "Masculino", "1", "1", "Caracas",
                    "Venezuela", "123456789", "jesus@correo.com",
                    "15198109", "1", "0", "0",
                    "deault.png", "2018-01-01", "2018-01-01",
                    "api/users/1");
    }
}
