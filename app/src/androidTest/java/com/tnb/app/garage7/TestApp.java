package com.tnb.app.garage7;

import android.app.Application;

/* Se usa en las pruebas para prevenir la injeccion de dependencias */
public class TestApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
