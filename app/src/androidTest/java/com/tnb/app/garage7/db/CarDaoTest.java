package com.tnb.app.garage7.db;

import com.tnb.app.garage7.data.entity.Car;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tnb.app.garage7.util.LiveDataTestUtil.getValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class CarDaoTest extends DbTest {

    @Test
    public void testInsertAndRead() throws InterruptedException {
        Car car = new Car("1", "1", "Dart", "Dodge",
                "Azul", "Caracas", "Venezuela", "1",
                "Gasolina", "Carro usado", "1000000",
                "1.000.000,00", "Bs.", "4",
                "1995", "20000", "Km", "Automatica",
                "1", "2018-01-01", "2018-01-01",
                "", new ArrayList<>());
        garageDb.carDao().insert(car);
        List<Car> list = getValue(garageDb.carDao().findAll());
        assertThat(list, notNullValue());
        assertThat(list.get(0).city, is("Caracas"));
        assertThat(list.get(0).country, is("Venezuela"));
        assertThat(list.get(0).doors, is("4"));
    }

    @Test
    public void testCreateIfNotExists_exists() throws InterruptedException {
        Car car = new Car("1", "1", "Dart", "Dodge",
                "Azul", "Caracas", "Venezuela", "1",
                "Gasolina", "Carro usado", "1000000",
                "1.000.000,00", "Bs.", "4",
                "1995", "20000", "Km", "Automatica",
                "1", "2018-01-01", "2018-01-01",
                "", new ArrayList<>());
        garageDb.carDao().insert(car);
        assertThat(garageDb.carDao().createCarIfNotExists(car), is( -1L));
    }

    @Test
    public void testCreateIfNotExists_doesNotExists() throws InterruptedException {
        Car car = new Car("1", "1", "Dart", "Dodge",
                "Azul", "Caracas", "Venezuela", "1",
                "Gasolina", "Carro usado", "1000000",
                "1.000.000,00", "Bs.", "4",
                "1995", "20000", "Km", "Automatica",
                "1", "2018-01-01", "2018-01-01",
                "", new ArrayList<>());
        assertThat(garageDb.carDao().createCarIfNotExists(car), is( 1L));
    }
}
