package com.tnb.app.garage7.db;

import com.tnb.app.garage7.data.entity.Brand;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tnb.app.garage7.util.LiveDataTestUtil.getValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class BrandDaoTest extends DbTest {

    @Test
    public void testInsertAndRead() throws InterruptedException {
        Brand brand = new Brand("1", "Toyota");

        garageDb.brandDao().insert(brand);
        List<Brand> list = getValue(garageDb.brandDao().findAll());
        assertThat(list, notNullValue());
        assertThat(list.get(0).brand, is("Toyota"));

        list = getValue(garageDb.brandDao().findByName("oyo"));
        assertThat(list, notNullValue());
        assertThat(list.get(0).brand, is("Toyota"));
    }

    @Test
    public void testCreateIfNotExists_exists() throws InterruptedException {
        Brand brand = new Brand("1", "Toyota");
        garageDb.brandDao().insert(brand);
        assertThat(garageDb.brandDao().createBrandIfNotExists(brand), is( -1L));
    }

    @Test
    public void testCreateIfNotExists_doesNotExists() throws InterruptedException {
        Brand brand = new Brand("1", "Toyota");
        assertThat(garageDb.brandDao().createBrandIfNotExists(brand), is( 1L));
    }

    @Test
    public void testDeleteAll() throws InterruptedException {
        Brand brand = new Brand("1", "Toyota");
        garageDb.brandDao().insert(brand);
        garageDb.brandDao().deleteAll();
        List<Brand> list = getValue(garageDb.brandDao().findAll());
        assertThat(list.size(), is(0));
    }

    @Test
    public void testInsertList() throws InterruptedException {
        Brand brand = new Brand("1", "Toyota");
        Brand brand2 = new Brand("2", "Fiat");
        List<Brand> toInsert = new ArrayList<>();
        toInsert.add(brand);
        toInsert.add(brand2);
        garageDb.brandDao().insertBrands(toInsert);
        List<Brand> list = getValue(garageDb.brandDao().findAll());
        assertThat(list.size(), is(2));
    }
}
