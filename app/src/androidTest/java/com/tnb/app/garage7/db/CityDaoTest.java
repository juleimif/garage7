package com.tnb.app.garage7.db;

import com.tnb.app.garage7.data.entity.City;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tnb.app.garage7.util.LiveDataTestUtil.getValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class CityDaoTest extends DbTest {

    @Test
    public void testInsertAndRead() throws InterruptedException {

        City city = new City("1", "Caracas", "1", "Venezuela");

        garageDb.cityDao().insert(city);
        List<City> list = getValue(garageDb.cityDao().findAll("1"));
        assertThat(list, notNullValue());
        assertThat(list.get(0).city, is("Caracas"));
        assertThat(list.get(0).country, is("Venezuela"));

        list = getValue(garageDb.cityDao().findByName("1", "aca"));
        assertThat(list, notNullValue());
        assertThat(list.get(0).city, is("Caracas"));

        list = getValue(garageDb.cityDao().findByName("1", ""));
        assertThat(list.size(), is(1));
    }

    @Test
    public void testCreateIfNotExists_exists() throws InterruptedException {
        City city = new City("1", "Caracas", "1", "Venezuela");
        garageDb.cityDao().insert(city);
        assertThat(garageDb.cityDao().createCityIfNotExists(city), is( -1L));
    }

    @Test
    public void testCreateIfNotExists_doesNotExists() throws InterruptedException {
        City city = new City("1", "Caracas", "1", "Venezuela");
        assertThat(garageDb.cityDao().createCityIfNotExists(city), is( 1L));
    }

    @Test
    public void testDeleteAll() throws InterruptedException {
        City city = new City("1", "Caracas", "1", "Venezuela");
        garageDb.cityDao().insert(city);
        garageDb.cityDao().deleteAll();
        List<City> list = getValue(garageDb.cityDao().findAll("1"));
        assertThat(list.size(), is(0));
    }

    @Test
    public void testInsertList() throws InterruptedException {
        City city1 = new City("1", "Caracas", "1", "Venezuela");
        City city2 = new City("2", "Maracay", "1", "Venezuela");

        List<City> toInsert = new ArrayList<>();
        toInsert.add(city1);
        toInsert.add(city2);
        garageDb.cityDao().insertCities(toInsert);
        List<City> list = getValue(garageDb.cityDao().findAll("1"));
        assertThat(list.size(), is(2));
    }
}
